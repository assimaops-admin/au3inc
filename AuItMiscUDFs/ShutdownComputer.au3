#include-once

Func ComputerNameStd($computerName)
	If __Computer_StringStartsWith($computerName, "\\") Then $computerName = StringTrimLeft($computerName, 2)
	$computerName = StringUpper($computerName)
	Return $computerName
EndFunc   ;==>ComputerNameStd


; -----------------
;     HIGHER
; -----------------

Func VirtualMachineShutdown($COMPUTER_NAME)
	ComputerSwitchOff($COMPUTER_NAME)
	ComputerWaitSwitchOff($COMPUTER_NAME)
EndFunc   ;==>VirtualMachineShutdown

Func PhysicalComputerReboot($COMPUTER_NAME)
	ComputerReboot($COMPUTER_NAME)
EndFunc   ;==>PhysicalComputerReboot

; -----------------
;     LOWER
; -----------------

Func ComputerReboot($COMPUTER_NAME)
	$COMMAND_LINE = 'shutdown -t 10 -r -m \\' & $COMPUTER_NAME & ' -c "Reboot requested by administrator"'
	RunWait($COMMAND_LINE, "", @SW_HIDE)
EndFunc   ;==>ComputerReboot

Func ComputerSwitchOff($COMPUTER_NAME)
	$COMMAND_LINE = 'shutdown -t 05 -s -m \\' & $COMPUTER_NAME & ' -c "Shutdown requested by administrator"'
	RunWait($COMMAND_LINE, "", @SW_HIDE)
EndFunc   ;==>ComputerSwitchOff

Func ComputerWaitSwitchOff($COMPUTER_NAME)
	While Ping($COMPUTER_NAME)
		Sleep(1000)
	WEnd
EndFunc   ;==>ComputerWaitSwitchOff


;-------------------------------------------------------------------------------
; Name .........: __Computer_StringStartsWith
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func __Computer_StringStartsWith($string, $begin, $casesense = 0)
	Return Not StringCompare(StringLeft($string, StringLen($begin)), $begin, $casesense)
EndFunc   ;==>__Computer_StringStartsWith
