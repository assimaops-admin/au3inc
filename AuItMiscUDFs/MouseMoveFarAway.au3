#include-once
;
; This module requires two functions and a global variable
;   Or needs to be implemented and use a callback.
;
; Func MouseMoveFarAwayV1()
;
;	; REF. MouseMoveFarAwaySnippet0
;	$fCursor = False
;
;	; REF. MouseMoveFarAwaySnippet1
; 	$aOriginalCursorPos = MouseGetPos()
; 	If Not $fCursor Then MouseMove(5000, 5000, 1)
;
;	; REF. MouseMoveFarAwaySnippetX, CALLBACK-BASED IMPLEMENTATION
; 	; <CALLBACK HERE>
; 	;   <EXEMPLE: $screenshotPath = ScrShots_PrivateWndTestShotVx($funcVersion, $screenshotNickname, $window)>
;
;	; REF. MouseMoveFarAwaySnippet2
; 	If Not $fCursor Then MouseMove($aOriginalCursorPos[0], $aOriginalCursorPos[1], 1)
;
; EndFunc
;