#include-once

Global Const $GPA_INDEX_OF_FIRST_ELEMENT = 1
Global Const $GPA_ASSIGN_FORCE_GLOBAL_SCOPE = 2

; GlobalPseudoArrayPrivate_QuickTest01() ; EXPECT: $sOut = "|a|B|3|"

; ===============================================================================================================================
; GlobalPseudoArray_AddElement
; ===============================================================================================================================
Func GlobalPseudoArray_AddElement($standardPrefix, $__value__)
	$newCountValue = GlobalPseudoArrayPrivate_IncrementCount($standardPrefix)
	$__element__ = $standardPrefix & "_elt" & $newCountValue
	Assign($__element__, $__value__, $GPA_ASSIGN_FORCE_GLOBAL_SCOPE)
	Return $newCountValue
EndFunc   ;==>GlobalPseudoArray_AddElement

; ===============================================================================================================================
; GlobalPseudoArray_ToString
; ===============================================================================================================================
Func GlobalPseudoArray_ToString($standardPrefix, $eltsSeparator = @CRLF)
	Return GlobalPseudoArrayPrivate_ToStringEx($standardPrefix, GlobalPseudoArrayPrivate_GetCount($standardPrefix), $GPA_INDEX_OF_FIRST_ELEMENT, $eltsSeparator)
EndFunc   ;==>GlobalPseudoArray_ToString

; ===============================================================================================================================
; GlobalPseudoArrayPrivate_ToStringEx [PRIVATE]
; ===============================================================================================================================
Func GlobalPseudoArrayPrivate_ToStringEx($standardPrefix, $indexOfLastElement, $indexOfFirstElement = 1, $eltsSeparator = @CRLF)
	$outputDumpStr = ""
	$outputDumpStr &= GlobalPseudoArrayPrivate_GetElement($standardPrefix, $indexOfFirstElement)
	For $iElt = $indexOfFirstElement + 1 To $indexOfLastElement
		$outputDumpStr &= $eltsSeparator
		$outputDumpStr &= GlobalPseudoArrayPrivate_GetElement($standardPrefix, $iElt)
	Next
	Return $outputDumpStr
EndFunc   ;==>GlobalPseudoArrayPrivate_ToStringEx

; ===============================================================================================================================
; GlobalPseudoArrayPrivate_GetCount [PRIVATE]
; ===============================================================================================================================
Func GlobalPseudoArrayPrivate_GetCount($standardPrefix)
	$__count__ = $standardPrefix & "_count"
	If Not IsDeclared($__count__) Then Return 0
	Return Eval($__count__)
EndFunc   ;==>GlobalPseudoArrayPrivate_GetCount

; ===============================================================================================================================
; GlobalPseudoArrayPrivate_IncrementCount [PRIVATE]
; ===============================================================================================================================
Func GlobalPseudoArrayPrivate_IncrementCount($standardPrefix)
	$__count__ = $standardPrefix & "_count"
	If Not IsDeclared($__count__) Then Assign($__count__, 0, $GPA_ASSIGN_FORCE_GLOBAL_SCOPE)
	$newCountValue = Eval($__count__) + 1
	Assign($__count__, $newCountValue, $GPA_ASSIGN_FORCE_GLOBAL_SCOPE)
	Return $newCountValue
EndFunc   ;==>GlobalPseudoArrayPrivate_IncrementCount

; ===============================================================================================================================
; GlobalPseudoArrayPrivate_GetElement [PRIVATE]
; ===============================================================================================================================
Func GlobalPseudoArrayPrivate_GetElement($standardPrefix, $indexOfElementToRetrieve)
	$__element__ = $standardPrefix & "_elt" & $indexOfElementToRetrieve
	If Not IsDeclared($__element__) Then Return ""
	Return Eval($__element__)
EndFunc   ;==>GlobalPseudoArrayPrivate_GetElement

; ===============================================================================================================================
; GlobalPseudoArrayPrivate_QuickTest01 [PRIVATE]
; EXPECT: $sOut = "|a|B|3|"
; ===============================================================================================================================
Func GlobalPseudoArrayPrivate_QuickTest01()
	GlobalPseudoArray_AddElement("gBlaTest", "a")
	GlobalPseudoArray_AddElement("gBlaTest", "B")
	GlobalPseudoArray_AddElement("gBlaTest", "3")
	$sOut = "|" & GlobalPseudoArray_ToString("gBlaTest", "|") & "|"
	ConsoleWrite('$sOut = ' & $sOut & @CRLF)
	Exit
EndFunc   ;==>GlobalPseudoArrayPrivate_QuickTest01
