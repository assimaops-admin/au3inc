#include-once

;-------------------------------------------------------------------------------
; Name .........: _MouseMoveAtRandomV1
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func _MouseMoveAtRandomV1()
	$nRadius = 150
	$aPos = MouseGetPos()
	MouseMove($aPos[0] + $nRadius, $aPos[1] + $nRadius)
	MouseMove($aPos[0] + $nRadius, $aPos[1] - $nRadius)
	MouseMove($aPos[0] - $nRadius, $aPos[1] - $nRadius)
	MouseMove($aPos[0] - $nRadius, $aPos[1] + $nRadius)
	MouseMove($aPos[0], $aPos[1])
	MouseMove(10, 10)
	MouseMove($aPos[0], $aPos[1])
EndFunc   ;==>_MouseMoveAtRandomV1
