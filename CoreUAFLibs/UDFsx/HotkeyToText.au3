#include-once
#include <String.au3> ; <<< _StringProper

Func _HotkeyToTxt($hotkey)
	Return _HotkeyToTxtV1($hotkey)
EndFunc   ;==>_HotkeyToTxt

Func _HotkeyToTxtV1($hotkey)
	$hotkey = StringReplace($hotkey, "^!+", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "!^+", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "+^!", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "+!^", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "^+!", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "!+^", "Ctrl[+]Alt[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "^+", "Ctrl[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "+^", "Ctrl[+]Shift[+]")
	$hotkey = StringReplace($hotkey, "+!", "Shift[+]Alt[+]")
	$hotkey = StringReplace($hotkey, "!+", "Shift[+]Alt[+]")
	$hotkey = StringReplace($hotkey, "^!", "Ctrl[+]Alt[+]")
	$hotkey = StringReplace($hotkey, "!^", "Ctrl[+]Alt[+]")
	$hotkey = StringReplace($hotkey, "+", "Shift[+]")
	$hotkey = StringReplace($hotkey, "^", "Ctrl[+]")
	$hotkey = StringReplace($hotkey, "!", "Alt[+]")
	$hotkey = StringReplace($hotkey, "[Shift[+]]", "+")
	$hotkey = StringReplace($hotkey, "[+]", "+")
	$hotkey = StringReplace($hotkey, "{", "")
	$hotkey = StringReplace($hotkey, "}", "")
	Return _StringProper($hotkey)
EndFunc   ;==>_HotkeyToTxtV1
