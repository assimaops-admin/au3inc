#include-once
#include <Debug.au3>

Const $CUI_OUTPUT_VIEWER_TITLE = @ScriptName & " - Output viewer"
$CUI_BATCH_MODE = 0
$CUI_SILENT_MODE = 0

Func _Cui_Setup($batch_mode = 0, $silent_mode = 0)
	Global $AUTOIT3WRAPPER_CHANGE2CUI_FLAG = 1
	$CUI_BATCH_MODE = $batch_mode
	$CUI_SILENT_MODE = $silent_mode
	If $CUI_SILENT_MODE Then Return
	If @Compiled And (@ScriptFullPath = @AutoItExe) Then
		; NOP
	Else
		_DebugSetup($CUI_OUTPUT_VIEWER_TITLE)
	EndIf
EndFunc   ;==>_Cui_Setup

Func _Cui_Write($text, $isLast = 0)
	If $CUI_SILENT_MODE Then Return
	If @Compiled And (@ScriptFullPath = @AutoItExe) Then
		ConsoleWrite($text)
	Else
		$lastChar = StringRight($text, 1)
		While ($lastChar = @CR) Or ($lastChar = @LF)
			$text = StringTrimRight($text, 1)
			$lastChar = StringRight($text, 1)
		WEnd
		$text = StringReplace($text, "'", '"')
		If $isLast And $CUI_BATCH_MODE Then
			WinClose($CUI_OUTPUT_VIEWER_TITLE)
		Else
			_DebugOut($text) ; Warning pauses the script by the end... (bad for batch processing!)
		EndIf
	EndIf
EndFunc   ;==>_Cui_Write
