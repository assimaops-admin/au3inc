#include-once

;-------------------------------------------------------------------------------
; Name .........: StringSplitExcerptByPos
; Description ..:
; Parameters ...: $sides      - Number of lines to extract before and after the
;                               position
;                 $pos        - As returned by StringInStr
;                 $pattern    - See 'StringSplit' (default is '@CRLF')
;                 $splitflag  - See 'StringSplit' (default is '1')
; Return .......: String made of a few lines (2*$sides+1)
;                 @error contains marker index
; History ......: AUG 18, 2010 - Created
;-------------------------------------------------------------------------------
Const $DEFAULT_EXCERPT_LINES_COUNT = 7
Const $DEFAULT_EXCERPT_SIDES_SIZE = ($DEFAULT_EXCERPT_LINES_COUNT - 1) / 2
Const $DEFAULT_EXCERPT_SIDES_BIAS = 0
Const $DEFAULT_EXCERPT_MAX_LINE_LENGTH = 180
Const $DEFAULT_EXCERPT_ELIPSE = " (...)"

Func StringSplitExcerptByPosV2($string, $pos, $sides = Default, $bias = Default, $maxlinelength = Default, $pattern = @CRLF, $splitflag = 1)

	; parameters checks
	If $sides < 0 Then $sides = 0
	If $pos = StringLen($string) Then $pos = 1
	If $pos > StringLen($string) Then $pos = StringLen($string)

	; defaults
	If $sides = Default Then $sides = $DEFAULT_EXCERPT_SIDES_SIZE
	If $bias = Default Then $bias = $DEFAULT_EXCERPT_SIDES_BIAS
	If $maxlinelength = Default Then $maxlinelength = $DEFAULT_EXCERPT_MAX_LINE_LENGTH

	; lines split
	$array = StringSplit($string, $pattern, $splitflag)

	; boundary condition
	If $sides > $array[0] Then Return $string

	; search
	$iMarker = 0
	$cCharsMax = 0
	For $i = 1 To $array[0]
		$cCharsMin = $cCharsMax
		$cCharsMax += StringLen($array[$i])
		If $i < $array[0] Then $cCharsMax += StringLen(@CRLF)
		If ($pos >= $cCharsMin) And ($pos <= $cCharsMax) Then
			$iMarker = $i
			ExitLoop
		EndIf
	Next

	; exception/assert
	If $iMarker = 0 Then
		MsgBox(48, "Algorithm error", _
				"StringExtractLines" & @CRLF _
				 & "$string = " & $string & @CRLF _
				 & "$pos = " & $pos & @CRLF _
				 & "$sides = " & $sides & @CRLF _
				)
		Return SetError(0, 0, "")
	EndIf

	; general case
	$sRet = ""
	$cSplits = 0
	For $relative_index = (-$sides + $bias) To ($sides + $bias)

		$absolute_index = $iMarker + $relative_index

		$is_proc = 0
		$add_crlf = 0
		If $absolute_index = $array[0] Then
			$is_proc = 1
			$add_crlf = 0
		ElseIf $absolute_index >= 1 And $absolute_index < $array[0] Then
			$is_proc = 1
			$add_crlf = 1
		Else
			; NOP
		EndIf

		If $is_proc Then
			$cSplits += 1
			$sAppend = $array[$absolute_index]
			If ($maxlinelength > 0) And (StringLen($sAppend) > $maxlinelength) Then
				$sSuffix = ""
				$sPossibleSuffix = @CRLF
				If StringRight($sAppend, StringLen($sPossibleSuffix)) = $sPossibleSuffix Then
					$sSuffix = $sPossibleSuffix
					StringTrimRight($sAppend, StringLen($sPossibleSuffix))
				EndIf
				$sAppend = StringLeft($sAppend, $maxlinelength - StringLen($DEFAULT_EXCERPT_ELIPSE) - StringLen($sSuffix)) _
						 & $DEFAULT_EXCERPT_ELIPSE & $sSuffix
			EndIf
			$sRet &= $sAppend
			If $add_crlf Then $sRet &= @CRLF
		EndIf

	Next
	Return SetError($iMarker, $cSplits, $sRet)

EndFunc   ;==>StringSplitExcerptByPosV2

;-------------------------------------------------------------------------------
; Name .........: StringSplitExcerptByPos
; Description ..:
; Parameters ...: $sides      - Number of lines to extract before and after the
;                               position
;                 $pos        - As returned by StringInStr
;                 $pattern    - See 'StringSplit' (default is '@CRLF')
;                 $splitflag  - See 'StringSplit' (default is '1')
; Return .......: String made of a few lines (2*$sides+1)
;                 @error contains marker index
; History ......: AUG 18, 2010 - Created
;-------------------------------------------------------------------------------
Func StringSplitExcerptByPos($string, $pos, $sides = Default, $maxlinelength = Default, $pattern = @CRLF, $splitflag = 1)

	$bias = Default
	Return StringSplitExcerptByPosV2($string, $pos, $sides, $bias, $maxlinelength, $pattern, $splitflag)

EndFunc   ;==>StringSplitExcerptByPos

;-------------------------------------------------------------------------------
; Name .........: StringSplitExcerptByTextV2
; Description ..: See 'StringSplitExcerptByPos' for more info
; Return .......: String - text (empty string if excerpt not found)
; History ......: MAR 28, 2011 - Created
;-------------------------------------------------------------------------------
Func StringSplitExcerptByTextV2($string, $text, ByRef $pos, $sides = Default, $bias = Default, $maxlinelength = Default, $pattern = @CRLF, $splitflag = 1)

	Local $casesense = 1
	Local $sRet = "", $iMarker = 0, $cSplits = 0

	; defaults
	If $sides = Default Then $sides = $DEFAULT_EXCERPT_SIDES_SIZE
	If $maxlinelength = Default Then $maxlinelength = $DEFAULT_EXCERPT_MAX_LINE_LENGTH

	; do the search
	$pos = StringInStr($string, $text, $casesense)
	If $pos <> 0 Then
		$sRet = StringSplitExcerptByPosV2($string, $pos, $sides, $bias, $maxlinelength, $pattern, $splitflag)
		$iMarker = @error
		$cSplits = @extended
	EndIf
	Return SetError($iMarker, $cSplits, $sRet)

EndFunc   ;==>StringSplitExcerptByTextV2

;-------------------------------------------------------------------------------
; Name .........: StringSplitExcerptByText
; Description ..: See 'StringSplitExcerptByPos' for more info
; Return .......: String - text (empty string if excerpt not found)
; History ......: MAR 28, 2011 - Created
;-------------------------------------------------------------------------------
Func StringSplitExcerptByText($string, $text, ByRef $pos, $sides = Default, $maxlinelength = Default, $pattern = @CRLF, $splitflag = 1)

	$bias = Default
	Return StringSplitExcerptByTextV2($string, $text, $pos, $sides, $bias, $maxlinelength, $pattern, $splitflag)

EndFunc   ;==>StringSplitExcerptByText
