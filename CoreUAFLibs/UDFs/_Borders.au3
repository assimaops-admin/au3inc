#cs
	;#=#INDEX#==============================================================================================================================#
	;#  Title .........: _Borders.au3                                                                                                       #
	;#  Description....: Two GUI based functions to retrieve a window's client area, border size and title and menu bar height              #
	;#  Date ..........: 23.10.08                                                                                                           #
	;#  Version .......: 1.0                                                                                                                #
	;#  Author ........: jennico (jennicoattminusonlinedotde)                                                                               #
	;#  Remarks .......: Important calculations if you want your gui to work on pcs that use different window themes.                       #
	;#  Functions......: _WinGetBorderSize ( $hWnd [, $s_text] )                                                                            #
	;#                   _WinGetClientPos ( $hWnd [, $s_text] ) => a modification of WinGetClientSize                                       #
	;#======================================================================================================================================#
#ce

#include-once

#cs
	;#=#Function#===========================================================================================================================#
	;#  Name ..........: _WinGetBorderSize ( $hWnd [, $s_text] )                                                                            #
	;#  Description....: Retrieves the size of a given window's border and title and menu bar.                                              #
	;#  Parameters.....: $hWnd :    The title of the window to read. See Title special definition.                                          #
	;#                   $s_text :  [optional] The text of the window to read.                                                              #
	;#  Return Value ..: Success:   Returns a 2-element array containing the following information:                                         #
	;#                              $array[0] = border size                                                                                 #
	;#                              $array[1] = top border size incl. title and menu bar height                                             #
	;#                   Failure:   Returns 0 and sets @error to 1 if windows is not found.                                                 #
	;#  Author ........: jennico (jennicoattminusonlinedotde)                                                                               #
	;#  Date ..........: 14.10.08                                                                                                           #
	;#  Remarks .......: _WinGetBorderSize returns negative numbers such as -32000 for minimized windows, but works fine with               #
	;#                   (non-minimized) hidden windows. If multiple windows match the criteria, the most recently active window is used.   #
	;#                   Array[1] - Array[0] equals title + menu bar height.                                                                #
	;#  Related .......: _WinGetClientPos, WinGetPos, WinGetClientSize, WinMove                                                             #
	;#  Example........: yes                                                                                                                #
	;#======================================================================================================================================#
#ce

Func _WinGetBorderSize($hWnd, $s_text = "")
	Local $ret[2], $s_pos = WinGetPos($hWnd, $s_text)
	If @error Then Return SetError(1, 0, 0)
	Local $s_size = WinGetClientSize($hWnd, $s_text)
	If @error Then Return SetError(1, 0, 0)
	$ret[0] = ($s_pos[2] - $s_size[0]) / 2
	$ret[1] = $s_pos[3] - $s_size[1] - $ret[0]
	Return $ret
EndFunc   ;==>_WinGetBorderSize

#cs
	;#=#Function#===========================================================================================================================#
	;#  Name ..........: _WinGetClientPos ( $hWnd [, $s_text] )                                                                             #
	;#  Description....: Retrieves the position and size of a given window' client area.                                                    #
	;#  Parameters.....: $hWnd :    The title of the window to read. See Title special definition.                                          #
	;#                   $s_text :  [optional] The text of the window to read.                                                              #
	;#  Return Value ..: Success:   Returns a 4-element array containing the following information:                                         #
	;#                              $array[0] = X position (absolute screen coordinates)                                                    #
	;#                              $array[1] = Y position (absolute screen coordinates)                                                    #
	;#                              $array[2] = Width                                                                                       #
	;#                              $array[3] = Height                                                                                      #
	;#                   Failure:   Returns 0 and sets @error to 1 if windows is not found.                                                 #
	;#  Author ........: jennico (jennicoattminusonlinedotde)                                                                               #
	;#  Date ..........: 14.10.08                                                                                                           #
	;#  Remarks .......: _WinGetClientPos returns negative numbers such as -32000 for minimized windows, but works fine with                #
	;#                   (non-minimized) hidden windows. If multiple windows match the criteria, the most recently active window is used.   #
	;#  Related .......: _WinGetBorderSize, WinGetPos, WinGetClientSize, WinMove                                                            #
	;#  Example........: yes                                                                                                                #
	;#======================================================================================================================================#
#ce

Func _WinGetClientPos($hWnd, $s_text = "")
	Local $s_pos = WinGetPos($hWnd, $s_text)
	If @error Then Return SetError(1, 0, 0)
	Local $s_size = WinGetClientSize($hWnd, $s_text)
	If @error Then Return SetError(1, 0, 0)
	Local $s_temp = ($s_pos[2] - $s_size[0]) / 2, $ret[4] = [$s_pos[0] + $s_temp, $s_pos[1] + $s_pos[3] - $s_size[1] - $s_temp, $s_size[0], $s_size[1]]
	Return $ret
EndFunc   ;==>_WinGetClientPos
