#include-once

;-------------------------------------------------------------------------------
; Name .........: DummyFuncReturningVOID
;-------------------------------------------------------------------------------
Func DummyFuncReturningVOID($param1 = "", $param2 = "")
	; NOP
EndFunc   ;==>DummyFuncReturningVOID

;-------------------------------------------------------------------------------
; Name .........: EnsureFuncWithOneParam()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func EnsureFuncWithOneParam($pref, $default = "DummyFuncReturningVOID")
	Call($pref, 123)
	If Not @error Then
		Return $pref
	Else
		Return $default
	EndIf
EndFunc   ;==>EnsureFuncWithOneParam

;-------------------------------------------------------------------------------
; Name .........: EnsureFuncWithTwoParams()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func EnsureFuncWithTwoParams($pref, $default = "DummyFuncReturningVOID")
	Call($pref, 123, 456)
	If Not @error Then
		Return $pref
	Else
		Return $default
	EndIf
EndFunc   ;==>EnsureFuncWithTwoParams

;-------------------------------------------------------------------------------
; Name .........: EnsureFuncWithThreeParams()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func EnsureFuncWithThreeParams($pref, $default = "DummyFuncReturningVOID")
	Call($pref, 123, 456, 789)
	If Not @error Then
		Return $pref
	Else
		Return $default
	EndIf
EndFunc   ;==>EnsureFuncWithThreeParams

;-------------------------------------------------------------------------------
; Name .........: CallFuncWithOneParam()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func CallFuncWithOneParam($pref, $param, $default = "DummyFuncReturningVOID")
	$func = EnsureFuncWithOneParam($pref, $default)
	Return Call($func, $param)
EndFunc   ;==>CallFuncWithOneParam

;-------------------------------------------------------------------------------
; Name .........: CallFuncWithTwoParams()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func CallFuncWithTwoParams($pref, $param1, $param2, $default = "DummyFuncReturningVOID")
	$func = EnsureFuncWithTwoParams($pref, $default)
	Return Call($func, $param1, $param2)
EndFunc   ;==>CallFuncWithTwoParams

;-------------------------------------------------------------------------------
; Name .........: CallFuncWithTwoParams()
; Description ..: tool for dynamic binding of function/modifiers
; Parameters ...: $pref - the one we would like to get as output
;                 $default - otherwise return this one
; Return .......: JUN 29, 2010 - Created
;-------------------------------------------------------------------------------
Func CallFuncWithThreeParams($pref, $param1, $param2, $param3, $default = "DummyFuncReturningVOID")
	$func = EnsureFuncWithThreeParams($pref, $default)
	Return Call($func, $param1, $param2, $param3)
EndFunc   ;==>CallFuncWithThreeParams
