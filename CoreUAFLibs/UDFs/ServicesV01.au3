#include-once
; Windows System Services Management
; Source: http://www.autoitscript.com/forum/index.php?showtopic=5388
;

; History
; MAR 29, 2013 - Rename $STANDARD_RIGHTS_REQUIRED to $__STANDARD_RIGHTS_REQUIRED
; # include "SecurityConstants.au3" implements $STANDARD_RIGHTS_REQUIRED from around AutoIt3.3.8.1
;

Global $__STANDARD_RIGHTS_REQUIRED = 0x000F0000
Global $SC_MANAGER_CONNECT = 0x0001
Global $SC_MANAGER_CREATE_SERVICE = 0x0002
Global $SC_MANAGER_ENUMERATE_SERVICE = 0x0004
Global $SC_MANAGER_LOCK = 0x0008
Global $SC_MANAGER_QUERY_LOCK_STATUS = 0x0010
Global $SC_MANAGER_MODIFY_BOOT_CONFIG = 0x0020

Global $SC_MANAGER_ALL_ACCESS = BitOR($__STANDARD_RIGHTS_REQUIRED, _
		$SC_MANAGER_CONNECT, _
		$SC_MANAGER_CREATE_SERVICE, _
		$SC_MANAGER_ENUMERATE_SERVICE, _
		$SC_MANAGER_LOCK, _
		$SC_MANAGER_QUERY_LOCK_STATUS, _
		$SC_MANAGER_MODIFY_BOOT_CONFIG)

Global $SERVICE_QUERY_CONFIG = 0x0001
Global $SERVICE_CHANGE_CONFIG = 0x0002
Global $SERVICE_QUERY_STATUS = 0x0004
Global $SERVICE_ENUMERATE_DEPENDENTS = 0x0008
Global $SERVICE_START = 0x0010
Global $SERVICE_STOP = 0x0020
Global $SERVICE_PAUSE_CONTINUE = 0x0040
Global $SERVICE_INTERROGATE = 0x0080
Global $SERVICE_USER_DEFINED_CONTROL = 0x0100

Global $SERVICE_ALL_ACCESS = BitOR($__STANDARD_RIGHTS_REQUIRED, _
		$SERVICE_QUERY_CONFIG, _
		$SERVICE_CHANGE_CONFIG, _
		$SERVICE_QUERY_STATUS, _
		$SERVICE_ENUMERATE_DEPENDENTS, _
		$SERVICE_START, _
		$SERVICE_STOP, _
		$SERVICE_PAUSE_CONTINUE, _
		$SERVICE_INTERROGATE, _
		$SERVICE_USER_DEFINED_CONTROL)

Global $SERVICE_CONTROL_STOP = 0x00000001
Global $SERVICE_CONTROL_INTERROGATE = 0x00000004

#comments-start***
	Example - stop print spooler service
	If _ServiceRunning("Spooler") Then
	_StopService("Spooler")
	If @error Then
	MsgBox(64, "", "Failed to stop Spooler service")
	Else
	MsgBox(64, "", "Spooler service stopped")
	EndIf
	Else
	_StartService("Spooler")
	If @error Then
	MsgBox(64, "", "Failed to start Spooler service")
	Else
	MsgBox(64, "", "Spooler service started")
	EndIf
	EndIf
#comments-end***

;===============================================================================
;
; Description:      Starts a service
; Syntax:           _StartService($sServiceName)
; Parameter(s):     $sServiceName - Name of service to start
; Requirement(s):   None
; Return Value(s):  On Success - Sets   @error = 0
;                   On Failure - Sets:
;                                       @error = 1056: Already running
;                                       @error = 1060: Service does not exist
; Author(s):        SumTingWong
; Documented by:    noone
;
;===============================================================================
Func _StartService($sServiceName)
	Local $arRet
	Local $hSC
	Local $hService
	Local $lError = -1

	$arRet = DllCall("advapi32.dll", "long", "OpenSCManager", _
			"str", "", _
			"str", "ServicesActive", _
			"long", $SC_MANAGER_CONNECT)
	If $arRet[0] = 0 Then
		$arRet = DllCall("kernel32.dll", "long", "GetLastError")
		$lError = $arRet[0]
	Else
		$hSC = $arRet[0]
		$arRet = DllCall("advapi32.dll", "long", "OpenService", _
				"long", $hSC, _
				"str", $sServiceName, _
				"long", $SERVICE_START)
		If $arRet[0] = 0 Then
			$arRet = DllCall("kernel32.dll", "long", "GetLastError")
			$lError = $arRet[0]
		Else
			$hService = $arRet[0]
			$arRet = DllCall("advapi32.dll", "int", "StartService", _
					"long", $hService, _
					"long", 0, _
					"str", "")
			If $arRet[0] = 0 Then
				$arRet = DllCall("kernel32.dll", "long", "GetLastError")
				$lError = $arRet[0]
			EndIf
			DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hService)
		EndIf
		DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hSC)
	EndIf
	If $lError <> -1 Then SetError($lError)
EndFunc   ;==>_StartService


;===============================================================================
;
; Description:      Stops a service
; Syntax:           _StopService($sServiceName)
; Parameter(s):     $sServiceName - Name of service to stop
; Requirement(s):   None
; Return Value(s):  On Success - Sets:
;                                       @error = 0
;                   On Failure - Sets:
;                                       @error = 1062: Already stopped
;                                       @error = 1060: Service does not exist
; Author(s):        SumTingWong
; Documented by:    noone
;
;===============================================================================
Func _StopService($sServiceName)
	Local $arRet
	Local $hSC
	Local $hService
	Local $lError = -1

	$arRet = DllCall("advapi32.dll", "long", "OpenSCManager", _
			"str", "", _
			"str", "ServicesActive", _
			"long", $SC_MANAGER_CONNECT)
	If $arRet[0] = 0 Then
		$arRet = DllCall("kernel32.dll", "long", "GetLastError")
		$lError = $arRet[0]
	Else
		$hSC = $arRet[0]
		$arRet = DllCall("advapi32.dll", "long", "OpenService", _
				"long", $hSC, _
				"str", $sServiceName, _
				"long", $SERVICE_STOP)
		If $arRet[0] = 0 Then
			$arRet = DllCall("kernel32.dll", "long", "GetLastError")
			$lError = $arRet[0]
		Else
			$hService = $arRet[0]
			$arRet = DllCall("advapi32.dll", "int", "ControlService", _
					"long", $hService, _
					"long", $SERVICE_CONTROL_STOP, _
					"str", "")
			If $arRet[0] = 0 Then
				$arRet = DllCall("kernel32.dll", "long", "GetLastError")
				$lError = $arRet[0]
			EndIf
			DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hService)
		EndIf
		DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hSC)
	EndIf
	If $lError <> -1 Then SetError($lError)
EndFunc   ;==>_StopService

;===============================================================================
;
; Description:      Checks to see if a service is installed
; Syntax:           _ServiceExists($sServiceName)
; Parameter(s):     $sServiceName - Name of service to check
; Requirement(s):   None
; Return Value(s):  On Success - Returns 1
;                   On Failure - Returns 0
; Author(s):        SumTingWong
; Documented by:    noone
;
;===============================================================================
Func _ServiceExists($sServiceName)
	Local $arRet
	Local $hSC
	Local $bExist = 0

	$arRet = DllCall("advapi32.dll", "long", "OpenSCManager", _
			"str", "", _
			"str", "ServicesActive", _
			"long", $SC_MANAGER_CONNECT)
	If $arRet[0] <> 0 Then
		$hSC = $arRet[0]
		$arRet = DllCall("advapi32.dll", "long", "OpenService", _
				"long", $hSC, _
				"str", $sServiceName, _
				"long", $SERVICE_INTERROGATE)
		If $arRet[0] <> 0 Then
			$bExist = 1
			DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $arRet[0])
		EndIf
		DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hSC)
	EndIf
	Return $bExist
EndFunc   ;==>_ServiceExists

;===============================================================================
;
; Description:      Checks to see if a service is running
; Syntax:           _ServiceRunning($sServiceName)
; Parameter(s):     $sServiceName - Name of service to check
; Requirement(s):   None
; Return Value(s):  On Success - Returns 1
;                   On Failure - Returns 0
; Author(s):        SumTingWong
; Documented by:    noone
;
;===============================================================================
Func _ServiceRunning($sServiceName)
	Local $arRet
	Local $hSC
	Local $hService
	Local $bRunning = 0

	$arRet = DllCall("advapi32.dll", "long", "OpenSCManager", _
			"str", "", _
			"str", "ServicesActive", _
			"long", $SC_MANAGER_CONNECT)
	If $arRet[0] <> 0 Then
		$hSC = $arRet[0]
		$arRet = DllCall("advapi32.dll", "long", "OpenService", _
				"long", $hSC, _
				"str", $sServiceName, _
				"long", $SERVICE_INTERROGATE)
		If $arRet[0] <> 0 Then
			$hService = $arRet[0]
			$arRet = DllCall("advapi32.dll", "int", "ControlService", _
					"long", $hService, _
					"long", $SERVICE_CONTROL_INTERROGATE, _
					"str", "")
			$bRunning = $arRet[0]
			DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hService)
		EndIf
		DllCall("advapi32.dll", "int", "CloseServiceHandle", "long", $hSC)
	EndIf
	Return $bRunning
EndFunc   ;==>_ServiceRunning

;===============================================================================
; Description:   Delete a Windows Service
; Syntax:   _ServDelete($iName[, $Computer])
; Parameter(s):   $iName - The name of the service to delete
;                            $Computer - The network name of the computer (optional) The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Deletes the service
;                                Failure Sets @Error = -1 if service is not found
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):
;===============================================================================

Func _ServDelete($iName, $Computer = ".")
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service")
	For $objService In $sItems
		If $objService.Name == $iName Then
			$objService.StopService($objService.Name)
			$objService.Delete($objService.Name)
			Return
		EndIf
	Next
	Return SetError(-1)
EndFunc   ;==>_ServDelete

;===============================================================================
; Description:   Return the details of a Windows Service
; Syntax:   _ServGetDetails($iName[, $Computer])
; Parameter(s):   $iName - The name of the service to check
;                            $Computer - The network name of the computer (optional) The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Returns an array of the service details where element (-1 = Yes, 0 = No)
;                                    [1] = Computer Network Name
;                                    [2] = Service Name
;                                    [3] = Service Type (Own Process, Share Process)
;                                    [4] = Service State (Stopped, Running, Paused)
;                                    [5] = Exit Code (0, 1077)
;                                    [6] = Process ID
;                                    [7] = Can Be Paused (-1, 0)
;                                    [8] = Can Be Stopped (-1, 0)
;                                    [9] = Caption
;                                    [10] = Description
;                                    [11] = Can Interact With Desktop (-1, 0)
;                                    [12] = Display Name
;                                    [13] = Error Control (Normal, Ignore)
;                                    [14] = Executable Path Name
;                                    [15] = Service Started (-1, 0)
;                                    [16] = Start Mode (Auto, Manual, Disabled)
;                                    [17] = Account Name (LocalSystem, NT AUTHORITY\LocalService, NT AUTHORITY\NetworkService)
;                                Failure Sets @Error = -1 if service not found
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):   $Var = _ServGetDetails("ATI Smart")
;                         $Dtl = "System Name|Name|Type|State|ExitCode|Process ID|Can Pause|Can Stop|Caption|Description|"
;                         $Dtl = StringSplit($Dtl & "Interact With DskTop|Display Name|Error Control|Exec File Path|Started|Start Mode|Account", '|')
;                         For $I = 1 To $Var[0]
;                         MsgBox(4096,$Dtl[$I], $Var[$I])
;                         Next
;===============================================================================

Func _ServGetDetails($iName, $Computer = ".")
	Local $Rtn = ''
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service")
	For $objService In $sItems
		If $objService.Name == $iName Then
			$Rtn &= $objService.SystemName & '|' & $objService.Name & '|' & $objService.ServiceType & '|' & $objService.State & '|'
			$Rtn &= $objService.ExitCode & '|' & $objService.ProcessID & '|' & $objService.AcceptPause & '|' & $objService.AcceptStop & '|'
			$Rtn &= $objService.Caption & '|' & $objService.Description & '|' & $objService.DesktopInteract & '|' & $objService.DisplayName & '|'
			$Rtn &= $objService.ErrorControl & '|' & $objService.PathName & '|' & $objService.Started & '|' & $objService.StartMode & '|'
			$Rtn &= $objService.StartName
			Return StringSplit($Rtn, '|')
		EndIf
	Next
	Return SetError(-1)
EndFunc   ;==>_ServGetDetails

;===============================================================================
; Description:   Return the current state of a Windows Service
; Syntax:   _ServGetState($iName[, $Computer])
; Parameter(s):   $iName - The name of the service to check
;                            $Computer - The network name of the computer (optional) The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Returns the state of the service
;                                Failure Sets @Error = -1 if service not found
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):
;===============================================================================

Func _ServGetState($iName, $Computer = ".")
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service")
	For $objItem In $sItems
		If $objItem.Name == $iName Then Return $objItem.State
	Next
	Return SetError(-1)
EndFunc   ;==>_ServGetState

;===============================================================================
; Description:   List the currently installed services
; Syntax:   _ServListInstalled([,$Computer])
; Parameter(s):   $Computer - The network name of the computer (optional) The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Returns the state of the service
;                                Failure Sets @Error = -1 if service not found
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):
;===============================================================================

Func _ServListInstalled($Computer = ".")
	Local $Rtn = ''
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service")
	For $objService In $sItems
		$Rtn &= $objService.Name & '|'
	Next
	Return StringSplit(StringTrimRight($Rtn, 1), '|')
EndFunc   ;==>_ServListInstalled

;===============================================================================
; Description:   Pause a Windows Service
; Syntax:   _ServPause($iName[, $Computer])
; Parameter(s):   $iName - The name of the service to start
;                            $Computer - The network name of the computer (optional). The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Pauses the service
;                                Failure Sets @Error = -1 if service not found or service is already paused
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):
;===============================================================================

Func _ServPause($iName, $Computer = ".")
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service Where State = 'Running' ")
	For $objService In $sItems
		If $objService.Name == $iName Then
			$objService.PauseService($objService.Name)
			Return
		EndIf
	Next
	Return SetError(-1)
EndFunc   ;==>_ServPause

;===============================================================================
; Description:   Resumes a  previously paused Windows auto-start service
; Syntax:   _ServResume($iName[, $Computer])
; Parameter(s):   $iName - The name of the service to start
;                            $Computer - The network name of the computer (optional). The local computer is default
; Requirement(s):   None
; Return Value(s):   Success - Resumes the service
;                                Failure Sets @Error = -1 if service not found
; Author(s)   GEOSoft
; Modification(s):
; Note(s):
; Example(s):
;===============================================================================

Func _ServResume($iName, $Computer = ".")
	$Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	$sItems = $Service.ExecQuery("Select * from Win32_Service Where State = 'Paused' and StartMode = 'Auto'")
	For $objService In $sItems
		If $objService.Name == $iName Then
			$objService.ResumeService($objService.Name)
			Return
		EndIf
	Next
	Return SetError(-1)
EndFunc   ;==>_ServResume
