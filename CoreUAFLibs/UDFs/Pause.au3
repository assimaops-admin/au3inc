#include-once

Global $g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A = ""

Func _PauseHotkeyToTxt($hotkey)
	$func = "_HotkeyToTxt"
	$txt = Call($func, $hotkey)
	If @error = 0xDEAD And @extended = 0xBEEF Then
		Return $hotkey
	Else
		Return $txt
	EndIf
EndFunc   ;==>_PauseHotkeyToTxt

Func _Pause($hotkey = "^+{Pause}", $txtMsg = "")
	; First implem:
	;    MsgBox(0, @ScriptName & " is paused", "Click OK to continue...")
	; Second implem:
	;    Sleep(30*60*1000)
	; Third implem:
	;    Add a key hook
	$g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A = $hotkey
	HotKeySet($g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A _
			, "UNPAUSE_CALLBACK_0AB7C63F_32F3_4462_8F3B_C488F6FB436A")
	If ($txtMsg <> "") Then ConsoleWrite($txtMsg & @CRLF)
	ConsoleWrite("Script paused. Press '" & _PauseHotkeyToTxt($hotkey) & "' to continue..." & @CRLF)
	While 1
		Sleep(500)
		If $g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A = "" _
				Then ExitLoop
	WEnd
EndFunc   ;==>_Pause

Func UNPAUSE_CALLBACK_0AB7C63F_32F3_4462_8F3B_C488F6FB436A()
	HotKeySet($g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A)
	$g_PAUSE_HOTKEY_0AB7C63F_32F3_4462_8F3B_C488F6FB436A = ""
EndFunc   ;==>UNPAUSE_CALLBACK_0AB7C63F_32F3_4462_8F3B_C488F6FB436A

#cs
	; MANUAL TESTING
	; # include "HotkeyToText.au3" ; try with and without this include
	_Pause()
	_Pause("{F7}")
	_Pause("^{PAUSE}") ; <<< CONFLICT WITH SCRIPT INTERRUPTION IN SCITE, BUT WORKS...
	_Pause("^+{ENTER}")
	;_Pause("+{PAUSE}") ; <<< DOES NOT WORK
	;_Pause("{PAUSE}") ; <<< DOES NOT WORK
#ce
