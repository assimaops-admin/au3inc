#include-once
#include "..\UDFsx\CUI.au3"
#include ".\XcptSystem.GlobalLog.DYNBIND.STUB.au3"

;-------------------------------------------------------------------------------
; Name .........: Warning
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func Warning($szMsg)
	DYNBIND_WarningSub($szMsg)
	DYNBIND_GlobalLog("Warning", $szMsg)
EndFunc   ;==>Warning

;-------------------------------------------------------------------------------
; Name .........: WarningLogOnly
; Description ..: Do not send an email about this warning
; Return .......:
;-------------------------------------------------------------------------------
Func WarningLogOnly($szMsg)
	$no_email = 1
	DYNBIND_WarningSub($szMsg)
	DYNBIND_GlobalLog("Warning", $szMsg, $no_email)
EndFunc   ;==>WarningLogOnly

;-------------------------------------------------------------------------------
; Name .........: FatalError
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FatalError($szMsg, $nErrorCode = 1)
	PrivateFatalErrorSub($szMsg, $nErrorCode)
	PrivateFatalErrorGlobalLog($szMsg, "Generic", $nErrorCode)
	Exit $nErrorCode
EndFunc   ;==>FatalError

;-------------------------------------------------------------------------------
; Name .........: Assert
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func Assert($test, $sTest, $nErrorCode = 1, $sScriptPath = @ScriptFullPath, $sLine = @ScriptLineNumber)
	If Not $test Then
		$msg = $sScriptPath & ":" & $sLine & ":" & " " & "Failed assertion " & $sTest & "."
		PrivateFatalErrorSub($msg, $nErrorCode)
		PrivateFatalErrorGlobalLog($msg, "Assert", $nErrorCode)
		Exit $nErrorCode
	EndIf
EndFunc   ;==>Assert

;-------------------------------------------------------------------------------
; Name .........: NotImplemented
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func NotImplemented($szMsg, $nErrorCode = 1)
	PrivateFatalErrorSub("Feature not implemented." & @CRLF & $szMsg, $nErrorCode)
	PrivateFatalErrorGlobalLog($szMsg, "NotImplemented", $nErrorCode)
	Exit $nErrorCode
EndFunc   ;==>NotImplemented

;-------------------------------------------------------------------------------
; Name .........: FatalError_PreCondition
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FatalError_PreCondition($func_name, $comment = "", $nErrorCode = 1)
	$msg = "Pre-condition(s) violated in '" & $func_name & "'."
	If $comment <> "" Then $msg &= @CRLF & "(" & $comment & ")"
	PrivateFatalErrorSub($msg, $nErrorCode)
	PrivateFatalErrorGlobalLog($msg, "Contracts", $nErrorCode)
	Exit $nErrorCode
EndFunc   ;==>FatalError_PreCondition

;-------------------------------------------------------------------------------
; Name .........: FatalError_PostCondition
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FatalError_PostCondition($func_name, $comment = "", $nErrorCode = 1)
	$msg = "Post-condition(s) violated in '" & $func_name & "'."
	If $comment <> "" Then $msg &= @CRLF & "(" & $comment & ")"
	PrivateFatalErrorSub($msg, $nErrorCode)
	PrivateFatalErrorGlobalLog($msg, "Contracts", $nErrorCode)
	Exit $nErrorCode
EndFunc   ;==>FatalError_PostCondition

;-------------------------------------------------------------------------------
; Name .........: FatalError_ContractViolation_ReturnValue
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FatalError_ContractViolation_ReturnValue($func_name, $comment = "", $nErrorCode = 1)
	$msg = "Contract violation by '" & $func_name & "' regarding its return value."
	If $comment <> "" Then $msg &= @CRLF & "(" & $comment & ")"
	PrivateFatalErrorSub($msg, $nErrorCode)
	PrivateFatalErrorGlobalLog($msg, "Contracts", $nErrorCode)
	Exit $nErrorCode
EndFunc   ;==>FatalError_ContractViolation_ReturnValue

;-------------------------------------------------------------------------------
; Name .........: PrivateFatalErrorSub [PRIVATE]
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func PrivateFatalErrorSub($szMsg, $nErrorCode = 1)
	$errTtl = "AutoTestingVx fatal error"
	$errStr1 = $errTtl & ": " & $szMsg
	$errStr2 = "Exiting (err_code = " & $nErrorCode & ")..."
	If IsDeclared("AUTOIT3WRAPPER_CONSOLE_ONLY_PREF") Then
		ConsoleWrite($errStr1 & @CRLF)
		ConsoleWrite($errStr2 & @CRLF)
	ElseIf IsDeclared("AUTOIT3WRAPPER_CHANGE2CUI_FLAG") Then
		_Cui_Write($errStr1)
		_Cui_Write($errStr2)
	Else
		MsgBox(48, $errTtl, $szMsg & @CRLF & $errStr2)
	EndIf
EndFunc   ;==>PrivateFatalErrorSub

;-------------------------------------------------------------------------------
; Name .........: PrivateFatalErrorGlobalLog [PRIVATE]
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func PrivateFatalErrorGlobalLog($szMsg_in, $szLogType, $nErrorCode = 1)
	$errStr2 = "Exiting (err_code = " & $nErrorCode & ")..."
	$szMsg2 = $szMsg_in & " " & $errStr2
	DYNBIND_ErrorSub($szMsg2)
	DYNBIND_GlobalLog("FatalError.AllType", $szMsg2)
	DYNBIND_GlobalLog("FatalError." & $szLogType, $szMsg2)
EndFunc   ;==>PrivateFatalErrorGlobalLog
