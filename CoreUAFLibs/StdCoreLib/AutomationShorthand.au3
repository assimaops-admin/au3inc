#include-once
#include ".\LogShorthand.Trace.au3"


;-------------------------------------------------------------------------------
; Name .........: SendSlow
; Description ..:
; Return .......: none
;-------------------------------------------------------------------------------
Func SendSlow($text)
	FuncTrace("SendSlow", $text)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	Sleep(500)
	$chars = StringSplit($text, "")
	For $i = 1 To $chars[0]
		Sleep(25)
		Send($chars[$i])
		Sleep(25)
	Next
EndFunc   ;==>SendSlow

;-------------------------------------------------------------------------------
; Name .........: SendV2
; Description ..:
; Return .......:
; History ......: JAN 13, 2009 - Created (wrap 'Send')
;-------------------------------------------------------------------------------
Func SendV2($someText, $flag = 0)
	FuncTrace("Send", $someText, $flag)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	Send($someText, $flag)
EndFunc   ;==>SendV2

;-------------------------------------------------------------------------------
; Name .........: SleepV2
; Description ..:
; Return .......:
; History ......: JAN 13, 2009 - Created (wrap 'Send')
;-------------------------------------------------------------------------------
Func SleepV2($time)
	FuncTrace("Sleep", $time)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	Sleep($time)
EndFunc   ;==>SleepV2

;-------------------------------------------------------------------------------
; Name .........: AtmTrace
; Description ..:
; Return .......:
; History ......: FEB 4, 2016 - Extacted/Refactored
;-------------------------------------------------------------------------------
Func AtmTrace()
	ValTrace(WinGetTitle("[ACTIVE]"), 'WinGetTitle("[ACTIVE]")')
	ValTrace(WinGetHandle("[ACTIVE]"), 'WinGetHandle("[ACTIVE]")')
EndFunc   ;==>AtmTrace

;-------------------------------------------------------------------------------
; Name .........: Click
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func Click($nPosX, $nPosY, $callbackBeforeMouseMove = "", $callbackBeforeClick = "", $callbackAfterClick = "")
	FuncTrace("Click", $nPosX, $nPosY)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return

	AtmTrace()
	If $callbackBeforeMouseMove <> "" Then Call($callbackBeforeMouseMove)
	MouseMove($nPosX, $nPosY)
	If $callbackBeforeClick <> "" Then Call($callbackBeforeClick)

	MouseDown("left")
	MouseUp("left")
	If $callbackAfterClick <> "" Then Call($callbackAfterClick)
EndFunc   ;==>Click

;-------------------------------------------------------------------------------
; Name .........: ClickAbsolute
; Description ..:
; Return .......:
; History ......: MAY 07, 2009 - Created
;-------------------------------------------------------------------------------
Func ClickAbsolute($nPosX, $nPosY)
	FuncTrace("ClickAbsolute", $nPosX, $nPosY)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	$optionOldVal = Opt("MouseCoordMode", 1)
	MouseMove($nPosX, $nPosY)
	MouseDown("left")
	MouseUp("left")
	Opt("MouseCoordMode", $optionOldVal)
EndFunc   ;==>ClickAbsolute

;-------------------------------------------------------------------------------
; Name .........: MouseClickAbsolute
; Description ..:
; Return .......:
; History ......: FEB 16, 2011 - Created
;-------------------------------------------------------------------------------
Func MouseClickAbsolute($button, $x = Default, $y = Default, $clicks = 1, $speed = 10)
	FuncTrace("MouseClickAbsolute", $button, $x, $y, $clicks, $speed)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	$optionOldVal = Opt("MouseCoordMode", 1)
	MouseClick($button, $x, $y, $clicks, $speed)
	Opt("MouseCoordMode", $optionOldVal)
EndFunc   ;==>MouseClickAbsolute

;-------------------------------------------------------------------------------
; Name .........: MouseMoveAbsolute
; Description ..:
; Return .......:
; History ......: FEB 16, 2011 - Created
;-------------------------------------------------------------------------------
Func MouseMoveAbsolute($x, $y, $speed = 10)
	FuncTrace("MouseMoveAbsolute", $x, $y, $speed)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	$optionOldVal = Opt("MouseCoordMode", 1)
	MouseMove($x, $y, $speed)
	Opt("MouseCoordMode", $optionOldVal)
EndFunc   ;==>MouseMoveAbsolute

;-------------------------------------------------------------------------------
; Name .........: DblClick
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DblClick($nPosX, $nPosY, $callbackBeforeMouseMove = "", $callbackBeforeClick = "", $callbackAfterClick = "")
	FuncTrace("DblClick", $nPosX, $nPosY)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return

	AtmTrace()
	If $callbackBeforeMouseMove <> "" Then Call($callbackBeforeMouseMove)
	MouseMove($nPosX, $nPosY)
	If $callbackBeforeClick <> "" Then Call($callbackBeforeClick)

	MouseClick("left", $nPosX, $nPosY, 2)
	If $callbackAfterClick <> "" Then Call($callbackAfterClick)
EndFunc   ;==>DblClick

;-------------------------------------------------------------------------------
; Name .........: RightClick
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func RightClick($nPosX, $nPosY, $callbackBeforeMouseMove = "", $callbackBeforeClick = "", $callbackAfterClick = "")
	FuncTrace("RightClick", $nPosX, $nPosY)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return

	AtmTrace()
	If $callbackBeforeMouseMove <> "" Then Call($callbackBeforeMouseMove)
	MouseMove($nPosX, $nPosY)
	If $callbackBeforeClick <> "" Then Call($callbackBeforeClick)

	MouseClick("right", $nPosX, $nPosY)
	If $callbackAfterClick <> "" Then Call($callbackAfterClick)
EndFunc   ;==>RightClick

;-------------------------------------------------------------------------------
; Name .........: Drag
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func Drag($nPosX, $nPosY, $nPosX2, $nPosY2, $callbackBeforeMouseMove = "", $callbackBeforeClick = "", $callbackAfterRelease = "")
	FuncTrace("Drag", $nPosX, $nPosY, $nPosX2, $nPosY2)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return

	AtmTrace()
	If $callbackBeforeMouseMove <> "" Then Call($callbackBeforeMouseMove)
	MouseMove($nPosX, $nPosY)
	If $callbackBeforeClick <> "" Then Call($callbackBeforeClick)

	MouseDown("left")
	Sleep(500)
	MouseMove($nPosX2, $nPosY2)
	MouseUp("left")
	If $callbackAfterRelease <> "" Then Call($callbackAfterRelease)
EndFunc   ;==>Drag
