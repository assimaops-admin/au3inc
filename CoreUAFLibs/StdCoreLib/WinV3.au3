#include-once
#include "..\UDFs\_Borders.au3"

;-------------------------------------------------------------------------------
; Name .........: ControlSetPosViaSplitter
; Description ..: Drag-and-drop a splitter-control in order to resize a control
;                 that it is delimitating.
; Parameters ...: $operated_coord - set to 0 (Default) if the spitter is
;                                   on the right hand side of the control
;                                   (ie. vertical splitter - operate 'y' coord.)
;                                 - set to 1 if the splitter is
;                                   under the control (ie. operate 'x' coord)
; Remark .......: See 'ControlGetPos' if you want to better understand the
;                 rationale underneath '$operated_coord' variable.
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ControlSetPosViaSplitter($win_title, $ctl_classnameNN, $desired_size, $operated_coord = 0)

	Const $win_text = ""

	; exception
	If Not WinExists($win_title, $win_text) Then Return

	; get more info
	$borders = _WinGetBorderSize($win_title, $win_text)

	; are we using SAP Signature theme as the default theme? (instead of SAP Tradeshow.)
	;   $borders[0] = 21 on PERSES with SAP Signature theme
	;   $borders[0] = 4 on PERSES with SAP Tradeshow theme
	If IsDeclared("AutoTestingV2._RELEASE152_") Then $borders[0] = 1

	; get the pos
	$pos = ControlGetPos($win_title, $win_text, $ctl_classnameNN)
	$actual_width = $pos[2]
	$actual_height = $pos[3]

	; so, is there anything to do at all?!
	If $operated_coord = 0 Then
		If $actual_width = $desired_size Then Return
	Else
		If $actual_height = $desired_size Then Return
	EndIf

	; define the movement to perform
	Local $sliderX, $sliderY, $sliderX2, $sliderY2
	If $operated_coord = 0 Then
		$sliderX = $pos[0] + $pos[2] + $borders[0]
		$sliderY = 400 ; << WARNING: Approx.
		If IsDeclared("AutoTestingV2._RELEASE152_") Then $sliderY = 380
		$sliderX2 = $sliderX + $desired_size - $actual_width
		$sliderY2 = $sliderY
	Else
		$sliderX = 100 ; << WARNING: Approx.
		$sliderY = $pos[1] + $pos[3] + $borders[0]
		$sliderX2 = $sliderX
		$sliderY2 = $sliderY + $desired_size - $actual_height
	EndIf

	; record who has the focus
	$hWnd = WinGetHandle("[ACTIVE]")
	$cctrl = ControlGetFocus($hWnd)

	; record how do we control the mouse
	$optionOldVal = Opt("MouseCoordMode", 2) ; relative coords to the client area of the active window

	; perform GUI changes
	If Not WinActive($win_title, "") Then
		WinActivate($win_title, "")
		WinWaitActive($win_title, "")
	EndIf
	MouseClickDrag("left", $sliderX, $sliderY, $sliderX2, $sliderY2)

	; restore original settings
	Opt("MouseCoordMode", $optionOldVal)
	ControlFocus($hWnd, "", $cctrl)

EndFunc   ;==>ControlSetPosViaSplitter
