#include-once
#include ".\LogServices.au3"
;$MODULE_VERBOSITY_LEVEL = Default

#cs ----------------------------------------------------------------------------
	ConsoleWrite("!blabla"&@CRLF)
	ConsoleWrite("+blabla"&@CRLF)
	ConsoleWrite("-blabla"&@CRLF)
	ConsoleWrite(">blabla"&@CRLF)
	ConsoleWrite("bla(1,1)"&@CRLF)
#ce ----------------------------------------------------------------------------

;-------------------------------------------------------------------------------
; Name .........: Scenario
; Description ..: High level. Use it to comment automation scripts.
;-------------------------------------------------------------------------------
Func Scenario($txt)
	LogServices_AddEntry("+ " & $txt)
EndFunc   ;==>Scenario

;-------------------------------------------------------------------------------
; Name .........: WarningSub
; Description ..: Low level. This is a sub-function.
; Remarks ......: There is a function called Warning() that _also_ sends a
;                 notification email to the test-bench administrator
;-------------------------------------------------------------------------------
Func WarningSub($txt)
	LogServices_AddEntry("- " & $txt)
EndFunc   ;==>WarningSub

;-------------------------------------------------------------------------------
; Name .........: ErrorSub
; Description ..: Low level. This is a sub-function.
; Remarks ......: There is a function called FatalError() that _also_ sends a
;                 notification email to the test-bench administrator _AND_ that
;                 interrupts the execution of the script.
;                 There is no function called Error(): use Warning() instead.
;-------------------------------------------------------------------------------
Func ErrorSub($txt)
	LogServices_AddEntry("! " & $txt)
EndFunc   ;==>ErrorSub
