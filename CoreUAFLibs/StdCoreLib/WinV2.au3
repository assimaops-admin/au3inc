#include-once
#include <AuItCustLibs\AutoItEx.au3>
#include <BaseUAFLibs\UDFs\FuncTimer.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include ".\LogShorthand.Trace.au3"

; TODO: FILTER BY PROCESS/APPLICATION (cf. 'WinListEx')

;-------------------------------------------------------------------------------
; Name .........: WinExpect
; Description ..: Wait and Activate
; Return .......: 'true' if found and activated
;-------------------------------------------------------------------------------
Func WinExpect($title, $text = "", $timeoutWait = 0, $timeoutActivate = Default)
	FuncTrace("WinExpect", $title, $text, $timeoutWait, $timeoutActivate)
	If IsSet("ENTIRELY_DRY_RUN___GLOBAL_SET") Then Return
	$ret = 0
	If $timeoutWait = Default Then $timeoutWait = 0
	If $timeoutActivate = Default Then $timeoutActivate = $timeoutWait
	If WinExists($title, $text) Then
		$ret = WinGetHandle($title, $text)
	Else
		$ret = WinWait($title, $text, $timeoutWait)
	EndIf
	If $ret Then $ret = WinActivateV2($title, $text, $timeoutActivate)
	If $ret Then $ret = WinWaitVisible($title, $text, $timeoutActivate)
	Return $ret
EndFunc   ;==>WinExpect

;-------------------------------------------------------------------------------
; Name .........: WinActivateV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func WinActivateV2($title, $text = "", $timeout = 0)
	FuncTrace("WinActivateV2", $title, $text, $timeout)
	$ret = 0
	If $timeout = Default Then $timeout = 0
	If WinActive($title, $text) Then
		$ret = WinGetHandle($title, $text)
	Else
		WinActivate($title, $text)
		$ret = WinWaitActive($title, $text, $timeout)
	EndIf
	Return $ret
EndFunc   ;==>WinActivateV2

;-------------------------------------------------------------------------------
; Name .........: WinSetPosStandard
; Description ..: By default the standard maximized position is 768 x 1024
;                   but we reserve 30 pixels on the Y axis for the taskbar.
;                   + remove 'maximized' attribute (if any)
; Parameters ...:
; Return .......:
; History ......: JUN 10, 2010 - Removed 'not implemented' Fatal error
;                                "if $shiftX or $shiftY"
;-------------------------------------------------------------------------------
Const $STANDARD_WIDTH_V1 = 1024
Const $STANDARD_WIDTH_V2 = $STANDARD_WIDTH_V1 - 1
Const $STANDARD_HEIGHT_V1 = 768 - 30
Const $STANDARD_HEIGHT_V2 = $STANDARD_HEIGHT_V1 - 1
Func WinSetPosStandard($szTitle = "", $szText = "", $shiftX = 0, $shiftY = 0, $nSizeX = $STANDARD_WIDTH_V1, $nSizeY = $STANDARD_HEIGHT_V1)
	FuncTrace('WinSetPosStandard', $szTitle, $szText, $shiftX, $shiftY, $nSizeX, $nSizeY)

	; remove 'maximized' attribute
	$state = WinGetState($szTitle, $szText)
	$flag_maximized = 32
	If BitAND($state, $flag_maximized) <> 0 Then
		WinSetState($szTitle, $szText, @SW_RESTORE)
	EndIf

	; move to (0;0) and resize to standard size screen
	$aPos = WinGetPos($szTitle, $szText)
	If $aPos <> 0 Then
		WinMove($szTitle, $szText, $shiftX, $shiftY, $nSizeX, $nSizeY)
	EndIf

EndFunc   ;==>WinSetPosStandard

;-------------------------------------------------------------------------------
; Name .........: WinSetPosOrigin
; Description ..:
; Parameters ...:
; Return .......:
; History ......:
;-------------------------------------------------------------------------------
Func WinSetPosOrigin($szTitle = "", $szText = "", $shiftX = 0, $shiftY = 0)
	FuncTrace('WinSetPosOrigin', $szTitle, $szText, $shiftX, $shiftY)

	; remove 'maximized' attribute
	$state = WinGetState($szTitle, $szText)
	$flag_maximized = 32
	If BitAND($state, $flag_maximized) <> 0 Then
		WinSetState($szTitle, $szText, @SW_RESTORE)
	EndIf

	; move to (0;0) and resize to standard size screen
	$aPos = WinGetPos($szTitle, $szText)
	If $aPos <> 0 Then
		WinMove($szTitle, $szText, $shiftX, $shiftY)
	EndIf

EndFunc   ;==>WinSetPosOrigin

;-------------------------------------------------------------------------------
; Name .........: WinWaitNotExist
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func WinWaitNotExist($wndTitle, $wndText = "", $timeout = 0)
	FuncTrace('WinWaitNotExist', $wndTitle, $wndText, $timeout)
	If $timeout = Default Then $timeout = 0
	Local $timer = FuncTimerCreate($timeout)
	While True
		If Not WinExists($wndTitle, $wndText) Then Return True
		Sleep($DEFAULT_FUNCT_TIMER_CYCLE_DURATION)
		If FuncTimerIsElapsed($timer) Then Return False
	WEnd
EndFunc   ;==>WinWaitNotExist

;-------------------------------------------------------------------------------
; Name .........: WinIsVisible
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func WinIsVisible($wndTitle, $wndText = "")
	If BitAND(WinGetState($wndTitle, $wndText), 2) Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>WinIsVisible

;-------------------------------------------------------------------------------
; Name .........: WinWaitVisible
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func WinWaitVisible($wndTitle, $wndText = "", $timeout = 0)
	FuncTrace('WinWaitVisible', $wndTitle, $wndText, $timeout)
	If $timeout = Default Then $timeout = 0
	Local $timer = FuncTimerCreate($timeout)
	$ret = 0
	While Not $ret
		If WinIsVisible($wndTitle, $wndText) Then
			$ret = WinGetHandle($wndTitle, $wndText)
			If $ret Then
				ExitLoop
			Else
				Trace(" WTF ?!!")
			EndIf
		EndIf
		If FuncTimerIsElapsed($timer) Then ExitLoop
		Sleep($DEFAULT_FUNCT_TIMER_CYCLE_DURATION)
	WEnd
	Return $ret
EndFunc   ;==>WinWaitVisible

;-------------------------------------------------------------------------------
; Name .........: WinWaitNotVisible
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func WinWaitNotVisible($wndTitle, $wndText = "", $timeout = 0)
	FuncTrace('WinWaitNotVisible', $wndTitle, $wndText, $timeout)
	If $timeout = Default Then $timeout = 0
	Local $timer = FuncTimerCreate($timeout)
	While True
		If Not WinIsVisible($wndTitle, $wndText) Then Return True
		Sleep($DEFAULT_FUNCT_TIMER_CYCLE_DURATION)
		If FuncTimerIsElapsed($timer) Then Return False
	WEnd
EndFunc   ;==>WinWaitNotVisible

;-------------------------------------------------------------------------------
; Name .........: WinListHandles
; Description ..: Like WinList - But returns only handles
; Parameters ...:
; Return .......:
; History ......: OCT 12, 2010 - Created
;-------------------------------------------------------------------------------
Func WinListHandles($title, $text = "", $include_controls = 0)
	$list = WinListEx($title, $text, $include_controls)
	$list2 = _ArrayCreateV2()
	For $i = 1 To $list[0][0]
		_ArrayAddV2($list2, $list[$i][1])
	Next
	Return $list2
EndFunc   ;==>WinListHandles

;-------------------------------------------------------------------------------
; Name .........: WinCountVisibles
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func WinCountVisibles($szTitle, $szText = "", $szProcessName = "", $include_controls = 0)
	FuncTrace('WinCountVisibles', $szTitle, $szText, $szProcessName, $include_controls)
	$var = WinListEx($szTitle, $szText, $include_controls)
	$cVisible = 0
	For $i = 1 To $var[0][0]
		$handle = $var[$i][1]
		; skip this window if not part of the process we are interested in
		If IsString($szProcessName) _
				And ($szProcessName <> "") _
				And (ProcessExists($szProcessName) <> WinGetProcess($handle)) Then
			ContinueLoop
		EndIf
		; count this window if it is visible
		If WinIsVisible($handle) Then
			$cVisible += 1
		EndIf
	Next
	Return $cVisible
EndFunc   ;==>WinCountVisibles

;-------------------------------------------------------------------------------
; Name .........: WinCountVisibleProgressBars
; Description ..: Check if there is a progress bar displayed
;                   (cf. LessonToolRes.rc)
;                   CONTROL		"ProgressAction",IDC_PROGRESS_ACTION,"msctls_progress32"
;                   LTEXT		"........",IDC_PROGRESS_ACTION_DESCRIPTION,
; Return .......:
;-------------------------------------------------------------------------------
Func WinCountVisibleProgressBars($szProcessName = "")
	Return WinCountVisibles("[CLASS:msctls_progress32]", "", $szProcessName, 1)
EndFunc   ;==>WinCountVisibleProgressBars

;-------------------------------------------------------------------------------
; Name .........: WinWaitProgressIfAny
; Description ..:
; Parameters ...:
; Return .......: none
;-------------------------------------------------------------------------------
Func WinWaitProgressIfAny($nRetry_in = 0, $processName = "")
	FuncTrace("WinWaitProgressIfAny", $nRetry_in)

	; REF.PS/19E47FF9: Special behavior for SAP Grabbing test scripts
	If IsDeclared("g_szScriptParams") And IsString(Eval("g_szScriptParams")) Then
		If StringInStr(Eval("g_szScriptParams"), "GrabbingSap") Then
			Trace("Skip 'WinWaitProgressIfAny' when automating SAP Capture.")
			Return
		EndIf
	EndIf

	; REF.PS/20110112_173124: Another global
	If $processName = "" And IsDeclared("g_szTargetPath") And IsString(Eval("g_szTargetPath")) Then
		$processName = GetFilenameFromPath(Eval("g_szTargetPath"))
	EndIf

	$delay = 500
	$nRetry = $nRetry_in

	Trace("Waiting " & (2 * $delay) & "ms...")
	Sleep(2 * $delay)

	While 1
		$visibleProgressBarsCount = WinCountVisibleProgressBars($processName)
		If $visibleProgressBarsCount <> 1 And $nRetry = 0 Then
			Trace("Progress-bar NOT displayed ($visibleProgressBarsCount=" & $visibleProgressBarsCount & "). Exiting...")
			ExitLoop
		ElseIf $visibleProgressBarsCount <> 1 Then
			Trace("Progress-bar NOT displayed ($visibleProgressBarsCount=" & $visibleProgressBarsCount & "). Checking again...")
			Sleep($delay)
			$nRetry = $nRetry - 1
		Else
			Trace("Progress-bar displayed. Waiting " & $delay & "ms...")
			Sleep($delay)
			$nRetry = $nRetry_in
		EndIf
	WEnd
	Trace("Returning from 'WinWaitProgressIfAny'.")
EndFunc   ;==>WinWaitProgressIfAny

;-------------------------------------------------------------------------------
; Name .........: ControlsList
; Description ..: Like WinList - But with 'WinSearchChildren' enabled
; Parameters ...:
; Return .......:
; History ......: OCT 12, 2010 - Created
;-------------------------------------------------------------------------------
Func ControlsList($title, $text = "")
	$WinSearchChildren = AutoItSetOption('WinSearchChildren', 1)
	$list = WinList($title, $text)
	AutoItSetOption('WinSearchChildren', $WinSearchChildren)
	Return $list
EndFunc   ;==>ControlsList

;-------------------------------------------------------------------------------
; Name .........: WinListEx
; Description ..: Switch between 'WinList' and 'ControlsList'
; Parameters ...:
; Return .......: See 'WinList'
; History ......: OCT 12, 2010 - Created
;-------------------------------------------------------------------------------
Func WinListEx($title, $text = "", $include_controls = 0)
	If $include_controls Then
		Return ControlsList($title, $text)
	Else
		Return WinList($title, $text)
	EndIf
EndFunc   ;==>WinListEx

;-------------------------------------------------------------------------------
; Name .........: WinGetHandleFwd
; Description ..: Fix/Workaround - Forward compatibility for new titles
; Parameters ...:
; Return .......: See 'WinList'
; History ......: OCT 12, 2010 - Created
; Testing ......:
;
;     $hWnd = WinGetHandleFwd("[CLASS:CAssistantBubble::CImpl]")
;     ConsoleWrite($hWnd & @CRLF)
;     $hWnd = WinGetHandleFwd("[CLASS:CAssistantBubble::CImpl; INSTANCE:2]")
;     ConsoleWrite($hWnd & @CRLF)
;     $hWnd = WinGetHandleFwd("[CLASS:CAssistantBubble::CImpl; INSTANCE:3]")
;     ConsoleWrite($hWnd & @CRLF)
;
;-------------------------------------------------------------------------------
Func WinGetHandleFwd($title, $text = "")
	$classname = StringRegExpSingleEx($title, WINDOW_COMPACT_PROP_PATTERN("CLASS"))
	$instance = StringRegExpSingleEx($title, WINDOW_COMPACT_PROP_PATTERN("INSTANCE"))

	; by default
	$ret = WinGetHandle($title, $text)

	; try using the old mode
	If StringInStr($classname, ":") Then

		; set old mode
		$WinTitleMatchMode = 4
		$title = "classname=" & $classname

		If $instance > 0 Then

			; do not set the instance, list the windows instead
			$WinTitleMatchMode = AutoItSetOption("WinTitleMatchMode", $WinTitleMatchMode)
			ConsoleWrite($title & @CRLF)
			$list = WinList($title, $text)
			AutoItSetOption("WinTitleMatchMode", $WinTitleMatchMode) ; revert the mode back

			; find the instance
			If $instance > $list[0][0] Then
				$ret = ""
			Else
				$ret = $list[$instance][1]
			EndIf

		Else

			; or just search for that window
			$WinTitleMatchMode = AutoItSetOption("WinTitleMatchMode", $WinTitleMatchMode)
			$ret = WinGetHandle($title, $text)
			AutoItSetOption("WinTitleMatchMode", $WinTitleMatchMode) ; revert the mode back

		EndIf
	EndIf
	Return $ret
EndFunc   ;==>WinGetHandleFwd

;-------------------------------------------------------------------------------
; Name .........: ControlIsVisible
; Description ..:
; Parameters ...:
; Return .......:
; History ......: JUL 5, 2011 - Created
;-------------------------------------------------------------------------------
Func ControlIsVisible($class, $instance)
	$WinSearchChildren = AutoItSetOption('WinSearchChildren', 1)
	$flags = WinGetState("[CLASS:" & $class & "; INSTANCE:" & $instance & "]")
	AutoItSetOption('WinSearchChildren', $WinSearchChildren)
	Return (BitAND($flags, 2) <> 0)
EndFunc   ;==>ControlIsVisible


;-------------------------------------------------------------------------------
; Name .........: ControlWait   ( ie. ControlGetHandleWithTimeout )
; Parameters ...: See ‘ControlGetHandle’
;                 - timeout in seconds
; Return .......: Returns handle to the control, or "" (blank string) if the
;                 window was not been found by the time the timeout occurred.
;                 Sets @error to 1 if no window matched the criteria.
;                 Sets @extended to the approximate time elapsed (in milliseconds)
; History ......: OCT 23, 2012 - Created
;-------------------------------------------------------------------------------
Func ControlWait($title, $text, $controlID, $timeout = 0)
	FuncTrace('ControlWait', $title, $text, $controlID, $timeout)
	Local Const $LOCAL_FUNCT_TIMER_CYCLE_DURATION = 100 ; << to adjust depending on context, or make it a parameter
	If $timeout = Default Then $timeout = 0
	Local $timer = FuncTimerCreate($timeout)
	Local $handle = ControlGetHandle($title, $text, $controlID)
	Local $errorCode = @error
	Local $extendedErrorCode = 0
	While $handle = ""
		Sleep($LOCAL_FUNCT_TIMER_CYCLE_DURATION)
		$handle = ControlGetHandle($title, $text, $controlID)
		$errorCode = @error
		If FuncTimerIsElapsed($timer) Then ExitLoop
	WEnd
	Local $extendedErrorCode = FuncTimerGetTimeInMs($timer)
	Return SetError($errorCode, $extendedErrorCode, $handle)
EndFunc   ;==>ControlWait


#cs
	UnitTest__ControlWait__SciteWait()
	; RETURNED
	; $ctrlHandle = 0x00000000000505A6
	; @extended = 0
	; $ctrlHandle = 0x00000000001E0616
	; @extended = 0
	Func UnitTest__ControlWait__SciteWait()
	$timeout = 1
	Local $ctrlHandle = ControlWait("", "", "Scintilla1", $timeout)
	ConsoleWrite('$ctrlHandle = ' & $ctrlHandle & @CRLF & '@extended = ' & @extended & @CRLF)
	$timeout = 1
	Local $ctrlHandle = ControlWait("", "", "Scintilla2", $timeout)
	ConsoleWrite('$ctrlHandle = ' & $ctrlHandle & @CRLF & '@extended = ' & @extended & @CRLF)
	EndFunc
#ce

#cs
	UnitTest__ControlWait__TimeoutOnNonExist()
	; RETURNED
	; $ctrlHandle =
	; @extended = 3004
	Func UnitTest__ControlWait__TimeoutOnNonExist()
	$timeout = 3
	Local $ctrlHandle = ControlWait("", "", "NONEXISTING", $timeout)
	ConsoleWrite('$ctrlHandle = ' & $ctrlHandle & @CRLF & '@extended = ' & @extended & @CRLF)
	EndFunc
#ce
