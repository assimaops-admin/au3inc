#include-once
#include <BaseUAFLibs\UDFsx\FileGetTimeV2.au3>

; main session variables
; New format (eg. "201007-12-15-0007")
$g_szSessionId = ""

; session id sub-elements
$g_szSessionId_YEAR = ""
$g_szSessionId_MON = ""
$g_szSessionId_MDAY = ""
$g_szSessionId_HOUR = ""
$g_szSessionId_MIN = ""
$g_szSessionId_SEC = ""
$g_szSessionId_FORMAT = ""

;-------------------------------------------------------------------------------
; Name .........: EnsureSessionId()
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func EnsureSessionId($format = "%04d%02d-%02d-%02d-%02d%02d")
	If $g_szSessionId = "" Then
		$g_szSessionId_YEAR = MyYEAR()
		$g_szSessionId_MON = MyMON()
		$g_szSessionId_MDAY = MyMDAY()
		$g_szSessionId_HOUR = MyHOUR()
		$g_szSessionId_MIN = MyMIN()
		$g_szSessionId_SEC = MySEC()
	EndIf
	If $g_szSessionId = "" Or $g_szSessionId_FORMAT <> $format Then
		$g_szSessionId = StringFormat( _
				$format _
				, $g_szSessionId_YEAR _
				, $g_szSessionId_MON _
				, $g_szSessionId_MDAY _
				, $g_szSessionId_HOUR _
				, $g_szSessionId_MIN _
				, $g_szSessionId_SEC _
				)
		$g_szSessionId_FORMAT = $format
	EndIf
	Return $g_szSessionId
EndFunc   ;==>EnsureSessionId
