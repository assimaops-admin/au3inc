#include-once
#include <GUIConstantsEx.au3> ; $GUI_EVENT_CLOSE
#include <WindowsConstants.au3>
; custom
#include <AuItCustLibs\ArrayV2.au3>
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include ".\WinV2.au3"

$g_hLastTokenWindow = ""
$g_tokenWindowsCount = 0

#cs
	; MANUAL TESTING
	Set_("TokenWnd.Opt.OnCreate.Show")
	;TokenWnd_Create("Blabla Title", "Some advanced text and info...")
	;Sleep(5000)
#ce


; EXPORTED

Func TokenWnd_Inst($title, $label1 = "")
	$handle = WinGetHandle($title)
	If $handle = "" Then
		Return TokenWnd_Create($title, $label1)
	Else
		ControlSetText($handle, "", "Static1", $label1)
		Return $handle
	EndIf
EndFunc   ;==>TokenWnd_Inst


; PRIVATE

Func TokenWnd_Create($title, $label1 = "")
	Const $CTRL_HEIGHT = 15
	Const $CTRL_LENGTH = 190
	Const $shiftXY = $g_tokenWindowsCount * 50
	$handle = GUICreate($title, 400, 50, 2000 + $shiftXY, 300 + $shiftXY, "", $WS_EX_TOOLWINDOW)
	If IsSet("TokenWnd.Opt.OnCreate.Show") Then GUISetState(@SW_SHOW, $handle)
	GUICtrlCreateLabel($label1, 0, 0, $CTRL_LENGTH, $CTRL_HEIGHT)
	If $g_tokenWindowsCount = 0 Then
		AdlibRegister("TokenWnd_PollForCloseEvent")
	EndIf
	$g_tokenWindowsCount += 1
	$g_hLastTokenWindow = $handle
	Return $handle
EndFunc   ;==>TokenWnd_Create

Func TokenWnd_GetValue($title, $index = 1)
	$handle = WinGetHandle($title)
	Return ControlGetText($title, "", "Static" & $index)
EndFunc   ;==>TokenWnd_GetValue

Func TokenWnd_Queue($title)
	$list = WinListHandles($title)
	TokenWnd_Create($title)
	While $list[0]
		$window = _ArrayPopV2($list)
		$pid = WinGetProcess($window)
		ProcessWaitCloseSlowlyForever($pid)
		; Remark: See REF.PS/20111108_223803
	WEnd
EndFunc   ;==>TokenWnd_Queue

Func TokenWnd_PollForCloseEvent()
	$msg = GUIGetMsg()
	If $msg = $GUI_EVENT_CLOSE Then
		Exit 555 ; $AU3_EXIT_SCRIPT_GLOBAL_TIMEOUT
	EndIf
EndFunc   ;==>TokenWnd_PollForCloseEvent
