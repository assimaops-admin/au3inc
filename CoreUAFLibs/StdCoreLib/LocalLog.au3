#include-once
#include ".\XcptSystem.au3"
#include ".\LogServices.au3"

$g_szLogPath = ""

;-------------------------------------------------------------------------------
; Name .........: LocalLog_IsReady
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func LocalLog_IsReady()
	Return ($g_szLogPath <> "")
EndFunc   ;==>LocalLog_IsReady

;-------------------------------------------------------------------------------
; Name .........: LocalLog_GetPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func LocalLog_GetPath()
	Return $g_szLogPath
EndFunc   ;==>LocalLog_GetPath

;-------------------------------------------------------------------------------
; Name .........: LocalLog_Init
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func LocalLog_Init($logFilePath, $szTextLine)
	Global $g_szLogPath
	ConsoleWrite("LocalLog_Init()... #ConsoleOnly" & @CRLF)
	ConsoleWrite("  `-> $logFilePath = '" & $logFilePath & "' #ConsoleOnly" & @CRLF)
	ConsoleWrite("  `-> $szTextLine = '" & $szTextLine & "' #ConsoleOnly" & @CRLF)
	; set the global and write first line
	Assert($g_szLogPath = "", '$g_szLogPath = ""')
	$g_szLogPath = $logFilePath
	ConsoleWrite("-----" & @CRLF & @CRLF)
	Return __Private_LocalLog_AddEntry($szTextLine)
EndFunc   ;==>LocalLog_Init

;-------------------------------------------------------------------------------
; Name .........: LocalLog_AddEntryCallback [SERVICE]
; Description ..: Log in the file
; Return .......: n/a
;-------------------------------------------------------------------------------
Func LocalLog_AddEntryCallback($szTextLine)
	Return LocalLog_AddEntry($szTextLine)
EndFunc   ;==>LocalLog_AddEntryCallback

;-------------------------------------------------------------------------------
; Name .........: LocalLog_AddEntry
; Description ..: Log in the file
; Return .......: n/a
;-------------------------------------------------------------------------------
Func LocalLog_AddEntry($szTextLine)
	; check initialisation
	If Not LocalLog_IsReady() Then Return 0
	; write to console and write to file
	Return EasyLogWrite($g_szLogPath, $szTextLine)
EndFunc   ;==>LocalLog_AddEntry

;-------------------------------------------------------------------------------
; Name .........: LocalLog_Term
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func LocalLog_Term($szTextLine = "")
	; fatal errors tracking
	If $szTextLine <> "" Then __Private_LocalLog_AddEntry($szTextLine)
	; terminate session
	$g_szLogPath = ""
EndFunc   ;==>LocalLog_Term

;-------------------------------------------------------------------------------
; Name .........: __Private_LocalLog_AddEntry
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func __Private_LocalLog_AddEntry($szTextLine = "")
	Return LogServices_AddEntry($szTextLine) ; will call LocalLog_AddEntryCallback in return
EndFunc   ;==>__Private_LocalLog_AddEntry
