#include-once
#include <AuItCustLibs\UDFs\AutoItEx.Globals.Accessors.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>

;-------------------------------------------------------------------------------
; Name .........: FileRegisterIsEntry
; Description ..:
; Notes ........:
; Return .......:
; History ......: NOV 18, 2010 - Created header comment
;-------------------------------------------------------------------------------
Func FileRegisterIsEntry($file, $line, $lock = 1)

	$ret = False

	If $lock Then
		; lock the register
		While FileExists($file & ".lock")
			Sleep(500)
		WEnd
		FileWrite($file & ".lock", "")
	EndIf

	; read the file
	Local $array
	If _FileReadToArray($file, $array) Then
		If $array[0] Then
			If _ArraySearch($array, $line) <> -1 Then
				$ret = 1
			EndIf
		EndIf
	EndIf

	; <UNLOCK_THE_REGISTER_AND_RETURN>
	FileDelete($file & ".lock")
	Return $ret
	; </UNLOCK_THE_REGISTER_AND_RETURN>

EndFunc   ;==>FileRegisterIsEntry

;-------------------------------------------------------------------------------
; Name .........: FileRegisterWriteLine
; Description ..:
; Notes ........:
; Return .......:
; History ......: NOV 18, 2010 - Added return values and created header comment
;-------------------------------------------------------------------------------
Func FileRegisterWriteLine($file, $line, $allow_duplicates = 0)

	$ret = False

	; lock the register
	While FileExists($file & ".lock")
		Sleep(500)
	WEnd
	FileWrite($file & ".lock", "")

	; nothing to add?!
	; WARNING: do not attempt to set another lock
	If (Not $allow_duplicates) And FileRegisterIsEntry($file, $line, 0) Then
		; <UNLOCK_THE_REGISTER_AND_RETURN>
		FileDelete($file & ".lock")
		Return $ret
		; </UNLOCK_THE_REGISTER_AND_RETURN>
	EndIf

	; add the entry in the register
	$ret = FileWriteLine($file, $line)

	; <UNLOCK_THE_REGISTER_AND_RETURN>
	FileDelete($file & ".lock")
	Return $ret
	; </UNLOCK_THE_REGISTER_AND_RETURN>

EndFunc   ;==>FileRegisterWriteLine

;-------------------------------------------------------------------------------
; Name .........: FileRegisterDeleteLine
; Description ..:
; Notes ........:
; Return .......:
; History ......: NOV 18, 2010 - Added return values and created header comment
;-------------------------------------------------------------------------------
Func FileRegisterDeleteLine($file, $line)

	$ret = False

	; lock the register
	While FileExists($file & ".lock")
		Sleep(500)
	WEnd
	FileWrite($file & ".lock", "")

	; remove entry from the register
	$content = FileRead($file)
	$content = StringReplace($content, $line & @CRLF, "")

	; nothing to remove?!
	If Not @extended Then
		; <UNLOCK_THE_REGISTER_AND_RETURN>
		FileDelete($file & ".lock")
		Return $ret
		; </UNLOCK_THE_REGISTER_AND_RETURN>
	EndIf

	; Note: for the sake of testing, add two more seconds between the
	; read and the write of the file (REF.PS/20101020_184812)
	If IsSet("REF.INSIDER/20101020_184812") Then
		Sleep(2000)
	EndIf

	$ret = FileSetContent($file, $content)

	; <UNLOCK_THE_REGISTER_AND_RETURN>
	FileDelete($file & ".lock")
	Return $ret
	; </UNLOCK_THE_REGISTER_AND_RETURN>

EndFunc   ;==>FileRegisterDeleteLine

#cs ------------------------------ THE BUG ------------------------------
	# include "..\StdOSLib\FileV2.au3"

	if $CmdLine[0] = 0 then

	FileDelete("test.txt")
	FileWriteLine("test.txt", "line1")
	FileWriteLine("test.txt", "line2")
	FileWriteLine("test.txt", "line3")
	FileWriteLine("test.txt", "line4")
	Run(@AutoItExe & " " & @ScriptName & " " & 1, "", @SW_HIDE)

	$remove = "line2"
	$content = FileRead("test.txt")
	$content = StringReplace($content, $remove&@CRLF, "")
	FileSetContent("test.txt", $content)

	Sleep(1000)

	$append = "line2"
	$sCommand = ">> test.txt echo "&$append
	Run(@ComSpec & " /C " & $sCommand, "", @SW_HIDE)
	else

	Sleep(500)

	$remove = "line3"
	$content = FileRead("test.txt")
	$content = StringReplace($content, $remove&@CRLF, "")
	; Note: for the sake of testing, add two more seconds between the
	; read and the write of the file (REF.PS/20101020_184812)
	Sleep(2000)
	FileSetContent("test.txt", $content)

	$append = "line3"
	$sCommand = ">> test.txt echo "&$append
	Run(@ComSpec & " /C " & $sCommand, "", @SW_HIDE)
	endif
#ce


#cs ------------------------------ THE FIX ------------------------------
	Set_("REF.INSIDER/20101020_184812")
	# include "FileRegister.au3"

	; SetUp
	if $CmdLine[0] = 0 then
	FileDelete("test.txt")
	FileWriteLine("test.txt", "line1")
	FileWriteLine("test.txt", "line2")
	FileWriteLine("test.txt", "line3")
	FileWriteLine("test.txt", "line4")
	Run(@AutoItExe & " " & @ScriptName & " " & 1, "", @SW_HIDE)
	endif

	; Testing
	if $CmdLine[0] = 0 then
	; Process 1
	FileRegisterDeleteLine("test.txt", "line2")
	Sleep(1000)
	FileRegisterWriteLine("test.txt", "line2")
	else
	; Process 2
	Sleep(500)
	FileRegisterDeleteLine("test.txt", "line3")
	; Note: for the sake of testing, add two more seconds between the
	; read and the write of the file (REF.PS/20101020_184812)
	FileRegisterWriteLine("test.txt", "line3")
	endif
#ce
