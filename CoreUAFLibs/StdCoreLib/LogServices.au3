#include-once
#include <AuItCustLibs\UDFs\AutoItEx.Globals.Accessors.au3>
#include <AuItCustLibs\StringV2.au3>
#include ".\EasyLog.au3"
#include ".\HostInfoProxy.au3"

#cs
	; INLINE TESTING #1
	; -----------------
	;# include <StdLibXXL\LocalLog.au3>
	;LogServices_Opt_EnableTraceToLogTool("LocalLog_AddEntryCallback")
	;LocalLog_Init(@DesktopDir & "\AutomationLog.txt", "Session initialized.")

	; INLINE TESTING #2
	; -----------------
	;# include <Debug.au3>
	;LogServices_Opt_EnableTraceToDebugTool()

	; INLINE TESTING ALL
	; -----------------
	LogServices_AddEntry("test0")
	LogServices_AddEntry("test1 'bla")
	LogServices_AddEntry("test2")
	LogServices_AddEntry('test3 "bla"')
	LogServices_AddEntry('test4')
#ce

;-------------------------------------------------------------------------------
; Name .........: LogServices_Opt_EnableTraceToLogTool
; Description ..: WARNING: Formatting is automatically performed by
; LogServices_TranslateEntryForLogFile, the third parameter is not currently used!
;-------------------------------------------------------------------------------
Func LogServices_Opt_EnableTraceToLogTool($callbackName_Out, $callbackName_GetPath, $callbackName_Format = "")
	If $callbackName_Format <> "" Then LogServices_SetPrimaryCallback_Format($callbackName_Format)
	If $callbackName_GetPath <> "" Then LogServices_SetPrimaryCallback_GetPath($callbackName_Out)
	If $callbackName_Out <> "" Then LogServices_SetPrimaryCallback_Out($callbackName_Out)
EndFunc   ;==>LogServices_Opt_EnableTraceToLogTool

;-------------------------------------------------------------------------------
; Name .........: LogServices_Opt_EnableTraceToDebugTool
; Description ..: WARNING: requires # include < Debug . au3 > to be included
; by callee, otherwise this will have no effect!
;-------------------------------------------------------------------------------
Func LogServices_Opt_EnableTraceToDebugTool($callbackName_Out = "_DebugOut", $callbackName_Format = "LogServices_FormatStringForDebug")
	$debugSetupCallback = "_DebugSetup"
	Call($debugSetupCallback, @ScriptName & " - " & @AutoItPID & " (TraceToDebug service)")
	If $callbackName_Format <> "" Then LogServices_SetSecondaryCallback_Format($callbackName_Format)
	If $callbackName_Out <> "" Then LogServices_SetSecondaryCallback_Out($callbackName_Out)
EndFunc   ;==>LogServices_Opt_EnableTraceToDebugTool

;-------------------------------------------------------------------------------
; Name .........: LogServices_FormatStringForDebug
; Description ..:
;-------------------------------------------------------------------------------
Func LogServices_FormatStringForDebug($str)
	Return StringReplace($str, "'", '`')
EndFunc   ;==>LogServices_FormatStringForDebug

;-------------------------------------------------------------------------------
; Name .........: LogServices_SetPrimaryCallback_Out
; Description ..:
;-------------------------------------------------------------------------------
Func LogServices_SetPrimaryCallback_Out($callbackName)
	Assign("LogServicePrimaryCallback_Out", $callbackName, 2)
EndFunc   ;==>LogServices_SetPrimaryCallback_Out

;-------------------------------------------------------------------------------
; Name .........: LogServices_SetPrimaryCallback_GetPath
; Description ..: The path returned should exist. Output directory.
; The callback takes a parameter which is the name of the computer
;-------------------------------------------------------------------------------
Func LogServices_SetPrimaryCallback_GetPath($callbackName)
	Assign("LogServicePrimaryCallback_GetPath", $callbackName, 2)
EndFunc   ;==>LogServices_SetPrimaryCallback_GetPath

;-------------------------------------------------------------------------------
; Name .........: LogServices_SetPrimaryCallback_Format
; Description ..:
;-------------------------------------------------------------------------------
Func LogServices_SetPrimaryCallback_Format($callbackName)
	Assign("LogServicePrimaryCallback_Format", $callbackName, 2)
EndFunc   ;==>LogServices_SetPrimaryCallback_Format

;-------------------------------------------------------------------------------
; Name .........: LogServices_SetSecondaryCallback_Out
; Description ..:
;-------------------------------------------------------------------------------
Func LogServices_SetSecondaryCallback_Out($callbackName)
	Assign("LogServiceSecondaryCallback_Out", $callbackName, 2)
EndFunc   ;==>LogServices_SetSecondaryCallback_Out

;-------------------------------------------------------------------------------
; Name .........: LogServices_SetSecondaryCallback_Format
; Description ..:
;-------------------------------------------------------------------------------
Func LogServices_SetSecondaryCallback_Format($callbackName)
	Assign("LogServiceSecondaryCallback_Format", $callbackName, 2)
EndFunc   ;==>LogServices_SetSecondaryCallback_Format

;-------------------------------------------------------------------------------
; Name .........: LogServices_AddEntry
; Description ..: Mediator between the console logger and the persistant logger
;-------------------------------------------------------------------------------
Func LogServices_AddEntry($entry)
	$ret = 1

	$isoTime = _NowISO_Std()

	$entry = LogServices_TranslateEntryForUser($entry)
	$logEntry = LogServices_TranslateEntryForLogFile($entry)
	$consoleEntry = LogServices_TranslateEntryForConsole($entry)

	;$logEntry = Call(Eval("LogServicePrimaryCallback_Format"), $logEntry)
	$debugEntry = Call(Eval("LogServiceSecondaryCallback_Format"), $entry)

	; Service 3
	; Trace everything
	; - WARNING: if two processes have the same name, file writing might fail
	$isAtvxTraceEnv = EnvGet("AtvxTrace")
	$isAtvxTraceReg = RegRead("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", "AtvxTrace")
	If ($isAtvxTraceEnv = 1) Or ($isAtvxTraceReg = 1) Then
		$simpleLocalLogs = HostInfoProxy_GetSimpleLocalLogDir_EnsureExists()
		$traceFileName = $simpleLocalLogs & "\AutoIt.TraceEx." & StringTrimRight(@ScriptName, 4) & ".Log.txt"
		$fileHandle = FileOpen($traceFileName, 1)
		EasyLogWrite($fileHandle, $logEntry, $isoTime)
		FileClose($fileHandle)
	EndIf

	; Service 2
	; The function call below should theoretically resolve into: '_DebugOut', '_DebugOutV2' or else
	If IsDeclared("LogServiceSecondaryCallback_Out") Then
		$debugEntry = Call(Eval("LogServiceSecondaryCallback_Format"), $entry)
		$ret = Call(Eval("LogServiceSecondaryCallback_Out"), $debugEntry)
	EndIf

	; Service 1
	; The function call below should theoretically resolve into: 'AtmSession_LogEntry'
	If IsDeclared("LogServicePrimaryCallback_Out") Then
		$ret = Call(Eval("LogServicePrimaryCallback_Out"), $logEntry)
	EndIf

	; Console
	If LogServices_IsEntryForConsole($entry) Then
		If IsSet("ENABLE_TIME_PREFIXED_LOGGING_IN_CONSOLE") Then
			ConsoleWrite(EasyLogString($consoleEntry, $isoTime) & @CRLF)
		Else
			ConsoleWrite($consoleEntry & @CRLF)
		EndIf
	EndIf

	Return $ret
EndFunc   ;==>LogServices_AddEntry

;-------------------------------------------------------------------------------
; Name .........: LogServices_TranslateEntryForUser
; Description ..: Add a trailing dot
; Remarks ......: n/a
;-------------------------------------------------------------------------------
Func LogServices_TranslateEntryForUser($szTextLine)
	$szTextLine = StringReplaceRightChars($szTextLine, $STRING_HORIZONTAL_WHITESPACE_CHARS, "")
	If Not StringInStr(".?!", StringRight($szTextLine, 1)) Then $szTextLine &= "."
	Return $szTextLine
EndFunc   ;==>LogServices_TranslateEntryForUser

;-------------------------------------------------------------------------------
; Name .........: LogServices_TranslateEntryForLogFile
; Description ..: Add prefixes such as 'Trace', 'Warning', etc.
; Remarks ......: Backward compatible with old comments.
;-------------------------------------------------------------------------------
Func LogServices_TranslateEntryForLogFile($szTextLine)

	$isTrace = StringStartsWith($szTextLine, "Trace: ", 1)
	$isWarning = StringStartsWith($szTextLine, "Warning: ", 1)
	$isError = StringStartsWith($szTextLine, "Error: ", 1)

	$isScenario = StringStartsWith($szTextLine, "Scenario: ", 1)
	$isScenario = StringStartsWith($szTextLine, "Story: ", 1)
	$isInfo = StringStartsWith($szTextLine, "Comment: ", 1)

	$isSymbol = StringInStr("+>-!", StringLeft($szTextLine, 1))

	$isPrefix = $isTrace Or $isWarning Or $isError _
			Or $isScenario Or $isInfo Or $isSymbol

	If (Not $isPrefix) Then

		$szTextLine = "Trace: " & $szTextLine

	ElseIf $isSymbol Then

		$symbolicPrefix = StringLeft($szTextLine, 2)
		$withoutPrefix = StringTrimLeft($szTextLine, 2)
		Switch $symbolicPrefix
			Case "+ "
				$szTextLine = "Story: " & $withoutPrefix
			Case "> "
				$szTextLine = "Comment: " & $withoutPrefix
			Case "- "
				$szTextLine = "Warning: " & $withoutPrefix
			Case "! "
				$szTextLine = "Error: " & $withoutPrefix
		EndSwitch

	EndIf

	Return $szTextLine

EndFunc   ;==>LogServices_TranslateEntryForLogFile

;-------------------------------------------------------------------------------
; Name .........: LogServices_TranslateEntryForConsole
; Description ..: Add prefixes such as 'Trace', 'Warning', etc.
; Remarks ......: Backward compatible with old comments.
;-------------------------------------------------------------------------------
Func LogServices_TranslateEntryForConsole($szTextLine)

	$szTextLineOut = StringReplaceLeftSeqs($szTextLine, "Trace: ", "")
	If $szTextLineOut = $szTextLine Then $szTextLineOut = StringReplaceLeftSeqs($szTextLine, "[TRACE] ", "")
	Return $szTextLineOut

EndFunc   ;==>LogServices_TranslateEntryForConsole

;-------------------------------------------------------------------------------
; Name .........: LogServices_IsEntryForConsole
; Description ..:
; Remarks ......: Backward compatible with old comments.
;-------------------------------------------------------------------------------
Func LogServices_IsEntryForConsole($szTextLine)
	$ret = 1
	If IsSet("PRINT_SCENARIO_ONLY___GLOBAL_SET") Then
		If (Not StringStartsWith($szTextLine, "Scenario:")) _
				And (Not StringStartsWith($szTextLine, "+")) _
				Then $ret = 0
	EndIf
	If IsDeclared("AUTOIT3WRAPPER_CHANGE2CUI_FLAG") Then $ret = 0
	;If IsDeclared("AUTOIT3WRAPPER_CONSOLE_ONLY_PREF") Then $ret = 0 ; << Do not declare otherwise traces are not displayed anymore :(
	Return $ret
EndFunc   ;==>LogServices_IsEntryForConsole
