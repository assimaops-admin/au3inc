; ========================================================================================
; Module ...........: DirMoveV2.au3
; Author ...........: Patryk S. ( pszczepa at gmail dot com )
; Description ......: Specifically designed to move/upload and merge directories.
; History ..........: JUL 01, 2010 - Created
; ========================================================================================
#include-once
#include <File.au3>

;-------------------------------------------------------------------------------
; Name .........: DirMoveV2 (could have eventually been called 'DirRename')
; Description ..: Same as 'DirMove' except for the part that says "If the
;                 destination already exists and the overwrite flag is
;                 specified then the source directory will be moved inside
;                 the destination." instead we move the content inside the
;                 destination directory and delete the original.
; Return .......: binary AND of all the 'move' and 'delete' operations
;-------------------------------------------------------------------------------
Func DirMoveV2($src, $dest, $flag)
	$ret = 1
	If $flag = 1 And FileExists($dest) And StringInStr(FileGetAttrib($dest), "D") Then
		; move all the content in the destination
		$ret = DirTransfert($src, $dest, $flag)
		; and delete the original
		If $ret Then
			$ret = DirRemove($src, $flag)
		EndIf
	Else
		; the old bahavior for all the other cases
		$ret = DirMove($src, $dest, $flag)
	EndIf
	Return $ret
EndFunc   ;==>DirMoveV2

;-------------------------------------------------------------------------------
; Name .........: DirTransfert
; Description ..: Move the content of a directory inside another directory.
; Note .........: DO NOT delete the original directory. Create the destination
;                 directory if required.
; Return .......: binary AND of all the 'move' and 'delete' operations
;-------------------------------------------------------------------------------
Func DirTransfert($src, $dest, $flag)
	$ret = 1
	; create destination if doesn't exit
	If Not FileExists($dest) Then
		$ret_ = DirCreate($dest)
		$ret = BitAND($ret, $ret_)
	EndIf
	; move all the content in the other directory
	$entries = _FileListToArray($src)
	If Not @error Then
		For $i = 1 To $entries[0]
			$entry = $src & "\" & $entries[$i]
			$entry_dest = $dest & "\" & $entries[$i]
			If StringInStr(FileGetAttrib($entry), "D") Then
				$ret_ = DirMoveV2($entry, $entry_dest, $flag)
				$ret = BitAND($ret, $ret_)
			Else
				$ret_ = FileMove($entry, $entry_dest, $flag)
				$ret = BitAND($ret, $ret_)
			EndIf
		Next
	EndIf
	Return $ret
EndFunc   ;==>DirTransfert
