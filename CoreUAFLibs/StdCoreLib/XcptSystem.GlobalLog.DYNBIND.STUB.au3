#include-once
#include "..\UDFs\DynamicCodeBinding.au3"

Func DYNBIND_WarningSub($txt)
	CallFuncWithOneParam("WarningSub", $txt)
EndFunc   ;==>DYNBIND_WarningSub

Func DYNBIND_ErrorSub($txt)
	CallFuncWithOneParam("ErrorSub", $txt)
EndFunc   ;==>DYNBIND_ErrorSub

Func DYNBIND_GlobalLog($type, $txt, $LogOnly = 0)
	CallFuncWithThreeParams("GlobalLog", $type, $txt, $LogOnly)
EndFunc   ;==>DYNBIND_GlobalLog
