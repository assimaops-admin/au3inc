#include-once
#include ".\LogServices.au3"

;-------------------------------------------------------------------------------
; Name .........: Trace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func Trace($txt)
	LogServices_AddEntry($txt)
EndFunc   ;==>Trace

;-------------------------------------------------------------------------------
; Name .........: FuncTrace
; Description ..: Low level. Use it to comment helpers and libraries.
; Return .......: n/a
;-------------------------------------------------------------------------------
Func FuncTrace($func, $vvv = 0, $v_0 = 0, $v_1 = 0, $v_2 = 0, $v_3 = 0, $v_4 = 0, $v_5 = 0, $v_6 = 0, $v_7 = 0, $v_8 = 0)
	; CODE-IN-SYNC-BELOW ------------------------------------------------------
	Local $av_Array[9] = [$v_0, $v_1, $v_2, $v_3, $v_4, $v_5, $v_6, $v_7, $v_8]
	$params = ""
	If @NumParams > 1 Then $params &= '"' & $vvv & '"'
	If @NumParams > 2 Then
		ReDim $av_Array[@NumParams - 2]
		For $v_n In $av_Array
			$params &= ", " & '"' & $v_n & '"'
		Next
	EndIf
	LogServices_AddEntry($func & '(' & $params & ')... ')
	; ---------------------------------------------- END-CODE-IN-SYNC ---------
	Return $func
EndFunc   ;==>FuncTrace

;-------------------------------------------------------------------------------
; Name .........: FuncTraceIf
; Description ..: Low level. Use it to comment helpers and libraries.
; Return .......: n/a
;-------------------------------------------------------------------------------
Func FuncTraceIf($cond, $func, $vvv = 0, $v_0 = 0, $v_1 = 0, $v_2 = 0, $v_3 = 0, $v_4 = 0, $v_5 = 0, $v_6 = 0, $v_7 = 0, $v_8 = 0)
	If Not $cond Then Return
	; CODE-IN-SYNC-BELOW ------------------------------------------------------
	Local $av_Array[9] = [$v_0, $v_1, $v_2, $v_3, $v_4, $v_5, $v_6, $v_7, $v_8]
	$params = ""
	If @NumParams > 1 Then $params &= '"' & $vvv & '"'
	If @NumParams > 2 Then
		ReDim $av_Array[@NumParams - 2]
		For $v_n In $av_Array
			$params &= ", " & '"' & $v_n & '"'
		Next
	EndIf
	LogServices_AddEntry($func & '(' & $params & ')... ')
	; ---------------------------------------------- END-CODE-IN-SYNC ---------
EndFunc   ;==>FuncTraceIf

;-------------------------------------------------------------------------------
; Name .........: ValTraceStr
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func ValTraceStr($value, $name = "")
	Return ";  " & $name & " = '" & $value & "'"
EndFunc   ;==>ValTraceStr

;-------------------------------------------------------------------------------
; Name .........: ValTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func ValTrace($value, $name = "")
	LogServices_AddEntry(ValTraceStr($value, $name))
EndFunc   ;==>ValTrace

;-------------------------------------------------------------------------------
; Name .........: MultiValTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func MultiValTrace($values_and_names)
	Local $fullString = ""
	Local $isDisplayingRoundedValues = 0
	For $i = 1 To $values_and_names[$i - 1] Step 2
		$variable_value = $values_and_names[$i]
		$variable_name = $values_and_names[$i + 1]
;~ 		If IsFloat($variable_value) Then
;~ 			$variable_value = Round($variable_value) ; WARNING: (#NoDecimals), (#TwoDecimals), etc.
;~ 			$isDisplayingRoundedValues = 1
;~ 		EndIf
		$fullString &= ValTraceStr($variable_value, $variable_name)
	Next
	If $isDisplayingRoundedValues Then $fullString &= " (#NoDecimals) (#TraceShowsRoundedValuesWarning)"
	LogServices_AddEntry($fullString)
EndFunc   ;==>MultiValTrace

;-------------------------------------------------------------------------------
; Name .........: ArrayTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func ArrayTrace($value, $name = "")
	If Not IsArray($value) Then
		LogServices_AddEntry("  " & $name & " is not an array.")
	EndIf
	LogServices_AddEntry("  " & $name)
	$digits_count = StringLen(UBound($value) - 1)
	For $i = 0 To UBound($value) - 1
		LogServices_AddEntry(StringFormat("  " & "  " & "| %0" & $digits_count & "d | '%s'", $i, $value[$i]))
	Next
EndFunc   ;==>ArrayTrace

;-------------------------------------------------------------------------------
; Name .........: TimeTrace (param in milliseconds - print in seconds)
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func TimeTrace($time, $desc = "")
	Const $TIME_TRACE_INPUT_UNITS = "ms"
	Const $TIME_TRACE_OUTPUT_UNITS = "s"
	Const $TIME_TRACE_ROUND_PRECISION_IN_SECONDS = 1
	$time_in_s = $time / 1000
	$time_in_s = Round($time_in_s, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS)
	LogServices_AddEntry("  dT = " & $time_in_s & "s " & "(" & $desc & ")")
EndFunc   ;==>TimeTrace

;-------------------------------------------------------------------------------
; Name .........: RunTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func RunTrace($cmd)
	$callType = "Run"
	LogServices_AddEntry($callType & "[" & $cmd & "]...")
	Return $cmd
EndFunc   ;==>RunTrace

;-------------------------------------------------------------------------------
; Name .........: RunWaitTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func RunWaitTrace($cmd)
	$callType = "RunWait"
	LogServices_AddEntry($callType & "[" & $cmd & "]...")
	Return $cmd
EndFunc   ;==>RunWaitTrace

;-------------------------------------------------------------------------------
; Name .........: RetTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func RetTrace($ret, $funcTrace = "", $sLine = @ScriptLineNumber)
	$str = ""
	$str &= LineNumTracePrefix($sLine)
	$str &= " " & 'Return "' & $ret & '"'
	If IsString($funcTrace) And $funcTrace <> "" Then
		$str &= " " & "(" & $funcTrace & ")"
	EndIf
	LogServices_AddEntry($str)
	Return $ret
EndFunc   ;==>RetTrace

;-------------------------------------------------------------------------------
; Name .........: ExitTrace
; Description ..: Low level. Use it to comment helpers and libraries.
;-------------------------------------------------------------------------------
Func ExitTrace($exit_code, $sLine = @ScriptLineNumber)
	$str = ""
	$str &= LineNumTracePrefix($sLine)
	$str &= " " & "Exit " & $exit_code & "...... x_x."
	LogServices_AddEntry($str)
	Return $exit_code
EndFunc   ;==>ExitTrace

;-------------------------------------------------------------------------------
; Name .........: LineNumTracePrefix
; Description ..: Private.
;-------------------------------------------------------------------------------
Func LineNumTracePrefix($sLine)
	Return "[l. " & StringFormat("%04d", Int($sLine)) & "]"
EndFunc   ;==>LineNumTracePrefix
