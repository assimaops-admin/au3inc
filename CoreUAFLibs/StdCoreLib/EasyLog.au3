#include-once
#include <BaseUAFLibs\UDFsx\FileGetTimeV2.au3>

Func EasyLogString($string, $isoTime = "")
	If $isoTime = "" Then $isoTime = _NowISO_Std()
	Return $isoTime & " " & $string
EndFunc   ;==>EasyLogString

Func EasyLogWrite($file, $string, $isoTime = "")
	Return FileWriteLine($file, EasyLogString($string, $isoTime))
EndFunc   ;==>EasyLogWrite
