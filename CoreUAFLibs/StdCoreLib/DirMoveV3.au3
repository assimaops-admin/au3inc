; ========================================================================================
; Module ...........: DirMoveV3.au3
; Author ...........: Patryk S. ( pszczepa at gmail dot com )
; Description ......: Specifically designed to move/upload directories safely and rapidly
;                     whether they are on the same drive or in different places on the
;                     network.
; Note .............: See EasyRobocopy.au3 and EasyRobocopy.My.au3 for alternative tools.
; Note .............: Robocopy return codes meanings
;                      0 - No errors occurred and no files were copied.
;                      1 - One of more files were copied successfully.
;                      2 - Extra files or directories were detected. Examine the log file
;                          for more information.
;                      4 - Mismatched files or directories were detected.  Examine the
;                          log file for more information.
;                      8 - Some files or directories could not be copied and the retry
;                          limit was exceeded.
;                     16 - Robocopy did not copy any files. Check the command line
;                          parameters and verify that Robocopy has enough rights to write
;                          to the destination folder.
; History ..........: AUG 13, 2010 - Created
; ========================================================================================
#include-once
#include ".\DirMoveV2.au3"

;-------------------------------------------------------------------------------
; Name .........: DirMoveV3 (could have eventually been called 'DirRename')
; Description ..: Same as 'DirMove' except for the part that says "If the
;                 destination already exists and the overwrite flag is
;                 specified then the source directory will be moved inside
;                 the destination." instead we move the content inside the
;                 destination directory and delete the original.
; Return .......: binary AND of all the 'move' and 'delete' operations
;-------------------------------------------------------------------------------
Func DirMoveV3($src, $dest, $flag)

	Dim $szDrive, $szDir, $szFName, $szExt
	_PathSplit($src, $szDrive, $szDir, $szFName, $szExt)
	Dim $szDrive2, $szDir2, $szFName2, $szExt2
	_PathSplit($dest, $szDrive2, $szDir2, $szFName2, $szExt2)

	If $szDrive <> $szDrive2 Then
		Return DirMoveV2($src, $dest, $flag)
	Else
		$robocopy_flags = "/e /move"
		If Not $flag Then
			$robocopy_flags &= " /xo /xn"
		EndIf
		$commandLine = StringFormat('robocopy %s "%s" "%s"', $robocopy_flags, $src, $dest)
		$ret = RunWait($commandLine, "", @SW_HIDE)
		Return ($ret = 0 Or $ret = 1)
	EndIf

EndFunc   ;==>DirMoveV3
