#include-once
#include <Date.au3>
#include <Array.au3>
#include "..\UDFs\ServicesV01.au3"

$_DummyDate_NewDate = 0
$_DummyDate_RestoreW32Time = 0
$_DummyDate_tOriginalTime = 0


;-------------------------------------------------------------------------------
; Name .........: DummyDate_SetDate [METHOD]
; Description ..:
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func DummyDate_SetDate($year, $month, $day)
	$_DummyDate_NewDate = _ArrayCreate($year, $month, $day)
EndFunc   ;==>DummyDate_SetDate

;-------------------------------------------------------------------------------
; Name .........: DummyDate_IsInUse [METHOD]
; Description ..:
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func DummyDate_IsInUse()
	Return ($_DummyDate_NewDate <> 0) Or ($_DummyDate_tOriginalTime <> 0)
EndFunc   ;==>DummyDate_IsInUse

;-------------------------------------------------------------------------------
; Name .........: DummyDateStartupProc [CALLBACK] [REF.PS/20100608_221558]
; Description ..: Call at the begining of the dual process
;                   -> Set the new system time
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func DummyDateStartupProc()
	If ($_DummyDate_NewDate <> 0) Then
		$_DummyDate_tOriginalTime = _Date_Time_GetLocalTime()
		If _ServiceRunning("W32Time") Then
			$_DummyDate_RestoreW32Time = True
			RunWait("net stop W32Time", "", @SW_HIDE)
		EndIf
		FileWrite("C:\DummyDateChange.token", "")
		_SetDate($_DummyDate_NewDate[2], $_DummyDate_NewDate[1], $_DummyDate_NewDate[0])
		FileDelete("C:\DummyDateChange.token")
		$_DummyDate_NewDate = 0
	EndIf
EndFunc   ;==>DummyDateStartupProc

;-------------------------------------------------------------------------------
; Name .........: DummyDateShutdownProc [CALLBACK] [REF.PS/20100608_222009]
; Description ..: Call before closing the dual process (whether timeout or not)
;                   -> Restore the real date/time
;                   -> Restore the windows time service ("Horloge Windows")
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;                 JUN 9, 2010 - Added code for handling tests that start just
;                               before midnight. Find trace-output below:
;                                 --------------
;                                 $month = 6
;                                 $day = 30
;                                 $year = 2010
;                                 $hour = 23
;                                 --------------
;                                 $newHour = 23
;                                 $newMin = 59
;                                 $newSec = 59
;                                 --------------
;                                 $newHour = 23 and $newMin = 59 and $newSec = 59
;                                 $newHour = 0
;                                 --------------
;                                 $newHour < $hour
;                                 $szOldDate = 2010/6/30
;                                 $szNewDate = _DateAdd('d', 1, $szOldDate)
;                                 $szNewDate = 2010/07/01
;                                 $month = 7
;                                 $day = 1
;                                 $year = 2010
;                                 --------------
;                                 _SetDate($day,$month,$year)
;                                 --------------
;-------------------------------------------------------------------------------
Func DummyDateShutdownProc()
	If ($_DummyDate_tOriginalTime <> 0) Then
		;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		$aOriginalTime = _Date_Time_SystemTimeToArray($_DummyDate_tOriginalTime)
		$month = $aOriginalTime[0]
		$day = $aOriginalTime[1]
		$year = $aOriginalTime[2]
		$hour = $aOriginalTime[3]
		;FileWriteLine("C:\debug.dummydate.txt", "$month = "&$month)
		;FileWriteLine("C:\debug.dummydate.txt", "$day = "&$day)
		;FileWriteLine("C:\debug.dummydate.txt", "$year = "&$year)
		;FileWriteLine("C:\debug.dummydate.txt", "$hour = "&$hour)
		;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		$tNewTime = _Date_Time_GetLocalTime()
		$aNewTime = _Date_Time_SystemTimeToArray($tNewTime)
		$newHour = $aNewTime[3]
		$newMin = $aNewTime[4]
		$newSec = $aNewTime[5]
		;FileWriteLine("C:\debug.dummydate.txt", "$newHour = "&$newHour)
		;FileWriteLine("C:\debug.dummydate.txt", "$newMin = "&$newMin)
		;FileWriteLine("C:\debug.dummydate.txt", "$newSec = "&$newSec)
		;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		If $newHour = 23 And $newMin = 59 And $newSec = 59 Then
			; we"re about to pass midnight
			Sleep(2000)
			$tNewTime = _Date_Time_GetLocalTime()
			$aNewTime = _Date_Time_SystemTimeToArray($tNewTime)
			$newHour = $aNewTime[3]
			;FileWriteLine("C:\debug.dummydate.txt", "$newHour = 23 and $newMin = 59 and $newSec = 59")
			;FileWriteLine("C:\debug.dummydate.txt", "$newHour = "&$newHour)
			;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		EndIf
		If $newHour < $hour Then
			; we passed midnight -- add one day to the original date
			$szOldDate = $year & '/' & $month & '/' & $day
			$szNewDate = _DateAdd('d', 1, $szOldDate)
			$aNewDate = ""
			$aNewTime = ""
			_DateTimeSplit($szNewDate, $aNewDate, $aNewTime)
			$year = $aNewDate[1]
			$month = $aNewDate[2]
			$day = $aNewDate[3]
			;FileWriteLine("C:\debug.dummydate.txt", "$newHour < $hour")
			;FileWriteLine("C:\debug.dummydate.txt", "$szOldDate = "&$szOldDate)
			;FileWriteLine("C:\debug.dummydate.txt", "$szNewDate = _DateAdd('d', 1, $szOldDate)")
			;FileWriteLine("C:\debug.dummydate.txt", "$szNewDate = "&$szNewDate)
			;FileWriteLine("C:\debug.dummydate.txt", "$month = "&$month)
			;FileWriteLine("C:\debug.dummydate.txt", "$day = "&$day)
			;FileWriteLine("C:\debug.dummydate.txt", "$year = "&$year)
			;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		EndIf
		FileWrite("C:\DummyDateChange.token", "")
		_SetDate($day, $month, $year)
		FileDelete("C:\DummyDateChange.token")
		;FileWriteLine("C:\debug.dummydate.txt", "_SetDate($day,$month,$year)")
		;FileWriteLine("C:\debug.dummydate.txt", "--------------")
		$_DummyDate_tOriginalTime = 0
		If $_DummyDate_RestoreW32Time Then
			RunWait("net start W32Time", "", @SW_HIDE)
			$_DummyDate_RestoreW32Time = 0
		EndIf
	EndIf
EndFunc   ;==>DummyDateShutdownProc
