; ========================================================================================
; Module ...........: MyRobocopy.au3
; Author ...........: Patryk S. ( pszczepa at gmail dot com )
; Description ......: Specifically designed for voluminous downloads: first, the copy is
;                     performed into a temporary directory and it's only when the
;                     download is finished that the directory is moved/renamemed to its
;                     final destination
; Note .............: See EasyRobocopy.au3 and DirMoveV3.au3 for alternative tools.
; Note .............: Robocopy return codes meanings
;                      0 - No errors occurred and no files were copied.
;                      1 - One of more files were copied successfully.
;                      2 - Extra files or directories were detected. Examine the log file
;                          for more information.
;                      4 - Mismatched files or directories were detected.  Examine the
;                          log file for more information.
;                      8 - Some files or directories could not be copied and the retry
;                          limit was exceeded.
;                     16 - Robocopy did not copy any files. Check the command line
;                          parameters and verify that Robocopy has enough rights to write
;                          to the destination folder.
; Note .............: Translated and/or added return codes meanings: see 'RobocopyErrorDesc'
; History ..........: JUL 01, 2010 - Created
; ========================================================================================
#include-once
#include <File.au3>
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include ".\EasyRobocopyCmd.au3"

Const $ROBOCOPY_TEMPDIR_PREFIX = "~"
Const $ROBOCOPY_TEMPDIR_SUFFIX = ".rbcpy.tmp"
Const $ROBOCOPY_TEMPDIR_PATTERN = $ROBOCOPY_TEMPDIR_PREFIX & "???????" & $ROBOCOPY_TEMPDIR_SUFFIX

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyCleanUp
; Description ..: Delete interrupted downloads
; Parameters ...:
; Return .......:
; History ......: JUN 22, 2010 - Created for interrupted scripts
;-------------------------------------------------------------------------------
Func MyRobocopyCleanUp($path)
	While 1
		$file = 0
		$search = FileFindFirstFile($path & "\" & $ROBOCOPY_TEMPDIR_PATTERN)
		If $search = -1 Then
			Return
		Else
			$file = FileFindNextFile($search)
			DirRemove($path & "\" & $file, 1)
			FileClose($search)
		EndIf
	WEnd
EndFunc   ;==>MyRobocopyCleanUp

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyGetCmdLine()
; Description ..:
; Parameters ...:
; Return .......: String with command to run in 'MSDOS' mode
;                 empty string otherwise
; History ......: JUN 21, 2010 - Created
;-------------------------------------------------------------------------------
Func MyRobocopyGetCmdLine($srcdir, $destdir, $srcname, $isdir = False)
	$sCommand = ""
	While StringRight($srcdir, 1) = "\"
		$srcdir = StringTrimRight($srcdir, 1)
	WEnd
	While StringRight($destdir, 1) = "\"
		$destdir = StringTrimRight($destdir, 1)
	WEnd
	If FileExists($srcdir & "\" & $srcname) Then
		If Not $isdir Then
			; we are copying a file
			$sCommand = 'robocopy.exe /z "' & $srcdir & '" "' & $destdir & '" "' & $srcname & '"'
		Else
			; we are copying a directory
			$sCommand = EasyRobocopyCmd($srcdir & "\" & $srcname, $destdir & "\" & $srcname)
		EndIf
	EndIf
	Return $sCommand
EndFunc   ;==>MyRobocopyGetCmdLine

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyFile
; Description ..: Similar to AutoIT 'FileCopy' function but using Robocopy tool
; Parameters ...: See 'FileMove'
; Return .......: integer - error code (see module header
;                 - or - 'RobocopyErrorDesc' for more info)
; History ......: JUN 21, 2010 - Created function that copies data into
;                 temporary directory before copying it to the final location
;                 JUL 28, 2010 - Changed return values (0 <=> success!)
; TODO: If '$dest' exists and is a directory, then copy file inside of it
; TODO: Wildcards usage (cf. 'FileCopy')
;-------------------------------------------------------------------------------
Func MyRobocopyFile($src, $dest, $flag = 0)
	Return MyRobocopySub($src, $dest, $flag, False)
EndFunc   ;==>MyRobocopyFile

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyFileReadOnly
; Description ..: MyRobocopyFile + Flag file as read-only
; Parameters ...: See 'FileMove'
; Return .......: integer - error code (see module header
;                 - or - 'RobocopyErrorDesc' for more info)
; History ......: JUN 24, 2010 - Created this new wrapper
;                 JUL 28, 2010 - Changed return values (0 <=> success!)
;-------------------------------------------------------------------------------
Func MyRobocopyFileReadOnly($src, $dest, $flag = 0)
	Return MyRobocopySub($src, $dest, $flag, False, True)
EndFunc   ;==>MyRobocopyFileReadOnly

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyDir
; Description ..: Similar to AutoIT 'DirCopy' function but using Robocopy tool
; Parameters ...: See 'DirMove'
; Return .......: integer - error code (see module header
;                 - or - 'RobocopyErrorDesc' for more info)
; History ......: JUN 21, 2010 - Created function that copies data into
;                 temporary directory before copying it to the final location
;                 JUL 28, 2010 - Changed return values (0 <=> success!)
;-------------------------------------------------------------------------------
Func MyRobocopyDir($src, $dest, $flag = 0)
	Return MyRobocopySub($src, $dest, $flag, True)
EndFunc   ;==>MyRobocopyDir

;-------------------------------------------------------------------------------
; Name .........: MyRobocopyDirReadOnly
; Description ..: MyRobocopyDir + Flag all content as read-only
; Parameters ...: See 'DirMove'
; Return .......: integer - error code (see module header
;                 - or - 'RobocopyErrorDesc' for more info)
; History ......: JUN 24, 2010 - Created this new wrapper
;                 JUL 28, 2010 - Changed return values (0 <=> success!)
;-------------------------------------------------------------------------------
Func MyRobocopyDirReadOnly($src, $dest, $flag = 0)
	Return MyRobocopySub($src, $dest, $flag, True, True)
EndFunc   ;==>MyRobocopyDirReadOnly

;-------------------------------------------------------------------------------
; Name .........: RobocopyErrorDesc
; Description ..: Transform an error code into a string explaining the problem.
; Return .......: string - explaination of the problem
; History ......: JUL 29, 2010 - Created
;-------------------------------------------------------------------------------
Func RobocopyErrorDesc($err_code)
	Switch $err_code
		Case 0
			Return "Successful download."
		Case 2
			Return "Extra files or directories were detected. Examine the log file for more information."
		Case 4
			Return "Mismatched files or directories were detected.  Examine the log file for more information."
		Case 8
			Return "Some files or directories could not be copied and the retry limit was exceeded."
		Case 16
			Return "Robocopy did not copy any files. Check the command line parameters and verify that Robocopy has enough rights to write to the destination folder."
		Case 32
			Return "Robocopy returned '0'. No files were copied."
		Case 64
			Return "Destination already exists and should not be overwritten. Check MyRobocopy function parameters."
		Case 128
			Return "A move, copy or delete operation failed."
		Case Else
			Return "Unknown error code."
	EndSwitch
EndFunc   ;==>RobocopyErrorDesc

;-------------------------------------------------------------------------------
; Name .........: MyRobocopySub
; Description ..: Sub for MyRobocopyFile and MyRobocopyDir
; Parameters ...: Similar to 'DirMove' and 'FileMove' AutoIT functions.
;                 + $isdir - switch between modes
;                 + $mark_readonly - mark copied content as read-only
; Return .......: integer - error code (see module header
;                 - or - 'RobocopyErrorDesc' for more info)
; History ......: JUN 21, 2010 - Created function that copies data into
;                 temporary directory before copying it to the final location
;                 JUN 24, 2010 - Added $mark_readonly=false parameter
;                 JUL 28, 2010 - Changed return values (0 <=> success!)
; TODO: exception: source is not what it's meant to be (ie. file or dir)?
;-------------------------------------------------------------------------------
Func MyRobocopySub($src, $dest, $flag, $isdir = False, $mark_readonly = False)

	;FuncTrace("MyRobocopySub", $src, $dest, $flag, $isdir, $mark_readonly)

	$ret = 128

	; exception: destination already exists and should not be overwritten
	If FileExists($dest) And Not BitAND($flag, 1) Then Return 64

	; split the 'source' and 'dest' info
	Dim $szDrive, $szDir, $szFName, $szExt
	_PathSplit($src, $szDrive, $szDir, $szFName, $szExt)
	Dim $szDrive2, $szDir2, $szFName2, $szExt2
	_PathSplit($dest, $szDrive2, $szDir2, $szFName2, $szExt2)

	; create the intermediate tempororay (hidden) directory
	; NOTE: we create it in the same drive to be able to move it faster
	$temp = _TempFile($szDrive2 & $szDir2, $ROBOCOPY_TEMPDIR_PREFIX, $ROBOCOPY_TEMPDIR_SUFFIX)
	DirCreate($temp)
	FileSetAttrib($temp, "+H")

	; get the name of the source
	$srcdir = $szDrive & $szDir
	$srcname = $szFName & $szExt

	; do the copy to temporary location
	$sCommand = MyRobocopyGetCmdLine($srcdir, $temp, $srcname, $isdir)
	$rbcpy_ret = 0 ; no files copied yet.
	If $sCommand <> "" Then
		ConsoleWrite("[" & $sCommand & "]..." & @CRLF)
		If (@ComputerName <> "TESTCENTER") And (@ComputerName <> "PATRYK1") Then
			; Default behaviour is to wait as long as necessary
			$rbcpy_ret = RunWait($sCommand, "", @SW_HIDE)
		Else
			; Central server cannot hang on a failed download
			$rbcpy_ret = RunWithBoldTimeout($sCommand, "", @SW_HIDE)
		EndIf
	EndIf

	; process return value
	If $rbcpy_ret = 1 Then
		; so far, so good
		; (files were copied successfully)
		$ret = 0
	Else
		If $rbcpy_ret = 0 Then
			If FileExists($temp & "\" & $srcname) Then
				$ret = 0 ; The directory has been copied - but it's an empty one
			Else
				$ret = 32 ; Robocopy returned '0'. No files were copied.
			EndIf
		Else
			$ret = $rbcpy_ret
		EndIf
	EndIf

	; move the downloaded content to its final location
	If $ret = 0 Then
		If $isdir Then
			If $mark_readonly Then FileSetAttrib($temp & '\' & $srcname, "+R", 1)
			If Not DirMove($temp & '\' & $srcname, $dest, $flag) Then $ret = 128
		Else
			If Not FileMove($temp & '\' & $srcname, $dest, $flag) Then $ret = 128
			If $mark_readonly Then FileSetAttrib($temp & '\' & $srcname, "+R")
		EndIf
	EndIf

	; delete temporary
	If Not DirRemove($temp, 1) Then
		MsgBox(48, "Error", "Could not delete directory '" & $temp & "'.")
		$ret = 128
	EndIf

	Return $ret

EndFunc   ;==>MyRobocopySub
