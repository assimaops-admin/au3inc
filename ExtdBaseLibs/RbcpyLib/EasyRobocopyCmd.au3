; ========================================================================================
; Module ...........: EasyRobocopy.au3
; Author ...........: Patryk S. ( pszczepa at gmail dot com )
; Description ......: Specifically designed to move directories safely across the network.
; Note .............: See MyRobocopy.au3 and DirMoveV3.au3 for alternative tools.
; Note .............: Robocopy return codes meanings
;                      0 - No errors occurred and no files were copied.
;                      1 - One of more files were copied successfully.
;                      2 - Extra files or directories were detected. Examine the log file
;                          for more information.
;                      4 - Mismatched files or directories were detected.  Examine the
;                          log file for more information.
;                      8 - Some files or directories could not be copied and the retry
;                          limit was exceeded.
;                     16 - Robocopy did not copy any files. Check the command line
;                          parameters and verify that Robocopy has enough rights to write
;                          to the destination folder.
; History ..........: JUL 01, 2010 - Created
; ========================================================================================
#include-once

$ER_ACTUAL_CMD_PREFIX_OPT = ""
$ER_ACTUAL_CMD_SUFFIX_OPT = ""

;-------------------------------------------------------------------------------
; Name .........: EasyRobocopyOpt
; Description ..:
; Return .......: n/a
; History ......: NOV 3, 2010 - Created
;-------------------------------------------------------------------------------
Func EasyRobocopyOpt($opt)
	If $opt = "/mir" Then
		$ER_ACTUAL_CMD_PREFIX_OPT &= " " & $opt
	Else
		$ER_ACTUAL_CMD_SUFFIX_OPT &= " " & $opt
	EndIf
EndFunc   ;==>EasyRobocopyOpt

;-------------------------------------------------------------------------------
; Name .........: EasyRobocopyCmd
; Description ..:
; Return .......: string - command line
; History ......: AUG 13, 2010 - Created
;-------------------------------------------------------------------------------
Func EasyRobocopyCmd($src, $dest, $interact = False)

	; copy sub-directories, even the empty ones
	$robocopy_options = "/z /e"
	$robocopy_options &= $ER_ACTUAL_CMD_PREFIX_OPT

	$commandLine = StringFormat('robocopy %s "%s" "%s"', $robocopy_options, $src, $dest)
	$commandLine &= $ER_ACTUAL_CMD_SUFFIX_OPT

	$ER_ACTUAL_CMD_PREFIX_OPT = ""
	$ER_ACTUAL_CMD_SUFFIX_OPT = ""

	; pause at the end of the copy
	If $interact Then
		$commandLine &= " & pause"
	EndIf

	; return the command line
	Return $commandLine

EndFunc   ;==>EasyRobocopyCmd

;-------------------------------------------------------------------------------
; Name .........: EasyRobocopyDir
; Description ..:
; Return .......: value returned by 'RunWait'
; History ......: AUG 13, 2010 - Created
;-------------------------------------------------------------------------------
Func EasyRobocopyDir($src, $dest, $interact = False)
	$cmdFlag = @SW_HIDE
	If $interact Then
		$cmdFlag = @SW_SHOW
	EndIf
	$commandLine = EasyRobocopyCmd($src, $dest, $interact)
	Return RunWait('"' & @ComSpec & '" /c ' & $commandLine, '', $cmdFlag)
EndFunc   ;==>EasyRobocopyDir
