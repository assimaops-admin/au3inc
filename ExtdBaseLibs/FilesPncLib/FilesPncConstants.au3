#include-once

; See also:
; # include <UAFXA1\AbcQa.ThirdParty.Assima.Rnd\AssimaConstants.au3>
; # include <UAFXA1\AbcQa.ThirdParty.Assima.Rnd.Qa\AssimaQaConstants.au3>

; Backlinks:
; ctrl\inc\CustLibs\AbcQa.ThirdParty.Assima.Rnd\AssimaConstants.au3:4:;# include <CustLibs\StdLibXXL\StdLibXXLConstants.au3>
; ctrl\inc\CustLibs\AbcQa.ThirdParty.Assima.Rnd.Qa\AssimaQaConstants.au3:4:;# include <CustLibs\StdLibXXL\StdLibXXLConstants.au3>
; inc2\ExtdLibs\ThirdParty.Assima.Rnd.Ats\AssimaThes.au3:5:# include "..\StdLibXXL\StdLibXXLConstants.au3"
; inc2\ExtdLibs\ThirdParty.Assima.Rnd.Qa\XmlWriter.au3:4:# include "..\StdLibXXL\StdLibXXLConstants.au3"

; comparator behavior
Enum $COMPARATOR_KEEP_UNPACKED_CONTENT _			; 1/ Default behaviour: keep but don't reuse the unpacked content
		, $COMPARATOR_DELETE_UNPACKED_CONTENT _				; 2/ Don't keep unpacked content
		, $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT _		; 3/ Similar to $COMPARATOR_KEEP_UNPACKED_CONTENT but doesn't override previously unpacked content
		, $COMPARATOR_CHECK_RES_SIZE_ONLY ; 4/ The comparator will only consider the files sizes
