#include-once
#include <AuItCustLibs\UDFs\AutoItEx.StringRegExpReplaceV2.au3>
#include <BaseUAFLibs\UDFsx\FileGetShortNameV2.au3>
#include <BaseUAFLibs\UDFsx\FileGetTimeV2.au3>
#include ".\FilesPncConstants.au3"

;-------------------------------------------------------------------------------
; Name .........: FilterTextFileLinesOnSite
; Description ..: Wrap FilterTextFileLines and replace the original file
; Return .......: String - Path to the filtered file
;                 - empty string "" on failure
;-------------------------------------------------------------------------------
Func FilterTextFileLinesOnSite($szFile_in, $szFilterFile_in, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	$ret = ""

	; filter the file
	$szFileFiltered = FilterTextFileLinesToFiltered($szFile_in, $szFilterFile_in)
	$error = @error
	$replacements_count = @extended

	; exception handling
	If $error Then
		FileDelete($szFileFiltered)
		FileDelete($szFileFiltered & ".origin_info")
		Return SetError($error, $replacements_count, $ret)
	EndIf

	; replace the original file with the new one
	If $replacements_count Then

		; operate using shortnames
		$szFile_in_shortname = FileGetShortNameV2($szFile_in)
		$szFileFiltered_shortname = FileGetShortNameV2($szFileFiltered)

		; manipulate the files
		If Not FileDelete($szFile_in_shortname) Then
			ConsoleWrite("!Error: '" & $szFile_in_shortname & "' could not be deleted." & @CRLF)
		EndIf
		If Not FileMove($szFileFiltered_shortname, $szFile_in_shortname, 1) Then
			ConsoleWrite("!Error: move '" & $szFileFiltered_shortname & "' -> '" & $szFile_in_shortname & "' failed." & @CRLF)
		EndIf

	EndIf

	; delete extra file
	FileDelete($szFileFiltered & ".origin_info")

	; the new file replaced the original file
	$error = 0
	$ret = $szFile_in
	Return SetError(0, $replacements_count, $ret)

EndFunc   ;==>FilterTextFileLinesOnSite

;-------------------------------------------------------------------------------
; Name .........: FilterTextFileLinesToFiltered
; Description ..:
; Return .......: String - Path to the filtered file
;                 - empty string "" on failure
;-------------------------------------------------------------------------------
Func FilterTextFileLinesToFiltered($szFile_in_longname, $szFilter_longname, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	$replacements_count = 0

	; process input parameter
	$szFile_in_shortname = FileGetShortNameV2($szFile_in_longname)

	; process input parameter
	$szFilter_shortname = FileGetShortNameV2($szFilter_longname)

	; prepare output names
	$szFile_filtered_longname = $szFile_in_longname & ".filtered"
	$szFile_filtered_shortname = FileGetShortNameV2($szFile_filtered_longname)

	; exception: file does not exist
	If Not FileExists($szFile_in_shortname) Then Return SetError(1, 0, "")
	; exception: filter does not exist
	If Not FileExists($szFilter_shortname) Then Return SetError(1, 0, "")

	; was this file already filtered before?
	If Not FileExists($szFile_filtered_shortname) Or $eUnpacking <> $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT Then

		; was this file already filtered before (bis)?
		$last_decoded_1 = FileReadLine($szFile_filtered_shortname & ".origin_info", 1)
		$last_decoded_2 = FileReadLine($szFile_filtered_shortname & ".origin_info", 2)
		$this_file_date_1 = FileGetTimeV2($szFile_in_shortname, 0, $eFileGetTimeFormat_1)
		$this_file_date_2 = FileGetTimeV2($szFilter_shortname, 0, $eFileGetTimeFormat_1)
		If $last_decoded_1 = "" Or $last_decoded_2 = "" Or $last_decoded_1 <> $this_file_date_1 Or $last_decoded_2 <> $this_file_date_2 Then

			; pseudo command-line
			$command_line = StringFormat('filter.exe "%s" "%s" "%s" /opt:%d', $szFile_in_shortname, $szFile_filtered_shortname, $szFilter_shortname, $eUnpacking)
			ConsoleWrite("[" & $command_line & "]..." & @CRLF)

			; get original content
			$szFileContent = FileRead($szFile_in_shortname)
			$iFileEncoding = FileGetEncoding($szFile_in_shortname)
			ConsoleWrite('$iFileEncoding = ' & $iFileEncoding & @CRLF)

			; get filters list
			$hFilter = FileOpen($szFilter_shortname, 0)
			If $hFilter <> -1 Then
				While 1

					; get replacements
					$szSearchPattern = FileReadLine($hFilter)
					If @error = -1 Then ExitLoop
					$szReplacementPattern = FileReadLine($hFilter)
					If @error = -1 Then ExitLoop

					$szFileContent = StringRegExpReplaceV2($szFileContent, $szSearchPattern, $szReplacementPattern)
					$replacements_count += @extended

				WEnd
				FileClose($hFilter)

			EndIf

			; create the new file
			; REF.PS/E848B3BE
			If FileExists($szFile_filtered_shortname) Then FileDelete($szFile_filtered_shortname)
			$hFile_out = FileOpen($szFile_filtered_shortname, 1 + $iFileEncoding)
			FileWrite($hFile_out, $szFileContent)
			FileClose($hFile_out)

			; record last filter
			FileDelete($szFile_filtered_shortname & ".origin_info")
			FileWriteLine($szFile_filtered_shortname & ".origin_info", $this_file_date_1)
			FileWriteLine($szFile_filtered_shortname & ".origin_info", $this_file_date_2)

		EndIf

	EndIf

	Return SetExtended($replacements_count, $szFile_filtered_longname)
EndFunc   ;==>FilterTextFileLinesToFiltered
