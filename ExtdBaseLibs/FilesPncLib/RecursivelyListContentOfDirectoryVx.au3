#include-once
#include <AuItCustLibs\AutoItEx.au3>
#include <BaseUAFLibs\StdBaseLib\ProcessFiles.au3>
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include ".\FilesPncConstants.au3"

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectoryVxSub
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectoryVxSub( _
		$memorizeLists _
		, ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		)

	FuncTrace("RecursivelyListContentOfDirectoryVxSub", _
			$memorizeLists _
			, $arrayDirectories _
			, $arrayFiles _
			, $arrayFolders _
			, $arrayFilenames _
			, $isEntryToSkipCallback _
			, $eUnpacking _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
			)

	Const $__DBG_FUNC_RUN = 0

	Const $LATEST_FILES_LISTS_VERSION = 3

	$ret = False

	; directory to process
	$arrayDirectories[$arrayDirectories[0]] = NormalizePath($arrayDirectories[$arrayDirectories[0]] _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths)
	$szDirectory = $arrayDirectories[$arrayDirectories[0]]
	$szDirectory_short = FileGetShortNameV2($szDirectory)
	$szDirectory_len = StringLen($szDirectory)

	If $__DBG_FUNC_RUN Then
		ValTrace($szDirectory, "$szDirectory")
		ValTrace($szDirectory_short, "$szDirectory_short")
		ValTrace($szDirectory_len, "$szDirectory_len")
	EndIf

	; exception
	If Not FileExists($szDirectory_short) Then FatalError("Directory '" & $szDirectory & "' does not exist.")

	; meta directory
	$szMetaDirectory = $szDirectory & "\" & ".atv2" & "\" & "files_lists"
	$szMetaDirectory_short = FileGetShortNameV2($szMetaDirectory)

	$deprecatedLists = (Not FileExists($szMetaDirectory_short & "\" & "version" & ".dat")) _
			Or (FileRead($szMetaDirectory_short & "\" & "version" & ".dat") <> $LATEST_FILES_LISTS_VERSION)

	; should we build the files lists or do we already have that?
	If $deprecatedLists Then

		$bFillArrays = True
		$bSkipAtv2Dir = True
		$ret = __RecursivelyListOrProcessContentOfDirectory( _
				$bFillArrays _
				, $bSkipAtv2Dir _
				, $arrayDirectories _
				, $arrayFiles _
				, $arrayFolders _
				, $arrayFilenames _
				, $isEntryToSkipCallback _
				, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
				)

		If $memorizeLists Then

			DirCreate($szMetaDirectory_short)

			$File = $szMetaDirectory_short & "\" & "version" & ".dat"
			If FileExists($File) Then FileDelete($File)
			FileWrite($File, $LATEST_FILES_LISTS_VERSION)

			; 1111111111111111111 ARRAY 1111111111111111111
			$arrayDirectories__RelativePaths = $arrayDirectories
			_ArrayTrim($arrayDirectories__RelativePaths, $szDirectory_len + 1, 0, 1) ; create the relative paths
			If $__DBG_FUNC_RUN Then ArrayTrace($arrayDirectories__RelativePaths, "$arrayDirectories__RelativePaths")
			$File = $szMetaDirectory_short & "\" & "$arrayDirectories__RelativePaths" & ".dat"
			If FileExists($File) Then FileDelete($File)
			_FileWriteFromArrayV2($File, $arrayDirectories__RelativePaths, 1)

			; 2222222222222222222 ARRAY 2222222222222222222
			$arrayFiles__RelativePaths = $arrayFiles
			_ArrayTrim($arrayFiles__RelativePaths, $szDirectory_len + 1, 0, 1) ; create the relative paths
			If $__DBG_FUNC_RUN Then ArrayTrace($arrayFiles__RelativePaths, "$arrayFiles__RelativePaths")
			$File = $szMetaDirectory_short & "\" & "$arrayFiles__RelativePaths" & ".dat"
			If FileExists($File) Then FileDelete($File)
			_FileWriteFromArrayV2($File, $arrayFiles__RelativePaths, 1)

			; 3333333333333333333 ARRAY 3333333333333333333
			$File = $szMetaDirectory_short & "\" & "$arrayFolders" & ".dat"
			If FileExists($File) Then FileDelete($File)
			_FileWriteFromArrayV2($File, $arrayFolders, 1)

			; 4444444444444444444 ARRAY 4444444444444444444
			$File = $szMetaDirectory_short & "\" & "$arrayFilenames" & ".dat"
			If FileExists($File) Then FileDelete($File)
			_FileWriteFromArrayV2($File, $arrayFilenames, 1)

		EndIf

	Else

		; 1111111111111111111 ARRAY 1111111111111111111
		$File = $szMetaDirectory_short & "\" & "$arrayDirectories__RelativePaths" & ".dat"
		$arrayDirectories__RelativePaths = 0
		_FileReadToArrayV2($File, $arrayDirectories__RelativePaths)
		; expand the relative paths
		Dim $arrayDirectories[UBound($arrayDirectories__RelativePaths)]
		$arrayDirectories[0] = $arrayDirectories__RelativePaths[0]
		For $i = 1 To $arrayDirectories[0]
			$arrayDirectories[$i] = $szDirectory & QuestionMark($arrayDirectories__RelativePaths[$i] <> "", "\" & $arrayDirectories__RelativePaths[$i], "")
		Next

		; 2222222222222222222 ARRAY 2222222222222222222
		$File = $szMetaDirectory_short & "\" & "$arrayFiles__RelativePaths" & ".dat"
		$arrayFiles__RelativePaths = 0
		_FileReadToArrayV2($File, $arrayFiles__RelativePaths)
		; expand the relative paths
		Dim $arrayFiles[UBound($arrayFiles__RelativePaths)]
		$arrayFiles[0] = $arrayFiles__RelativePaths[0]
		For $i = 1 To $arrayFiles[0]
			$arrayFiles[$i] = $szDirectory & QuestionMark($arrayFiles__RelativePaths[$i] <> "", "\" & $arrayFiles__RelativePaths[$i], "")
		Next

		; 3333333333333333333 ARRAY 3333333333333333333
		$File = $szMetaDirectory_short & "\" & "$arrayFolders" & ".dat"
		_FileReadToArrayV2($File, $arrayFolders)

		; 4444444444444444444 ARRAY 4444444444444444444
		$File = $szMetaDirectory_short & "\" & "$arrayFilenames" & ".dat"
		_FileReadToArrayV2($File, $arrayFilenames)

	EndIf

	If $__DBG_FUNC_RUN Then
		ArrayTrace($arrayDirectories, "$arrayDirectories")
		ArrayTrace($arrayFiles, "$arrayFiles")
		ArrayTrace($arrayFolders, "$arrayFolders")
		ArrayTrace($arrayFilenames, "$arrayFilenames")
	EndIf

	Return $ret
EndFunc   ;==>RecursivelyListContentOfDirectoryVxSub

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectoryV2
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectoryV2( _
		ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		)

	$memorizeLists = True
	RecursivelyListContentOfDirectoryVxSub( _
			$memorizeLists _
			, $arrayDirectories _
			, $arrayFiles _
			, $arrayFolders _
			, $arrayFilenames _
			, $isEntryToSkipCallback _
			, $eUnpacking _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
			)

EndFunc   ;==>RecursivelyListContentOfDirectoryV2

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectoryV3
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectoryV3( _
		ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		)

	$memorizeLists = False
	RecursivelyListContentOfDirectoryVxSub( _
			$memorizeLists _
			, $arrayDirectories _
			, $arrayFiles _
			, $arrayFolders _
			, $arrayFilenames _
			, $isEntryToSkipCallback _
			, $eUnpacking _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
			)

EndFunc   ;==>RecursivelyListContentOfDirectoryV3
