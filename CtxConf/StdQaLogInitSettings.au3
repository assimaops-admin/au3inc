#include-once
#include <CoreUAFLibs\StdCoreLib\LocalLog.au3>
#include <CoreUAFLibs\UDFs\StaticCodeBinding.au3>
#include ".\HostInfo.Lite.au3"

;-------------------------------------------------------------------------------
; Name .........: StdQaLog_Opt_BindLogServices___OnAutoItStartCallback
; Remark .......: Can't use #OnAutoItStartRegister => causes cryptic error
;                 with Date.au3 not knowing what $tagFILETIME is, or sth...
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func StdQaLog_Opt_BindLogServices___OnAutoItStartCallback()
	ConsoleWrite("StdQaLog_Opt_BindLogServices___OnAutoItStartCallback()... #ConsoleOnly" & @CRLF)
	StaticCodeBinding(HostInfo_GetSimpleLocalLogDir_EnsureExists_IsDefined())
	LogServices_Opt_EnableTraceToLogTool("LocalLog_AddEntryCallback", "HostInfo_GetSimpleLocalLogDir_EnsureExists")
	;# include <Debug.au3>
	;LogServices_Opt_EnableTraceToDebugTool()
EndFunc   ;==>StdQaLog_Opt_BindLogServices___OnAutoItStartCallback

Func StdQaLog_Opt_BindLogServices___CompilStaticChecker_UNUSED_FUNCTION()
	LocalLog_AddEntryCallback("") ; Just to make sure included. Required callback
EndFunc   ;==>StdQaLog_Opt_BindLogServices___CompilStaticChecker_UNUSED_FUNCTION
