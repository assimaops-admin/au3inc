#NoIncTidy
#include-once
#include ".\HostInfo.Lite.Include.au3"
#include ".\HostInfo.Lite.Locations.au3"
#include ".\HostInfo.Lite.Repositories.au3"

Const $REAL_LOCAL_DRIVE = "C:"

; REPLACED JUN 14, 2018
;Const $UK_CENTRAL_PHYS_COMPUTER = "BANK"
;Const $UK_CENTRAL_VIRT_COMPUTER = "TESTCENTER" ; TODO: clean one more occurence!

Const $UK_CENTRAL_PHYS_COMPUTER = "PATRYK1"
Const $UK_CENTRAL_VIRT_COMPUTER = "PATRYK1" ; TODO: clean one more occurence!
Const $UK_PSEUDO_PROXY_DRIVE = "V:"

Const $AL_CENTRAL_PHYS_COMPUTER = "QASRV01"
Const $AL_CENTRAL_VIRT_COMPUTER = "QASRV01" ; there is only one machine here
Const $AL_PSEUDO_PROXY_DRIVE = "Q:"

Const $SL_CENTRAL_PHYS_COMPUTER = "QASRV02"
Const $SL_CENTRAL_VIRT_COMPUTER = "QASRV02" ; there is only one machine here
Const $SL_PSEUDO_PROXY_DRIVE = "Q:"

Const $MAPPED_REMOTE_UNISON_DRIVE = "U:"
Const $EXPANDED_REMOTE_UNISON_DRIVE = "C:\AutoTestingV2.Map\U_DRIVE"
Const $UNISON_REPOSITORY_COMPUTER = $UK_CENTRAL_PHYS_COMPUTER

Const $AL_VERSIONS_STORAGE_EXP = "\\vsserver01\versions$"
Const $UK_VERSIONS_STORAGE_EXP = "\\storage01\versions$"
Const $UK_VERSIONS_STORAGE_SHORT = "V:"


;-------------------------------------------------------------------------------
; Name .........: $HOSTINFO_VM_LIST (REF.PS/HOSTINFO_CST_CFG -- PART 1/3)
; Description ..: Virtual machines
; History ......: JUN 23, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_VM_LIST = _HostInfo_Lite_Private_CreateArray( _
		"testcenter" _
		, "testserv--xp" _
		, "testserv--xp--2" _
		, "acmstestserver" _
		, "siebel80-ie8" _
		, "qacenter" _
		, "iissrv01" _
		, "testvm01" _
		)

;-------------------------------------------------------------------------------
; Name .........: $HOSTINFO_PM_LIST (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Physical-like machines (Root machines)
; History ......: JUN 23, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_PM_LIST = _HostInfo_Lite_Private_CreateArray( _
		"bank" _  ; << IMPORTANT NOTE: Reboot BANK first (use timer)
		, "qasrv01" _ ; << IMPORTANT NOTE: Reboot QASRV01 second (use timer)
		, "testserver-08" _
		, "assima-009" _
		, "assima-012" _
		, "thales" _
		, "testxvm01" _
		, "testxvm02" _
		, "testxvm03" _
		, "testxvm04" _
		, "vamigo-a1" _
		)

;-------------------------------------------------------------------------------
; Name .........: $HOSTINFO_PRIV_LIST (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Machines connected using Assima UK VPN
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_PRIV_LIST = _HostInfo_Lite_Private_CreateArray( _
		"perses" _
		, "hyperserv2" _
		)

;-------------------------------------------------------------------------------
; Name .........: $OPS_ROOT_HIDDEN_FILE (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Operation's mapped drive for lightweight machines
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
$sysdrv = EnvGet("SystemDrive")
Const $OPS_ROOT_CONF_1_FILE = $sysdrv & "\AutoTestingVx.Ctrl\.SiteConfig\" & @ComputerName & ".ini"
Const $OPS_ROOT_CONF_2_FILE = $sysdrv & "\AutoTestingVx.Ctrl\.SiteConfig\DefaultSiteConf.ini"
Const $OPS_ROOT_HIDDEN_FILE = OPS_ROOT_HIDDEN_FILE("O:")
Func OPS_ROOT_HIDDEN_FILE($root)
	Return $root & "\.drive_mapping\atv2_ops_root_o"
EndFunc   ;==>OPS_ROOT_HIDDEN_FILE

;-------------------------------------------------------------------------------
; Name .........: $PROXY_ROOT_HIDDEN_FILE (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Operation's mapped drive for lightweight machines
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Const $AL_REAL_PROXY_DRIVE = "P:"
Const $PROXY_ROOT_HIDDEN_FILE = $AL_REAL_PROXY_DRIVE & "\.drive_mapping\atv2_proxy_root_p"

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetLocalRoot
; Description ..: We use a flat structure for most of the testbench
;                 except for the 'Suites' directory that adds a level of
;                 indirection
; Remarks ......: - The path to the test-suite to be run should be automatically
;                 inferred WITHIN the host (testcenter is not supposed to know
;                 this path). [REF.PS/20101123_TODO]
; Return .......: String - Path to the current 'AutoTestingV2' directory
; History ......: OCT 24, 2011 - Used to return EnvGetV2("LocalAutoTesting")
;                 but too heavy to put in place. Simply returns "C:\AutoTestingV2"
;                 now.
;-------------------------------------------------------------------------------
Func HostInfo_GetLocalRoot($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return "C:\AutoTestingV2"
EndFunc   ;==>HostInfo_GetLocalRoot

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetRemoteRoot (for uploads)
; Description ..: We use a flat structure for most of the testbench
;                 except for the 'Suites' directory that adds a level of
;                 indirection
; Return .......: String - Path to the current 'AutoTestingV2' directory
; History ......: OCT 24, 2011 - Used to return EnvGetV2("RemoteAutoTesting")
;                 but too heavy to put in place. Simply returns "C:\AutoTestingV2"
;                 now.
;-------------------------------------------------------------------------------
Func HostInfo_GetRemoteRoot($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return HostInfo_GetRemoteRootDrive($computerName) & "\AutoTestingV2"
EndFunc   ;==>HostInfo_GetRemoteRoot

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetOpsRoot
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: "C:\AutoTestingV2" for most computers,
;                 "O:\AutoTestingV2" for lightweight machines
;                 "V:\AutoTestingV2" for testcenter
;                 "Q:\AutoTestingV2" for qacenter.
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetOpsRoot($computerName = @ComputerName)
	; CODE SYNC REF.PS/20111028_224112
	If $computerName = Default Then $computerName = @ComputerName
	;
	; - 1 - By default data is stored locally
	;
	$root = HostInfo_GetLocalRoot($computerName)
	;
	; - 2 - Lighweight machines work with a mapped drive as if it was a local one
	;          ( no upload )
	;
	$isLightVirt = HostInfo_GetLightweightVirt($computerName)
	If $isLightVirt <> "" Then
		$root = $isLightVirt
	EndIf
	;
	; - 3 - Central servers work on data provided by other hosts (uploaded)
	;          ( available on a mapped network drive )
	;
	; REMARK: We could get rid of this case and start using the concept of "Lightweight" machine.
	; The only problem with this: a dev computer won't be able to use both O:\ *AND* V:\ or Q:\
	; remember that testcenter shows paths as it uses them.
	;
	If HostInfo_IsCentralVirt($computerName) Then
		$root = HostInfo_GetRemoteRoot($computerName)
	EndIf
	Return $root
EndFunc   ;==>HostInfo_GetOpsRoot

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetRemoteRootDrive
; Return .......: String - Two character string such as "Q:" or "V:"
; History ......: OCT 24, 2011 - Made fairly static
;-------------------------------------------------------------------------------
Func HostInfo_GetRemoteRootDrive($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	If HostInfo_IsAlionis($computerName) Then
		Return $AL_PSEUDO_PROXY_DRIVE
	Else
		Return $UK_PSEUDO_PROXY_DRIVE
	EndIf
EndFunc   ;==>HostInfo_GetRemoteRootDrive

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetProxyRoot
; Description ..:
; Remarks ......:
; Return .......: "P:\AutoTestingV2" (if any)
; History ......: OCT 25, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetProxyRoot($computerName = @ComputerName)
	; -1- Search data on dedicated drive
	If HostInfo_IsUsingProxyRoot($computerName) _
			And FileExists("P:\AutoTestingV2.Static") Then
		Return "P:\AutoTestingV2"
	EndIf
	; -2- Search data on working drive
	If HostInfo_IsLightweightVirt($computerName) _
			And FileExists(HostInfo_GetOpsRoot($computerName) & ".Static") Then
		Return HostInfo_GetOpsRoot($computerName)
	EndIf
	; -3- By default return the local drive
	; Assert - FileExists(HostInfo_GetLocalRoot() & ".Static") ?
	Return HostInfo_GetLocalRoot()
EndFunc   ;==>HostInfo_GetProxyRoot

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsUsingProxyRoot
; Description ..:
; Remarks ......:
; Return .......: Returns True if host uses "P:\AutoTestingV2.Static"
; History ......: OCT 25, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsUsingProxyRoot($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	; Autodiscovery for Lightweight
	Return FileExists($PROXY_ROOT_HIDDEN_FILE) _
			And StringInStr(FileRead($PROXY_ROOT_HIDDEN_FILE), $computerName, 2) ; Not case sensitive
EndFunc   ;==>HostInfo_IsUsingProxyRoot

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsCentralVirt
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsCentralVirt($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return ($computerName = $UK_CENTRAL_VIRT_COMPUTER) _
			Or ($computerName = $AL_CENTRAL_VIRT_COMPUTER)
EndFunc   ;==>HostInfo_IsCentralVirt

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsLightweightVirt
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------

Func HostInfo_IsLightweightTest($root, $computerName = @ComputerName)
	Return FileExists(OPS_ROOT_HIDDEN_FILE($root)) _
			And StringInStr(FileRead(OPS_ROOT_HIDDEN_FILE($root)), $computerName, 2) ; Not case sensitive
EndFunc   ;==>HostInfo_IsLightweightTest

Func HostInfo_IsLightweightVirt($computerName = @ComputerName)
	Return (HostInfo_GetLightweightVirt($computerName) <> "")
EndFunc   ;==>HostInfo_IsLightweightVirt

Func HostInfo_GetLightweightVirt($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName

	; Autodiscovery for Lightweight
	$OutputsLocationRoot = "O:"
	$isLocationValid = HostInfo_IsLightweightTest($OutputsLocationRoot, $computerName)

	; Configuration-based for Lightweight
	; -- REMARK: Does *NOT* require use of OPS_ROOT_HIDDEN_FILE
	If Not $isLocationValid Then
		$confFilePath = $OPS_ROOT_CONF_1_FILE
		If Not FileExists($confFilePath) Then $confFilePath = $OPS_ROOT_CONF_2_FILE
		If FileExists($confFilePath) Then
			$OutputsLocationRoot = IniRead($confFilePath, "UPLOAD_CONFIG", "MAIN_OUT_PATH", "")
			$isLocationValid = FileExists($OutputsLocationRoot)
		EndIf
	EndIf

	$trcr = ""
	$trcr &= "FileExists(OPS_ROOT_HIDDEN_FILE('O:')) = " & FileExists(OPS_ROOT_HIDDEN_FILE('O:')) & @CRLF
	$trcr &= "FileExists(OPS_ROOT_HIDDEN_FILE($O)) = " & FileExists(OPS_ROOT_HIDDEN_FILE($OutputsLocationRoot)) & @CRLF
	;$trcr &= "FileRead($OPS_ROOT_HIDDEN_FILE) = " & FileRead($OPS_ROOT_HIDDEN_FILE) & @CRLF
	$trcr &= "$computerName = " & $computerName & @CRLF
	$trcr &= 'Return $isLocationValid is ' & $isLocationValid
	;MsgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & '$trcr' & @lf & @lf & 'Return:' & @lf & $trcr) ;### Debug MSGBOX

	If $isLocationValid Then
		Return $OutputsLocationRoot & "\AutoTestingV2"
	Else
		Return ""
	EndIf

EndFunc   ;==>HostInfo_GetLightweightVirt

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsCentralUKVirt
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsCentralUKVirt($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return ($computerName = $UK_CENTRAL_VIRT_COMPUTER)
EndFunc   ;==>HostInfo_IsCentralUKVirt

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsCentralPhys
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsCentralPhys($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return ($computerName = $UK_CENTRAL_PHYS_COMPUTER) _
			Or ($computerName = $AL_CENTRAL_PHYS_COMPUTER)
EndFunc   ;==>HostInfo_IsCentralPhys

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsCentralUKPhys
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsCentralUKPhys($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return ($computerName = $UK_CENTRAL_PHYS_COMPUTER)
EndFunc   ;==>HostInfo_IsCentralUKPhys

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetClosestCentralVirt
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: OCT 25, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetClosestCentralVirt($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	$ret = $UK_CENTRAL_VIRT_COMPUTER
	If HostInfo_IsAlionis($computerName) Then $ret = $AL_CENTRAL_VIRT_COMPUTER
	Return $ret
EndFunc   ;==>HostInfo_GetClosestCentralVirt

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetClosestDataDir
; Description ..: 'HostInfo_GetDataDir' wrapper
; Parameters ...: [optional] BOOL - Remote or not?
; Remarks ......: Previously called 'OutputsManagement_GetTMODirLoc'
; Return .......: n/a
; History ......: FEB 11, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetClosestDataDir($bUpload = False)
	$ret = HostInfo_GetDataDir()
	If $bUpload = Default Then $bUpload = False
	If $bUpload And Not HostInfo_IsLightweightVirt() Then
		$ret = HostInfo_GetDataDir(HostInfo_GetClosestCentralVirt())
	EndIf
	Return $ret
EndFunc   ;==>HostInfo_GetClosestDataDir

#cs INLINE TESTING INLINE TESTING INLINE TESTING INLINE TESTING INLINE TESTING
	$testvar = HostInfo_GetClosestDataDir()
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $testvar = ' & $testvar & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	$testvar = HostInfo_GetClosestDataDir(1)
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $testvar = ' & $testvar & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
#ce INLINE TESTING INLINE TESTING INLINE TESTING INLINE TESTING INLINE TESTING

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetDataDir, HostInfo_GetSsdbDir,
;                 HostInfo_GetRefsDir, HostInfo_GetBoldDir
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetDataDir($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return HostInfo_GetOpsRoot($computerName) & ".Data"
EndFunc   ;==>HostInfo_GetDataDir
Func HostInfo_GetSsdbDir($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return HostInfo_GetDataDir($computerName) & "\_SSDB_"
EndFunc   ;==>HostInfo_GetSsdbDir
Func HostInfo_GetRefsDir($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return HostInfo_GetOpsRoot($computerName) & ".Refs"
EndFunc   ;==>HostInfo_GetRefsDir
Func HostInfo_GetBoldDir($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Return HostInfo_GetRefsDir($computerName) & "\Bold"
EndFunc   ;==>HostInfo_GetBoldDir

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetChildrenHosts (REF.PS/HOSTINFO_CST_CFG -- PART 3/3)
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: JUN 22, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetChildrenHosts($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	Dim $childrenComputers[1] = [0]
	Switch $computerName
		Case "bank"
			$childrenComputers = _HostInfo_Lite_Private_CreateArray("testcenter")
		Case "testserver-08"
			$childrenComputers = _HostInfo_Lite_Private_CreateArray("testserv--xp")
		Case "assima-009"
			$childrenComputers = _HostInfo_Lite_Private_CreateArray("testserv--xp--2", "siebel80-ie8")
		Case "assima-012"
			$childrenComputers = _HostInfo_Lite_Private_CreateArray("acmstestserver")
		Case "qasrv01"
			;$childrenComputers = _HostInfo_Lite_Private_CreateArray("qacenter")
			;$childrenComputers = _HostInfo_Lite_Private_CreateArray("iissrv01")
	EndSwitch
	Return $childrenComputers
EndFunc   ;==>HostInfo_GetChildrenHosts

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetParentHost
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: JUN 22, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetParentHost($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	$parentComputer = ""
	For $iPM = 1 To $HOSTINFO_PM_LIST[0]
		$candidate = $HOSTINFO_PM_LIST[$iPM]
		$childrenComputers = HostInfo_GetChildrenHosts($candidate)
		For $iChild = 1 To $childrenComputers[0]
			$childComputer = $childrenComputers[$iChild]
			If Not StringCompare($childComputer, $computerName) Then
				$parentComputer = $candidate
				ExitLoop 2
			EndIf
		Next
	Next
	Return $parentComputer
EndFunc   ;==>HostInfo_GetParentHost

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsPriv
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsPriv($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	For $iPRIV = 1 To $HOSTINFO_PRIV_LIST[0]
		$candidate = $HOSTINFO_PRIV_LIST[$iPRIV]
		If Not StringCompare($candidate, $computerName) Then Return True
	Next
	Return False
EndFunc   ;==>HostInfo_IsPriv

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetVMXInfo
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: JUN 22, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetVMXInfo(ByRef $path, ByRef $title, ByRef $is_enabled, $computerName = @ComputerName)
	$ret = 1
	$is_enabled = 1
	Switch $computerName
		Case "testcenter"
			$title = "TestCenterVMv1"
			$path = "C:\VM for TestCenter v1\XP-pro-SP2_clean.vmx"
		Case "testserv--xp"
			$title = "XP pro SP2"
			$path = "C:\VM for AutoTesting 1.1\XP-pro-SP2_clean.vmx"
		Case "testserv--xp--2"
			$title = "XP pro SP2"
			$path = "C:\VM for AutoTesting 1.2\XP-pro-SP2_clean.vmx"
		Case "acmstestserver"
			$title = "ACMS - Server2003SE"
			$path = "C:\VM ACMS Autotesting\Server2003StandardEdition.vmx"
		Case "siebel80-ie8"
			$title = "Siebel 8.0"
			$path = "C:\VM Siebel 8.0 - Upgrade from 7.8 - IE8\Windows XP Professional.vmx"
			;Case "qacenter"
			;	$title = "QaCenter"
			;	$path = "C:\vm.qacenter\XP-pro-SP2_clean.vmx"
		Case "iissrv01"
			$title = "IISSrv01"
			$path = "E:\vm.iissrv01\Server2003StandardEdition.vmx"
		Case Else
			;NotImplemented($CmdLineRaw, 4)
			$ret = 0
	EndSwitch
	Return $ret
EndFunc   ;==>HostInfo_GetVMXInfo

;-------------------------------------------------------------------------------
; Name .........: HostInfo_TempHistoryInDaysMax
; Description ..: To be used with the Maintenance script.
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: Integer - Number of days that the local (temporary) files have
;                           to be kept (typically between '0' and '15').
; History ......: JAN 28, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_DEFAULT_HISTORY_FOR_DIRCLEANER = 9
Func HostInfo_TempHistoryInDaysMax($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	$ret = $HOSTINFO_DEFAULT_HISTORY_FOR_DIRCLEANER
	Switch $computerName
		Case "testserv--xp--2"
			$ret = 6
		Case "siebel80-ie8"
			$ret = 1
		Case Else
			; NOP
	EndSwitch
	Return $ret
EndFunc   ;==>HostInfo_TempHistoryInDaysMax

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetSimpleLocalLogDir_EnsureExists
; Description ..: Get location of the local log file
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: NOV 8, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetSimpleLocalLogDir_EnsureExists($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	$simpleLocalLogs = HostInfo_GetOpsRoot($computerName) & ".Logs\SimpleLocalLogs"
	If Not FileExists($simpleLocalLogs) Then DirCreate($simpleLocalLogs)
	Return $simpleLocalLogs
EndFunc   ;==>HostInfo_GetSimpleLocalLogDir_EnsureExists

;-------------------------------------------------------------------------------
; Name .........: HostInfo_GetSimpleLocalLogDir_EnsureExists_IsDefined
; Description ..: Get location of the local log file
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: JAN 23, 2016 - Created
;-------------------------------------------------------------------------------
Func HostInfo_GetSimpleLocalLogDir_EnsureExists_IsDefined()
	Return True
EndFunc   ;==>HostInfo_GetSimpleLocalLogDir_EnsureExists_IsDefined
