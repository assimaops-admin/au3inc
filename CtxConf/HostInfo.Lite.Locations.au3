#include-once
#include ".\HostInfo.Lite.Include.au3"

;-------------------------------------------------------------------------------
; Name .........: $HOSTINFO_AL_LIST (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Machines located at alionis
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_AM_LIST = _HostInfo_Lite_Private_CreateArray( _
		"qasrv01" _
		, "thales" _
		, "qacenter" _
		, "iissrv01" _
		, "testvm01" _
		, "testxvm01" _
		, "testxvm02" _
		, "testxvm03" _
		, "testxvm04" _
		, "vamigo-a1" _
		)

;-------------------------------------------------------------------------------
; Name .........: $HOSTINFO_AL_LIST (REF.PS/HOSTINFO_CST_CFG -- PART 2/3)
; Description ..: Machines located at alionis
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Const $HOSTINFO_SM_RE_LIST = _HostInfo_Lite_Private_CreateArray( _
		"qasrv02" _
		, "vamigo\-.*\-.*" _
		, "desk\-.*\-.*" _
		)

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsAlionis
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsAlionis($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	For $iAM = 1 To $HOSTINFO_AM_LIST[0]
		$candidate = $HOSTINFO_AM_LIST[$iAM]
		If Not StringCompare($candidate, $computerName) Then Return True
	Next
	Return False
EndFunc   ;==>HostInfo_IsAlionis

;-------------------------------------------------------------------------------
; Name .........: HostInfo_IsSoftLayer
; Description ..:
; Parameters ...: [optional] The host's name (Default: the current computer)
; Return .......: n/a
; History ......: OCT 20, 2011 - Created
;-------------------------------------------------------------------------------
Func HostInfo_IsSoftLayer($computerName = @ComputerName)
	If $computerName = Default Then $computerName = @ComputerName
	For $iAM = 1 To $HOSTINFO_SM_RE_LIST[0]
		$candidate = $HOSTINFO_SM_RE_LIST[$iAM]
		If Not StringRegExp($candidate, $computerName) Then Return True
	Next
	Return False
EndFunc   ;==>HostInfo_IsSoftLayer
