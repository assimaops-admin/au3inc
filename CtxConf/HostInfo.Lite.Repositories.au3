#include-once

#include ".\HostInfo.Lite.Locations.au3"

Func HostInfo_ReplaceByCloserRepository($szRepository)
	If HostInfo_IsAlionis() Then
		Return StringReplace($szRepository, "\\storage01\versions$\", "\\vsserver01\versions$\")
	EndIf
	Return $szRepository
EndFunc   ;==>HostInfo_ReplaceByCloserRepository
