#include-once
;; ------------------------
;; taken from <BaseUAFLibs\PubUDFs\ArrayV2.Lite.au3> ; this is needed for the setup scripts!
Func _HostInfo_Lite_Private_CreateArray($v_0 = 0, $v_1 = 0, $v_2 = 0, $v_3 = 0, $v_4 = 0, $v_5 = 0, $v_6 = 0, $v_7 = 0, $v_8 = 0, $v_9 = 0, $v_10 = 0, $v_11 = 0, $v_12 = 0, $v_13 = 0, $v_14 = 0, $v_15 = 0, $v_16 = 0, $v_17 = 0, $v_18 = 0, $v_19 = 0, $v_20 = 0)
	Local $av_Array[22] = [@NumParams, $v_0, $v_1, $v_2, $v_3, $v_4, $v_5, $v_6, $v_7, $v_8, $v_9, $v_10, $v_11, $v_12, $v_13, $v_14, $v_15, $v_16, $v_17, $v_18, $v_19, $v_20]
	ReDim $av_Array[@NumParams + 1]
	Return $av_Array
EndFunc   ;==>_HostInfo_Lite_Private_CreateArray
;; ------------------------
