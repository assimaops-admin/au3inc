#include-once

#include <Date.au3>

;-------------------------------------------------------------------------------
; Name .........: _DateTimeFormatForSAP
; Parameters ...: $sDate - Input date in the format "YYYY/MM/DD[ HH:MM:SS]"
; Description ..:
; Return .......: String - Formatted date as it appears in the SAP application
;-------------------------------------------------------------------------------
Func _DateTimeFormatForSAP($sDate)
	Dim $MyDate, $MyTime
	_DateTimeSplit($sDate, $MyDate, $MyTime)
	If $MyDate[0] <> 3 Then Return ""
	Return StringFormat("%02d.%02d.%04d", $MyDate[3], $MyDate[2], $MyDate[1])
EndFunc   ;==>_DateTimeFormatForSAP

;-------------------------------------------------------------------------------
; Name .........: _DateFromFlatTimestamp
; Parameters ...: $ts - Input 'timestamp' in the format "YYYYMMDDHHMMSS"
; Description ..: Convert a timestamp into string usable by Date.au3 API
; Return .......: String - Formatted date "YYYY/MM/DD HH:MM:SS"
; Testing ......:
;                    # include <ExtdCtxPrivMods\LegacyDevMods\ExtdLibs\UDFs\AutoItQuickTestv1.au3>
;                    $in = "20100712150007"
;                    $out = _DateFromFlatTimestamp($in)
;                    $expected = "2010/07/12 15:00:07"
;                    QuickTest_ExpectAnonym($out, $expected)
;
; History ......: JAN 28, 2011 - Created for use with _DateDiff
;                        ( needed in FileIsAgeInDaysGreaterThan )
;-------------------------------------------------------------------------------
Func _DateFromFlatTimestamp($ts, $sep1 = "/", $sep2 = ":")
	ConsoleWrite("Timestamp is '" & $ts & "'" & @CRLF)
	Return StringMid($ts, 1, 4) & $sep1 & StringMid($ts, 5, 2) & $sep1 & StringMid($ts, 7, 2) _
			 & " " & StringMid($ts, 9, 2) & ":" & StringMid($ts, 11, 2) & ":" & StringMid($ts, 13, 2)
EndFunc   ;==>_DateFromFlatTimestamp
