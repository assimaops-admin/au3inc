#include-once

#include <Date.au3>

Const $INVALID_FILEHANDLE_FROM_WINAPIv2 = 0xFFFFFFFF


;-------------------------------------------------------------------------------
; Name .........: MyBIAS
; Description ..: Return Bias in minutes.
;
;    The current bias for absolute time translations. The bias is the
;    difference in minutes between Coordinated Universal Time (UTC) and
;    local time. All translations between UTC and local time use the
;    following formula: UTC = local time + bias.
;
; Parameters ...:
; Return .......: String. See Remarks.
; Remarks ......:
; History ......: APR 3, 2013 - Created as a simpler version than MyOFFSETv1()
;-------------------------------------------------------------------------------
Func MyBIAS()
	$fullBIAS = 0
	$tzInfo = _Date_Time_GetTimeZoneInformation()
	If ($tzInfo[0] = 0) Then ; No Daylight Saving Time in this Zone, at all.
		$fullBIAS = $tzInfo[1]
	ElseIf ($tzInfo[0] = 1) Then ; Standard Time
		$fullBIAS = $tzInfo[1] + $tzInfo[4]
	ElseIf ($tzInfo[0] = 2) Then ; Daylight Saving Time
		$fullBIAS = $tzInfo[1] + $tzInfo[7]
	EndIf
	Return $fullBIAS
EndFunc   ;==>MyBIAS

;-------------------------------------------------------------------------------
; Name .........: MyOFFSETv1 [DEPRECATED]
; Description ..: Return offset in minutes.
; Parameters ...:
; Return .......: String. See Remarks.
; Remarks ......:
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 APR 3, 2013 - Renamed from MyBIAS to MyOFFSETv1
;                           it actually returns the opposite value than MyBIAS
;-------------------------------------------------------------------------------
Func MyOFFSETv1()
	Do
		Local $tSyst = _Date_Time_GetLocalTime()
		Local $tTime = _Date_Time_SystemTimeToFileTime(DllStructGetPtr($tSyst))
		Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Until ($aTime[6] < 950 Or $aTime[5] <> 59 Or $aTime[4] <> 59)
	Return (@HOUR - MyHOUR()) * 60
EndFunc   ;==>MyOFFSETv1

;-------------------------------------------------------------------------------
; Name .........: MyOFFSET ( <=> Return -MyBIAS() <=> MyAntiBIAS )
; Description ..: Return Bias in minutes. Formula: "UTC = local time + bias"
; Parameters ...:
; Return .......: String. "120" in PARIS in summer
; Remarks ......:
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 APR 3, 2013 - Renamed from MyBIAS to MyOFFSET
;                           it actually returns the opposite value than MyBIAS
;-------------------------------------------------------------------------------
Func MyOFFSET()
	Return -MyBIAS()
EndFunc   ;==>MyOFFSET

;-------------------------------------------------------------------------------
; Name .........: MyOFFSET4ISO ( <=> Return -MyBIAS() <=> MyAntiBIAS )
; Description ..: Return Bias in minutes. Formula: "UTC = local time + bias"
; Parameters ...:
; Return .......: String. "+0200" in PARIS in summer
; Remarks ......:
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 APR 3, 2013 - Renamed from MyBIAS to MyOFFSET
;                           it actually returns the opposite value than MyBIAS
;-------------------------------------------------------------------------------
Func MyOFFSET4ISO()
	$antibias_min = -MyBIAS()
	$sign = "+"
	If $antibias_min < 0 Then $sign = "-"
	$antibias_h = $antibias_min / 60
	Return StringFormat("%s%02d00", $sign, $antibias_h)
EndFunc   ;==>MyOFFSET4ISO

;-------------------------------------------------------------------------------
; Name .........: _NowISO_Ext ( _NowISO_ExtV1 )
; Description ..:
; Parameters ...: $flag = 0 - local time
;                 $flag = 1 - UTC time
; Return .......: String in the ISO 8601 format.
;                 (eg. "2011-07-25 02:39:51.800+01")
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 MAR 21, 2019 - Added _NowISO_ExtV2 with $aggreg_format
;                             and improved compliance with ISO 8601.
;                              (eg. "2011-07-25T02:39:51.800+0100")
;-------------------------------------------------------------------------------
Func _NowISO_Ext($flag = 0, $format = "%04d-%02d-%02d %02d:%02d:%02d%s", $tz_suffix_format = "%s%02d00", $millisec_suffix_format = ".%03d")
	Return _NowISO_ExtV1($flag, $format, $tz_suffix_format, $millisec_suffix_format)
EndFunc   ;==>_NowISO_Ext

;-------------------------------------------------------------------------------
; Name .........: _NowISO_ExtV1 ( DEPRECATED )
; Description ..:
; Parameters ...: $flag = 0 - local time
;                 $flag = 1 - UTC time
; Return .......: String in the ISO 8601 format.
;                 (eg. "2011-07-25 02:39:51.800+01")
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 MAR 21, 2019 - Added _NowISO_ExtV2 with $aggreg_format
;                             and improved compliance with ISO 8601.
;                              (eg. "2011-07-25T02:39:51.800+0100")
;-------------------------------------------------------------------------------
Func _NowISO_ExtV1($flag = 0, $format = "%04d-%02d-%02d %02d:%02d:%02d%s", $tz_suffix_format = "%s%02d00", $millisec_suffix_format = ".%03d")

	; Use UTC by default (flag <> 0)
	Local $bias = 0
	Local $tSyst = _Date_Time_GetSystemTime()
	Local $offset = 0
	If $flag = 0 Then
		$offset = MyOFFSET()
		$tSyst = _Date_Time_GetLocalTime()
	EndIf

	Local $tTime = _Date_Time_SystemTimeToFileTime(DllStructGetPtr($tSyst))
	Local $aTime = _Date_Time_FileTimeToArray($tTime)

	$tTime = _Date_Time_GetSystemTimeAsFileTime()

	Local $sign = "+"
	Local $diff = $offset / 60
	If $diff < 0 Then
		$sign = "-"
		$diff *= -1
	EndIf

	Local $tz_suffix = StringFormat($tz_suffix_format _
			, $sign _
			, $diff _
			)

	Local $millisec_suffix = StringFormat($millisec_suffix_format, $aTime[6])

	Return StringFormat($format _
			, $aTime[2] _
			, $aTime[0] _
			, $aTime[1] _
			, $aTime[3] _
			, $aTime[4] _
			, $aTime[5] _
			, $millisec_suffix & $tz_suffix _
			)

EndFunc   ;==>_NowISO_ExtV1

;-------------------------------------------------------------------------------
; Name .........: _NowISO_ExtV2 ( NEW VERSION 2019+ )
; Description ..: Allows reduced precision formats, such as: 2019-03-21 12:29 +0100
; _NowISO_ExtV2(0, "%s %s%s %s", "%04d-%02d-%02d", "%02d:%02d", "", "%s%02d00")
; Parameters ...: $flag = 0 - local time
;                 $flag = 1 - UTC time
; Return .......: String in the ISO 8601 format.
;                 (eg. "2011-07-25T02:39:51.800+0100")
; History ......: NOV 3, 2010 - Created for 'EasyLog.au3'
;                 MAR 21, 2019 - Added _NowISO_ExtV2 with $aggreg_format
;                             and improved compliance with ISO 8601.
;                              (eg. "2011-07-25T02:39:51.800+0100")
;-------------------------------------------------------------------------------
Func _NowISO_ExtV2($flag = 0, $aggreg_format = "%sT%s%s%s", $date_format = "%04d-%02d-%02d", $time_format = "%02d:%02d:%02d%s", $millisec_suffix_format = ".%03d", $tz_suffix_format = "%s%02d00")

	; Use UTC by default (flag <> 0)
	Local $bias = 0
	Local $tSyst = _Date_Time_GetSystemTime()
	Local $offset = 0
	If $flag = 0 Then
		$offset = MyOFFSET()
		$tSyst = _Date_Time_GetLocalTime()
	EndIf

	Local $tTime = _Date_Time_SystemTimeToFileTime(DllStructGetPtr($tSyst))
	Local $aTime = _Date_Time_FileTimeToArray($tTime)

	$tTime = _Date_Time_GetSystemTimeAsFileTime()

	Local $date_string = StringFormat($date_format _
			, $aTime[2] _
			, $aTime[0] _
			, $aTime[1] _
			)

	Local $time_string = StringFormat($time_format _
			, $aTime[3] _
			, $aTime[4] _
			, $aTime[5] _
			)

	Local $millisec_suffix = StringFormat($millisec_suffix_format, $aTime[6])

	Local $sign = "+"
	Local $diff = $offset / 60
	If $diff < 0 Then
		$sign = "-"
		$diff *= -1
	EndIf

	Local $tz_suffix = StringFormat($tz_suffix_format _
			, $sign _
			, $diff _
			)

	Return StringFormat($aggreg_format _
			, $date_string _
			, $time_string _
			, $millisec_suffix _
			, $tz_suffix _
			)

EndFunc   ;==>_NowISO_ExtV2

;-------------------------------------------------------------------------------
; Name .........: _NowISO_Std
; History ......: MAY 12, 2011 - Created
;-------------------------------------------------------------------------------
Func _NowISO_Std($flag = 0)
	Return _NowISO_Ext($flag)
EndFunc   ;==>_NowISO_Std

;-------------------------------------------------------------------------------
; Name .........: _NowISO_Tag
; Description ..:
; Parameters ...: $flag = 0 - local time
;                 $flag = 1 - UTC time
; Return .......: String in the ISO 8601 format.
; History ......: MAY 12, 2011 - Created
;-------------------------------------------------------------------------------
Func _NowISO_Tag($flag = 0)
	Return _NowISO_Ext($flag, "%04d%02d%02d%02d%02d%02d%03d")
EndFunc   ;==>_NowISO_Tag

;-------------------------------------------------------------------------------
; Name .........: MonthToStr
; Description ..: Returns the name of the month.
; Parameters ...: integer between 1 and 12
; Return .......: String.
; History ......: SEP 24, 2010 - Created for filtering-out dates
;-------------------------------------------------------------------------------
Func MonthToStr($i)
	If $i < 1 Or $i > 12 Then Return ""
	Dim Const $MONTHS_IN_ENGLISH[12] = [ _
			"January", "February", "March", "April", "May", "June" _
			, "July", "August", "September", "October", "November", "December"]
	Return $MONTHS_IN_ENGLISH[$i - 1]
EndFunc   ;==>MonthToStr

;-------------------------------------------------------------------------------
; Name .........: MyYEAR
; Description ..: @YEAR but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: year (four digits)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MyYEAR()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%04d", $aTime[2])
EndFunc   ;==>MyYEAR

;-------------------------------------------------------------------------------
; Name .........: MyMON
; Description ..: @MON but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: month (range 01 - 12)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MyMON()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%02d", $aTime[0])
EndFunc   ;==>MyMON

;-------------------------------------------------------------------------------
; Name .........: MyMDAY
; Description ..: @MDAY but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: day (range 01 - 31)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MyMDAY()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%02d", $aTime[1])
EndFunc   ;==>MyMDAY

;-------------------------------------------------------------------------------
; Name .........: MyHOUR
; Description ..: @HOUR but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: hour (range 00 - 23)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MyHOUR()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%02d", $aTime[3])
EndFunc   ;==>MyHOUR

;-------------------------------------------------------------------------------
; Name .........: MyMIN
; Description ..: @MIN but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: min (range 00 - 59)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MyMIN()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%02d", $aTime[4])
EndFunc   ;==>MyMIN

;-------------------------------------------------------------------------------
; Name .........: MySEC
; Description ..: @SEC but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: String. See Remarks.
; Remarks ......: sec (range 00 - 59)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func MySEC()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%02d", $aTime[5])
EndFunc   ;==>MySEC

;-------------------------------------------------------------------------------
; Name .........: _NowCalcV2
; Description ..: Similar to _NowCalc but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: Formated String "YYYY/MM/DD HH:MM:SS"
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func _NowCalcV2()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%04d/%02d/%02d %02d:%02d:%02d" _
			, $aTime[2], $aTime[0], $aTime[1], $aTime[3], $aTime[4], $aTime[5])
EndFunc   ;==>_NowCalcV2

;-------------------------------------------------------------------------------
; Name .........: _NowCalcDateV2
; Description ..: Similar to _NowCalcDate but without the 'daylight saving' info.
; Parameters ...: none
; Return .......: Formated String "YYYY/MM/DD"
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func _NowCalcDateV2()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Return StringFormat("%04d/%02d/%02d" _
			, $aTime[2], $aTime[0], $aTime[1])
EndFunc   ;==>_NowCalcDateV2

;-------------------------------------------------------------------------------
; Name .........: _MyNowV2
; Description ..: Array with info such as @YEAR, @MON, @HOUR etc. but without
;                 the 'daylight saving' info.
; Parameters ...: none
; Return .......: Array. See Remarks.
; Remarks ......: The array is a single dimension array containing six elements:
;                 $array[0] = year (four digits)
;                 $array[1] = month (range 01 - 12)
;                 $array[2] = day (range 01 - 31)
;                 $array[3] = hour (range 00 - 23)
;                 $array[4] = min (range 00 - 59)
;                 $array[5] = sec (range 00 - 59)
;                 Notice that return values are zero-padded.
; History ......: JUN 24, 2010 - Created for 'BuildManager.au3'
;-------------------------------------------------------------------------------
Func _MyNowV2()
	Local $tTime = _Date_Time_GetSystemTimeAsFileTime()
	Local $aTime = _Date_Time_FileTimeToArray($tTime)
	Dim $return[6] = [ _
			StringFormat("%04d", $aTime[2]) _
			, StringFormat("%02d", $aTime[0]) _
			, StringFormat("%02d", $aTime[1]) _
			, StringFormat("%02d", $aTime[3]) _
			, StringFormat("%02d", $aTime[4]) _
			, StringFormat("%02d", $aTime[5])]
	Return $return
EndFunc   ;==>_MyNowV2

;-------------------------------------------------------------------------------
; Name .........: FileGetTimeV2 (v2013-04-03)
; Description ..: Similar to FileGetTime but without the 'daylight saving'
;                 nor 'bias' part. See 'FileGetTimeLocal' for the equivalent
;                 function dealing with the  local time.
; Parameters ...: $filename - Filename to check.
;
;                 $option [optional] - Flag to indicate which timestamp
;                                        - 0 = Modified (default)
;                                        - 1 = Created
;                                        - 2 = Accessed
;
;                 $format [optional] - To specify type of return
;                                - Integer 0 = return an array (default)
;                                - Integer 1 = return a string YYYYMMDDHHMMSS
;                                - Integer 2 = return a string YYYYMMDD_HHMMSS
;                                - Integer 3 = return a string YYYY/MM/DD HH:MM:SS
;                                - String such as "%04d|%02d|%02d|%02d|%02d|%02d"
;
;                 $RBTMode [optional] - Boolean - Real/Relative British time mode.
;                       Basically, return times as felt and seen on electronic
;                       clocks by people in the UK at that time/date.
;                         (AKA. GMT/BST mode) --WARNING: NOT IMPLEMENTED!
;
;
;        TODO:
;          IMPLEMENT THE FOLLOWING FORMATS INSTEAD
;            - 2004-02-12T15:19:21+00:00   (FORMAT ID = 1)
;            - 20040212151921  (FORMAT ID = 2)
;            - YYYY/MM/DD HH:MM:SS  (FORMAT ID = 3)
;            - 20040212_151921  (FORMAT ID = 4)
;            - RETURN A STRING YYYYMM-DD-HH-MMSS  (FORMAT ID = 5)
;
;
;
; Return .......: Success: Returns an array or string that contains the file
;                          time information. See Remarks.
;                 Failure: Returns empty string and sets @error to 1.
; Remarks ......: The array is a single dimension array containing six elements:
;                 $array[0] = year (four digits)
;                 $array[1] = month (range 01 - 12)
;                 $array[2] = day (range 01 - 31)
;                 $array[3] = hour (range 00 - 23)
;                 $array[4] = min (range 00 - 59)
;                 $array[5] = sec (range 00 - 59)
;                 Notice that return values are zero-padded.
; Testing ......:
;                 ConsoleWrite("0 -> "&FileGetTimeV2(@ScriptFullPath, 0, 0)&@CRLF)
;                 ConsoleWrite("1 -> "&FileGetTimeV2(@ScriptFullPath, 0, 1)&@CRLF)
;                 ConsoleWrite("2 -> "&FileGetTimeV2(@ScriptFullPath, 0, 2)&@CRLF)
;                 ConsoleWrite("3 -> "&FileGetTimeV2(@ScriptFullPath, 0, 3)&@CRLF)
;                 ConsoleWrite("CUSTOM STRING -> "&FileGetTimeV2(@ScriptFullPath, 0, "%04d|%02d|%02d|%02d|%02d|%02d")&@CRLF)
;
;    0 ->
;    1 -> 20130409091728
;    2 -> 20130409_091728
;    3 -> 2013/04/09 09:17:28
;    CUSTOM STRING -> 2013|04|09|09|17|28
;
; History ......: JUN 9, 2010 - Created for 'HostMonitor.au3'
;                 JUL 14, 2010 - Added format '2' (eg. "201007-12-15-0007")
;                 JAN 28, 2011 - Added format '3' (eg. "YYYY/MM/DD[ HH:MM:SS]")
;                 APR 3, 2013 - Format '2' now is YYYYMMDD_HHMMSS
;                               Previous format ID '2' becomes ID '101'
;                 APR 25, 2013 - Removed custom format '101' completely.
;                               Use custom string instead.
;-------------------------------------------------------------------------------

Enum $eFileGetTimeFormat_0 = 0 _
		, $eFileGetTimeFormat_1 _
		, $eFileGetTimeFormat_2 _
		, $eFileGetTimeFormat_3

Func FileGetTimeV2($filename, $option = 0, $format = 0, $RBTMode = 0)
	If $option = Default Then $option = 0
	Local $pattern = "%04d%02d%02d%02d%02d%02d"
	If IsString($format) Then
		$pattern = $format
		$format = 100
	ElseIf Not IsInt($format) Then
		$format = 0
	EndIf
	Local $return = ""
	Local $hFile = _WinAPI_CreateFile($filename, 2, 2, 2)
	If $hFile = $INVALID_FILEHANDLE_FROM_WINAPIv2 Then
		SetError(1)
		Return $return
	Else
		Local $a_tFiletime = _Date_Time_GetFileTime($hFile)
		;If $RBTMode Then
		; TODO: FIND OUT WHETHER $A_TFILETIME IS IN SUBJECT TO DAYLIGHT SAVING TIME BIAS
		;Global Const $tagTIME_ZONE_INFORMATION = "struct; long Bias;wchar StdName[32];word StdDate[8];long StdBias;wchar DayName[32];word DayDate[8];long DayBias; endstruct"
		;$tzInfo = _Date_Time_GetTimeZoneInformation()
		;$a_tSystemtime = _Date_Time_FileTimeToSystemTime($a_tFiletime)
		;_Date_Time_SystemTimeToTzSpecificLocalTime($a_tSystemtime)
		;EndIf
		Local $index = 2 ; by default we read modification date
		If $option = 1 Then $index = 0 ; creation date
		If $option = 2 Then $index = 1 ; last access date
		Local $aTime = _Date_Time_FileTimeToArray($a_tFiletime[$index])
		If $format Then
			If $format = 2 Then
				$pattern = "%04d%02d%02d_%02d%02d%02d"
			ElseIf $format = 3 Then
				$pattern = "%04d/%02d/%02d %02d:%02d:%02d"
			ElseIf $format = 100 Then
				; custom format already defined
			ElseIf $format = 101 Then
				; Not implemented
				; eg. > $pattern = "%04d%02d%02d-%02d%02d"
				; eg. > $pattern = "%04d|%02d|%02d|%02d|%02d|%02d"
			EndIf
			$return = StringFormat($pattern _
					, $aTime[2], $aTime[0], $aTime[1], $aTime[3], $aTime[4], $aTime[5])
		Else
			Dim $return[6] = [ _
					StringFormat("%04d", $aTime[2]) _
					, StringFormat("%02d", $aTime[0]) _
					, StringFormat("%02d", $aTime[1]) _
					, StringFormat("%02d", $aTime[3]) _
					, StringFormat("%02d", $aTime[4]) _
					, StringFormat("%02d", $aTime[5])]
		EndIf
		_WinAPI_CloseHandle($hFile)
		Return $return
	EndIf
EndFunc   ;==>FileGetTimeV2
