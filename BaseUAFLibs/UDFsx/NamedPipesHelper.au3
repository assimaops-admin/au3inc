#include-once

#include <WinAPI.au3>

; ===============================================================================================================================
; This function reads a message from the pipe
; Source: C:\Program FIles (x86)\AutoIt3\Examples\GUI\Advanced\_NamedPipes_Client.au3
; -- EDITED FEB 13, 2015
; -- Removed GUI, return string instead
; -- Use UNICODE
; -- + RENAMED: ReadMsg -> NamedPipesHelper_GetUnicodeText
; ===============================================================================================================================
Func NamedPipesHelper_GetUnicodeText($hPipe)

	$sText = ""

	Const $BUFSIZE = 4096
	Const $ERROR_MORE_DATA = 234

	Local $bSuccess, $iRead, $pBuffer, $tBuffer

	$tBuffer = DllStructCreate("wchar Text[" & $BUFSIZE & "]")
	$pBuffer = DllStructGetPtr($tBuffer)

	$bSuccess = _WinAPI_ReadFile($hPipe, $pBuffer, $BUFSIZE, $iRead, 0)
	;ConsoleWrite("Reader_Info: |" & $hPipe & "|" & $pBuffer & "|" & $BUFSIZE & "|" & $iRead & "|" & @CRLF)
	;ConsoleWrite("Reader_Buffer_Text: |" & DllStructGetData($tBuffer, "Text") & "|" & @CRLF)

	If $iRead = 0 Then Return $sText
	If Not $bSuccess Or (_WinAPI_GetLastError() = $ERROR_MORE_DATA) Then Return $sText

	$sText &= StringLeft(DllStructGetData($tBuffer, "Text"), $iRead)

	Return $sText

EndFunc   ;==>NamedPipesHelper_GetUnicodeText

; ===============================================================================================================================
; This function writes a message to the pipe
; Source: C:\Program FIles (x86)\AutoIt3\Examples\GUI\Advanced\_NamedPipes_Client.au3
; -- EDITED FEB 13, 2015
; -- Use UNICODE
; -- + RENAMED: WriteMsg -> NamedPipesHelper_SetUnicodeText
; ===============================================================================================================================
Func NamedPipesHelper_SetUnicodeText($hPipe, $sMessage)

	Local $iWritten, $iBuffer, $pBuffer, $tBuffer

	;$charCount = StringLen($sMessage) + 1 ; <<<< Add the '\0' char
	$charCount = StringLen($sMessage) ; <<<<<<<<< DO ***NOT*** ADD THE ZERO CHAR!
	$tBuffer = DllStructCreate("wchar Text[" & $charCount & "]")
	$pBuffer = DllStructGetPtr($tBuffer)
	$iBufSize = DllStructGetSize($tBuffer)
	DllStructSetData($tBuffer, "Text", $sMessage)
	$isWriteOK = _WinAPI_WriteFile($hPipe, $pBuffer, $iBufSize, $iWritten, 0)
	If Not $isWriteOK Then
		ConsoleWrite("!ERROR: NamedPipesHelper_SetUnicodeText: _WinAPI_WriteFile failed." & @CRLF)
		Return False
	EndIf
	Return True

EndFunc   ;==>NamedPipesHelper_SetUnicodeText
