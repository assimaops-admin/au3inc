#include-once

Global Const $DEFAULT_FUNCT_TIMER_CYCLE_DURATION = 100 ; << to adjust depending on context, or make it a parameter (see. $LOCAL_FUNCT_TIMER_CYCLE_DURATION)


;-------------------------------------------------------------------------------
; Name .........: FuncTimerCreate
; Description ..:
; Parameters ...: $timeout - timeout in seconds
; Return .......: Pseudo object (array with values to use by methods)
;-------------------------------------------------------------------------------
Func FuncTimerCreate($timeout = Default)
	Dim $myObj[2] = [TimerInit(), $timeout * 1000]

	If Not IsInt($timeout) Then Return 0
	If $timeout <= 0 Then Return 0

	Return $myObj
EndFunc   ;==>FuncTimerCreate

;-------------------------------------------------------------------------------
; Name .........: FuncTimerGetTimeout
; Description ..:
; Parameters ...: $myObj - Pseudo object (array with values to use by methods)
; Return .......: 'true' if timer is elapsed; 'false' otherwise
;-------------------------------------------------------------------------------
Func FuncTimerGetTimeout($myObj)
	If Not IsArray($myObj) Then Return False
	If UBound($myObj) <> 2 Then Return False
	Return $myObj[1] / 1000
EndFunc   ;==>FuncTimerGetTimeout

;-------------------------------------------------------------------------------
; Name .........: FuncTimerIsElapsed
; Description ..:
; Parameters ...: $myObj - Pseudo object (array with values to use by methods)
; Return .......: 'true' if timer is elapsed; 'false' otherwise
;-------------------------------------------------------------------------------
Func FuncTimerIsElapsed($myObj)
	If Not IsArray($myObj) Then Return False
	If UBound($myObj) <> 2 Then Return False
	If TimerDiff($myObj[0]) >= $myObj[1] Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>FuncTimerIsElapsed

;-------------------------------------------------------------------------------
; Name .........: FuncTimerGetTimeInMs
; Description ..:
; Parameters ...: $myObj - Pseudo object (array with values to use by methods)
; Return .......: 'true' if timer is elapsed; 'false' otherwise
;-------------------------------------------------------------------------------
Func FuncTimerGetTimeInMs($myObj)
	If Not IsArray($myObj) Then Return ""
	If UBound($myObj) <> 2 Then Return ""
	Return TimerDiff($myObj[0])
EndFunc   ;==>FuncTimerGetTimeInMs

;-------------------------------------------------------------------------------
; Name .........: FuncTimerGetTimeInSec
; Description ..:
; Parameters ...: $myObj - Pseudo object (array with values to use by methods)
; Return .......: 'true' if timer is elapsed; 'false' otherwise
;-------------------------------------------------------------------------------
Func FuncTimerGetTimeInSec($myObj)
	Const $TIME_TRACE_ROUND_PRECISION_IN_SECONDS = 1
	If Not IsArray($myObj) Then Return ""
	If UBound($myObj) <> 2 Then Return ""
	Return Round(TimerDiff($myObj[0]) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS)
EndFunc   ;==>FuncTimerGetTimeInSec


#cs
	UnitTest__FuncTimer__InfiniteWait()
	Func UnitTest__FuncTimer__InfiniteWait()
	$timer = FuncTimerCreate(0)
	While true
	ConsoleWrite("Sleep..." & @CRLF)
	Sleep(250)
	If FuncTimerIsElapsed($timer) Then ExitLoop
	WEnd
	EndFunc
#ce

#cs
	UnitTest__FuncTimer__SmallWait()
	Func UnitTest__FuncTimer__SmallWait()
	$timer = FuncTimerCreate(1)
	While true
	ConsoleWrite("Sleep..." & @CRLF)
	Sleep(250)
	If FuncTimerIsElapsed($timer) Then ExitLoop
	WEnd
	EndFunc
#ce
