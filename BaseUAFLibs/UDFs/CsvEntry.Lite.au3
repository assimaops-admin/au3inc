#include-once

Func CsvEntryDel($string, $sub_entry, $separator = ";")

	$string2 = $string
	$found = 1
	$extended = 0
	While $found And ($string2 <> "")
		$found = 0

		; begining of the string fixes
		$pattern = $separator
		If StringCompare(StringLeft($string2, StringLen($pattern)), $pattern) = 0 Then
			$string2 = StringTrimLeft($string2, StringLen($pattern))
			$found = 1
			$extended = 1
			ContinueLoop
		EndIf

		; end of string fixes
		$pattern = $separator
		If StringCompare(StringRight($string2, StringLen($pattern)), $pattern) = 0 Then
			$string2 = StringTrimRight($string2, StringLen($pattern))
			$found = 1
			$extended = 1
			ContinueLoop
		EndIf

		; at the begining of the string
		$pattern = $sub_entry & $separator
		If StringCompare(StringLeft($string2, StringLen($pattern)), $pattern) = 0 Then
			$string2 = StringTrimLeft($string2, StringLen($pattern))
			$found = 1
			ContinueLoop
		EndIf

		; in the middle of the string
		$pattern = $separator & $sub_entry & $separator
		If StringInStr($string2, $pattern) Then
			$string2 = StringReplace($string2, $pattern, $separator)
			$found = 1
			$extended = 1
			ContinueLoop
		EndIf

		; at the end of the string
		$pattern = $separator & $sub_entry
		If StringCompare(StringRight($string2, StringLen($pattern)), $pattern) = 0 Then
			$string2 = StringTrimRight($string2, StringLen($pattern))
			$found = 1
			$extended = 1
			ContinueLoop
		EndIf

		; or the string itself?!
		$pattern = $sub_entry
		If $string2 = $pattern Then
			$string2 = ""
			$found = 1
			$extended = 1
			ContinueLoop
		EndIf

	WEnd

	Return SetExtended($extended, $string2)

EndFunc   ;==>CsvEntryDel
