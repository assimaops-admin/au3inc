#include-once

Func _ProcessGetWindow($pid, $wndTitle = "", $wndText = "")
	; Check the title
	; because we might be looking for "XNote Timer"
	; and get XNote "Timer / Results"
	Local $anyMatchHandle = ""
	Local $list = WinList($wndTitle, $wndText)
	Local $i
	For $i = 1 To $list[0][0]
		If WinGetProcess($list[$i][1]) = $pid Then
			$anyMatchHandle = $list[$i][1]
			If ($list[$i][0] = $wndTitle) Then
				; $exactMatchHandle
				Return $list[$i][1]
			EndIf
		EndIf
	Next
	Return $anyMatchHandle
EndFunc   ;==>_ProcessGetWindow
