#include-once

Func RegReadV2($key_path, $key_name, $expand = 1)
	Const $__REG_EXPAND_SZ = 2
	$key_value = RegRead($key_path, $key_name)
	If $expand And @extended = $__REG_EXPAND_SZ Then
		$a = StringRegExp($key_value, "%([a-zA-Z_]+[a-zA-Z_0-9]*)%", 1)
		If Not @error Then
			For $i = 0 To UBound($a) - 1
				$r = RegReadV2($key_path, $a[$i], $expand)
				$key_value = StringReplace($key_value, "%" & $a[$i] & "%", $r)
			Next
		EndIf
	EndIf
	Return $key_value
EndFunc   ;==>RegReadV2
