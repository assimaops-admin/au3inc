#include-once
#include <Debug.au3>
$UAF_DEBUG_ENVVAR = EnvGet("UAF_DEBUG")
If $UAF_DEBUG_ENVVAR = "0" Then $UAF_DEBUG_ENVVAR = ""
If Not IsDeclared("dbgOn") Then Assign("dbgOn", $UAF_DEBUG_ENVVAR, 2)
Global $dbgOn
;ConsoleWrite('@@ Debug(' & @ScriptLineNumber & ') : $UAF_DEBUG_ENVVAR = ' & $UAF_DEBUG_ENVVAR & @CRLF & '>Error code: ' & @error & @CRLF) ;### Debug Console
;MsgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & '$UAF_DEBUG_ENVVAR' & @lf & @lf & 'Return:' & @lf & $UAF_DEBUG_ENVVAR) ;### Debug MSGBOX
; If IsSet("TRACE_WRITE") ~~> IsSet("ASSERT_EVAL")

#cs
	;# include <CustLibs\Dbg.au3>
	Func _DbgFun($name, $p1 = "", $p2 = "", $p3 = "")
	EndFunc   ;==>_DbgFun
	Func _DbgRet($ret)
	Return $ret
	EndFunc   ;==>_DbgRet
	Func _DbgOut($text)
	EndFunc   ;==>_DbgOut
#ce

; #FUNCTION# ====================================================================================================================
; Name ..........: _Assrt
; Description ...:
; Syntax ........: _Assrt($bCondition[, $bExit = True[, $nCode = 0x7FFFFFFF[, $sLine = @ScriptLineNumber[, $curerr = @error[,
;                  $curext = @extended]]]]])
; Parameters ....: $bCondition - IN - The evaluation of the condition that must evaluate to true.
;                  $bExit - IN/OPTIONAL - If true, the script is aborted.
;                  $nCode - IN/OPTIONAL - The exit code to use if the script is aborted.
;                  $sLine - IN/OPTIONAL - Displays the line number where the assertion failed.  If this value is not
;                                         changed, then the default value will show the correct line.
; Return values .: None
; Author ........: JUL 11, 2016 - Written based on original _Assert function
; Modified ......:
; Remarks .......: @error and @extended are not destroyed on return.
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Assrt($bCondition, $sCondition, $bExit = True, $nCode = 0x7FFFFFFF, $sLine = @ScriptLineNumber, Const $curerr = @error, Const $curext = @extended)
	If Not $dbgOn Then Return
	If Not $bCondition Then
		MsgBox(16 + 262144, "Autoit Assert", "Assertion Failed (Line " & $sLine & "): " & @CRLF & @CRLF & $sCondition)
		If $bExit Then Exit $nCode
	EndIf
	Return SetError($curerr, $curext, $bCondition)
EndFunc   ;==>_Assrt

; If $dbgOn Then _DebugSetup(@ScriptDir & " - Debug window")
Global $DBG_TABS = " |DBG| "

; #FUNCTION# ====================================================================================================================
; Name ..........: _DbgFun
; Description ...:
; Syntax ........: _DbgFun($name[, $p1 = ""[, $p2 = ""[, $p3 = ""]]])
; Parameters ....: $name                - A floating point number value.
;                  $p1                  - [optional] A pointer value. Default is "".
;                  $p2                  - [optional] A pointer value. Default is "".
;                  $p3                  - [optional] A pointer value. Default is "".
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _DbgFun($name, $p1 = "", $p2 = "", $p3 = "")
	$strOut = $name & "()..."
	If $p1 <> "" Then $strOut = $name & "('" & $p1 & "')..."
	If $p2 <> "" Then $strOut = $name & "('" & $p1 & "', '" & $p2 & "')..."
	If $p3 <> "" Then $strOut = $name & "('" & $p1 & "', '" & $p2 & "', '" & $p3 & "')..."
	_DebugOut($DBG_TABS)
	_DebugOut($DBG_TABS & $strOut)
	_DebugOut($DBG_TABS)
	If $dbgOn Then
		ConsoleWrite($DBG_TABS & @CRLF)
		ConsoleWrite($DBG_TABS & $strOut & @CRLF)
		ConsoleWrite($DBG_TABS & @CRLF)
	EndIf
	$DBG_TABS = @TAB & $DBG_TABS
EndFunc   ;==>_DbgFun


; #FUNCTION# ====================================================================================================================
; Name ..........: _DbgRet
; Description ...:
; Syntax ........: _DbgRet($ret)
; Parameters ....: $ret                 - An unknown value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _DbgRet($ret)
	$strOut = "=> RETURNING '" & $ret & "'."
	_DebugOut($DBG_TABS)
	_DebugOut($DBG_TABS & $strOut)
	_DebugOut($DBG_TABS)
	If $dbgOn Then
		ConsoleWrite($DBG_TABS & @CRLF)
		ConsoleWrite($DBG_TABS & $strOut & @CRLF)
		ConsoleWrite($DBG_TABS & @CRLF)
	EndIf
	$DBG_TABS = StringTrimLeft($DBG_TABS, 1)
	Return $ret
EndFunc   ;==>_DbgRet

; #FUNCTION# ====================================================================================================================
; Name ..........: _DbgOut
; Description ...:
; Syntax ........: _DbgOut($text)
; Parameters ....: $text                - A dll struct value.
; Return values .: None
; Author ........: Your Name
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _DbgOut($text)
	$strOut = $text
	_DebugOut($DBG_TABS & $strOut)
	If $dbgOn Then
		ConsoleWrite($DBG_TABS & $strOut & @CRLF)
	EndIf
EndFunc   ;==>_DbgOut
