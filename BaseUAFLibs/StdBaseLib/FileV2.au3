#include-once
#include <AuItCustLibs\StringV2.au3>
#include "..\UDFsx\FileGetShortNameV2.au3"
#include "..\UDFsx\DateV2.au3"


;-------------------------------------------------------------------------------
; Name .........: FileGetTimeLocal
; Description ..: Same as FileGetTime but with additional options/formats
; Parameters ...: $filename - Filename to check.
;                 $option [optional] - Flag to indicate which timestamp
;                                        0 = Modified (default)
;                                        1 = Created
;                                        2 = Accessed
;                 $format [optional] - To specify type of return
;                                        0 = return an array (default)
;                                        1 = return a string YYYYMMDDHHMMSS
;                                        2 = return a string YYYY/MM/DD HH:MM:SS
; Return .......: Success: Returns an array or string that contains the file
;                          time information. See Remarks.
;                 Failure: Returns empty string and sets @error to 1.
; Testing ......:
;                 ConsoleWrite(FileGetTimeLocal(@ScriptFullPath, 0, 0)&@CRLF)
;                 ConsoleWrite(FileGetTimeLocal(@ScriptFullPath, 0, 1)&@CRLF)
;                 ConsoleWrite(FileGetTimeLocal(@ScriptFullPath, 0, 2)&@CRLF)
;                 ConsoleWrite(FileGetTimeLocal(@ScriptFullPath, 0, 3)&@CRLF)
;
; History ......: JAN 28, 2011 - Created with formats '2' and '3'
;-------------------------------------------------------------------------------
Func FileGetTimeLocal($filename, $option = 0, $format = 0)
	$fw_format = $format
	If $format > 1 Then $fw_format = 1
	$time = FileGetTime($filename, $option, $fw_format)
	If $format = 2 Then
		$time = _DateFromFlatTimestamp($time)
	EndIf
	ConsoleWrite("File time is '" & $time & "'" & @CRLF)
	Return $time
EndFunc   ;==>FileGetTimeLocal

;-------------------------------------------------------------------------------
; Name .........: FileEncodingToBinaryFlag
; Description ..: Translate FileOpen flag type to BinaryToString flag type
;                 (see FileOpen, FileGetEncoding and BinaryToString functions)
;
;                 from $iFileEncoding
;                   0 = ANSI
;                   32 = UTF16 Little Endian.
;                   64 = UTF16 Big Endian.
;                   128 = UTF8 (with BOM).
;                   256 = (without BOM).
;                 into:
;                   flag = 1 (default), binary data is taken to be ANSI
;                   flag = 2, binary data is taken to be UTF16 Little Endian
;                   flag = 3, binary data is taken to be UTF16 Big Endian
;                   flag = 4, binary data is taken to be UTF8
;
; Return .......: Flag
; History ......: JUL 1, 2011
;-------------------------------------------------------------------------------
Func FileEncodingToBinaryFlag($iFileEncoding)
	$flag = 1
	Switch $iFileEncoding
		Case 32
			$flag = 2
		Case 64
			$flag = 3
		Case 128
			$flag = 4
		Case 256
			$flag = 4
	EndSwitch
	Return $flag
EndFunc   ;==>FileEncodingToBinaryFlag

;-------------------------------------------------------------------------------
; Name .........: FileSetContent
; Description ..: Put old file in recycle bin and write new content in the file.
; Notes ........: 1/ Do nothing at all if the file is up-to-date.
;                 2/ Write temporary file first and move the file to its final
;                 location (faster).
; Return .......: BitAND of all the other operations.
; History ......: JUL 22, 2010 - Created for writing configuration files with
;                                      EasyPHP-3.1-config/_Main.au3
;-------------------------------------------------------------------------------
Func FileSetContent($file_path, $file_content)
	$ret = 1
	If FileRead($file_path) = $file_content Then
		Return 1 ; Already up-to-date
	EndIf
	If FileExists($file_path & ".tmp") Then
		$ret_ = FileDelete($file_path & ".tmp")
		$ret = BitAND($ret, $ret_)
	EndIf
	$ret_ = FileWrite($file_path & ".tmp", $file_content)
	$ret = BitAND($ret, $ret_)
	If FileExists($file_path) Then
		$ret_ = FileRecycle($file_path)
		$ret = BitAND($ret, $ret_)
	EndIf
	$ret_ = FileMove($file_path & ".tmp", $file_path)
	$ret = BitAND($ret, $ret_)
	Return $ret
EndFunc   ;==>FileSetContent

;-------------------------------------------------------------------------------
; Name .........: IsDirectory
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func IsDirectory($szPath, $isShortName = 0)
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	$attrib = FileGetAttrib($szPath_short)
	;;;ValTrace($szPath_short, '$szPath_short')
	;;;ValTrace($attrib, '$attrib')
	Return StringInStr($attrib, "D")
EndFunc   ;==>IsDirectory

;-------------------------------------------------------------------------------
; Name .........: DirRemoveV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DirRemoveV2($szPath, $isShortName = 0)
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	Return DirRemove($szPath_short, 1)
EndFunc   ;==>DirRemoveV2

;-------------------------------------------------------------------------------
; Name .........: FileDeleteV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileDeleteV2($szPath, $isShortName = 0)
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	Return FileDelete($szPath_short)
EndFunc   ;==>FileDeleteV2

;-------------------------------------------------------------------------------
; Name .........: GetNameFromPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetNameFromPath($szRepositoryPath)
	Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
	_PathSplit($szRepositoryPath, $szDrive, $szDir, $szFName, $szExt)
	Return $szFName
EndFunc   ;==>GetNameFromPath

;-------------------------------------------------------------------------------
; Name .........: GetFilenameFromPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFilenameFromPath($szPath)
	Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
	StringReplaceRightChars($szPath, "\/", "")
	_PathSplit($szPath, $szDrive, $szDir, $szFName, $szExt)
	Return $szFName & $szExt
EndFunc   ;==>GetFilenameFromPath

;-------------------------------------------------------------------------------
; Name .........: GetFolderNameFromPath (aka. GetFilenameFromPath)
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFolderNameFromPath($szPath)
	Return GetFilenameFromPath($szPath)
EndFunc   ;==>GetFolderNameFromPath

;-------------------------------------------------------------------------------
; Name .........: GetExtensionFromPath
; Description ..:
; Remarks ......: Returns the DOT character as well!
; Return .......:
;-------------------------------------------------------------------------------
Func GetExtensionFromPath($szPath)
	Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
	_PathSplit($szPath, $szDrive, $szDir, $szFName, $szExt)
	Return $szExt
EndFunc   ;==>GetExtensionFromPath

;-------------------------------------------------------------------------------
; Name .........: GetDriveFromPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetDriveFromPath($szPath)
	Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
	_PathSplit($szPath, $szDrive, $szDir, $szFName, $szExt)
	Return $szDrive
EndFunc   ;==>GetDriveFromPath

;-------------------------------------------------------------------------------
; Name .........: GetParentDirectory
; Description ..:
; Return .......:
; History ......: JUL 7, 2010 - $bRemoveTrailingSlashes set to True
;-------------------------------------------------------------------------------
Func GetParentDirectory($szPath)
	Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
	_PathSplit($szPath, $szDrive, $szDir, $szFName, $szExt)
	Return SetError(0, 0, NormalizePath($szDrive & $szDir))
EndFunc   ;==>GetParentDirectory

;-------------------------------------------------------------------------------
; Name .........: DenormalizePath (REF.PS/40A4213F_1)
; Description ..: Replace UNC path by Mapped Drive
;                   ( eg. "\\storage0x\versions$\AutoTesting\" -> "T:\" )
;
;                 For instance, using "diff.exe", if you call
;                     cmd.exe /c diff.exe --text "\\storage0x\share\file1.txt"_
;                       "C:\local\file2.txt"
;                 you will get the follwing error message:
;					diff: \storage0x\share\file1.txt: No such file or directory
;                 this is due to "diff.exe" that replaces '\\storage0x\(...)'
;                 by '\storage0x\(...)'.
;
;                 An other example is "EasyDump.bat" that fails dumping an
;                 assima thesaurus if this one is described using UNC paths.
;
;                 Calling this method before invoking some command line
;                 executable might prevent those problems.
; Return .......: string containing denormalized path
; TODO..........: use flags ( see "NormalizePath" ) and allow to also restore
;                 local paths ( eg. if @ComputerName == "testserv--xp" then
;                 "\\testserv--xp\C$\" <=> "C:\" )
;-------------------------------------------------------------------------------
Func DenormalizePath($szPath_in)
	$szDrive = ""
	$szMapping = ""
	$drives = DriveGetDrive("all")
	If Not @error Then
		For $i = 1 To $drives[0]
			If DriveGetType($drives[$i]) = "Network" Then
				$szMapping_tmp = DriveMapGet($drives[$i])
				If StringStartsWith($szPath_in, $szMapping_tmp) Then
					If StringLen($szMapping_tmp) > StringLen($szMapping) Then
						$szDrive = $drives[$i]
						$szMapping = $szMapping_tmp
					EndIf
				EndIf
			EndIf
		Next
	EndIf
	If $szDrive <> "" Then
		Return StringReplace($szPath_in, $szMapping, $szDrive)
	Else
		Return $szPath_in
	EndIf
EndFunc   ;==>DenormalizePath

;-------------------------------------------------------------------------------
; Name .........: DriveIsLocal
; Description ..:
; Parameters ...: $drive - is the drive name (eg. "P:")
; Return .......: Boolean
;-------------------------------------------------------------------------------
Func DriveIsLocal($drive)
	$type = DriveGetType($drive)
	Return ($type = "FIXED") Or ($type = "Removable") Or ($type = "CDROM") Or ($type = "RAMDisk") _
			Or StringInStr(NormalizePath($drive), "\\" & @ComputerName & "\")
EndFunc   ;==>DriveIsLocal

;-------------------------------------------------------------------------------
; Name .........: NormalizePath
; Description ..: Get a more generic path
;                 (ie. "not relative" + UNC instead of mapped drives)
;                 + Remove trailing slashes and spaces
; Parameters ...: $bReplaceAllDrivesByUNC (FALSE BY DEFAULT)
;                     C:\ -> \\computer-name\C$
;                 $bReplaceMappedDrivesByUNC (TRUE BY DEFAULT)
;                     D:\ -> \\storage0x\versions$
;                 $rootDirForRelativePaths (WORKING DIRECTORY BY DEFAULT)
;                     ..\Platform.ini -> C:\AutoTestingV2\Suites\Platform.ini
; Return .......: String - Normalized path
;-------------------------------------------------------------------------------
Func NormalizePath($szPath, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir)

	$bReplaceRelativePathsByAbsolutePaths = 0
	If $rootDirForRelativePaths = True Or $rootDirForRelativePaths = Default Then
		$rootDirForRelativePaths = @WorkingDir
	EndIf
	If IsDirectory($rootDirForRelativePaths) Then
		$bReplaceRelativePathsByAbsolutePaths = 1
	EndIf

	; check if we're working on a network path
	; => then we'll have to put it back when finished normalising paths
	$bNetworkPath = StringStartsWith($szPath, "\\")

	; normalize path:
	;  - remove repeted separators,
	;  - 'here' indicators,
	;  - 'parent_directory' indicators.
	;
	; test-cases:
	;   "C:\dir1\subdir7\subdir6\subdir5\subdir4\subdir3\subdir2\subdir1\\../../../../.././..\dir2"
	;   ".\subdir5\subdir4\subdir3\subdir2\subdir1\\../../../../.././..\dir2"
	;
	$szPath_amended = $szPath
	$szPath_error = 0

	$szPath_amended = StringRegExpReplace($szPath_amended, "[\\/]+", "\\")
	Do
		$szPath_amended = StringReplace($szPath_amended, "\.\", "\")
	Until @extended = 0
	Do
		$szPath_amended = StringRegExpReplace($szPath_amended, "\\[^\\]+[^(\\\.\.)(\\\.)]\\\.\.\\", "\\")
	Until @extended = 0

	If StringInStr($szPath_amended, "\..\") Then $szPath_error = 1

	; special situation n�1: this is a network path
	; => restore the double backslash at the begining of the network path
	If $bNetworkPath Then
		$szPath_amended = "\" & $szPath_amended
	Else

		; special situation n�2.1: this is a relative path
		; => replace relative paths by absolute paths
		If $bReplaceRelativePathsByAbsolutePaths Then
			If StringStartsWith($szPath_amended, ".") Then
				If StringStartsWith($szPath_amended, ".\") Then
					$szPath_amended = $rootDirForRelativePaths & "\" & StringTrimLeft($szPath_amended, StringLen(".\"))
				ElseIf StringStartsWith($szPath_amended, "..\") Then
					$szPath_amended = GetParentDirectory($rootDirForRelativePaths) & StringTrimLeft($szPath_amended, StringLen("..\"))
				EndIf
			EndIf
		EndIf

		; is it a mapped drive?
		$szDrive = ""
		$szDir = ""
		$szFilename = ""
		$szExt = ""
		_PathSplit($szPath_amended, $szDrive, $szDir, $szFilename, $szExt)
		$szMappingTarget = DriveMapGet($szDrive)
		;;~ConsoleWrite('$szDrive = ' & $szDrive & @crlf)
		;;~ConsoleWrite('$szMappingTarget = ' & $szMappingTarget & @crlf)

		; special situation n�2.2: this is a mapped drive
		; => replace drive (or mapped drive) by computer name and drive number
		If $bReplaceMappedDrivesByUNC And IsString($szMappingTarget) And $szMappingTarget <> "" Then
			; replace mapped drive by UNC
			$szPath_amended = _PathMake($szMappingTarget, $szDir, $szFilename, $szExt)
		ElseIf $bReplaceAllDrivesByUNC And StringLen($szDrive) = 2 Then
			; replace normal drive by UNC
			$szDriveLetter = StringTrimRight($szDrive, 1)
			$szNetworkPathUNC = "\\" & @ComputerName & "\" & $szDriveLetter & "$"
			$szPath_amended = _PathMake($szNetworkPathUNC, $szDir, $szFilename, $szExt)
		EndIf
	EndIf

	; remove trailing spaces and/or backslashes
	While StringEndsWith($szPath_amended, "\") Or StringEndsWith($szPath_amended, " ")
		$szPath_amended = StringTrimRight($szPath_amended, 1)
	WEnd

	; return the amended path
	Return SetError($szPath_error, 0, $szPath_amended)

EndFunc   ;==>NormalizePath

#cs INLINE-TESTING
	$normalizedPath = NormalizePath('V:\ATS\Daily_builds_6_14\6.14.16.1983-20121004_102825-FINAL')
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $normalizedPath = ' & $normalizedPath & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	;#cs
	$szDrive = V:
	$szMappingTarget = \\storage0x\Versions$
	@ Debug(340) : $normalizedPath = \\storage0x\Versions$\ATS\Daily_builds_6_14\6.14.16.1983-20121004_102825-FINAL
#ce

;-------------------------------------------------------------------------------
; Name .........: FindNonExistingFileName
; Description ..: get an unused name for a file or a folder Some
; Parameters ...: '$pathIn' - the type of filename we are looking for
; Preconditions : variables '$pathIn' and '$pathPart' must be coherent
;                 (ie. 'StringInStr($pathIn,$pathPart)' must be true)
; Testing ......: see 'UnitTest.Level1.FindNonExistingFileName.au3'
; Return .......: string containing an unused file/directory name
;                 eg. return 'myFile(3).ext' if 'myFile.ext' and 'myFile(2).ext'
;                 are already in use
;-------------------------------------------------------------------------------
Func FindNonExistingFileName($pathIn, $pathPart = "", $isSuffix = True)
	; preconditions
	$preconditionsRespected = True
	If $pathPart <> "" Then
		If $isSuffix Then
			If Not StringEndsWith($pathIn, $pathPart) Then
				$preconditionsRespected = False
			EndIf
		Else
			If Not StringStartsWith($pathIn, $pathPart) Then
				$preconditionsRespected = False
			EndIf
		EndIf
	EndIf
	If Not $preconditionsRespected Then
		MsgBox(48, "Fatal Error", _
				"Preconditions not respected on 'GetNonExistingName' call ($pathIn='" & $pathIn & "', $pathPart='" & $pathPart & "', $isSuffix='" & $isSuffix & "')!")
	EndIf

	$pathPart1 = ""
	$pathPart2 = ""
	If $pathPart = "" Then
		$pathPart1 = $pathIn
		$pathPart2 = ""
	Else
		If $isSuffix Then
			$pathPart1 = StringTrimRight($pathIn, StringLen($pathPart))
			$pathPart2 = $pathPart
		Else
			$pathPart1 = $pathPart
			$pathPart2 = StringTrimLeft($pathIn, StringLen($pathPart))
		EndIf
	EndIf

	$pathOut = $pathIn
	$pathIndex = 1
	While FileExists($pathOut)
		$pathIndex += 1
		$pathOut = $pathPart1 & "(" & $pathIndex & ")" & $pathPart2
	WEnd
	Return $pathOut
EndFunc   ;==>FindNonExistingFileName

;-------------------------------------------------------------------------------
; Name .........: StringSearchIntersection
; Description ..:
; Return .......: '0' if prefix doesn't exist
;                 "" if the prefix equals the string (no suffix)
;                 "suffix_value"
;-------------------------------------------------------------------------------
Func StringSearchIntersection($szString1, $szString2)
	$szIntersect = ""
	Const $szDelim = "\/"
	$array1 = StringSplit($szString1, $szDelim)
	$array2 = StringSplit($szString2, $szDelim)
	$i_max = 0

	; get min value
	$val1 = UBound($array1) - 1
	$val2 = UBound($array2) - 1
	$i_min = $val2
	If $val1 < $val2 Then $i_min = $val1

	For $i = 1 To $i_min
		If $array1[$i] = $array2[$i] Then
			$i_max = $i
		EndIf
	Next

	For $i = 1 To $i_max
		$szIntersect &= $array1[$i]
		$szIntersect &= "\"
	Next

	Return $szIntersect
EndFunc   ;==>StringSearchIntersection

;-------------------------------------------------------------------------------
; Name .........: GatherDirectoryAndName
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GatherDirectoryAndName($szDirectory, $szName, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir)
	$szPath = ""
	If $szDirectory <> "" And $szName <> "" Then
		$szDirectory = NormalizePath($szDirectory, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths)
		$szPath = $szDirectory & $szName
	EndIf
	Return $szPath
EndFunc   ;==>GatherDirectoryAndName

;-------------------------------------------------------------------------------
; Name .........: _FileReadToArrayV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func _FileReadToArrayV2($file, ByRef $array)
	$ret = _FileReadToArray($file, $array)
	If $ret And ($array[$array[0]] = "") Then
		;and ($array[0]<>1)?
		_ArrayDelete($array, $array[0])
		$array[0] -= 1
	EndIf
	Return $ret
EndFunc   ;==>_FileReadToArrayV2

Func _FileWriteFromArrayV2($file, $a_Array, $i_Base = 0, $i_UBound = 0)
	; Check if we have a valid array as input
	If Not IsArray($a_Array) Then Return SetError(2, 0, 0)

	; determine last entry
	Local $last = UBound($a_Array) - 1
	If $i_UBound < 1 Or $i_UBound > $last Then $i_UBound = $last
	If $i_Base < 0 Or $i_Base > $last Then $i_Base = 0

	; Open output file for overwrite by default, or use input file handle if passed
	Local $hFile
	If IsString($file) Then
		$hFile = FileOpen($file, 2)
	Else
		$hFile = $file
	EndIf
	If $hFile = -1 Then Return SetError(1, 0, 0)

	; Write array data to file
	Local $ErrorSav = 0
	For $x = $i_Base To $i_UBound
		If FileWrite($hFile, $a_Array[$x] & @CRLF) = 0 Then
			$ErrorSav = 3
			ExitLoop
		EndIf
	Next

	; Close file only if specified by a string path
	If IsString($file) Then FileClose($hFile)

	; Return results
	If $ErrorSav Then
		Return SetError($ErrorSav, 0, 0)
	Else
		Return 1
	EndIf
EndFunc   ;==>_FileWriteFromArrayV2

;-------------------------------------------------------------------------------
; Name .........: FileIsAgeInDaysGreaterThan
; Description ..: Used for maintenance purpose
; Return .......: Boolean
;-------------------------------------------------------------------------------
Func FileIsAgeInDaysGreaterThan($path, $days_count)
	$age = _DateDiff('d', FileGetTimeLocal($path, 0, 2), _NowCalc())
	ConsoleWrite("Age of '" & $path & "' is " & $age & " days" & @CRLF)
	Return $age >= $days_count
EndFunc   ;==>FileIsAgeInDaysGreaterThan

;-------------------------------------------------------------------------------
; Name .........: FileOrDirIsAgeInDaysGreaterThan
; Description ..: Used for maintenance purpose
; Return .......: Boolean
;-------------------------------------------------------------------------------
Func FileOrDirIsAgeInDaysGreaterThan($path, $days_count)
	$path_sub = $path
	If IsDirectory($path_sub) And FileExists($path_sub & "\log.txt") Then
		$path_sub = $path_sub & "\log.txt"
	EndIf
	Return FileIsAgeInDaysGreaterThan($path_sub, $days_count)
EndFunc   ;==>FileOrDirIsAgeInDaysGreaterThan
