#include-once
#include ".\FileV2.au3"

;-------------------------------------------------------------------------------
; Name .........: ProcessFilesOnly
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
$_ProcessFilesOnly_CallbackIsEntryToSkip = ""
Func ProcessFilesOnly($szRepository, $isEntryToSkipCallback = "CallbackForProcessFilesOnly_DefaultCallback")
	$_ProcessFilesOnly_CallbackIsEntryToSkip = $isEntryToSkipCallback
	RecursivelyProcessContentOfDirectory($szRepository, "CallbackForProcessFilesOnly_IndirectionCallback")
EndFunc   ;==>ProcessFilesOnly

;-------------------------------------------------------------------------------
; Name .........: CallbackForProcessFilesOnly_DefaultCallback
; Description ..: Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func CallbackForProcessFilesOnly_DefaultCallback($entry, $entry_name, $entry_location)
	ConsoleWrite("File ........: " & $entry & @CRLF)
EndFunc   ;==>CallbackForProcessFilesOnly_DefaultCallback

;-------------------------------------------------------------------------------
; Name .........: CallbackForProcessFilesOnly_IndirectionCallback
; Description ..: Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func CallbackForProcessFilesOnly_IndirectionCallback($entry, $entry_name, $entry_location)
	If StringInStr(FileGetAttrib($entry), "D") Then
		Return False
	Else
		; Invoke callback-type "REF.PS/B757E73E"
		Call($_ProcessFilesOnly_CallbackIsEntryToSkip, $entry, $entry_name, $entry_location)
		Return True
	EndIf
EndFunc   ;==>CallbackForProcessFilesOnly_IndirectionCallback

;-------------------------------------------------------------------------------
; Name .........: ProcessEntries
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func ProcessEntries($szRepository, $isEntryToSkipCallback = "CallbackForProcessFiles_DefaultCallback")
	RecursivelyProcessContentOfDirectory($szRepository, $isEntryToSkipCallback)
EndFunc   ;==>ProcessEntries

;-------------------------------------------------------------------------------
; Name .........: ProcessEntriesRev
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func ProcessEntriesRev($szRepository, $isEntryToSkipCallback = "CallbackForProcessFiles_DefaultCallback")
	RecursivelyProcessContentOfDirectory($szRepository, $isEntryToSkipCallback, True)
EndFunc   ;==>ProcessEntriesRev

;-------------------------------------------------------------------------------
; Name .........: CallbackForProcessFiles_DefaultCallback
; Description ..: Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func CallbackForProcessFiles_DefaultCallback($entry, $entry_name, $entry_location)
	; don't skip directories!
	If StringInStr(FileGetAttrib($entry), "D") Then
		ConsoleWrite("Directory ...: " & $entry & @CRLF)
		Return False
	Else
		ConsoleWrite("File ........: " & $entry_name & @CRLF)
		Return True
	EndIf
EndFunc   ;==>CallbackForProcessFiles_DefaultCallback

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectoryV1
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectoryV1( _
		ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		)

	$bFillArrays = True
	$bSkipAtv2Dir = False
	Return __RecursivelyListOrProcessContentOfDirectory( _
			$bFillArrays _
			, $bSkipAtv2Dir _
			, $arrayDirectories _
			, $arrayFiles _
			, $arrayFolders _
			, $arrayFilenames _
			, $isEntryToSkipCallback _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
			)

EndFunc   ;==>RecursivelyListContentOfDirectoryV1

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectoryVx
; Description ..: Mid-level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
; History ......: AUG 30, 2016
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectoryVx( _
		Const $rootDirectory _
		, ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		)

	Dim $szDrive, $szDir, $szFName, $szExt
	_PathSplit($rootDirectory, $szDrive, $szDir, $szFName, $szExt)
	$arrayDirectories = _ArrayCreate(1, $rootDirectory)
	$arrayFilenames = _ArrayCreate(0)
	$arrayFolders = _ArrayCreate(1, $szFName & $szExt)
	$arrayFiles = _ArrayCreate(0)
	Return RecursivelyListContentOfDirectoryV1($arrayDirectories, $arrayFiles, $arrayFolders, $arrayFilenames, $isEntryToSkipCallback)

EndFunc   ;==>RecursivelyListContentOfDirectoryVx

;-------------------------------------------------------------------------------
; Name .........: RecursivelyProcessContentOfDirectory
; Description ..: High level. Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func RecursivelyProcessContentOfDirectory($szRepositoryPath, $isEntryToSkipCallback, $reverse = False)

	Dim $szDrive, $szDir, $szFName, $szExt
	_PathSplit($szRepositoryPath, $szDrive, $szDir, $szFName, $szExt)
	$arrayDirectories = _ArrayCreate(1, $szRepositoryPath)
	$arrayFilenames = _ArrayCreate(0)
	$arrayFolders = _ArrayCreate(1, $szFName & $szExt)
	$arrayFiles = _ArrayCreate(0)

	$bFillArrays = False
	$bSkipAtv2Dir = False
	Return __RecursivelyListOrProcessContentOfDirectory( _
			$bFillArrays _
			, $bSkipAtv2Dir _
			, $arrayDirectories _
			, $arrayFiles _
			, $arrayFolders _
			, $arrayFilenames _
			, $isEntryToSkipCallback _
			, False, False, False _
			, $reverse _
			)

EndFunc   ;==>RecursivelyProcessContentOfDirectory

;-------------------------------------------------------------------------------
; Name .........: __RecursivelyListOrProcessContentOfDirectory
; Description ..: Expects callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func __RecursivelyListOrProcessContentOfDirectory( _
		$bFillArrays _
		, $bSkipAtv2Dir _
		, ByRef $arrayDirectories _
		, ByRef $arrayFiles _
		, ByRef $arrayFolders _
		, ByRef $arrayFilenames _
		, $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback" _
		, $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = @WorkingDir _
		, $reverse = False _
		)

	$ret = True ; << default, skip file

	; directory to process
	$arrayDirectories[$arrayDirectories[0]] = NormalizePath($arrayDirectories[$arrayDirectories[0]] _
			, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths)
	$szDirectory = $arrayDirectories[$arrayDirectories[0]]
	$szDirectory_short = FileGetShortNameV2($szDirectory)

	; exception
	; if not FileExists($szDirectory_short) then FatalError("Directory '"&$szDirectory&"' does not exist.")

	; list content of the repository
	$arrayEntries = _FileListToArray($szDirectory_short)
	If @error Then Return False

	; check each entry wheter it is a file or a directory
	$iStart = 1
	$iEnd = $arrayEntries[0]
	$iStep = 1
	If $reverse Then
		$iStart = $arrayEntries[0]
		$iEnd = 1
		$iStep = -1
	EndIf
	For $i = $iStart To $iEnd Step $iStep
		$szEntryName = $arrayEntries[$i]
		$szEntryPath = $szDirectory & "\" & $szEntryName

		If $szEntryName = ".atv2" And $bSkipAtv2Dir Then ContinueLoop

		; WARNING: 'FileGetShortName' crashes on some files [REF.PS/20100715_164541]
		; -> Use 'FileGetShortNameV2' instead
		$szEntryShort = FileGetShortNameV2($szEntryPath)

		If Not FileExists($szEntryShort) Then
			; NOTE: this can happen if the path is too long (MS Windows limitation)
			; SUR-NOTE: this shouldn't happen anymore because we are using short name
			; hence replacing Warning with Fatal Error.
			MsgBox(48, "Fatal Error", "Invalid file or directory: '" & $szEntryShort & "'.")
			Exit 1
		ElseIf IsString($isEntryToSkipCallback) And $isEntryToSkipCallback <> "" Then
			$call_ret = Call($isEntryToSkipCallback, $szEntryPath, $szEntryName, $szDirectory) ; Invoke callback-type "REF.PS/B757E73E"
			If @error = 0xDEAD And @extended = 0xBEEF Then
				MsgBox(48, "Fatal Error", "Function '" & $isEntryToSkipCallback & "' does not exist.")
				Exit 1
			EndIf
			$ret = $call_ret
		EndIf

		If Not $ret Then

			; We are interested in this file
			; => Check if it's a directory and recurs

			$szAttrib = FileGetAttrib($szEntryShort)
			If Not StringInStr($szAttrib, "D") Then

				If $bFillArrays Then
					$arrayFilenames[0] += 1
					_ArrayAdd($arrayFilenames, $szEntryName)
					$arrayFiles[0] += 1
					_ArrayAdd($arrayFiles, $szEntryPath)
				EndIf

			Else

				If StringInStr($szAttrib, "S") Then WarningMsgWpr("Contains system files. (See " & $szEntryShort & ".)")
				If StringInStr($szAttrib, "T") Then WarningMsgWpr("Contains temporary files (See " & $szEntryShort & ".)")
				;if StringInStr($szAttrib,"H") then WarningMsgWpr("Contains hidden files (See " & $szEntryShort & ".)")
				If StringInStr($szAttrib, "0") Then WarningMsgWpr("Contains offline files (See " & $szEntryShort & ".)")
				If StringInStr($szAttrib, "C") Then WarningMsgWpr("Contains compressed files (See " & $szEntryShort & ".)")

				If $bFillArrays Then
					$arrayDirectories[0] += 1
					_ArrayAdd($arrayDirectories, $szEntryPath)
					$arrayFolders[0] += 1
					_ArrayAdd($arrayFolders, $szEntryName)
				Else
					$arrayDirectories[0] = $szEntryPath
				EndIf

				__RecursivelyListOrProcessContentOfDirectory( _
						$bFillArrays _
						, $bSkipAtv2Dir _
						, $arrayDirectories _
						, $arrayFiles _
						, $arrayFolders _
						, $arrayFilenames _
						, $isEntryToSkipCallback _
						, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths _
						)

			EndIf
		EndIf
	Next

	#cs
		; check indexes
		if $arrayFilenames[0] <> (UBound($arrayFilenames)-1) then
		FatalError("$arrayFilenames[0] <> (UBound($arrayFilenames)-1)")
		endif
		if $arrayFiles[0] <> (UBound($arrayFiles)-1) then
		FatalError("$arrayFilenames[0] <> (UBound($arrayFilenames)-1)")
		endif
		if $arrayFolders[0] <> (UBound($arrayFolders)-1) then
		FatalError("$arrayFolders[0] <> (UBound($arrayFolders)-1)")
		endif
		if $arrayDirectories[0] <> (UBound($arrayDirectories)-1) then
		FatalError("$arrayDirectories[0] <> (UBound($arrayDirectories)-1)")
		endif

		; check parallel data
		if $arrayFilenames[0] <> $arrayFiles[0] then
		FatalError("$arrayFilenames[0] <> $arrayFiles[0]")
		endif
		if $arrayFolders[0] <> $arrayDirectories[0] then
		FatalError("$arrayFolders[0] <> $arrayDirectories[0]")
		endif
	#ce

	Return True

EndFunc   ;==>__RecursivelyListOrProcessContentOfDirectory

Func WarningMsgWpr($text)
	;MsgBox(0, "Warning", $text, 1)
	ConsoleWrite("-WARNING: " & $text)
EndFunc   ;==>WarningMsgWpr
