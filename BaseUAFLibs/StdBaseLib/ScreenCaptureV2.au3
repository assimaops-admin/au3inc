#include-once
#include "..\UDFsx\_ScreenCapture_CaptureWndV2.au3"


; #FUNCTION# ====================================================================================================================
; Name...........: _ScreenCapture_CaptureV2
; Description ...: Captures a region of the screen
; Syntax.........: _ScreenCapture_Capture([$sFileName = ""[, $iLeft = 0[, $iTop = 0[, $iRight = -1[, $iBottom = -1[, $fCursor = True[, $iBitBltFlag = Default]]]]]]])
; Parameters ....: $sFileName   - Full path and extension of the image file
;                  $iLeft       - X coordinate of the upper left corner of the rectangle
;                  $iTop        - Y coordinate of the upper left corner of the rectangle
;                  $iRight      - X coordinate of the lower right corner of the rectangle.  If this is  -1,  the  current  screen
;                  +width will be used.
;                  $iBottom     - Y coordinate of the lower right corner of the rectangle.  If this is  -1,  the  current  screen
;                  +height will be used.
;                  $fCursor     - If True the cursor will be captured with the image
;                  $iBitBltFlag - Flag to pass to the _WinAPI_BitBlt operation (default it BitOR($SRCCOPY, $CAPTUREBLT))
; Return values .: See remarks
; Author ........: Paul Campbell (PaulIA)
; History .......: OCT 25, 2011 - Added $iBitBltFlag parameter. It uses $CAPTUREBLT by default in order to capture layered windows
; Remarks .......: If FileName is not blank this function will capture the screen and save it to file. If FileName is blank, this
;                  function will capture the screen and return a HBITMAP handle to the bitmap image.  In this case, after you are
;                  finished with the bitmap you must call _WinAPI_DeleteObject to delete the bitmap handle.
;+
;                  Requires GDI+: GDI+ requires a redistributable for applications  that
;                  run on the Microsoft Windows NT 4.0 SP6, Windows 2000, Windows 98, and Windows Me operating systems.
; Related .......: _WinAPI_DeleteObject, _ScreenCapture_SaveImage
; Link ..........:
; Example .......: Yes
; ===============================================================================================================================
Func _ScreenCapture_CaptureV2($sFileName = "", $iLeft = 0, $iTop = 0, $iRight = -1, $iBottom = -1, $fCursor = True, $iBitBltFlag = Default)

	If $iRight = -1 Then $iRight = _WinAPI_GetSystemMetrics($SM_CXSCREEN)
	If $iBottom = -1 Then $iBottom = _WinAPI_GetSystemMetrics($SM_CYSCREEN)
	If $iRight < $iLeft Then Return SetError(-1, 0, 0)
	If $iBottom < $iTop Then Return SetError(-2, 0, 0)
	If $iBitBltFlag = Default Then $iBitBltFlag = $__SCREENCAPTURECONSTANT_DEFAULT_BITBLT_FLAG

	Local $iW = ($iRight - $iLeft) + 1
	Local $iH = ($iBottom - $iTop) + 1
	Local $hWnd = _WinAPI_GetDesktopWindow()
	Local $hDDC = _WinAPI_GetDC($hWnd)
	Local $hCDC = _WinAPI_CreateCompatibleDC($hDDC)
	Local $hBMP = _WinAPI_CreateCompatibleBitmap($hDDC, $iW, $iH)
	_WinAPI_SelectObject($hCDC, $hBMP)
	_WinAPI_BitBlt($hCDC, 0, 0, $iW, $iH, $hDDC, $iLeft, $iTop, $iBitBltFlag)

	If $fCursor Then
		Local $aCursor = _WinAPI_GetCursorInfo()
		If $aCursor[1] Then
			Local $hIcon = _WinAPI_CopyIcon($aCursor[2])
			Local $aIcon = _WinAPI_GetIconInfo($hIcon)
			_WinAPI_DeleteObject($aIcon[4]) ; delete bitmap mask return by _WinAPI_GetIconInfo()
			_WinAPI_DrawIcon($hCDC, $aCursor[3] - $aIcon[2] - $iLeft, $aCursor[4] - $aIcon[3] - $iTop, $hIcon)
			_WinAPI_DestroyIcon($hIcon)
		EndIf
	EndIf

	_WinAPI_ReleaseDC($hWnd, $hDDC)
	_WinAPI_DeleteDC($hCDC)
	If $sFileName = "" Then Return $hBMP

	_ScreenCapture_SaveImage($sFileName, $hBMP)
	_WinAPI_DeleteObject($hBMP)
EndFunc   ;==>_ScreenCapture_CaptureV2
