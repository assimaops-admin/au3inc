; ----------------------------------------------------------------------------------------
; Module .........: CmdLine.au3
; Author .........: Patryk Szczepankiewicz [pszczepa at gmail dot com]
; Note ...........: Use either of those two formats '/Opt:value' or '--option=value'.
;                   (The second is preferred and will be used in most parts of my code)
; Note ...........: We call "fully qualified" the options listed as '--some-var=some-val'
;                   and "partially qualifed" an option that is just present as '--some-var'
; Note ...........: Option '--some-var' is equivalent to '--some-var=1'
; Notes ..........: Double-quotes are used to gather string values that contain spaces;
;                   therefore, '--some-var=value' is equivalent to '--some-var="value"'
; History ........: xxx xx, 2008 - Created
; ----------------------------------------------------------------------------------------
#include-once
#include <AuItCustLibs\StringV2.au3>

$WritableCmdLine = $CmdLine ; Lossless Add/Append-only
$TempCmdLineCopy = $CmdLine ; WARNING: Can be destroyed


;;; -----------------------------------------------------------------------------
;;; Try debugging some of this capabilities by calling this page with parameters
;;;    eg.   $> C:\AutoTestingV2\inc\StdLibXL\CmdLine.au3 --testopt=123
;;; -----------------------------------------------------------------------------
;~ $value = GetCmdLineOptn("--testopt")
;~ MsgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & '$value' & @lf & @lf & 'Return:' & @lf & $value)



;-------------------------------------------------------------------------------
; Name .........: HackWritableCmdLine
; Description ..: hack the command line and insert an additional command
; Parameters ...: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func HackWritableCmdLine($stringOpt)

	ReDim $TempCmdLineCopy[UBound($TempCmdLineCopy) + 1]
	$TempCmdLineCopy[0] += 1
	$TempCmdLineCopy[$TempCmdLineCopy[0]] = $stringOpt

	ReDim $WritableCmdLine[UBound($WritableCmdLine) + 1]
	$WritableCmdLine[0] += 1
	$WritableCmdLine[$WritableCmdLine[0]] = $stringOpt

	Return $WritableCmdLine
EndFunc   ;==>HackWritableCmdLine

;-------------------------------------------------------------------------------
; Name .........: CmdLineOriginalParamsCopy
; Description ..: copy the original parameters
; Parameters ...: n/a
; Return .......: empty string or formatted "<SPACE> PARAM1 <SPACE> PARAM2"
;-------------------------------------------------------------------------------
Func CmdLineOriginalParamsCopy()
	$NewCmdLine = ""
	; copy the original parameters
	For $i = 1 To $WritableCmdLine[0]
		$NewCmdLine &= " "
		If Not StringInStr($WritableCmdLine[$i], ' ') Then
			$NewCmdLine &= $WritableCmdLine[$i]
		Else
			$NewCmdLine &= '"' & $WritableCmdLine[$i] & '"'
		EndIf
	Next
	Return $NewCmdLine
EndFunc   ;==>CmdLineOriginalParamsCopy

;-------------------------------------------------------------------------------
; Name .........: PrepareCmdLineOptn (WARNING : STRING EXPECTED IN INPUT!)
; Description ..: if you pass "option-dummy-name" this function will return
;                   a string such as "--option-dummy-name="
;                 (Note: "/OptionDummyName:" would be an alternative but is not
;                  implemented - we will follow the UNIX convention instead)
; Parameters ...: option name and value - value is expected to be a string
; Return .......: formated string
;-------------------------------------------------------------------------------
Func PrepareCmdLineOptn($szOpt, $szValue = Default)
	; -------- delimiters --------
	$szOptPartiallyQualified = $szOpt
	If Not StringStartsWith($szOptPartiallyQualified, "--") Then $szOptPartiallyQualified = "--" & $szOptPartiallyQualified
	$szOptFullyQualified = $szOptPartiallyQualified
	If Not StringEndsWith($szOptFullyQualified, "=") Then $szOptFullyQualified = $szOptFullyQualified & "="
	If StringEndsWith($szOptPartiallyQualified, "=") Then $szOptPartiallyQualified = StringTrimRight($szOptPartiallyQualified, 1)
	; ----------------------------
	If (Not IsString($szValue)) Then
		Return $szOptPartiallyQualified
	ElseIf Not StringInStr($szValue, " ") Then
		Return $szOptFullyQualified & $szValue
	Else
		Return $szOptFullyQualified & Quote($szValue)
	EndIf
EndFunc   ;==>PrepareCmdLineOptn

;-------------------------------------------------------------------------------
; Name .........: GetCmdLineOptnIndex
; Description ..:
; Parameters ...:
; Return .......: return 0 if doesn't exist
;                 return the index '$i' to use with '$TempCmdLineCopy[$i]'
;-------------------------------------------------------------------------------
Func GetCmdLineOptnIndex($szOpt)
	$iOptionIndex = 0
	; -------- delimiters --------
	$szOptPartiallyQualified = $szOpt
	If Not StringStartsWith($szOptPartiallyQualified, "--") Then $szOptPartiallyQualified = "--" & $szOptPartiallyQualified
	$szOptFullyQualified = $szOptPartiallyQualified
	If Not StringEndsWith($szOptFullyQualified, "=") Then $szOptFullyQualified = $szOptFullyQualified & "="
	If StringEndsWith($szOptPartiallyQualified, "=") Then $szOptPartiallyQualified = StringTrimRight($szOptPartiallyQualified, 1)
	; ----------------------------
	; search index, if any
	For $i = 1 To $TempCmdLineCopy[0]
		If ($TempCmdLineCopy[$i] = $szOptPartiallyQualified) Or StringStartsWith($TempCmdLineCopy[$i], $szOptPartiallyQualified, 1) Then
			$iOptionIndex = $i
			ExitLoop
		EndIf
	Next
	Return $iOptionIndex
EndFunc   ;==>GetCmdLineOptnIndex

;-------------------------------------------------------------------------------
; Name .........: _GetCmdLineOptn_Sub
; Description ..: sub-call for 'GetCmdLineOptn' and 'ConsumeCmdLineOptn'
;                 -> see 'GetCmdLineOptn' for more info
;-------------------------------------------------------------------------------
Func _GetCmdLineOptn_Sub($szOpt, $bRemove = False)
	$szValue = 0
	; -------- delimiters --------
	$szOptPartiallyQualified = $szOpt
	If Not StringStartsWith($szOptPartiallyQualified, "--") Then $szOptPartiallyQualified = "--" & $szOptPartiallyQualified
	$szOptFullyQualified = $szOptPartiallyQualified
	If Not StringEndsWith($szOptFullyQualified, "=") Then $szOptFullyQualified = $szOptFullyQualified & "="
	If StringEndsWith($szOptPartiallyQualified, "=") Then $szOptPartiallyQualified = StringTrimRight($szOptPartiallyQualified, 1)
	; ----------------------------
	; search index, if any
	$iOption = GetCmdLineOptnIndex($szOpt)
	If $iOption Then
		If $TempCmdLineCopy[$iOption] = $szOptPartiallyQualified Then
			$szValue = "1"
		Else
			$szValue = StringTrimLeft($TempCmdLineCopy[$iOption], StringLen($szOptFullyQualified))
			$szValue = UnQuote($szValue)
		EndIf
	EndIf
	; delete this option from the list
	If $bRemove And IsString($szValue) Then
		_ArrayDelete($TempCmdLineCopy, $iOption)
		$TempCmdLineCopy[0] -= 1
	EndIf
	Return $szValue
EndFunc   ;==>_GetCmdLineOptn_Sub

;-------------------------------------------------------------------------------
; Name .........: ConsumeCmdLineOptn
; Description ..: similar to 'GetCmdLineOptn' but the option is removed from the
;                 command line.
;                 -> see 'GetCmdLineOptn' for more info
;-------------------------------------------------------------------------------
Func ConsumeCmdLineOptn($szOpt, $default = 0)
	$ret = _GetCmdLineOptn_Sub($szOpt, True)
	If IsInt($ret) And $ret = 0 Then
		Return $default
	ElseIf IsString($ret) Then
		Return $ret
	Else
		; FatalError_ContractViolation_ReturnValue("_GetCmdLineOptn_Sub")
		MsgBox(48, "Fatal Error", "Contract violation regarding value returned from function '_GetCmdLineOptn_Sub'.")
		Exit 1
	EndIf
EndFunc   ;==>ConsumeCmdLineOptn

;-------------------------------------------------------------------------------
; Name .........: GetCmdLineOptn (As string?! Well, almost...)
; Description ..: if you pass "option-dummy-name" this function will also search
;                   the option "--option-dummy-name="
;                 (Note: "/OptionDummyName:" would be an alternative but is not
;                  implemented - we will follow the UNIX convention instead)
; Parameters ...: $default - default value to return if entry is missing
;                              (integer '0' by default)
; Return .......: return integer '0' if not set (see function parameters),
;                 or empty string "" if empty; returns the value otherwise.
;-------------------------------------------------------------------------------
Func GetCmdLineOptn($szOpt, $default = 0)
	$ret = _GetCmdLineOptn_Sub($szOpt)
	If IsInt($ret) And $ret = 0 Then
		Return $default
	ElseIf IsString($ret) Then
		Return $ret
	Else
		; FatalError_ContractViolation_ReturnValue("_GetCmdLineOptn_Sub")
		MsgBox(48, "Fatal Error", "Contract violation regarding value returned from function '_GetCmdLineOptn_Sub'.")
		Exit 1
	EndIf
EndFunc   ;==>GetCmdLineOptn

;-------------------------------------------------------------------------------
; Name .........: GetCmdLineOptnAsInt
; Description ..: if you pass "option-dummy-name" this function will also search
;                   the option "--option-dummy-name="
;                 (Note: "/OptionDummyName:" would be an alternative but is not
;                  implemented - we will follow the UNIX convention instead)
; Parameters ...: $default - default value to return if entry is missing
;                              (empty string "" by default)
; Return .......: return empty string "" if not set (see function parameters),
;                 or integer '0' if empty; returns the value otherwise.
;-------------------------------------------------------------------------------
Func GetCmdLineOptnAsInt($szOpt, $default = "")
	$ret = GetCmdLineOptn($szOpt, "")
	If IsString($ret) And StringIsInt($ret) Then
		Return Int($ret)
	ElseIf $ret = "" Then
		Return $default
	Else
		; FatalError_ContractViolation_ReturnValue("GetCmdLineOptn")
		MsgBox(48, "Fatal Error", "Contract violation regarding value returned from function 'GetCmdLineOptn'.")
		Exit 1
	EndIf
EndFunc   ;==>GetCmdLineOptnAsInt
