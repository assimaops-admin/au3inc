#include-once
#include "..\UDFs\CsvEntry.Lite.au3"

Func CsvRegEntryAdd($key_name, $key_path, $new_csv_entry, $key_type = "REG_EXPAND_SZ", $separator = ";")

	$key_value = RegRead($key_path, $key_name)
	If $key_value = "" Then
		RegWrite($key_path, $key_name, $key_type, $new_csv_entry)
	Else
		If Not StringInStr($key_value, $new_csv_entry) Then
			RegWrite($key_path, $key_name, $key_type, $key_value & $separator & $new_csv_entry)
		EndIf
	EndIf

EndFunc   ;==>CsvRegEntryAdd

Func CsvRegEntryDel($key_name, $key_path, $old_csv_entry, $key_type = "REG_EXPAND_SZ", $separator = ";")

	$ret = 0

	Const $___REG_NONE = 0
	Const $___REG_SZ = 1
	Const $___REG_EXPAND_SZ = 2
	Const $___REG_BINARY = 3
	Const $___REG_DWORD = 4
	Const $___REG_DWORD_BIG_ENDIAN = 5
	Const $___REG_LINK = 6
	Const $___REG_MULTI_SZ = 7
	Const $___REG_RESOURCE_LIST = 8
	Const $___REG_FULL_RESOURCE_DESCRIPTOR = 9
	Const $___REG_RESOURCE_REQUIREMENTS_LIST = 10

	$key_value = RegRead($key_path, $key_name)
	$old_type = @extended
	If @error Then Return SetError(1, 0, $ret)

	; if the entry already exists - check same type
	If Eval("___" & $key_type) <> $old_type Then
		MsgBox(48, "Fatal error" _
				, 'Key "' & $key_name & '" exists and type is different (' & $old_type & ').')
		Return $ret
	EndIf

	$key_value_updated = CsvEntryDel($key_value, $old_csv_entry, $separator)
	If @extended Then
		$ret = RegWrite($key_path, $key_name, $key_type, $key_value_updated)
	EndIf

	Return $ret

EndFunc   ;==>CsvRegEntryDel

; ---------------------------
; TESTING
;CsvRegEntryDel("AutoTesting", "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", "blaah")
;CsvRegEntryDel("AutoTesting", "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", "blaah", "REG_SZ")
; ---------------------------
