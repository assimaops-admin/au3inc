#include-once
#include "..\BaseLib\CsvRegEntry.au3"
#include "..\UDFs\RegReadV2.Lite.au3"

Func EnvGetV2($name, $expand = 1)
	Return RegReadV2("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", $name, $expand)
EndFunc   ;==>EnvGetV2

Func EnvSetV2($name, $value, $key_type = "REG_EXPAND_SZ")
	If EnvGetV2($name) <> $value Then
		RegWrite("HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", $name, $key_type, $value)
	EndIf
EndFunc   ;==>EnvSetV2

Func PathRegEntryAdd($new_path_entry)
	CsvRegEntryAdd("PATH", "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", $new_path_entry, "REG_EXPAND_SZ", ";")
EndFunc   ;==>PathRegEntryAdd

Func PathRegEntryDel($old_path_entry)
	CsvRegEntryDel("PATH", "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment", $old_path_entry, "REG_EXPAND_SZ", ";")
EndFunc   ;==>PathRegEntryDel

Func IncRegEntryAdd($new_path_entry)
	CsvRegEntryAdd("Include", "HKEY_CURRENT_USER\Software\AutoIt v3\AutoIt", $new_path_entry, "REG_EXPAND_SZ", ";")
EndFunc   ;==>IncRegEntryAdd

Func IncRegEntryDel($old_path_entry)
	CsvRegEntryDel("Include", "HKEY_CURRENT_USER\Software\AutoIt v3\AutoIt", $old_path_entry, "REG_EXPAND_SZ", ";")
EndFunc   ;==>IncRegEntryDel
