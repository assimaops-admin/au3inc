#include-once
#include "..\UDFs\_ProcessGetChildren.au3"
#include "..\UDFs\FuncTimer.au3"

Const $DEFAULT_PROCESS_BOLD_TIMEOUT = 60 * 60
Const $DEFAULT_BOLD_WAIT_ON_PROCESS_CLOSE = 3 * 60

#region ProcessV2.RunAndReadStdxxx

;-------------------------------------------------------------------------------
; Name .........: RunAndReadStdout
; Description ..: wait for the process to exit
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunAndReadStdout($szCmd, $szWorkingDir = "", $flags = @SW_HIDE, $binary = False)
	Return RunAndReadStdxxx(0x2, $szCmd, $szWorkingDir, $flags, $binary)
EndFunc   ;==>RunAndReadStdout

;-------------------------------------------------------------------------------
; Name .........: RunAndReadStdxxx
; Description ..: wait for the process to exit
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunAndReadStderr($szCmd, $szWorkingDir = "", $flags = @SW_HIDE, $binary = False)
	Return RunAndReadStdxxx(0x4, $szCmd, $szWorkingDir, $flags, $binary)
EndFunc   ;==>RunAndReadStderr

;-------------------------------------------------------------------------------
; Name .........: RunAndReadStdxxx
; Description ..: wait for the process to exit
; Parameters ...: $opt_flag
;                    - 0x2 - $STDOUT_CHILD
;                    - 0x4 - $STDERR_CHILD
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunAndReadStdxxx($opt_flag, $szCmd, $szWorkingDir = "", $flags = @SW_HIDE, $binary = False)
	$szResult = ""
	$child_pid = Run($szCmd, $szWorkingDir, $flags, $opt_flag)
	$child_err = @error
	ConsoleWrite("$child_pid = " & $child_pid & @CRLF)
	If $child_err Then
		MsgBox(48, "Error", "Failed run command '" & $szCmd & "'.")
		Return $szResult
	EndIf
	While ProcessExists($child_pid)
		$peek = False
		$szResult_line = ""
		Switch $opt_flag
			Case 0x2
				$szResult_line = StdoutRead($child_pid, $peek, $binary)
				$child_err = @error
			Case 0x4
				$szResult_line = StderrRead($child_pid, $peek, $binary)
				$child_err = @error
		EndSwitch
		;ConsoleWrite("|" & $szResult_line)
		If $child_err Then ExitLoop
		$szResult &= $szResult_line
	WEnd
	Return $szResult
EndFunc   ;==>RunAndReadStdxxx

#endregion ProcessV2.RunAndReadStdxxx

;-------------------------------------------------------------------------------
; Name .........: RunAndReadBinaryStdout
; Description ..: wait for the process to exit
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunAndReadBinaryStdout($szCmd, $szWorkingDir = "", $flags = @SW_HIDE)
	Return RunAndReadStdout($szCmd, $szWorkingDir, $flags, True)
EndFunc   ;==>RunAndReadBinaryStdout

;-------------------------------------------------------------------------------
; Name .........: RunAndFwdStdout
; Description ..: Same as RunAndReadStdout but with a ConsoleWrite
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunAndFwdStdout($szCmd, $szWorkingDir = "", $flags = @SW_HIDE) ; << WARNING: CODE IN SYNC
	$szResult = ""
	$child_pid = Run($szCmd, $szWorkingDir, $flags, 0x2) ; 0x2 - $STDOUT_CHILD
	While 1
		$szResult_line = StdoutRead($child_pid)
		If @error Then ExitLoop
		$szResult &= $szResult_line
		ConsoleWrite($szResult_line)
	WEnd
	Return $szResult
EndFunc   ;==>RunAndFwdStdout

;-------------------------------------------------------------------------------
; Name .........: RunAndCheckFileExists
; Description ..:
; Parameters ...:
; Return .......:
; History ......: AUG 10, 2010 - Created (Problem with .idb file in APS)
;-------------------------------------------------------------------------------
Func RunAndCheckFileExists($command_line, $expected_file_out_shortname, $expected_file_out_longname)
	ConsoleWrite("[" & $command_line & "]..." & @CRLF)
	$stdout_read = RunAndReadStdout($command_line)
	If Not FileExists($expected_file_out_shortname) Then
		MsgBox(48, "Fatal error", "Missing '" & $expected_file_out_shortname & "' file." & @CRLF & "--" & @CRLF & $stdout_read)
		Exit 1
	EndIf
	Return $expected_file_out_longname
EndFunc   ;==>RunAndCheckFileExists

;-------------------------------------------------------------------------------
; Name .........: ProcessWaitCloseSlowlyForever
; Description ..:
; Parameters ...: wait for a process to close
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ProcessWaitCloseSlowlyForever($process, $poll_delay_in_sec = 2500)
	While ProcessExists($process)
		Sleep($poll_delay_in_sec)
	WEnd
EndFunc   ;==>ProcessWaitCloseSlowlyForever

;-------------------------------------------------------------------------------
; Name .........: ProcessCloseAll
; Description ..: Close all the processes with a given name
; Parameters ...: $processName - string - the name of the process to close
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ProcessCloseAll($processName, $timeout = 0)
	$ret = 1
	$pid = ProcessExists($processName)
	While $pid
		ProcessClose($pid)
		$ret = ProcessWaitClose($pid, $timeout)
		$pid = ProcessExists($processName)
	WEnd
	Return $ret
EndFunc   ;==>ProcessCloseAll

;-------------------------------------------------------------------------------
; Name .........: ProcessGetFirstChildOfType
; Description ..:
; Parameters ...: $processID - integer
;                 $type - name of the process to search
;                         (eg. 'cmd.exe' or  'AutoIt3.exe')
; Return .......: Process ID of the first child process
;-------------------------------------------------------------------------------
Func ProcessGetFirstChildOfType($pid_in, $type)
	$pid_out = ""
	$list = _ProcessGetChildren($pid_in)
	If IsArray($list) Then
		For $i = 1 To $list[0][0]
			If StringInStr($list[$i][1], $type) Then
				$pid_out = $list[$i][0]
				ExitLoop
			EndIf
		Next
	EndIf
	Return $pid_out
EndFunc   ;==>ProcessGetFirstChildOfType

;-------------------------------------------------------------------------------
; Name .........: ProcessWaitForChildOfType
; Description ..:
; Parameters ...: $processID - integer
;                 $type - name of the process to search
;                         (eg. 'cmd.exe' or  'AutoIt3.exe')
;                 $timeout - timeout in seconds
; Return .......: Process ID of the first child process
;                 - or - Empty string if timer elapsed
; Testing ......:
;
;    $index = $CmdLine[$CmdLine[0]] + 1
;    $ChildPid = 0
;    if $index < 4 then $ChildPid = Run(@AutoItExe&" "&@ScriptFullPath&" "&$index)
;    $SubChildPid = ProcessWaitForChildOfType($ChildPid, "AutoIt3.exe", 1)
;    $msg = ""
;    $msg &= "@AutoItPID = "&@AutoItPID&@CRLF
;    $msg &= "$ChildPid = "&$ChildPid&@CRLF
;    $msg &= "$SubChildPid = "&$SubChildPid&@CRLF
;    MsgBox(0, $index, $msg)
;
;
;    # include <LooseUDFs\AutoItCmdLineExport.au3>
;    AutoItCmdLineExport()
;    $index = $CmdLine[$CmdLine[0]] + 1
;    $ChildPid = 0
;    if $index < 4 then $ChildPid = Run(@AutoItExe&" "&@ScriptFullPath&" "&$index)
;    $SubChildPid = ProcessWaitForChildOfType($ChildPid, "AutoIt3.exe", 5)
;    ; Sleep(1000) ; 300ms is the threashold - 600ms seems safe - 1s to be sure [REF.PS/20101223_143945]
;    $msg = ""
;    $msg &= "@AutoItPID = "&@AutoItPID &@CRLF&@CRLF &" ("&AutoItCmdLineRead(@AutoItPID)&")"&@CRLF
;    $msg &= @CRLF
;    $msg &= "$ChildPid = "&$ChildPid &@CRLF&@CRLF &" ("&AutoItCmdLineRead($ChildPid)&")"&@CRLF
;    $msg &= @CRLF
;    $msg &= "$SubChildPid = "&$SubChildPid &@CRLF&@CRLF &" ("&AutoItCmdLineRead($SubChildPid)&")"&@CRLF
;    MsgBox(0, $index, $msg)
;
;-------------------------------------------------------------------------------
Func ProcessWaitForChildOfType($pid_in, $type, $timeout = Default)
	; TODO: Use SetError() and FuncTimerGetTimeInMs() + document (see ControlWait function)
	Local Const $FUNCT_TIMER_CYCLE_DURATION = 10 ; << to adjust depending on context, or make it a parameter
	;_DebugOut("ProcessWaitForChildOfType('"&$type&"')...")
	$timer = FuncTimerCreate($timeout)
	$pid_out = ProcessGetFirstChildOfType($pid_in, $type)
	While $pid_out = ""
		Sleep($FUNCT_TIMER_CYCLE_DURATION)
		If FuncTimerIsElapsed($timer) Then ExitLoop
		$pid_out = ProcessGetFirstChildOfType($pid_in, $type)
		;_DebugOut("$pid_out = "&$pid_out)
	WEnd
	Return $pid_out
EndFunc   ;==>ProcessWaitForChildOfType

;-------------------------------------------------------------------------------
; Name .........: ProcessCloseV2
; Description ..:
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;                 JUL 7, 2011 - Moved to ProcessV2.au3 file + renamed
;-------------------------------------------------------------------------------
Func ProcessCloseV2($processName, $timeout = 0)
	$ret = 1
	If ProcessExists($processName) Then
		ProcessClose($processName)
		$ret = ProcessWaitClose($processName, $timeout)
	EndIf
	Return $ret
EndFunc   ;==>ProcessCloseV2

;-------------------------------------------------------------------------------
; Name .........: RunWithBoldTimeout
; Parameters ...:
;
;                 $timeout = $DEFAULT_PROCESS_BOLD_TIMEOUT ( 1 HOUR )
;                 $isWaitCloseOnTimeout = True
;                 $waitCloseOnTimeoutDuratn = $DEFAULT_BOLD_WAIT_ON_PROCESS_CLOSE ( 3 MINUTES )
;
; Testing ......:
;
;                 RunWithBoldTimeout("notepad.exe", "", @SW_SHOW, 3)
;
; Description ..: wait for the process to exit
; Return .......: return string
;-------------------------------------------------------------------------------
Func RunWithBoldTimeout($szCmd, $szWorkingDir = "", $flags = @SW_HIDE, $timeout = $DEFAULT_PROCESS_BOLD_TIMEOUT, $isWaitCloseOnTimeout = True, $waitCloseOnTimeoutDuratn = $DEFAULT_BOLD_WAIT_ON_PROCESS_CLOSE)

	Local Const $FUNCT_TIMER_CYCLE__SECONDS__DURATION = 1 ; << to adjust depending on context, or make it a parameter
	$timer = FuncTimerCreate($timeout)

	$pid_out = Run($szCmd, $szWorkingDir, $flags)

	While $pid_out <> ""
		If ProcessWaitClose($pid_out, $FUNCT_TIMER_CYCLE__SECONDS__DURATION) Then
			$pid_out = ""
		Else
			If FuncTimerIsElapsed($timer) Then ExitLoop
		EndIf
	WEnd

	If $pid_out <> "" Then
		ProcessClose($pid_out)
		If $isWaitCloseOnTimeout Then
			ProcessWaitClose($pid_out, $waitCloseOnTimeoutDuratn)
		EndIf
	EndIf

	Return $pid_out
EndFunc   ;==>RunWithBoldTimeout
