#include-once
#include <AuItCustLibs\UDFs\AutoItEx.Globals.Accessors.au3>
; ; If IsSet("TRACE_WRITE") ~~> IsSet("ASSERT_EVAL")
; ; $dbgOn = EnvGet("UAF_DEBUG")
; $trceOn = EnvGet("UAF_TRACE")

Func EasyTraceWrite($text)
	If IsSet("TRACE_WRITE") Then ConsoleWrite(Eval("TRACE_WRITE_INDENT") & $text)
EndFunc   ;==>EasyTraceWrite

Func EasyTraceWriteLine($text)
	Return EasyTraceWrite($text & @CRLF)
EndFunc   ;==>EasyTraceWriteLine

Func EasyTraceIndent()
	Set_("TRACE_WRITE_INDENT", Eval("TRACE_WRITE_INDENT") & @TAB)
EndFunc   ;==>EasyTraceIndent

Func EasyTraceUnindent()
	Set_("TRACE_WRITE_INDENT", StringTrimRight(Eval("TRACE_WRITE_INDENT"), 1))
EndFunc   ;==>EasyTraceUnindent

Func EasyTraceInfoLine($text)
	ConsoleWrite("+ INFO: " & $text & " +" & @CRLF)
EndFunc   ;==>EasyTraceInfoLine

Func EasyTraceWarningLine($text)
	ConsoleWrite("- WARNING: " & $text & " -" & @CRLF)
EndFunc   ;==>EasyTraceWarningLine

Func EasyTraceErrorLine($text)
	ConsoleWrite("! ERROR: " & $text & " !" & @CRLF)
EndFunc   ;==>EasyTraceErrorLine
