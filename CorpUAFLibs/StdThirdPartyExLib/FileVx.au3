#include-once
;-;# include <BaseUAFLibs\Trce.au3>
#include ".\FileSystemEntry.au3"

;-------------------------------------------------------------------------------
; Name .........: GetModuleName
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetModuleName()
	Return GetNameFromPath(@ScriptFullPath)
EndFunc   ;==>GetModuleName

;-------------------------------------------------------------------------------
; Name .........: FileDeleteV3
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileDeleteV3($szPath, $isShortName = 0)
	$res = 1
	;-;EasyTraceWriteLine("DEL FILE '" & $szPath & "'...")
	$szPathShort = $szPath
	If Not $isShortName Then $szPathShort = FileGetShortNameV2($szPath)
	If StringInStr(FileGetAttrib($szPathShort), "R") Then $res = FileSetAttrib($szPathShort, "-R")
	If Not $res Then
		;-;EasyTraceWarningLine("FAILED REMOVE READ ATTRIBUTE FROM '" & $szPathShort & "'!")
	EndIf
	If Not IsSet("DRY_RUN") Then $res = FileDeleteV2($szPath, $isShortName)
	If Not $res Then
		;-;EasyTraceInfoLine("RETRY DEL FILE '" & $szPath & "'.")
		; Eventually retry
		Assert(Not IsSet("DRY_RUN"), "DRY_RUN GLOBAL VARIABLE SHOULD NOT BE SET AT THIS STAGE.")
		Sleep(10)
		If FileExists($szPathShort) Then $res = FileDeleteV2($szPath, $isShortName)
		; Failed again?
		If Not $res Then
			;-;EasyTraceErrorLine("DEL FILE '" & $szPath & "' FAILED.")
		EndIf
	EndIf
	Return $res
EndFunc   ;==>FileDeleteV3

;-------------------------------------------------------------------------------
; Name .........: DirRemoveV3
; Description ..: Recursively, use short names
; Return .......: 1 if succeeded
;-------------------------------------------------------------------------------
Func DirRemoveV3($pathToDelete, $isShortName = 0)

	$res = 1
	$shortPathToDelete = $pathToDelete
	If Not $isShortName Then $shortPathToDelete = FileGetShortNameV2($pathToDelete)

	; Is it really a directory?
	If Not StringInStr(FileGetAttrib($shortPathToDelete), "D") Then
		;-;EasyTraceWarningLine("'" & $shortPathToDelete & "' IS NOT A DIRECTORY. SKIP.")
		Return 0
	EndIf

	;-;EasyTraceWriteLine("LISTING '" & $shortPathToDelete & "'...")
	;-;EasyTraceIndent()
	$files = _FileListToArray($shortPathToDelete)
	If Not @error Then
		For $i = 1 To $files[0]
			$fileName = $files[$i]
			$filePath = $shortPathToDelete & "\" & $fileName
			$fileShortPath = FileGetShortNameV2($shortPathToDelete & "\" & $fileName)
			If StringInStr(FileGetAttrib($fileShortPath), "D") Then
				$res = DirRemoveV3($fileShortPath, 1) ; Only recursion
				If Not $res Then ExitLoop
			Else
				$res = FileDeleteV3($fileShortPath, 1) ; The actual delete
				If Not $res Then ExitLoop
			EndIf
		Next
	EndIf
	;-;EasyTraceUnindent()

	If $res Then
		; Wait for directory to be really empty
		_FileListToArray($shortPathToDelete)
		$totalWait = 0
		While Not @error
			Sleep(10)
			$totalWait += 10
			If $totalWait > 2000 Then ExitLoop
			_FileListToArray($shortPathToDelete)
		WEnd
		; Try to delete the directoy (that should be empty by now)
		;-;EasyTraceWriteLine("DEL DIR '" & $shortPathToDelete & "'...")
		If Not IsSet("DRY_RUN") Then $res = DirRemove($shortPathToDelete) ; The actual delete
		If Not $res Then
			;-;EasyTraceErrorLine("DEL DIR '" & $fileShortPath & "' FAILED.")
		EndIf
	EndIf

	Return $res

EndFunc   ;==>DirRemoveV3

;-------------------------------------------------------------------------------
; Name .........: FileBackupOnSite
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileBackupOnSite($szFilePath)
	If Not FileExists($szFilePath) Then
		Warning("File '" & $szFilePath & "' does not exist.")
		Return False
	EndIf
	$szBackupPath = $szFilePath & ".back." & MyYEAR() & MyMON() & "-" & MyMDAY() & "-" & MyHOUR() & "-" & MyMIN() & MySEC()
	If FileExists($szBackupPath) Then FatalError("Backup '" & $szBackupPath & "' already exists.")
	$ret = FSEMove($szFilePath, $szBackupPath)
	Return $ret
EndFunc   ;==>FileBackupOnSite
