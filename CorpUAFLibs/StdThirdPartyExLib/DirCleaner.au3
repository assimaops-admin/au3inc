#include-once
#include ".\FileSystemEntry.au3"

#cs
	; MANUAL TESTING

	# include <AuItCustLibs\UDFs\AutoItEx.Pause.au3>
	DirCleaner_Test01()
	Func DirCleaner_Test01()
	FileWrite("C:\tmp\blah_old.txt", "blah")
	FileSetTime("C:\tmp\blah_old.txt", "20111012" & "010000")
	Pause()
	DirCleaner_DeleteEntriesIfAgeInDaysGreaterThan("C:\tmp", 1, "*", 1)
	DirCleaner_DeleteEntriesIfAgeInDaysGreaterThan("C:\tmp", 0)
	EndFunc   ;==>DirCleaner_Test01
#ce

Func DirCleaner_CheckStrParams($containing_dir, $history_in_days, $entry_pattern, $entry_type_flag)
	If Not StringIsInt($history_in_days) Then Return False
	If $history_in_days < 0 Then Return False
	If Not StringIsInt($entry_type_flag) Then Return False
	If $entry_type_flag < 0 Then Return False
	If $entry_type_flag > 2 Then Return False
	Return True
EndFunc   ;==>DirCleaner_CheckStrParams

Func DirCleaner_DeleteEntriesIfAgeInDaysGreaterThan($containing_dir, $history_in_days = 10, $entry_pattern = "*", $entry_type_flag = 0)
	_Cui_Write("Searching for entries '" & $entry_pattern & "' in '" & $containing_dir & "'." & @CRLF)
	$entries = _FileListToArray($containing_dir, $entry_pattern, $entry_type_flag)
	If @error Then
		_Cui_Write("No entries found." & @CRLF)
	Else
		For $entryIndex = 1 To $entries[0]

			If $entries[$entryIndex] = "ZzZzZ" Then ContinueLoop

			$path = $containing_dir & "\" & $entries[$entryIndex]
			_Cui_Write("Checking '" & $path & "'..." & @CRLF)
			If FileOrDirIsAgeInDaysGreaterThan($path, $history_in_days) Then

				$delRet = 0
				_Cui_Write("- Deleting '" & $path & "'...")
				$delRet = FSEDelete($path)

				$delRetTxt = "FAILED"
				If $delRet Then $delRetTxt = "DONE"
				_Cui_Write(" " & $delRetTxt & "." & @CRLF)

			EndIf
		Next
	EndIf

EndFunc   ;==>DirCleaner_DeleteEntriesIfAgeInDaysGreaterThan
