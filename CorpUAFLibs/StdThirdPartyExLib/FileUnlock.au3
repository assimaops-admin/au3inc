; WARNING: This module requires unlocker to be part of the platform,
; installed on the machine or packaged within the executable.
#include-once
#include <AuItCustLibs\AutoItEx.au3>
#include <BaseUAFLibs\UDFsx\FileGetShortNameV2.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>

Const $UNLOCKER_PATH = FileUnlockSetUp()

;-------------------------------------------------------------------------------
; Name .........: FileUnlockSetUp
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileUnlockSetUp()
	$retPath = ""
	; 1
	$tryPath = "C:\AutoTestingV2\bin\unlocker.stt\Unlocker.exe"
	If ($retPath = "") And FileExists($tryPath) Then $retPath = $tryPath
	; 2
	$tryPath = @ProgramFilesDir & "\Unlocker\Unlocker.exe"
	If ($retPath = "") And FileExists($tryPath) Then $retPath = $tryPath
	; 2
	$tryPath = @ProgramFilesDir & " (x86)\Unlocker\Unlocker.exe"
	If ($retPath = "") And FileExists($tryPath) Then $retPath = $tryPath
	; 3
	$tryPath = "C:\Progra~n\AutoIt3\Extras\AutoTesting.Tools\Unlocker\Unlocker.exe"
	If ($retPath = "") And FileExists($tryPath) Then $retPath = $tryPath
	; n
	If ($retPath = "") And (Not FileExists(@TempDir & "\ZzZzZ\Unlocker.exe")) Then
		DirCreate(@TempDir & "\ZzZzZ")
		FileInstall("C:\AutoTestingV2\bin\unlocker.stt\Unlocker.exe", @TempDir & "\ZzZzZ\Unlocker.exe")
	EndIf
	If ($retPath = "") And FileExists(@TempDir & "\ZzZzZ\Unlocker.exe") Then $retPath = @TempDir & "\ZzZzZ\Unlocker.exe"
	Return FileGetShortNameV2($retPath)
EndFunc   ;==>FileUnlockSetUp

;-------------------------------------------------------------------------------
; Name .........: FileUnlock
; Description ..:
; Return .......:
; Testing ......:
;
;               FileUnlock("C:\temp", 1)  -- FAILS!
;
;-------------------------------------------------------------------------------
Func FileUnlock($szPath, $delete = 0)
	FuncTrace("FileUnlock", $szPath)
	ConsoleWrite('$UNLOCKER_PATH = ' & $UNLOCKER_PATH & @CRLF)
	$sCommand = $UNLOCKER_PATH & " " & Quote($szPath) & " /S" & QuestionMark($delete, " /D", "")
	$exit_code = RunWait($sCommand, "", @SW_HIDE)
	; exit value is opposite to return value
	Return (Not $exit_code)
EndFunc   ;==>FileUnlock

;-------------------------------------------------------------------------------
; Name .........: FileUnlockV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileUnlockV2($szPath, $isShortName = 0)
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	; exit value is opposite to return value
	Return FileUnlock($szPath_short)
EndFunc   ;==>FileUnlockV2
