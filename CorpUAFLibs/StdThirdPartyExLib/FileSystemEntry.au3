#include-once
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include <CoreUAFLibs\StdCoreLib\DirMoveV3.au3>
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>
#include ".\FileUnlock.au3"

;-------------------------------------------------------------------------------
; Name .........: FSEDeleteIfAny
; Description ..: See FSEDelete
; Description ..: Delete File System Entry (file or directory)
; Return .......: Bool - TRUE if succeeded
;-------------------------------------------------------------------------------
Func FSEDeleteIfAny($szPath, $isShortName = 0)
	FuncTrace("FSEDeleteIfAny", $szPath, $isShortName)
	$ret = True
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	If FileExists($szPath_short) Then
		$ret = FSEDelete($szPath_short, 1)
	EndIf
	Return $ret
EndFunc   ;==>FSEDeleteIfAny

;-------------------------------------------------------------------------------
; Name .........: FSEDelete
; Description ..: 1/ whether file or directory, 2/ unlock if necessary
;                 3/ use shortnames
; Description ..: Delete File System Entry (file or directory)
; Return .......: Bool - TRUE if succeeded
;-------------------------------------------------------------------------------
Func FSEDelete($szPath, $isShortName = 0)
	FuncTrace("FSEDelete", $szPath, $isShortName)
	$szPath_short = $szPath
	If Not $isShortName Then $szPath_short = FileGetShortNameV2($szPath)
	$ret = False
	If Not FileExists($szPath_short) Then
		Warning("File system entry '" & $szPath & "' not found!" & @CRLF)
	Else
		If IsDirectory($szPath_short, 1) Then
			$ret = DirRemoveV2($szPath_short, 1)
			If Not $ret Then
				FileUnlockV2($szPath_short, 1) ; try unlocking
				$ret = DirRemoveV2($szPath_short, 1)
				If Not $ret Then
					FSESubMoveForDelete($szPath_short) ; try renaming
					$ret = DirRemoveV2($szPath_short, 1)
					If Not $ret Then
						Warning("Directory '" & $szPath & "' could not be deleted (even after locks removal).")
					EndIf
				EndIf
			EndIf
		Else
			$ret = FileDeleteV2($szPath_short, 1)
			If Not $ret Then
				FileUnlockV2($szPath_short, 1)
				$ret = FileDeleteV2($szPath_short, 1)
				If Not $ret Then
					Warning("File '" & $szPath & "' could not be deleted (even after locks removal).")
				EndIf
			EndIf
		EndIf
	EndIf
	Return $ret
EndFunc   ;==>FSEDelete

;-------------------------------------------------------------------------------
; Name .........: FSEMove
; Description ..: 1/ whether file or directory, 2/ unlock if necessary
;                 3/ use shortnames
; Return .......: Bool - TRUE if succeeded
;-------------------------------------------------------------------------------
Func FSEMove($szPathSrc, $szPathDst, $isShortNames = 0)
	FuncTrace("FSEMove", $szPathSrc, $szPathDst, $isShortNames)
	$szPathSrc_short = $szPathSrc
	$szPathDst_short = $szPathDst
	If Not $isShortNames Then
		$szPathSrc_short = FileGetShortNameV2($szPathSrc)
		$szPathDst_short = FileGetShortNameV2($szPathDst)
	EndIf
	$ret = False
	If Not FileExists($szPathSrc_short) Then
		Warning("File system entry '" & $szPathSrc & "' not found!" & @CRLF)
	Else
		If IsDirectory($szPathSrc_short, 1) Then
			$ret = DirMoveV3($szPathSrc_short, $szPathDst_short, 1)
			If Not $ret Then
				FileUnlockV2($szPathSrc_short, 1)
				$ret = DirMoveV3($szPathSrc_short, $szPathDst_short, 1)
				If Not $ret Then
					Warning("Directory '" & $szPathSrc & "' could not be moved (even after locks removal).")
				EndIf
			EndIf
		Else
			$ret = FileMove($szPathSrc_short, $szPathDst_short, 1)
			If Not $ret Then
				FileUnlockV2($szPathSrc_short, 1)
				$ret = FileMove($szPathSrc_short, $szPathDst_short, 1)
				If Not $ret Then
					Warning("File '" & $szPathSrc & "' could not be moved (even after locks removal).")
				EndIf
			EndIf
		EndIf
	EndIf
	Return $ret
EndFunc   ;==>FSEMove

;-------------------------------------------------------------------------------
; Name .........: FSESubMoveForDelete
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func FSESubMoveForDelete($szPath_short)

	$ret = False

	$szSubdirOld = $szPath_short & "\Automation"
	$szSubdirNew = $szPath_short & "\A"
	If FileExists($szSubdirOld) Then
		If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

		$szSubdirOld = $szPath_short & "\A\Workspace"
		$szSubdirNew = $szPath_short & "\A\W"
		If FileExists($szSubdirOld) Then
			If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

			$szSubdirOld = $szPath_short & "\A\W\PerformanceData"
			$szSubdirNew = $szPath_short & "\A\W\D"
			If FileExists($szSubdirOld) Then
				If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

				$szSubdirOld = $szPath_short & "\A\W\D\delete synopsis"
				$szSubdirNew = $szPath_short & "\A\W\D\P"
				If FileExists($szSubdirOld) Then
					If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

					$szSubdirOld = $szPath_short & "\A\W\D\P\Backup"
					$szSubdirNew = $szPath_short & "\A\W\D\P\B"
					If FileExists($szSubdirOld) Then
						If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

						;$list = _FileListToArray($szSubdirNew, "*",2)
						;If Not @error Then
						;	For $i = 1 To $list[0]
						;		$szSubdirOld2 = $szSubdirNew & "\" & $list[$i]
						;		$szSubdirNew2 = $szSubdirNew & "\" & StringFormat("%03d", $i)
						;		If FileExists($szSubdirOld2) Then
						;			If Not DirMove($szSubdirOld2, $szSubdirNew2, 1) Then Return $ret
						;		EndIf
						;	Next
						;EndIf

						; Add other subdirectories here
						; (...)
					EndIf
				EndIf
			EndIf

			$szSubdirOld = $szPath_short & "\A\W\TrainingData"
			$szSubdirNew = $szPath_short & "\A\W\D"
			If FileExists($szSubdirOld) Then
				If Not DirMove($szSubdirOld, $szSubdirNew, 1) Then Return $ret

				; Add other subdirectories here
				; (...)
			EndIf
		EndIf
	EndIf

	$ret = True
	Return $ret

EndFunc   ;==>FSESubMoveForDelete
