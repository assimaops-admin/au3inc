#include-once
#include <File.au3> ; <<< _FileReadToArray <<< TEMPAPI160_IterBasedMemPerfQa_ASSERT
#include ".\MemPerfQa.AdlibBased.au3"
#include ".\MemPerfQa.PrintBased.au3"


; ITERBASED TESTING CONSTANTS

Const $ITERBASED_LIB_DEFAULT_MAX_ITERCOUNT = 10

; MEMPERF TESTING CONSTANTS

Const $MEMPERF_LIB_DEFAULT_ADLIBBASED_OUTFILE = ".\~" & "MemPerfLog.AdlibBased.csv" ; SOPHISTICATED, EASY BUT RISKY IMPLEMENTATION
Const $MEMPERF_LIB_DEFAULT_ITERBASED_OUTFILE = ".\~" & "MemPerfLog.IterBased.csv" ; STRAIGHT FORWARD
Const $MEMPERF_LIB_DEFAULT_PROCBASED_OUTFILE = ".\~" & "MemPerfLog.csv"; SOPHISTICATED, NOT IMPLEMENTED

; ASSERTN GLOBAL VARIABLES

Global $g__TEMPAPI160_PRIVATE__assertsTotalCount
Global $g__TEMPAPI160_PRIVATE__assertsPassedCount
Func Print_TEMPAPI160_PRIVATE__assertsTotalCount_CALLBACK()
	TEMPAPI160_IterBasedMemPerfQa_LINE("")
	ConsoleWrite("  " & $g__TEMPAPI160_PRIVATE__assertsPassedCount & " / " & $g__TEMPAPI160_PRIVATE__assertsTotalCount & " TESTS PASSED" & @CRLF)
	ConsoleWrite("    BYE!..." & @CRLF)
	TEMPAPI160_IterBasedMemPerfQa_LINE("")
EndFunc   ;==>Print_TEMPAPI160_PRIVATE__assertsTotalCount_CALLBACK
OnAutoItExitRegister("Print_TEMPAPI160_PRIVATE__assertsTotalCount_CALLBACK")

; PERFQA GLOBAL VARIABLES

Global $g__TEMPAPI160_PRIVATE__disableAutoPrintB
Global $g__TEMPAPI160_PRIVATE__adlibBasedCsvFile
Global $g__TEMPAPI160_PRIVATE__iterBasedCsvFile
Global $g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ

; PERFQA ADLIB-BASED INTERFACE

Func TEMPAPI160_IterBasedMemPerfQa_CFGX($aProgId, $callPeriod = $DEFAULT_SAMPLING_PERIOD, $csvFilePath = "")
	; DELETE THE TEMP LOG
	If FileExists($csvFilePath) Then FileDelete($csvFilePath)
	; INITIALIZE CUSTOM PERFQA LIBRARY
	; First, register the callback for process memory monitoring (using AdLib, or not if $callPeriod is '0')
	$g__TEMPAPI160_PRIVATE__adlibBasedCsvFile = $csvFilePath
	AdlibBasedMemPerfQa_Init($aProgId, $callPeriod, $csvFilePath) ; <<< param 2, default AdLib period is 250ms
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_CFGX

; PERFQA PRINT-BASED INTERFACE

Func TEMPAPI160_IterBasedMemPerfQa_CONF($aProgId, $csvFilePath = "", $disableAutoPrintB = 0)
	Global $g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ
	Global $g__TEMPAPI160_PRIVATE__disableAutoPrintB
	; DELETE THE TEMP LOG
	If FileExists($csvFilePath) Then FileDelete($csvFilePath)
	; INITIALIZE CUSTOM PERFQA LIBRARY
	; First, register the callback for process memory monitoring (using AdLib, or not if $callPeriod is '0')
	$g__TEMPAPI160_PRIVATE__iterBasedCsvFile = $csvFilePath
	$g__TEMPAPI160_PRIVATE__disableAutoPrintB = $disableAutoPrintB
	$g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ = PrintBasedMemPerfQa_New($aProgId, $csvFilePath)
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_CONF

Func TEMPAPI160_IterBasedMemPerfQa_ECHO()
	Global $g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ
	PrintBasedMemPerfQa_Prt($g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ)
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_ECHO

; PERFQA ITER-BASED INTERFACE

Func TEMPAPI160_IterBasedMemPerfQa_ITER($callbackIterBody, $MAIN_ITERATIONS_COUNT = $ITERBASED_LIB_DEFAULT_MAX_ITERCOUNT, $disableAutoPrintA = 0)
	Global $g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ
	Global $g__TEMPAPI160_PRIVATE__disableAutoPrintB
	$disableAutoPrintB = $g__TEMPAPI160_PRIVATE__disableAutoPrintB
	If Not IsArray($g__TEMPAPI160_PRIVATE__ITERBASED_MEMPERFCALLBACK_OBJ) Then $disableAutoPrintA = 1
	If $disableAutoPrintA Then $disableAutoPrintB = 1
	$implTimerObj = TimerInit()
	;
	; PRINT MEMORY USAGE AT THE BEGINING
	If Not $disableAutoPrintA Then TEMPAPI160_IterBasedMemPerfQa_ECHO() ; << Print in kB
	;
	; MAIN LOOP
	For $i = 1 To $MAIN_ITERATIONS_COUNT
		; MAIN SUB PROCEDURE
		Call($callbackIterBody, $MAIN_ITERATIONS_COUNT, $i, $implTimerObj)
		; Here, at this point of the execution, we should be back to Graphical and Functional Automation State �Zero�
		; ie., the state of the USUT (Userspace-System-Under-Test) needs to be Graphically and Functionally equivalent
		; to the state of the USUT before the call to TEMPAPI160_IterBasedCallback3_IMPL
		; PRINT MEMORY USAGE AT THE END OF EACH ITERATION
		If Not $disableAutoPrintA Then TEMPAPI160_IterBasedMemPerfQa_ECHO() ; << Print in kB
	Next
	;
	; PRINT TEST DURATION
	TEMPAPI160_IterBasedMemPerfQa_FINALWORD($implTimerObj, $disableAutoPrintB)
	; TODO: PRINT TEST DURATION INFO INTO A IterBasedMemPerfQaInfo.ini (or TMOInfo.ini) file
	; - unless $disableAutoPrintA ?
	;
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_ITER

Func TEMPAPI160_IterBasedMemPerfQa_FINALWORD($implTimerObj, $disableAutoPrintB = 1, $disableAutoPrintA = 1)
	;
	; FINAL WORD PART A
	If $disableAutoPrintA Then Return
	; Depending on whether the monitor values were printed to the console or to the file...
	TEMPAPI160_IterBasedMemPerfQa_LINE("")
	TEMPAPI160_IterBasedMemPerfQa_LINE("# ITERATIVE TEST-MODULE DURATION")
	TEMPAPI160_IterBasedMemPerfQa_LINE(Round(TimerDiff($implTimerObj) / 1000, 2) & " seconds")
	;
	; FINAL WORD PART B
	If $disableAutoPrintB Then Return
	If FileExists($g__TEMPAPI160_PRIVATE__adlibBasedCsvFile) Then
		TEMPAPI160_IterBasedMemPerfQa_LINE("")
		TEMPAPI160_IterBasedMemPerfQa_LINE("# FILE CONTENT")
		TEMPAPI160_IterBasedMemPerfQa_LINE("(" & $g__TEMPAPI160_PRIVATE__adlibBasedCsvFile & ")")
		TEMPAPI160_IterBasedMemPerfQa_LINE(FileRead($g__TEMPAPI160_PRIVATE__adlibBasedCsvFile))
	EndIf
	If FileExists($g__TEMPAPI160_PRIVATE__iterBasedCsvFile) Then
		TEMPAPI160_IterBasedMemPerfQa_LINE("")
		TEMPAPI160_IterBasedMemPerfQa_LINE("# FILE CONTENT" & @CRLF)
		TEMPAPI160_IterBasedMemPerfQa_LINE("(" & $g__TEMPAPI160_PRIVATE__iterBasedCsvFile & ")")
		TEMPAPI160_IterBasedMemPerfQa_LINE(FileRead($g__TEMPAPI160_PRIVATE__iterBasedCsvFile))
	EndIf
	TEMPAPI160_IterBasedMemPerfQa_LINE("")
	;
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_FINALWORD

Func TEMPAPI160_IterBasedMemPerfQa_LINE($szTxt)
	;
	ConsoleWrite($szTxt & " #ConsoleOnly" & @CRLF)
	;
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_LINE

Func TEMPAPI160_IterBasedMemPerfQa_ASSERT($isCond, $szAddErrorMsgB = "", $szAddErrorMsgA = "", $isSilent = False, $scriptLineNumber = @ScriptLineNumber, $scriptPath = @ScriptFullPath)
	;
	Global $g__TEMPAPI160_PRIVATE__assertsTotalCount
	Global $g__TEMPAPI160_PRIVATE__assertsFailedCount
	$g__TEMPAPI160_PRIVATE__assertsTotalCount += 1
	;
	If Not $isCond Then
		If (Not $isSilent) Then TEMPAPI160_IterBasedMemPerfQa_LINE(StringFormat("!ERROR: [%d] Assertion Failed.", $g__TEMPAPI160_PRIVATE__assertsTotalCount))
		If ($szAddErrorMsgA <> "") And (Not $isSilent) Then TEMPAPI160_IterBasedMemPerfQa_LINE($szAddErrorMsgA)
		If ($szAddErrorMsgB <> "") And (Not $isSilent) Then TEMPAPI160_IterBasedMemPerfQa_LINE($szAddErrorMsgB)
		Dim $scriptContentByLinesAsArray
		_FileReadToArray($scriptPath, $scriptContentByLinesAsArray)
		If (Not $isSilent) Then TEMPAPI160_IterBasedMemPerfQa_LINE($scriptPath & ":" & $scriptLineNumber & ":" & $scriptContentByLinesAsArray[$scriptLineNumber])
		Exit 666 ; ;TODO: INCUB: UAFXA1\AbcQa.IntegratedQa\AbcQa.ExitCodes.au3:14:$AU3_EXIT_ASSERT = 666
	Else
		If (Not $isSilent) Then TEMPAPI160_IterBasedMemPerfQa_LINE(StringFormat("[%d] Assertion passed. OK.", $g__TEMPAPI160_PRIVATE__assertsTotalCount))
		$g__TEMPAPI160_PRIVATE__assertsPassedCount += 1
		Return 0
	EndIf
	;
EndFunc   ;==>TEMPAPI160_IterBasedMemPerfQa_ASSERT
