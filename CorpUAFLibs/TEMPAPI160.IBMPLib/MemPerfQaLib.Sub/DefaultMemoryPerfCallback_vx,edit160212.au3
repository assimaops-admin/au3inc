#include-once
#include <Process.au3>
#include ".\Inc_GetProcessMemoryInfo_Wrapper.au3" ; <<< Inc_GetProcessMemoryInfo_Wrapper

; MEMPERF TESTING CONSTANTS

Const $DEFAULT_SAMPLING_PERIOD = 250

;;; GLOBALS ;;;
Global Enum $g_mpVers = 0 _
		, $g_mpIsInit = 1 _
		, $g_mpProgId _
		, $g_mpFilePath _
		, $g_mpFileHandle _
		, $g_mpIsNewLine _
		, $g_mpCallPeriod _
		, $g_mpMainTimer _
		, $g_mpMEMPERFCALLBACK_OBJ_SIZE

Global $g_MemPerfCallback_Obj


#region SEALED INITIALIZE VERSIONS
;
Func LIBRARY_INIT_DefaultMemoryPerfCallback_v3($progId, $callPeriod = 250, $csvFilePath = "") ; <<< FORWARD COMPATIBLE
	Return LIBRARY_INIT_DefaultMemoryPerfCallback_v4($progId, $callPeriod, $csvFilePath)
EndFunc   ;==>LIBRARY_INIT_DefaultMemoryPerfCallback_v3
;
Func LIBRARY_INIT_DefaultMemoryPerfCallback_v4($aProgId, $callPeriod = $DEFAULT_SAMPLING_PERIOD, $csvFilePath = "") ; <<< param 2, default AdLib period is 250ms
	Global $g_MemPerfCallback_Obj
	$g_MemPerfCallback_Obj = LIBRARY_INIT_DefaultMemoryPerfCallback_v5($aProgId, $callPeriod, $csvFilePath)
	$g_MemPerfCallback_Obj[$g_mpVers] = "4.0.0"
EndFunc   ;==>LIBRARY_INIT_DefaultMemoryPerfCallback_v4
;
Func LIBRARY_INIT_DefaultMemoryPerfCallback_v5($aProgId, $callPeriod = $DEFAULT_SAMPLING_PERIOD, $csvFilePath = "") ; <<< param 2, default AdLib period is 250ms
	Dim $mpObj[$g_mpMEMPERFCALLBACK_OBJ_SIZE]
	$mpObj[$g_mpVers] = "5.0.0"
	$mpObj[$g_mpIsInit] = 1
	$mpObj[$g_mpProgId] = $aProgId
	$mpObj[$g_mpIsNewLine] = 1
	$mpObj[$g_mpMainTimer] = 0
	;; output to file
	$mpObj[$g_mpFilePath] = $csvFilePath
	If $csvFilePath <> "" Then
		$mpObj[$g_mpFileHandle] = FileOpen($csvFilePath, 2)
	EndIf
	;; output by periods
	$mpObj[$g_mpCallPeriod] = $callPeriod
	If IsInt($callPeriod) And ($callPeriod <> 0) Then
		AdlibRegister("EASYCALLABLE__PRIVATE__AdLibBased_DefaultMemoryPerfCallback", $callPeriod)
	EndIf
	;; get name(s) of the apps-under-test
	$aProgName = $aProgId
	If IsArray($aProgId) Then
		For $i = 0 To UBound($aProgId) - 1
			$aProgName[$i] = _ProcessGetName($aProgId[$i])
			If ($aProgName[$i] = "") Then $aProgName[$i] = "(PROCESS-NOT-FOUND)"
		Next
	Else
		$aProgName = _ProcessGetName($aProgId)
		If ($aProgName = "") Then $aProgName = "(PROCESS-NOT-FOUND)"
	EndIf
	;; write the output file headers
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, 'Local timestamp')
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, 'Internal timer')
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValueEx($mpObj, $aProgName, '[%s, Current working set size (bytes)]')
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_NewLineSeparat($mpObj)
	;; initiate the timer
	$mpObj[$g_mpMainTimer] = TimerInit()
	Return $mpObj
EndFunc   ;==>LIBRARY_INIT_DefaultMemoryPerfCallback_v5
#endregion SEALED INITIALIZE VERSIONS


#region SEALED CALLBACK VERSIONS
Func EASYCALLABLE_DefaultMemoryPerfCallback_v0_9_9()
	Global $g_MemPerfCallback_Obj
	$mpObj = $g_MemPerfCallback_Obj
	Local $aData = Inc_GetProcessMemoryInfo_Wrapper($mpObj[$g_mpProgId])
	ConsoleWrite(@CRLF)
	ConsoleWrite('------------------------------------------------------------' & @CRLF)
	;ConsoleWrite('Number of page faults: ' & $aData[0] & @CRLF)
	;ConsoleWrite('Peak working set size: ' & $aData[1] & ' bytes' & @CRLF)
	ConsoleWrite('Current working set size: ' & $aData[2] & ' bytes, ' & Dec(Hex($aData[2])) & ' bytes, ' & Floor(Dec(Hex($aData[2])) / 1000) & ' kB.' & @CRLF)
	;ConsoleWrite('Peak paged pool usage: ' & $aData[3] & ' bytes' & @CRLF)

	;;  _WinAPI_GetProcessMemoryInfo -- FAILED INTEGRATION >>>
	;ConsoleWrite('Current paged pool usage: ' & $aData[4] & ' bytes' & @CRLF)
	;ConsoleWrite('Peak nonpaged pool usage: ' & $aData[5] & ' bytes' & @CRLF)
	;ConsoleWrite('Current nonpaged pool usage: ' & $aData[6] & ' bytes' & @CRLF)
	;ConsoleWrite('Current space allocated for the pagefile: ' & $aData[7] & ' bytes' & @CRLF)
	;ConsoleWrite('Peak space allocated for the pagefile: ' & $aData[8] & ' bytes' & @CRLF)
	;ConsoleWrite('Current private space: ' & $aData[9] & ' bytes' & @CRLF)
	;;  <<< FAILED INTEGRATION -- _WinAPI_GetProcessMemoryInfo
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v0_9_9
;
Func EASYCALLABLE_DefaultMemoryPerfCallback_v1_0_0()
	Global $g_MemPerfCallback_Obj
	$mpObj = $g_MemPerfCallback_Obj
	Local $aData = Inc_GetProcessMemoryInfo_Wrapper($mpObj[$g_mpProgId])
	$valueInBytes = Dec(Hex($aData[2]))
	ConsoleWrite('Current working set size: ' & $valueInBytes & ' bytes' & @CRLF)
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v1_0_0
;
Func EASYCALLABLE_DefaultMemoryPerfCallback_v1_2_0()
	Global $g_MemPerfCallback_Obj
	$mpObj = $g_MemPerfCallback_Obj
	$valueIn_kB_rnd0 = Inc_GetProcessMemoryInfo_Wrapper_MINIMAL_v1_2_1($mpObj[$g_mpProgId])
	ConsoleWrite('Current working set size: ' & $valueIn_kB_rnd0 & ' kB' & @CRLF)
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v1_2_0
;
Func EASYCALLABLE_DefaultMemoryPerfCallback_v3_0_0()
	Return EASYCALLABLE_DefaultMemoryPerfCallback_v4_0_0()
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v3_0_0
;
Func EASYCALLABLE_DefaultMemoryPerfCallback_v4_0_0()
	Global $g_MemPerfCallback_Obj
	$mpObj = $g_MemPerfCallback_Obj
	$internalTimerDiff_s_rnd2 = Round(TimerDiff($mpObj[$g_mpMainTimer]) / 1000, 2)
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, @YEAR & @MON & @MDAY & "T" & @HOUR & ":" & @MIN & ":" & @SEC & "." & @MSEC)
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, $internalTimerDiff_s_rnd2)
	__DefaultMemoryPerfCallback_v4_0_0_WriteCall_LineValuePIDs($mpObj, $mpObj[$g_mpProgId])
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_NewLineSeparat($mpObj)
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v4_0_0
;
Func EASYCALLABLE__PRIVATE__AdLibBased_DefaultMemoryPerfCallback()
	Global $g_MemPerfCallback_Obj
	$mpObj = $g_MemPerfCallback_Obj
	EASYCALLABLE_DefaultMemoryPerfCallback_v5_0_0($mpObj)
EndFunc   ;==>EASYCALLABLE__PRIVATE__AdLibBased_DefaultMemoryPerfCallback
;
Func EASYCALLABLE_DefaultMemoryPerfCallback_v5_0_0(ByRef $mpObj)
	$internalTimeStamp_s_rnd2 = @YEAR & "-" & @MON & "-" & @MDAY & "T" & @HOUR & ":" & @MIN & ":" & @SEC & "." & @MSEC
	$internalTimerDiff_s_rnd2 = Round(TimerDiff($mpObj[$g_mpMainTimer]) / 1000, 2)
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, $internalTimeStamp_s_rnd2)
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, $internalTimerDiff_s_rnd2)
	__DefaultMemoryPerfCallback_v4_0_0_WriteCall_LineValuePIDs($mpObj, $mpObj[$g_mpProgId])
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_NewLineSeparat($mpObj)
EndFunc   ;==>EASYCALLABLE_DefaultMemoryPerfCallback_v5_0_0
#endregion SEALED CALLBACK VERSIONS


#region PRIVATE METHODS
Func __DefaultMemoryPerfCallback_v3_0_0_WriteCall_IsHandle(ByRef $mpObj)
	;; check if handle and tell
	If $mpObj[$g_mpFileHandle] <> "" Then
		Return True
	EndIf
	Return False
EndFunc   ;==>__DefaultMemoryPerfCallback_v3_0_0_WriteCall_IsHandle
Func __DefaultMemoryPerfCallback_v3_0_0_WriteCall_BARE(ByRef $mpObj, $szText)
	;; check if handle and apply
	If $mpObj[$g_mpIsInit] Then
		If __DefaultMemoryPerfCallback_v3_0_0_WriteCall_IsHandle($mpObj) Then
			FileWrite($mpObj[$g_mpFileHandle], $szText)
		Else
			ConsoleWrite($szText)
		EndIf
	EndIf
EndFunc   ;==>__DefaultMemoryPerfCallback_v3_0_0_WriteCall_BARE
Func __DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue(ByRef $mpObj, $szText)
	;; check if handle and apply
	If $mpObj[$g_mpIsNewLine] Then
		__DefaultMemoryPerfCallback_v3_0_0_WriteCall_BARE($mpObj, $szText)
	Else
		__DefaultMemoryPerfCallback_v3_0_0_WriteCall_BARE($mpObj, ";" & $szText)
	EndIf
	$mpObj[$g_mpIsNewLine] = 0
EndFunc   ;==>__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue
Func __DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValueEx(ByRef $mpObj, $aValue, $szPattern = "%s")
	If Not IsArray($aValue) Then
		__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, StringFormat($szPattern, $aValue))
	Else
		For $val In $aValue
			__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValue($mpObj, StringFormat($szPattern, $val))
		Next
	EndIf
EndFunc   ;==>__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValueEx
Func __DefaultMemoryPerfCallback_v4_0_0_WriteCall_LineValuePIDs(ByRef $mpObj, $PIDs)
	If Not IsArray($PIDs) Then
		$pid = $PIDs
		$valueIn_kB_rnd0 = Inc_GetProcessMemoryInfo_Wrapper_MINIMAL_v1_2_1($pid)
		__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValueEx($mpObj, $valueIn_kB_rnd0)
	Else
		For $pid In $PIDs
			$valueIn_kB_rnd0 = Inc_GetProcessMemoryInfo_Wrapper_MINIMAL_v1_2_1($pid)
			__DefaultMemoryPerfCallback_v3_0_0_WriteCall_LineValueEx($mpObj, $valueIn_kB_rnd0)
		Next
	EndIf
EndFunc   ;==>__DefaultMemoryPerfCallback_v4_0_0_WriteCall_LineValuePIDs
Func __DefaultMemoryPerfCallback_v3_0_0_WriteCall_NewLineSeparat(ByRef $mpObj)
	$mpObj[$g_mpIsNewLine] = 1
	;; check if handle and apply
	__DefaultMemoryPerfCallback_v3_0_0_WriteCall_BARE($mpObj, @CRLF)
EndFunc   ;==>__DefaultMemoryPerfCallback_v3_0_0_WriteCall_NewLineSeparat
#endregion PRIVATE METHODS
