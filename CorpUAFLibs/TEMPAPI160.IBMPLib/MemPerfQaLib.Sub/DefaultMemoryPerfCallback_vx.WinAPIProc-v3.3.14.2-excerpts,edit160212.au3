#include-once
; AutoIt-v3.3.14.2-excerpts,edit160212
; https://www.autoitscript.com/autoit3/docs/libfunctions/_WinAPI_GetProcessMemoryInfo.htm


; ================================================== ;
; ADD SOME MORE EXCERPTS, HERE:
; .\Include-v3.3.14.2\WinAPIInternals.au3
#include ".\DefaultMemoryPerfCallback_vx.WinAPIInternals-v3.3.14.2-excerpts,edit160212.au3"
; -------------------------------------------------- ;
; REPLACE SOME BREAKING CODE, HERE:
#include <AuItCustLibs\AutoItEx.au3> ; <<< Func 'QuestionMark' instead of ternary '?:'
; -------------------------------------------------- ;
; REPLACE SOME BREAKING CODE, HERE:
Func __Iif($bTest, $vTrue, $vFalse)
	Return QuestionMark($bTest, $vTrue, $vFalse) ;  <<< INSTEAD OF BREAKING CODE: Return $bTest ? $vTrue : $vFalse
EndFunc   ;==>__Iif
; ================================================== ;


; .\Include\WinAPIProc.au3

; #FUNCTION# ====================================================================================================================
; Author.........: Yashied
; Modified.......: jpm
; ===============================================================================================================================
Func _WinAPI_GetProcessMemoryInfo($iPID = 0)
	If Not $iPID Then $iPID = @AutoItPID

	Local $hProcess = DllCall('kernel32.dll', 'handle', 'OpenProcess', 'dword', __Iif($__WINVER < 0x0600, 0x00000410, 0x00001010), _
			'bool', 0, 'dword', $iPID)
	If @error Or Not $hProcess[0] Then Return SetError(@error + 20, @extended, 0)

	Local $tPMC_EX = DllStructCreate('dword;dword;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr;ulong_ptr')
	Local $aRet = DllCall(@SystemDir & '\psapi.dll', 'bool', 'GetProcessMemoryInfo', 'handle', $hProcess[0], 'struct*', $tPMC_EX, _
			'int', DllStructGetSize($tPMC_EX))
	If __CheckErrorCloseHandle($aRet, $hProcess[0]) Then Return SetError(@error, @extended, 0)

	Local $aResult[10]
	For $i = 0 To 9
		$aResult[$i] = DllStructGetData($tPMC_EX, $i + 2)
	Next
	Return $aResult
EndFunc   ;==>_WinAPI_GetProcessMemoryInfo
