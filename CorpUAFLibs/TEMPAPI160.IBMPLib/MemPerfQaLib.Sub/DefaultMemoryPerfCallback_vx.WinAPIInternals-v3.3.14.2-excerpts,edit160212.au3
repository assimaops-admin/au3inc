#include-once
#include "WinAPIError.au3"

; .\Include-v3.3.14.2\WinAPIInternals.au3

Func __CheckErrorCloseHandle($aRet, $hFile, $bLastError = 0, $iCurErr = @error, $iCurExt = @extended)
	If Not $iCurErr And Not $aRet[0] Then $iCurErr = 10
	Local $iLastError = _WinAPI_GetLastError()
	DllCall("kernel32.dll", "bool", "CloseHandle", "handle", $hFile)
	If $iCurErr Then _WinAPI_SetLastError($iLastError)
	If $bLastError Then $iCurExt = $iLastError
	Return SetError($iCurErr, $iCurExt, $iCurErr)
EndFunc   ;==>__CheckErrorCloseHandle


; .\Include-v3.3.14.2\WinAPIInternals.au3

; #CONSTANTS# ===================================================================================================================
Global Const $tagOSVERSIONINFO = 'struct;dword OSVersionInfoSize;dword MajorVersion;dword MinorVersion;dword BuildNumber;dword PlatformId;wchar CSDVersion[128];endstruct'

Global Const $__WINVER = __WINVER()
; ===============================================================================================================================


; .\Include-v3.3.14.2\WinAPIInternals.au3

; #INTERNAL_USE_ONLY# ===========================================================================================================
; Name...........: __WINVER
; Description ...: Retrieves version of the current operating system
; Syntax.........: __WINVER ( )
; Parameters ....: none
; Return values .: Returns the binary version of the current OS.
;                            0x0603 - Windows 8.1
;                            0x0602 - Windows 8 / Windows Server 2012
;                            0x0601 - Windows 7 / Windows Server 2008 R2
;                            0x0600 - Windows Vista / Windows Server 2008
;                            0x0502 - Windows XP 64-Bit Edition / Windows Server 2003 / Windows Server 2003 R2
;                            0x0501 - Windows XP
; Author ........: Yashield
; Modified.......:
; Remarks .......: For Internal Use Only
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func __WINVER()
	Local $tOSVI = DllStructCreate($tagOSVERSIONINFO)
	DllStructSetData($tOSVI, 1, DllStructGetSize($tOSVI))

	Local $aRet = DllCall('kernel32.dll', 'bool', 'GetVersionExW', 'struct*', $tOSVI)
	If @error Or Not $aRet[0] Then Return SetError(@error, @extended, 0)
	Return BitOR(BitShift(DllStructGetData($tOSVI, 2), -8), DllStructGetData($tOSVI, 3))
EndFunc   ;==>__WINVER
