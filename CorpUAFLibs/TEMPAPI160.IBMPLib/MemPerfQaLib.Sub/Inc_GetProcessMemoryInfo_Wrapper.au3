#include-once
#include ".\DefaultMemoryPerfCallback_vx.WinAPIProc-v3.3.14.2-excerpts,edit160212.au3" ; <<< _WinAPI_GetProcessMemoryInfo

; --------------------------------------------------------
; forward compatible version
;
Func Inc_GetProcessMemoryInfo_Wrapper($processID)
	Return Inc_GetProcessMemoryInfo_Wrapper_v1($processID)
EndFunc   ;==>Inc_GetProcessMemoryInfo_Wrapper

; --------------------------------------------------------
; version 0.1 - simple/tiny wrapper - tried on AutoIt-v3.3.6.1
; source: https://www.autoitscript.com/forum/topic/24105-dllcall-psapidll-getprocessmemoryinfo/
; version 0.2 - converted into Hex($processID), etc.
;
Func Inc_GetProcessMemoryInfo_Wrapper_v0($processID)
	$pmc = DllStructCreate('dword;dword;int;int;int;int;int;int;int;int')
	$p_pmc = DllStructGetPtr($pmc)
	$handle = DllCall("kernel32.dll", 'int', 'OpenProcess', 'int', 0x1f0fff, 'int', False, 'long', $processID)
	$return = DllCall("psapi.dll", 'int', 'GetProcessMemoryInfo', 'long', $handle[0], 'ptr', $p_pmc, 'long', DllStructGetSize($p_pmc))
	ConsoleWrite("Wrapped Win32 API Function returned " & $return[0] & @CRLF)
	Local $iLastError = _WinAPI_GetLastError()
	ConsoleWrite('$iLastError = ' & $iLastError & @CRLF)
	DllCall('kernel32.dll', 'int', 'CloseHandle', 'int', $handle[0])
	Return $return
EndFunc   ;==>Inc_GetProcessMemoryInfo_Wrapper_v0

; --------------------------------------------------------
; version 1.0 - excerpt from official AutoIt3-v3.3.14.2
; SCRAPPED AS MemoryPerfQa799v01-scrap160212.7z
; RESTORED WITH USE OF DIRECTIVE:
;
;    #AutoIt3Wrapper_AutoIt3=C:\Progra~2\AutoIt3\AutoIt3-v3.3.14.2_x86.exe
;
Func Inc_GetProcessMemoryInfo_Wrapper_v1($processID)
	Return _WinAPI_GetProcessMemoryInfo($processID)
EndFunc   ;==>Inc_GetProcessMemoryInfo_Wrapper_v1

; --------------------------------------------------------
; version 1.2 = version 1.0 with formatting
;
Func Inc_GetProcessMemoryInfo_Wrapper_MINIMAL_v1_2_1($processID)
	Local $aData = Inc_GetProcessMemoryInfo_Wrapper($processID)
	If Not IsArray($aData) Then Exit 1
	$valueInBytes = Dec(Hex($aData[2]))
	$valueIn_kB_rnd0 = Floor($valueInBytes / 1000)
	Return $valueIn_kB_rnd0
EndFunc   ;==>Inc_GetProcessMemoryInfo_Wrapper_MINIMAL_v1_2_1
