#include-once

;-------------------------------------------------------------------------------
; Name .........: HTMLMisc_InlineCssStyleString
; Description ..:
;-------------------------------------------------------------------------------
Func HTMLMisc_InlineCssStyleString($allLinesPrefix = Default)
	If $allLinesPrefix = Default Then $allLinesPrefix = @CRLF
	Local $smsg = ""
	$smsg &= $allLinesPrefix & '<style type="text/css">'
	;
	$smsg &= $allLinesPrefix & '  .file-dflt {}'
	$smsg &= $allLinesPrefix & '  .file-ttl1 {color:darkblue;font-weight:bold;text-align:center;}'
	$smsg &= $allLinesPrefix & '  .file-ttl2 {color:darkblue;font-weight:bold;}'
	;
	$smsg &= $allLinesPrefix & '  .hunk-dflt {}'
	$smsg &= $allLinesPrefix & '  .hunk-ttl1 {color:blue;font-weight:bold;}'
	$smsg &= $allLinesPrefix & '  .hunk-data {font-family:Consolas;white-space:pre;}'
	;
	$smsg &= $allLinesPrefix & '  .tag-positive {font-weight:bold;color:red;}'
	$smsg &= $allLinesPrefix & '  .tag-negative {font-weight:bold;color:green;}'
	$smsg &= $allLinesPrefix & '  .tag-neutral {font-weight:bold;color:grey;}'
	$smsg &= $allLinesPrefix & '  .tag-summary {font-weight:bold;}'
	;
	$smsg &= $allLinesPrefix & '</style>'
	Return $smsg
EndFunc   ;==>HTMLMisc_InlineCssStyleString
