#include-once
#include ".\AtsTrayNotif.au3"

; Constant parameters
Const $GRABBER_STOP_CAPTURE_WAIT_TIME__DEFAULT = 60

; Universal constants
Const $DEFAULT_UGRABBER_PROCESS_NAME = "Grabber.exe"
Const $DEFAULT_UGRABBER_WERFAULT_TITLE = 'Grabber'
Const $DEFAULT_UGRABBER_WINDOW_TITLE = "Assima Standalone Grabber"

; Backward compatibility
Const $GRABBER_EXECUTABLE = $DEFAULT_UGRABBER_PROCESS_NAME

; Global variables
Global $_uGrabber_ScreenCaptured = 0
Global $_uGrabber_StateHidden = 0

Func uGrabber_StartCapture()
	FuncTrace('uGrabber_StartCapture')
	$handle = WinGetHandle($DEFAULT_UGRABBER_WINDOW_TITLE)
	If $handle <> "" Then
		uGrabber_PrivateControlClick($handle, "", "Button1")
	EndIf
EndFunc   ;==>uGrabber_StartCapture

Func uGrabber_StopCapture($timeout = $GRABBER_STOP_CAPTURE_WAIT_TIME__DEFAULT)
	FuncTrace('uGrabber_StopCapture')
	$windowFound = 0
	$error = 0
	$handle = WinGetHandle($DEFAULT_UGRABBER_WINDOW_TITLE)
	If $handle <> "" Then
		$windowFound = 1
		uGrabber_PrivateControlClick($handle, "", "Button2")
		$error = WinWaitClose($handle, "", $timeout)
	EndIf
	Return SetError($error, 0, $windowFound)
EndFunc   ;==>uGrabber_StopCapture

Func uGrabber_CaptureScreen($extra_time = 0)
	If $_uGrabber_ScreenCaptured = 0 Then AtsTrayNotif_FindAndRecordClassname()
	AtsTrayNotif_CheckPointBeforeCapture()
	Sleep($extra_time)
	Send("{LCTRL}")
	AtsTrayNotif_CheckPointAfterCapture()
	$_uGrabber_ScreenCaptured += 1
EndFunc   ;==>uGrabber_CaptureScreen

Func uGrabber_WaitForMe()
	FuncTrace('uGrabber_WaitUntilReady')
	WinWait($DEFAULT_UGRABBER_WINDOW_TITLE)
EndFunc   ;==>uGrabber_WaitForMe

Func uGrabber_Hide()
	FuncTrace('uGrabber_Hide')
	WinSetState($DEFAULT_UGRABBER_WINDOW_TITLE, "", @SW_HIDE)
	$_uGrabber_StateHidden = True
	Sleep(350)
EndFunc   ;==>uGrabber_Hide

Func uGrabber_Show()
	FuncTrace('uGrabber_Show')
	WinSetState($DEFAULT_UGRABBER_WINDOW_TITLE, "", @SW_SHOW)
	$_uGrabber_StateHidden = 0
	Sleep(350)
EndFunc   ;==>uGrabber_Show

Func uGrabber_PrivateControlClick($title, $text, $controlID)
	ControlClick($title, $text, $controlID)
	If $_uGrabber_StateHidden Then WinSetState($DEFAULT_UGRABBER_WINDOW_TITLE, "", @SW_HIDE)
EndFunc   ;==>uGrabber_PrivateControlClick
