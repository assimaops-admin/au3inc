#include-once
#include <Process.au3>
#include <Constants.au3> ; ADDED FOR AUTOIT 6.3.6.1 UPGRADE
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>

$_AtsTrayNotif_Classname = ""
$_AtsTrayNotif_uGrabber = False

;-------------------------------------------------------------------------------
; Name .........: AtsTrayNotif_FindAndRecordClassname
; Description ..: Call this function after pressing the button 'start capture'
; Parameters ...:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func AtsTrayNotif_FindAndRecordClassname()
	FuncTrace('AtsTrayNotif_FindAndRecordClassname')
	While True
		$aWinList = WinList()
		For $i = 0 To $aWinList[0][0]
			$szClassname = _WinAPI_GetClassName($aWinList[$i][1])
			If $aWinList[$i][0] = "" _
					And StringStartsWith($szClassname, "ATL:", 1) _
					And _WinAPI_GetWindow($aWinList[$i][1], $GW_CHILD) _
					And (_ProcessGetName(WinGetProcess($aWinList[$i][1])) = "TrainingStudio.exe" _
					Or _ProcessGetName(WinGetProcess($aWinList[$i][1])) = "Grabber.exe") _
					Then
				$_AtsTrayNotif_Classname = $szClassname
				ValTrace($_AtsTrayNotif_Classname, "$_AtsTrayNotif_Classname")
				If _ProcessGetName(WinGetProcess($aWinList[$i][1])) = "Grabber.exe" Then $_AtsTrayNotif_uGrabber = True
				ValTrace($_AtsTrayNotif_uGrabber, "$_AtsTrayNotif_uGrabber")
				Return
			EndIf
		Next
	WEnd
EndFunc   ;==>AtsTrayNotif_FindAndRecordClassname

;-------------------------------------------------------------------------------
; Name .........: AtsTrayNotif_WaitForState
; Description ..:
; Parameters ...:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func AtsTrayNotif_WaitUntilVisibleOrNot($bWaitVisible = True)
	FuncTrace('AtsTrayNotif_Expect', $bWaitVisible)
	$szClassname = $_AtsTrayNotif_Classname
	If $bWaitVisible Then
		WinWaitVisible("[CLASS:" & $szClassname & "]")
	Else
		WinWaitNotVisible("[CLASS:" & $szClassname & "]")
	EndIf
EndFunc   ;==>AtsTrayNotif_WaitUntilVisibleOrNot


;-------------------------------------------------------------------------------
; Name .........: AtsTrayNotif_CheckPointBeforeCapture
; Description ..:
; Parameters ...:
; Return .......: none
;-------------------------------------------------------------------------------
Func AtsTrayNotif_CheckPointBeforeCapture($bExpectTwoActionsDetected = False)
	FuncTrace('AtsTrayNotif_CheckPointBeforeCapture', $bExpectTwoActionsDetected)
	If Not $bExpectTwoActionsDetected Then
		AtsTrayNotif_WaitUntilVisibleOrNot()
	Else
		AtsTrayNotif_WaitUntilVisibleOrNot()
		Sleep(2000)
	EndIf
EndFunc   ;==>AtsTrayNotif_CheckPointBeforeCapture

;-------------------------------------------------------------------------------
; Name .........: AtsTrayNotif_CheckPointAfterCapture
; Description ..:
; Parameters ...:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func AtsTrayNotif_CheckPointAfterCapture()
	FuncTrace('AtsTrayNotif_CheckPointAfterCapture')
	AtsTrayNotif_WaitUntilVisibleOrNot(False)
	AtsTrayNotif_WaitUntilVisibleOrNot()
	AtsTrayNotif_WaitUntilVisibleOrNot(False)
EndFunc   ;==>AtsTrayNotif_CheckPointAfterCapture
