#include-once
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>
#include <CoreUAFLibs\StdCoreLib\WinV3.au3>


Global $g_SAPLogonPID = 0

Global $SAP_MULTIPLE_SESSIONS_DETECTED = 0


;-------------------------------------------------------------------------------
; Name .........: SAP_LoginPart2Sub_ResetWindowPosition
; Description ..: Enter credentials
; Remarks ......: Partially duplicated functionality with 'ApsCommon_SapLogin'
; Return .......: n/a
;-------------------------------------------------------------------------------
Func SAP_LoginPart2Sub_ResetWindowPosition()

	WinExpect("SAP Easy Access")
	Trace("Reset 'SAP Easy Access' window position to (0;0)...")
	WinSetPosStandard()
	Trace("Reset 'SAP Easy Access' Tree-view width...")
	SAP_ResetTreeViewWidth(456)
	Sleep(1000)

EndFunc   ;==>SAP_LoginPart2Sub_ResetWindowPosition

;-------------------------------------------------------------------------------
; Name .........: SAP_AnswerMultipleLogonsIfAny
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func SAP_AnswerMultipleLogonsIfAny()
	If WinWait("License Information for Multiple Logon", "", 3) Then
		$SAP_MULTIPLE_SESSIONS_DETECTED = 1
		WinActivateV2("License Information for Multiple Logon")
		Send("{TAB}")
		Sleep(200)
		Send("{UP}")
		Sleep(200)
		Send("{TAB}")
		Sleep(200)
		Send("{ENTER}")
		Sleep(200)
	EndIf
EndFunc   ;==>SAP_AnswerMultipleLogonsIfAny

;-------------------------------------------------------------------------------
; Name .........: SAP_ResetTreeViewWidth
; Description ..: Resize the main SAP tree-view
;                 (first screen after login - vertical splitter)
; Return .......: n/a
;-------------------------------------------------------------------------------
Func SAP_ResetTreeViewWidth($desired_width)
	Return ControlSetPosViaSplitter("SAP Easy Access", "SAPTreeList1", $desired_width, 0)
EndFunc   ;==>SAP_ResetTreeViewWidth
