#include-once
#include <CoreUAFLibs\StdCoreLib\AutomationShorthand.au3>
#include ".\SAPGeneric.Private.au3"

Global $g_SAP_SapLogonLaunchDisabled = 0

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_RunSapLogon
; Description ...: Start the SAPLogon.exe process.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_RunSapLogon()
	If $g_SAP_SapLogonLaunchDisabled Then Return
	Global $g_SAPLogonPID = Run(ProgramFilesDir32() & "\SAP\FrontEnd\SAPgui\saplogon.exe")
	ValTrace('$g_SAPLogonPID', $g_SAPLogonPID)
	Return $g_SAPLogonPID
EndFunc   ;==>_Sap_RunSapLogon

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_CloseSapLogon
; Description ...: Close the process started with _Sap_RunSapLogon().
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_CloseSapLogon()
	FuncTrace('_Sap_CloseSapLogon')
	;
	Local Const $TestedProcessUserName = "SAP"
	If Not $g_SAPLogonPID Then
		; STDMSG20150318.1
		Trace("WARNING: " & $TestedProcessUserName & " process was never launched!")
		Return False
	ElseIf Not ProcessExists($g_SAPLogonPID) Then
		; STDMSG20150318.2
		Trace("WARNING: Previously launched " & $TestedProcessUserName & " process was not found. It may have crashed!")
		Return False
	EndIf
	;
	;
	Trace('Closing active SAP windows (if any)...')
	$hWinActivePrev = 0
	$hWinActive = WinGetHandle("[ACTIVE]")
	While $hWinActive <> $hWinActivePrev
		$hWinActivePrev = $hWinActive
		If (WinGetProcess($hWinActive) = $g_SAPLogonPID) _
				And WinIsVisible($hWinActive) Then
			WinClose($hWinActive)
			WinWaitClose($hWinActive, "", 3)
		EndIf
		$hWinActive = WinGetHandle("[ACTIVE]")
	WEnd
	;
	;
	Trace('Closing non active SAP windows (if any)...')
	$aWinList = WinList()
	For $i = 0 To $aWinList[0][0]
		$hWinAny = $aWinList[$i][1]
		If (WinGetProcess($hWinAny) = $g_SAPLogonPID) _
				And WinIsVisible($hWinAny) Then
			WinActivate($hWinAny)
			WinWaitActive($hWinAny, "", 3)
			WinClose($hWinAny)
			WinWaitClose($hWinAny, "", 7)
		EndIf
	Next
	If $g_SAP_SapLogonLaunchDisabled Then Return
	;
	;
	$MAX_TIME_FOR_SAP_CLOSE = 15
	;
	; REF. SNIPPET_MAX_TIME_FOR_APP_CLOSE
	; HEADER
	$APP_PROCESS_IDENTIFIER = $g_SAPLogonPID
	$MAX_TIME_FOR_APP_CLOSE = $MAX_TIME_FOR_SAP_CLOSE
	;
	; REF. SNIPPET_MAX_TIME_FOR_APP_CLOSE
	; ACTUAL SNIPPET
	If ProcessExists($APP_PROCESS_IDENTIFIER) Then
		Trace('Waiting max ' & $MAX_TIME_FOR_APP_CLOSE & 's for process to close...')
		ProcessWaitClose($APP_PROCESS_IDENTIFIER, $MAX_TIME_FOR_APP_CLOSE)
	EndIf
	If ProcessExists($APP_PROCESS_IDENTIFIER) Then
		Trace('Process still exists. Closing forcefully now...')
		ProcessClose($APP_PROCESS_IDENTIFIER)
		ProcessWaitClose($APP_PROCESS_IDENTIFIER)
	EndIf
	;
	;
	Return True
	;
EndFunc   ;==>_Sap_CloseSapLogon

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_LoginServer
; Description ...: Opens a connection to the first server in the list.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; History ......: We use to keyboard shortcut, but it was too language dependant.
;                 So now we just simply ControlClick!
;
;                 FIRST IMPLEMENTATION
;
;                    We used to type the name of the server
;
;                      Send("autotesting")
;                      Send("{ENTER}")
;
;                    Now, we just open the first that is in the list!
;
;
;                 SECOND IMPLEMENTATION
;
;                    Alt+O or Alt+L
;
;                 THIRD IMPLEMENTATION
;
;                    ControlClick.
;
;
;                 Alternatively, we could probably also automate the list view directly
;                      $hSAPServersList = ControlGetHandle("", "", "[CLASS:SysListView32; INSTANCE:1]")
;                      (...) (BUT THIS IS OVERKILL)
;
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_LoginServer()
	; title = SAP Logon 710
	; title = SAP Logon 730
	If $g_SAP_SapLogonLaunchDisabled Then Return
	$hWinSapLogon7 = WinExpect("[REGEXPTITLE:SAP Logon 7\d0]")
	ValTrace($hWinSapLogon7, "$hWinSapLogon7")
	SleepV2(251)
	If @ComputerName = "testserv--xp" Then
		; Use the old version on the old test machine
		; (The new version doesn't work)
		Send("!l")
	Else
		ControlClick("[REGEXPTITLE:SAP Logon 7\d0]", "", "Button1")
	EndIf
	$hWinSapFrontend = WinExpect("[CLASS:SAP_FRONTEND_SESSION]")
	ValTrace($hWinSapFrontend, "$hWinSapFrontend")
	WinClose($hWinSapLogon7)
	SleepV2(252)
	WinWaitActive($hWinSapFrontend)
	Return $hWinSapFrontend
EndFunc   ;==>_Sap_LoginServer

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_LoginNwbcServer
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_LoginNwbcServer()
	If $g_SAP_SapLogonLaunchDisabled Then Return
	WinExpect("SAP NetWeaver Business Client")
	Send("{ENTER}")
	WinExpect("[CLASS:SAP_FRONTEND_SESSION]")
	Sleep(1000)
EndFunc   ;==>_Sap_LoginNwbcServer

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_LoginUser
; Description ...: Enter the users credentials.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......: Code extracted from 'SAP_LoginPart2'
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_LoginUser($username, $userpass, $sapclient = Default)

	If $g_SAP_SapLogonLaunchDisabled Then Return

	; Before: SAP Logon 710
	; Class:	#32770

	;
	; LOG-IN DETAILS
	;
	WinExpect("[CLASS:SAP_FRONTEND_SESSION]")
	WinSetPosStandard("[CLASS:SAP_FRONTEND_SESSION]")
	Click(7, 135)
	Trace("Log into SAP...")

	If $sapclient = "" Or $sapclient = "000" Or $sapclient = Default Then

		; NOTE: leave client value '000'

		SendSlow($username) ; Main user (used by everybody)
		Send("{TAB}")
		SendSlow($userpass) ; Password
		Send("{ENTER}")
		Sleep(1000)

	Else

		Sleep(1000) ; Select 'client' edit field
		Send("{SHIFTDOWN}")
		Sleep(250)
		Send("{TAB}")
		Sleep(250)
		Send("{SHIFTUP}")
		Sleep(250)
		SendSlow($sapclient) ; Client '800'
		Send("{TAB}")
		SendSlow($username) ; Specific user for autotesting
		Send("{TAB}")
		SendSlow($userpass) ; Password
		Send("{ENTER}")
		Sleep(1000)

	EndIf

	;
	; LOG-IN (PART 3/3): MULTIPLE SESSIONS
	;
	SAP_AnswerMultipleLogonsIfAny()


	;
	; RESET WINDOW POSITION
	;
	SAP_LoginPart2Sub_ResetWindowPosition()

EndFunc   ;==>_Sap_LoginUser
