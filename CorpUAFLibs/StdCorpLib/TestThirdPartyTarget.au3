#include-once
#include <+UAFCore.au3>

; Global
$_TestModule_Target2 = "" ; original name of the global variable
; eg. $szScriptParams, $REGISTERED_SAMPLES_LIST, $_TestModule_Target2, etc.

;-------------------------------------------------------------------------------
; Name .........: TestThirdPartyTarget_SetData
; Category .....: Target2
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestThirdPartyTarget_SetData($appData)
	FuncTrace("TestThirdPartyTarget_SetData", $appData)
	$_TestModule_Target2 = $appData
EndFunc   ;==>TestThirdPartyTarget_SetData

;-------------------------------------------------------------------------------
; Name .........: TestThirdPartyTarget_GetData
; Category .....: Target2
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestThirdPartyTarget_GetData()
	Return $_TestModule_Target2
EndFunc   ;==>TestThirdPartyTarget_GetData

;-------------------------------------------------------------------------------
; Name .........: TestThirdPartyTarget_PatientAppCleanup
; Category .....: Testing phases
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestThirdPartyTarget_PatientAppCleanup($APP_PROCESS_IDENTIFIER, $MAX_TIME_FOR_APP_CLOSE)
	; REF. SNIPPET_MAX_TIME_FOR_APP_CLOSE
	; ACTUAL SNIPPET
	If ProcessExists($APP_PROCESS_IDENTIFIER) Then
		Trace('Waiting max ' & $MAX_TIME_FOR_APP_CLOSE & 's for process to close...')
		ProcessWaitClose($APP_PROCESS_IDENTIFIER, $MAX_TIME_FOR_APP_CLOSE)
	EndIf
	If ProcessExists($APP_PROCESS_IDENTIFIER) Then
		Trace('Process still exists. Closing forcefully now...')
		ProcessClose($APP_PROCESS_IDENTIFIER)
		ProcessWaitClose($APP_PROCESS_IDENTIFIER)
	EndIf
EndFunc   ;==>TestThirdPartyTarget_PatientAppCleanup
