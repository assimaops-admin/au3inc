#include-once
#include <File.au3>

$_TestTarget_OriginalPath = ""
$_TestTarget_LocalPath = ""
$_TestTarget_UseLocalCopy = 0

;-------------------------------------------------------------------------------
; Name .........: TestTarget_SetPath
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestTarget_SetPath($path)
	If Not StringInStr(FileGetAttrib($path), "D") Then
		Dim $szDrive, $szDir, $szFName, $szExt
		_PathSplit($path, $szDrive, $szDir, $szFName, $szExt)
		$tag = FileReadLine($path)
		$path = $szDrive & $szDir & $tag
	EndIf
	$_TestTarget_OriginalPath = $path
EndFunc   ;==>TestTarget_SetPath

;-------------------------------------------------------------------------------
; Name .........: TestTarget_UseLocalCopy
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestTarget_UseLocalCopy($flag = 0)
	$_TestTarget_UseLocalCopy = $flag
EndFunc   ;==>TestTarget_UseLocalCopy

;-------------------------------------------------------------------------------
; Name .........: TestTarget_GetOriginalPath
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TestTarget_GetOriginalPath()
	Return $_TestTarget_OriginalPath
EndFunc   ;==>TestTarget_GetOriginalPath
