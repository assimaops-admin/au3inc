#include-once
; CorpUAFLibs\StdCorpLib\AppCrashReport.au3

; Universal
Const $WERFAULT_EXCEPTN_REPORTER = "WerFault.exe"
Const $WERFAULT_EXCEPTN_WIN_TEXT = "Check online for a solution and close the program"
Const $WERFAULT_EXCEPTN_LOG_FILE = "C:\AppCrashReport.log"

;-------------------------------------------------------------------------------
; Name .........: AppCrashReport_CloseByHWND
; Category .....: Testing phases
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func AppCrashReport_CloseByHWND($hwnd)
	;_DbgOut("CloseCrashReport")
	If $hwnd = "" Then Return
	$reporterPID = WinGetProcess($hwnd)
	;_DbgOut("$reporterPID = " & $reporterPID)
	If $reporterPID <> ProcessExists($WERFAULT_EXCEPTN_REPORTER) Then
		; Warning _
		; "Assertion Failed - " & $g_scrname, _
		; "The exception was expected to be reported by '" & $WERFAULT_EXCEPTN_REPORTER & "'..."
	EndIf
	ProcessClose($reporterPID)
	ProcessWaitClose($reporterPID)
EndFunc   ;==>AppCrashReport_CloseByHWND

;-------------------------------------------------------------------------------
; Name .........: AppCrashReport_CloseIfAny
; Category .....: Testing phases
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func AppCrashReport_CloseIfAny($APP_PROCESS_NAME = "")
	; Wait for crash report window
	; Remark: alternatively, we could look for 'WerFault.exe' process
	; ProcessWait($WERFAULT_EXCEPTN_REPORTER, 3)
	$hwnd = WinWait($APP_PROCESS_NAME, $WERFAULT_EXCEPTN_WIN_TEXT, 3)
	If $hwnd <> "" Then
		; TODO: This is IMPORTANT piece of information, log this! C:\vimupgxcpt.log
		$IMPORTANT_MESSAGE = "AppCrashReport_CloseByHWND invoked by " & @ScriptName & " on the " & @YEAR & "-" & @MON & "-" & @MDAY & " at " & @HOUR & ":" & @MIN & "."
		FileWriteLine($WERFAULT_EXCEPTN_LOG_FILE, $IMPORTANT_MESSAGE)
		AppCrashReport_CloseByHWND($hwnd)
	EndIf
EndFunc   ;==>AppCrashReport_CloseIfAny
