#include-once
#include <CoreUAFLibs\UDFs\Pause.au3>
#include <CoreUAFLibs\UDFsx\HotkeyToText.au3>
#include "..\AbcQa.IntegratedQa\AbcQa.au3"

; #FUNCTION# ====================================================================================================================
; Name...........: PauseV3
; Description ...: The default hotkey is Ctrl+Shift+Pause, it might not work on some systems, the developer may try F9 instead.
; ===============================================================================================================================
Func PauseV3($otherHotkey = "{F9}", $txtMsg = "")
	_Pause($otherHotkey, $txtMsg)
EndFunc   ;==>PauseV3
