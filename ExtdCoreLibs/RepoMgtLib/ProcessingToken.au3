#include-once
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>

Const $PROCESSING_TOKEN_FILENAME = "Processing.token"
Const $PROCESSING_TOKEN_CONTENT = "xrt://" & @ComputerName & "/" & @ScriptName & "[" & @AutoItPID & "]"

;-------------------------------------------------------------------------------
; Name .........: IsProcessingToken
; Description ..:
; Preconditions :
; Return .......: boolean - true if the token is set
;-------------------------------------------------------------------------------
Func IsProcessingToken($dir)
	Return FileExists($dir & "\" & $PROCESSING_TOKEN_FILENAME)
EndFunc   ;==>IsProcessingToken

;-------------------------------------------------------------------------------
; Name .........: SetProcessingToken
; Description ..:
; Preconditions :
; Return .......: string - token content
;-------------------------------------------------------------------------------
Func SetProcessingToken($dir, $set)
	$ret = 0
	$currentToken = $PROCESSING_TOKEN_CONTENT
	$tokenFilePath = $dir & "\" & $PROCESSING_TOKEN_FILENAME
	If $set Then
		; create destination directory
		If Not FileExists($dir) Then DirCreate($dir)
		; exception
		If IsProcessingToken($dir) Then
			; notify admin
			$msg = "[ProcessingToken]" _
					 & @CRLF & "File '" & $PROCESSING_TOKEN_FILENAME & "' exists: repository is in use." _
					 & @CRLF & "($tokenFilePath=" & $tokenFilePath & ")" _
					 & @CRLF & "($currentToken=" & $currentToken & ")" _
					 & ""
			FatalError_PreCondition($msg)
			$ret = 0
		Else
			; perform
			$fileContent = $currentToken
			FileWriteLine($tokenFilePath, $fileContent)
			$ret = 1
		EndIf
	Else
		; remove lock
		If Not IsProcessingToken($dir) Then
			; notify admin?
			$ret = 0
		Else
			; check the content
			$fileContent = FileReadLine($tokenFilePath)
			If $fileContent <> $currentToken Then
				; notify admin
				$msg = "[ProcessingToken]" _
						 & @CRLF & "File '" & $PROCESSING_TOKEN_FILENAME & "' was created by another process." _
						 & @CRLF & "($tokenFilePath=" & $tokenFilePath & ")" _
						 & @CRLF & "($fileContent=" & $fileContent & ")" _
						 & @CRLF & "($currentToken=" & $currentToken & ")" _
						 & ""
				Warning($msg)
				$ret = 0
			Else
				; perform
				FileDelete($tokenFilePath)
				$ret = 1
			EndIf
		EndIf
	EndIf
	Return $ret
EndFunc   ;==>SetProcessingToken

;-------------------------------------------------------------------------------
; Name .........: SetProcessingTokens
; Description ..:
; Preconditions :
; Return .......: string - token content
;-------------------------------------------------------------------------------
Func SetProcessingTokens($dir1, $dir2, $set)
	$ret = 0
	If SetProcessingToken($dir1, $set) Then
		If SetProcessingToken($dir2, $set) Then
			$ret = 1
		EndIf
	EndIf
	Return $ret
EndFunc   ;==>SetProcessingTokens
