#include-once
#include <Date.au3>
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>

Func TimestampV2_GetAgeInWeeks($stamp)
	Return TimestampV2_GetAgeInUnits($stamp)
EndFunc   ;==>TimestampV2_GetAgeInWeeks

Func TimestampV2_GetAgeInDays($stamp)
	Return TimestampV2_GetAgeInUnits($stamp, 'd')
EndFunc   ;==>TimestampV2_GetAgeInDays

Func TimestampV2_GetAgeInUnits($stamp, $age_unit = 'w')
	$year = StringLeft($stamp, 4)
	If Not StringIsInt($year) Then FatalError("[" & @ScriptName & "]" & " " & "Invalid timestamp '" & $stamp & "' (Tag1).")
	$stamp = StringTrimLeft($stamp, 4)
	$mon = StringLeft($stamp, 2)
	If Not StringIsInt($mon) Then FatalError("[" & @ScriptName & "]" & " " & "Invalid timestamp '" & $stamp & "' (Tag2).")
	$stamp = StringTrimLeft($stamp, 3)
	$mday = StringLeft($stamp, 2)
	If Not StringIsInt($mday) Then FatalError("[" & @ScriptName & "]" & " " & "Invalid timestamp '" & $stamp & "' (Tag3).")
	$tmo_date = $year & "/" & $mon & "/" & $mday
	$diff = _DateDiff($age_unit, $tmo_date, _NowCalc())
	Return $diff
EndFunc   ;==>TimestampV2_GetAgeInUnits
