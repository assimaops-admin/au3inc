:--------------------------------------
@REM When using @"%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system",
@REM it seems that in some cases the test always failed, even after being elevated.
@REM In my case when the script was called by my application.
@REM ALTERNATIVE #1:
@REM @net session
@REM ALTERNATIVE #2:
@REM @fsutil dirty query %systemdrive%

@>nul 2>&1 net session
@if '%errorlevel%' NEQ '0' (
	@echo Requesting administrative privileges...
	@goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
	@> "%temp%\.getadmin.vbs" echo Set UAC = CreateObject^("Shell.Application"^)
	@setlocal
		@set "_params= %*"
		@set "_params=SPECIAL_QUOTED_STRING"%_params:"=""%"
		@set "_params=%_params:~22%"
		@if "%_params%"==" " set "_params="
		@>> "%temp%\.getadmin.vbs" echo UAC.ShellExecute "cmd.exe", "/c """"%~f0""%_params%""", "", "runas", 1
		@"%temp%\.getadmin.vbs"
		@REM @del "%temp%\.getadmin.vbs"
	@endlocal
	@exit /B
:gotAdmin
	@pushd "%~dp0"
:--------------------------------------

@setlocal
	@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra-n\Git
	@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~n\Git
	@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~2\Git
	@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~1\Git
	@set PrivateModulePath=%~dp0..\AssimaAdminMods
	@set PrivateModuleRepo=https://assimaops-admin@bitbucket.org/assimaops-admin/au3priv.git
	@set PrivateBranchRebase=master
	@set PrivModIsAlreadyDl=1
	@if not exist "%PrivateModulePath%\.git" set PrivModIsAlreadyDl=0
	@if %PrivModIsAlreadyDl%==0 "%GITROOT%\bin\git.exe" clone "%PrivateModuleRepo%" "%PrivateModulePath%"
	@if %PrivModIsAlreadyDl%==1 pushd "%PrivateModulePath%"
	@if %PrivModIsAlreadyDl%==1 echo current dir is %CD%
	@if %PrivModIsAlreadyDl%==1 "%GITROOT%\bin\git.exe" pull --rebase origin-as-admin %PrivateBranchRebase%
	@if %PrivModIsAlreadyDl%==1 popd
@endlocal
@pause

:--------------------------------------
	@popd
:--------------------------------------
