#include-once
#include <String.au3>
#include <Array.au3>
#include ".\UDFs\AutoItEx.String.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.StringRegExpReplaceV2.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.StringRegExpV1.au3" ; <<< EMBEDDED_INCLUDE

;
;   AREAS
; #region TEXT-RELATED
; #region HTML-RELATED
; #region REGEXP-RELATED
;

#region TEXT-RELATED

Const $STRING_HORIZONTAL_WHITESPACE_CHARS = " " & @TAB
Const $STRING_VERTICAL_WHITESPACE_CHARS = @CR & @LF
Const $STRING_WHITESPACE_CHARS = $STRING_VERTICAL_WHITESPACE_CHARS & $STRING_VERTICAL_WHITESPACE_CHARS


;-------------------------------------------------------------------------------
; Name .........: BinaryToStringV2
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func BinaryToStringV2($expression, $flag)
	$isTruncated = False
	Const $MAX_BUFFER = 65534
	Const $OVERFLOW_WARN_SUFFIX = @CRLF & "(...)" & @CRLF & "-- Data truncated to prevent buffer overflow --"
	$resTxt = ""
	; truncate string
	$strlen = StringLen($expression)
	If StringLen($expression) > $MAX_BUFFER Then
		$isTruncated = True
		$expression = StringLeft($expression, $MAX_BUFFER - StringLen(StringToBinary($OVERFLOW_WARN_SUFFIX, $flag)))
		$resTxt = BinaryToString($expression, $flag) & $OVERFLOW_WARN_SUFFIX
	Else
		$resTxt = BinaryToString($expression, $flag)
	EndIf
	Return $resTxt
EndFunc   ;==>BinaryToStringV2

;-------------------------------------------------------------------------------
; Name .........: StringRegExpSingleEx
; Description ..: Get the part of the string that matches the pattern.
; Return .......: set @error to 3 if multiple matches
;-------------------------------------------------------------------------------
Func StringRegExpSingleEx($string, $pattern)
	$found = StringRegExpSingle($string, $pattern, True)
	If @error Then
		SetError(@error, 0, "")
	Else
		Return $found
	EndIf
	Return ""
EndFunc   ;==>StringRegExpSingleEx

;-------------------------------------------------------------------------------
; Name .........: StringRegExpSingle
; Description ..: 'StringRegExpSingleEx' with 'isSearchParenthesised' set false
; Return .......:
;-------------------------------------------------------------------------------
Func StringRegExpSingle($string, $pattern, $isSearchParenthesised = False)
	$matchesExpectedCount = 1
	If $isSearchParenthesised Then $matchesExpectedCount = 2
	$regexpMatches = StringRegExp($string, $pattern, 2)
	If Not @error Then
		If UBound($regexpMatches) = $matchesExpectedCount Then
			Return $regexpMatches[UBound($regexpMatches) - 1]
		Else
			SetError(3, 0, "")
		EndIf
	Else
		SetError(@error, 0, "")
	EndIf
	Return ""
EndFunc   ;==>StringRegExpSingle

;-------------------------------------------------------------------------------
; Name .........: StringRegExpV2 (ParenthesisedSearch)
; Description ..: like 'StringRegExp' but always returns an array with items count
;                 To be used for parenthesised search
; Return .......: Array
;-------------------------------------------------------------------------------
Func StringRegExpV2($str, $pat)
	$matches = StringRegExp($str, $pat, 3)
	If @error Then
		$matches = _ArrayCreate(0)
	Else
		_ArrayInsert($matches, 0, UBound($matches))
	EndIf
	Return $matches
EndFunc   ;==>StringRegExpV2

;-------------------------------------------------------------------------------
; Name .........: StringInStrCount
; Description ..:
; Return .......: number of occurences of a given string
;-------------------------------------------------------------------------------
Func StringInStrCount($szString1, $szString2)
	StringReplace($szString1, $szString2, $szString2)
	Return @extended
EndFunc   ;==>StringInStrCount

;-------------------------------------------------------------------------------
; Name .........: StringReplaceXmlValue
; Description ..:
; Return .......:
; Note .........:
;-------------------------------------------------------------------------------
Func StringReplaceXmlValue($XmlContent, $Key, $NewValue, $IsCData = False)
	$Searched = "<" & $Key & ">.*?</" & $Key & ">"
	$Replaced = "<" & $Key & ">" & $NewValue & "</" & $Key & ">"
	If $IsCData Then
		$Replaced = "<" & $Key & "><![CDATA[" & $NewValue & "]]></" & $Key & ">"
	EndIf
	Return StringRegExpReplace($XmlContent, $Searched, $Replaced, 1)
EndFunc   ;==>StringReplaceXmlValue

;-------------------------------------------------------------------------------
; Name .........: _StringBetweenV2
; Description ..: Enhancements for a more coherent behaviour of _StringBetween
; Return .......:
; Note .........:
; History ......: JUL 8, 2010 - Created
;                            - Remove last parameter
;                             (cf. AutoIT Script breaking change - 2009/05/03)
;-------------------------------------------------------------------------------
Func _StringBetweenV2($sString, $sStart, $sEnd, $vCase = Default)
	Dim $empty_array[1] = [0]
	$results_array = _StringBetween($sString, $sStart, $sEnd, $vCase)
	If Not IsArray($results_array) Then
		Return $empty_array
	Else
		_ArrayInsert($results_array, 0, UBound($results_array))
		Return $results_array
	EndIf
EndFunc   ;==>_StringBetweenV2

;-------------------------------------------------------------------------------
; Name .........: StringReplaceRightSeqs
; Description ..: Remove trailing character sequences (eg. extra empty lines)
; Return .......: @extended - number of replacements
; Note .........: KEEP IN SYNC WITH 'StringReplaceLeftSeqs' FUNCTION
; History ......: JUL 8, 2010 - Created
;-------------------------------------------------------------------------------
Func StringReplaceRightSeqs($sString, $sChars, $sRepl, $casesense = 0)
	; exceptions
	If Not IsString($sString) Then Return SetError(1, 0, $sString)
	If Not IsString($sChars) Then Return SetError(1, 0, $sString)
	If Not IsString($sRepl) Then Return SetError(1, 0, $sString)
	; shortcuts
	If StringLen($sString) < StringLen($sChars) Then Return $sString
	; processing
	$cRepl = 0
	$lastSeq = StringRight($sString, StringLen($sChars))
	While StringCompare($lastSeq, $sChars, $casesense) = 0
		$sString = StringTrimRight($sString, StringLen($sChars))
		$sString = $sString & $sRepl
		$cRepl += 0
		If StringLen($sString) < StringLen($sChars) Then ExitLoop
		$lastSeq = StringRight($sString, StringLen($sChars))
	WEnd
	Return SetError(0, $cRepl, $sString)
EndFunc   ;==>StringReplaceRightSeqs

;-------------------------------------------------------------------------------
; Name .........: StringReplaceLeftSeqs
; Description ..: Remove preceeding character sequences (eg. extra empty lines)
; Return .......: @extended - number of replacements
; Note .........: KEEP IN SYNC WITH 'StringReplaceRightSeqs' FUNCTION
; History ......: JUL 8, 2010 - Created
;-------------------------------------------------------------------------------
Func StringReplaceLeftSeqs($sString, $sChars, $sRepl, $casesense = 0)
	; exceptions
	If Not IsString($sString) Then Return SetError(1, 0, $sString)
	If Not IsString($sChars) Then Return SetError(1, 0, $sString)
	If Not IsString($sRepl) Then Return SetError(1, 0, $sString)
	; shortcuts
	If StringLen($sString) < StringLen($sChars) Then Return $sString
	; processing
	$cRepl = 0
	$firstSeq = StringLeft($sString, StringLen($sChars))
	While StringCompare($firstSeq, $sChars, $casesense) = 0
		$sString = StringTrimLeft($sString, StringLen($sChars))
		$sString = $sRepl & $sString
		$cRepl += 0
		If StringLen($sString) < StringLen($sChars) Then ExitLoop
		$firstSeq = StringLeft($sString, StringLen($sChars))
	WEnd
	Return SetError(0, $cRepl, $sString)
EndFunc   ;==>StringReplaceLeftSeqs

;-------------------------------------------------------------------------------
; Name .........: StringReplaceRightChars
; Description ..: Replace trailing whispaces ('space' and 'tab' characters)
; Return .......: @extended - number of replacements
; Note .........: KEEP IN SYNC WITH 'StringReplaceLeftChars' FUNCTION
; History ......: JUL 8, 2010 - Created
;-------------------------------------------------------------------------------
Func StringReplaceRightChars($sString, $sChars, $sRepl, $casesense = 0)
	; exceptions
	If Not IsString($sString) Then Return SetError(1, 0, $sString)
	If Not IsString($sChars) Then Return SetError(1, 0, $sString)
	If Not IsString($sRepl) Then Return SetError(1, 0, $sString)
	; shortcuts
	If StringLen($sString) = 0 Then Return $sString
	; processing
	$cRepl = 0
	$lastChar = StringRight($sString, 1)
	While StringInStr($sChars, $lastChar, $casesense)
		$sString = StringTrimRight($sString, 1)
		$cRepl += 0
		If StringLen($sString) = 0 Then ExitLoop
		$lastChar = StringRight($sString, 1)
	WEnd
	; replace characters removed
	If $sRepl <> "" Then
		$sString = $sString & _StringRepeat($sRepl, $cRepl)
	EndIf
	Return SetError(0, $cRepl, $sString)
EndFunc   ;==>StringReplaceRightChars

;-------------------------------------------------------------------------------
; Name .........: StringReplaceLeftChars
; Description ..: Replace preceeding whispaces ('space' and 'tab' characters)
; Return .......: @extended - number of replacements
; Note .........: KEEP IN SYNC WITH 'StringReplaceRightChars' FUNCTION
; History ......: JUL 27, 2010 - Created
;-------------------------------------------------------------------------------
Func StringReplaceLeftChars($sString, $sChars, $sRepl, $casesense = 0)
	; exceptions
	If Not IsString($sString) Then Return SetError(1, 0, $sString)
	If Not IsString($sChars) Then Return SetError(1, 0, $sString)
	If Not IsString($sRepl) Then Return SetError(1, 0, $sString)
	; shortcuts
	If StringLen($sString) = 0 Then Return $sString
	; processing
	$cRepl = 0
	$firstChar = StringLeft($sString, 1)
	While StringInStr($sChars, $firstChar, $casesense)
		$sString = StringTrimLeft($sString, 1)
		$cRepl += 0
		If StringLen($sString) = 0 Then ExitLoop
		$firstChar = StringLeft($sString, 1)
	WEnd
	; replace characters removed
	If $sRepl <> "" Then
		$sString = _StringRepeat($sRepl, $cRepl) & $sString
	EndIf
	Return SetError(0, $cRepl, $sString)
EndFunc   ;==>StringReplaceLeftChars

;-------------------------------------------------------------------------------
; Name .........: StringInStrV2
; Parameters ...:
; Description ..: See StringInStr.
; Return .......: Returns the position of the substring.
;-------------------------------------------------------------------------------
Func StringInStrV2($string, $substring, $casesense = 0, $occurrence = 1, $start = Default, $count = Default)
	If ($start = Default) And ($count = Default) Then
		Return StringInStr($string, $substring, $casesense, $occurrence)
	Else
		Return $start + StringInStr(StringMid($string, $start, $count), $substring, $casesense, $occurrence) - 1
	EndIf
EndFunc   ;==>StringInStrV2

;-------------------------------------------------------------------------------
; Name .........: StringReplaceFrom
; Parameters ...:
; Description ..: See StringInStr.
; Return .......: Returns the new string
;-------------------------------------------------------------------------------
Func StringReplaceFrom($string, $substring, $repl, $count, $from)
	$left = StringLeft($string, $from)
	$right = StringTrimLeft($string, $from)
	$newright = StringReplace($right, $substring, $repl, $count)
	Return $left & $newright
EndFunc   ;==>StringReplaceFrom

;-------------------------------------------------------------------------------
; Name .........: StringLowerFirstChar
; Parameters ...:
; Description ..: See StringInStr.
; Return .......: Returns the new string
;-------------------------------------------------------------------------------
Func StringLowerFirstChar($string)
	Return StringLower(StringLeft($string, 1)) & StringTrimLeft($string, 1)
EndFunc   ;==>StringLowerFirstChar

;-------------------------------------------------------------------------------
; Name .........: StringMinimizeNumberOfEmtpyLines
; Parameters ...:
; Description ..: Remove all multiple successive empty lines.
;                 Preserve paragraphs separation.
; Return .......: Returns the new string
;-------------------------------------------------------------------------------
Func StringMinimizeNumberOfEmtpyLines($string)
	$string = StringReplace($string, @CRLF & @CRLF & @CRLF, @CRLF & @CRLF)
	While @extended
		$string = StringReplace($string, @CRLF & @CRLF & @CRLF, @CRLF & @CRLF)
	WEnd
	Return $string
EndFunc   ;==>StringMinimizeNumberOfEmtpyLines


#region REGEXP-RELATED

;-------------------------------------------------------------------------------
; Name .........: MLREP_ParagraphFromFullLineV1
; Description ..: Multi-Line Regular Expression Pattern (MLREP)
;-------------------------------------------------------------------------------
Func MLREP_ParagraphFromFullLineV1($delimStartRaw, $patternVersion = Default)
	If $delimStartRaw = "" Then Return ""
	Local $s = "\Q" & $delimStartRaw & "\E"
	Return "(?s)(?U)\r\n" & $s & ".*\r\n(?:\r\n|\z)" ; << version 1.0.0
EndFunc   ;==>MLREP_ParagraphFromFullLineV1

;-------------------------------------------------------------------------------
; Name .........: MLREP_ParagraphFromFullLineVx
; Description ..: Multi-Line Regular Expression Pattern (MLREP)
;-------------------------------------------------------------------------------
Func MLREP_ParagraphFromFullLineVx($delimStartRaw, $patternVersion = Default)
	If $delimStartRaw = "" Then Return ""
	Local $s = "\Q" & $delimStartRaw & "\E"
	;Return "(?s)(?U)\r\n" & $s & ".*\r\n(?:\r\n|\z)"                         ; << version 1.0.0
	;Return "(?m)(?U)^" & $s & "\r\n(?:.+\r\n)*(?:(?=\r\n)|(?=\z))"           ; << version 2.0.0 # do not include preceding EOL characters
	;Return "(?m)(?U)^" & $s & "\r\n(?:.+(?:\r\n|\z))*(?=\r\n|\z)"            ; << version 2.1.0 # allow the paragraph to terminate the file without EOL characters
	;Return "(?m)(?U)^" & $s & "(?:\r\n)?(?:.+(?:\r\n|\z))*(?=\r\n|\z)"       ; << version 2.1.1 # (UNTESTED!) allow one-liner paragraphs at the end of file
	Return "(?m)^" & $s & "(?:\r\n.+)*(?:.+)(?=\r\n|\z)(?=\r\n|\z)" ; << version 2.2.0 # do not capture the last EOL characters
EndFunc   ;==>MLREP_ParagraphFromFullLineVx

;-------------------------------------------------------------------------------
; Name .........: MLRE_ExtractParagraphV1
; Description ..: Multi-Line Regular Expression Pattern (MLREP)
;-------------------------------------------------------------------------------
Func MLRE_ExtractParagraphFromFullLineV1($teststr, $delimStartRaw, $patternVersion = Default)
	$arrfound = StringRegExp($teststr, MLREP_ParagraphFromFullLineV1($delimStartRaw), 1)
	If @error Then Return ""
	Return $arrfound[0]
EndFunc   ;==>MLRE_ExtractParagraphFromFullLineV1

;-------------------------------------------------------------------------------
; Name .........: MLRE_ExtractParagraphVx
; Description ..: Multi-Line Regular Expression Pattern (MLREP)
;-------------------------------------------------------------------------------
Func MLRE_ExtractParagraphFromFullLineVx($teststr, $delimStartRaw, $patternVersion = Default)
	$arrfound = StringRegExp($teststr, MLREP_ParagraphFromFullLineV1($delimStartRaw), 1)
	If @error Then Return ""
	Return $arrfound[0]
EndFunc   ;==>MLRE_ExtractParagraphFromFullLineVx

#endregion REGEXP-RELATED

#region HTML-RELATED

;-------------------------------------------------------------------------------
; Name .........: HtmlSpecialChars
; Parameters ...:
; Return .......:
; History ......: SEP 20, 2010 - Created
;                 (see http://php.net/manual/en/function.HtmlSpecialChars.php)
;-------------------------------------------------------------------------------
Enum $ENT_COMPAT, $ENT_NOQUOTES, $ENT_QUOTES
Func HtmlSpecialChars($text, $quote_style = $ENT_COMPAT)
	$text = StringReplace($text, '&', '&amp;')
	If $quote_style <> $ENT_NOQUOTES Then
		$text = StringReplace($text, '"', '&quot;')
	EndIf
	If $quote_style = $ENT_QUOTES Then
		$text = StringReplace($text, "'", '&#039;')
	EndIf
	$text = StringReplace($text, '<', '&lt;')
	$text = StringReplace($text, '>', '&gt;')
	Return $text
EndFunc   ;==>HtmlSpecialChars

;-------------------------------------------------------------------------------
; Name .........: Tabs2TabsDds
; Parameters ...:
; Return .......:
; Testing ......:
; 					$str1 = ""
; 					$str1 &= @TAB&@TAB&@TAB&@TAB&"line 1"&@CRLF
; 					$str1 &= "line 2 has no tabs"&@CRLF
; 					$str1 &= @TAB&"| line 3"&@CRLF
; 					$str1 &= "line 4 |"&@TAB&"| has a tab in the middle"&@CRLF
; 					;//; $str1 &= @TAB&@TAB&@TAB&@TAB&@TAB&"| line 5 has too many tabs" ; XCPT
; 					$str1 &= @TAB&@TAB&@TAB&"| line 6 has 3 tabs"&@CRLF
; 					$str1 &= "last line ends with a tab (even if it is stupid) |"&@TAB&@CRLF
; 					$str2 = Tabs2TabsDds($str1)
; 					ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $str2 = ' & @CRLF & _
; 						$str2 & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
;
; History ......: JAN 21, 2011 - Created
;-------------------------------------------------------------------------------
Func Tabs2TabsDds($string, $is_xhtml = 1)

	; TODO: <dd> tag needs to be closed!! </dd>
	;NotImplemented("Tabs2TabsDds")

	Const $MAX_SEQ_TABS_SUPPORTED = 4
	$tab_tag = '<dd>'
	;if $is_xhtml then $tab_tag = '<dd />'
	If StringInStr($string, _StringRepeat(@TAB, $MAX_SEQ_TABS_SUPPORTED + 1)) Then
		; NotImplemented("[HtmlSpecialChars.au3] MAX_SEQ_TABS_SUPPORTED reached.")
	EndIf
	For $tabs_c = $MAX_SEQ_TABS_SUPPORTED To 1 Step -1
		$tabs_s = _StringRepeat(@TAB, $tabs_c)
		$tabs_t = _StringRepeat($tab_tag, $tabs_c)
		$not_tab = "([^" & '\t' & "])"
		$string = StringRegExpReplace($string, "\A" & $tabs_s & $not_tab, $tabs_s & $tabs_t & "\1")
		$string = StringRegExpReplace($string, $not_tab & $tabs_s & $not_tab, "\1" & $tabs_s & $tabs_t & "\2")
		$string = StringRegExpReplace($string, $not_tab & $tabs_s & "\Z", "\1" & $tabs_s & $tabs_t)
	Next
	Return $string

EndFunc   ;==>Tabs2TabsDds

;-------------------------------------------------------------------------------
; Name .........: Nl2BrNl
; Parameters ...:
; Return .......:
; History ......: SEP 20, 2010 - Created
;                 (see http://php.net/manual/en/function.nl2br.php)
;-------------------------------------------------------------------------------
Func Nl2BrNl($string, $is_xhtml = 1)
	$eol_tag = '<br>'
	If $is_xhtml Then $eol_tag = '<br />'
	$string = StringRegExpReplace($string, "\A" & @LF, $eol_tag & @LF)
	$string = StringRegExpReplace($string, "([^" & @CR & "])" & @LF, "\1" & $eol_tag & @LF)
	$string = StringReplace($string, @CRLF, $eol_tag & @CRLF)
	Return $string
EndFunc   ;==>Nl2BrNl

;-------------------------------------------------------------------------------
; Name .........: ToHtml
; Parameters ...:
; Return .......:
; History ......: JAN 21, 2011 - Created
;                    Gather multiple HTML-formatting related functions
;-------------------------------------------------------------------------------
Func ToHtml($text, $is_xhtml = 1)
	$html = HtmlSpecialChars($text)
	$html = Nl2BrNl($html, $is_xhtml)
	Return $html
EndFunc   ;==>ToHtml

#endregion HTML-RELATED
