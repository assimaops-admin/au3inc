#include-once
#include <Array.au3>
#include ".\UDFs\AutoItEx.ArrayV2.Lite.au3"

;-------------------------------------------------------------------------------
; Name .........: _ArrayDeleteV2
; Description ..: Remove element (by index) and update items count
; Return .......: Success: Returns 1 and the original Array is updated.
;                 Failure: Returns 0 and the original Array.
;                 @Error: 0 = No error.
;                         1 = $avArray isn't an array.
;                         2 = $avArray isn't a V2 array ($avArray[0] = cout).
;                         3 = there is no item to remove - or - invalid index.
; History ......: OCT 12, 2010 - Created
;-------------------------------------------------------------------------------
Func _ArrayDeleteV2(ByRef $avArray, $iElement)
	If Not IsArray($avArray) Then
		SetError(1)
		Return 0
	ElseIf Not IsInt($avArray[0]) Then
		SetError(2)
		Return 0
	ElseIf $iElement < 0 Or $iElement >= UBound($avArray) Then
		SetError(3)
		Return 0
	Else
		$ret = _ArrayDelete($avArray, $iElement)
		If $ret Then
			$avArray[0] -= 1
			SetError(0)
			Return 1
		Else
			SetError(@error)
			Return 0
		EndIf
	EndIf
EndFunc   ;==>_ArrayDeleteV2

;-------------------------------------------------------------------------------
; Name .........: _ArrayPopV2
; Description ..:
; Return .......:
; History ......: OCT 12, 2010 - Created
;-------------------------------------------------------------------------------
Func _ArrayPopV2(ByRef $avArray)
	If Not IsArray($avArray) Then
		SetError(1)
		Return ""
	ElseIf Not IsInt($avArray[0]) Then
		SetError(2)
		Return ""
	ElseIf $avArray[0] = 0 Then
		SetError(2)
		Return ""
	Else
		$ret = _ArrayPop($avArray)
		If Not @error Then
			$avArray[0] -= 1
			SetError(0)
			Return $ret
		Else
			SetError(@error)
			Return $ret
		EndIf
	EndIf
EndFunc   ;==>_ArrayPopV2

;-------------------------------------------------------------------------------
; Name .........: _ArraySearchV2
; Description ..: Search an array where the first index is the items count
; Return .......:
;-------------------------------------------------------------------------------
Func _ArraySearchV2(Const ByRef $avArray, $vValue)
	Return _ArraySearch($avArray, $vValue, 1)
EndFunc   ;==>_ArraySearchV2

;-------------------------------------------------------------------------------
; Name .........: _ArrayDisplayTwo
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func _ArrayDisplayTwo(Const ByRef $strings_array1, Const ByRef $strings_array2, $sTitle = "ListView array 1D and 2D Display", _
		$iItemLimit = -1, $iTranspose = 0, $sReplace = "|", $sHeader = "")

	$val1 = UBound($strings_array1)
	$val2 = UBound($strings_array2)
	If $val1 > $val2 Then
		$maxUBound = $val1
	Else
		$maxUBound = $val2
	EndIf

	Dim $aGathered[$maxUBound][2]
	For $i = 0 To $val1 - 1
		$aGathered[$i][0] = $strings_array1[$i]
	Next
	For $i = 0 To $val2 - 1
		$aGathered[$i][1] = $strings_array2[$i]
	Next
	_ArrayDisplay($aGathered, $sTitle, $iItemLimit, $iTranspose, $sReplace, $sHeader)

EndFunc   ;==>_ArrayDisplayTwo

;-------------------------------------------------------------------------------
; Name .........: _ArrayExtractColumn
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func _ArrayExtractColumn(Const ByRef $array, $column = 0, $first_index = 0, $last_index = Default)

	If $last_index = Default Then
		$last_index = UBound($array) - 1
	EndIf

	Dim $ret[$last_index - $first_index + 1]
	For $i = $first_index To $last_index
		$ret[$i - $first_index] = $array[$i][$column]
	Next

	Return $ret

EndFunc   ;==>_ArrayExtractColumn

;-------------------------------------------------------------------------------
; Name .........: _ArrayTranspose
; Description ..: Transpose array dimensions
; Return .......: Success: Returns 1 and the modified array.
;                 Failure: Returns a 1-element array with the value added to it.
;                 @Error: 0 = No error.
;                         1 = $avArray isn't an array.
;                         2 = $avArray isn't a V2 array ($avArray[0] = cout).
; History ......: OCT 12, 2010 - Updated to handle the case when the variable
;                   given in parameter is not an array
;-------------------------------------------------------------------------------
Func _ArrayTranspose(ByRef $avArray)
	Local $avTransposedArray = 0
	If Not IsArray($avArray) Then
		SetError(1)
		Return $avTransposedArray
	ElseIf Not IsInt($avArray[0]) Then
		SetError(2)
		Return $avTransposedArray
	Else
		;$dimensions = UBound($avArray)
		;For $dimention = 1 To $dimensions
		For $i = 0 To UBound($avArray, 1) - 1
			For $j = 0 To UBound($avArray, 2) - 1
				$avTransposedArray[$j][$i] = $avArray[$i][$j]
			Next
		Next
	EndIf
	Return $avTransposedArray
EndFunc   ;==>_ArrayTranspose

;-------------------------------------------------------------------------------
; Name .........: _ArrayConsolidate
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func _ArrayConsolidate(ByRef $avArray)

	;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $avArray = ' & $avArray & @crlf)

	If UBound($avArray, 0) >= 2 Then Return SetError(1)
	If UBound($avArray) = 0 Then Return SetError(2)
	If Not IsArray($avArray[0]) Then Return SetError(3)
	If UBound($avArray[0]) = 0 Then Return SetError(4)

	;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : UBound($avArray) = ' & UBound($avArray) & @crlf)
	;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : UBound($avArray[0]) = ' & UBound($avArray[0]) & @crlf)

	Dim $avArrayCpy[UBound($avArray)][UBound($avArray[0])]

	For $i = 0 To UBound($avArray) - 1
		;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $i = ' & $i & @crlf)
		Local $subarray = $avArray[$i]
		For $j = 0 To UBound($subarray) - 1
			;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $j = ' & $j & @crlf)
			;-;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $subarray[ = ' & $subarray[$j] & @crlf)
			$avArrayCpy[$i][$j] = $subarray[$j]
		Next
	Next

	$avArray = $avArrayCpy

EndFunc   ;==>_ArrayConsolidate

;-------------------------------------------------------------------------------
; Name .........: _ArrayDisplayAny
; Description ..: _ArrayDisplayArrOfArrs and _ArrayDisplayMultiDim
; Return .......:
;-------------------------------------------------------------------------------
Func _ArrayDisplayAny(Const ByRef $avArray, $sTitle = "ListView array 1D and 2D Display", _
		$iItemLimit = -1, $iTranspose = 0, $sReplace = "|", $sHeader = "")

	$avArrayCpy = $avArray
	_ArrayConsolidate($avArrayCpy)
	;-;ConsoleWrite('@ Debug(>Error code: ' & @error & @crlf)
	_ArrayDisplay($avArrayCpy, $sTitle, $iItemLimit, $iTranspose, $sReplace, $sHeader)

EndFunc   ;==>_ArrayDisplayAny

;;;
;;; TRY-ME-STYLE QUICK TEST
;;;
;~ Dim $testarr[3][3] = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
;~ Dim $testarr[3] = [_ArrayCreate(1, 2, 3),_ArrayCreate(4, 5, 6),_ArrayCreate(7, 8, 9)]
;~ $testarr = _ArrayCreate(_ArrayCreate(1, 2, 3),_ArrayCreate(4, 5, 6),_ArrayCreate(7, 8, 9))
;_ArrayDisplay($testarr)
;~ _ArrayDisplayAny($testarr)

; #FUNCTION# ====================================================================================================================
; Name...........: _ArrayRESearch
; Description ...: Finds an entry within a 1D or 2D array. Similar to _ArrayBinarySearch(), except that the array does not need to be sorted.
; Syntax.........: _ArraySearch(Const ByRef $avArray, $vValue[, $iStart = 0[, $iEnd = 0[, $iCase = 0[, $iPartial = 0[, $iForward = 1[, $iSubItem = -1]]]]]])
; Parameters ....: $avArray  - The array to search
;                  $vValue   - What to search $avArray for
;                  $iStart   - [optional] Index of array to start searching at
;                  $iEnd     - [optional] Index of array to stop searching at
;                  $iCase    - [optional] If set to 1, search is case sensitive
;                  $iPartial - [optional] If set to 1, executes a partial search
;                  $iForward - [optional] If set to 0, searches the array from end to beginning (instead of beginning to end)
;                  $iSubItem - [optional] Sub-index to search on in 2D arrays
; Return values .: Success - The index that $vValue was found at
;                  Failure - -1, sets @error:
;                  |1 - $avArray is not an array
;                  |2 - $avArray is not a 1 or 2 dimensional array
;                  |4 - $iStart is greater than $iEnd
;                  |6 - $vValue was not found in array
;                  |7 - $avArray has too many dimensions
;                  |(3, 5 - Deprecated error codes)
; Author ........: SolidSnake <MetalGX91 at GMail dot com>
; Modified.......: gcriaco <gcriaco at gmail dot com>, Ultima - 2D arrays supported, directional search, code cleanup, optimization
; Remarks .......: This function might be slower than _ArrayBinarySearch() but is useful when the array's order can't be altered.
; Related .......: _ArrayBinarySearch, _ArrayFindAll
; Link ..........:
; Example .......: Yes
; ===============================================================================================================================
; DONE: Copied from _ArrayRESearch + Replaced string equality by StringRegExp
; TODO: scrap $iPartial, etc.
Func _ArrayRESearch(Const ByRef $avArray, $vValue, $iStart = 0, $iEnd = 0, $iCase = 0, $iPartial = 0, $iForward = 1, $iSubItem = -1)
	If Not IsArray($avArray) Then Return SetError(1, 0, -1)
	If UBound($avArray, 0) > 2 Or UBound($avArray, 0) < 1 Then Return SetError(2, 0, -1)

	Local $iUBound = UBound($avArray) - 1

	; Bounds checking
	If $iEnd < 1 Or $iEnd > $iUBound Then $iEnd = $iUBound
	If $iStart < 0 Then $iStart = 0
	If $iStart > $iEnd Then Return SetError(4, 0, -1)

	; Direction (flip if $iForward = 0)
	Local $iStep = 1
	If Not $iForward Then
		Local $iTmp = $iStart
		$iStart = $iEnd
		$iEnd = $iTmp
		$iStep = -1
	EndIf

	; Search
	Switch UBound($avArray, 0)
		Case 1 ; 1D array search
			If Not $iPartial Then
				If Not $iCase Then
					For $i = $iStart To $iEnd Step $iStep
						If StringRegExp($avArray[$i], $vValue) Then Return $i
					Next
				Else
					For $i = $iStart To $iEnd Step $iStep
						If StringRegExp($avArray[$i], $vValue) Then Return $i
					Next
				EndIf
			Else
				For $i = $iStart To $iEnd Step $iStep
					If StringInStr($avArray[$i], $vValue, $iCase) > 0 Then Return $i
				Next
			EndIf
		Case 2 ; 2D array search
			Local $iUBoundSub = UBound($avArray, 2) - 1
			If $iSubItem > $iUBoundSub Then $iSubItem = $iUBoundSub
			If $iSubItem < 0 Then
				; will search for all Col
				$iSubItem = 0
			Else
				$iUBoundSub = $iSubItem
			EndIf

			For $j = $iSubItem To $iUBoundSub
				If Not $iPartial Then
					If Not $iCase Then
						For $i = $iStart To $iEnd Step $iStep
							If StringRegExp($avArray[$i][$j], $vValue) Then Return $i
						Next
					Else
						For $i = $iStart To $iEnd Step $iStep
							If StringRegExp($avArray[$i][$j], $vValue) Then Return $i
						Next
					EndIf
				Else
					For $i = $iStart To $iEnd Step $iStep
						If StringInStr($avArray[$i][$j], $vValue, $iCase) > 0 Then Return $i
					Next
				EndIf
			Next
		Case Else
			Return SetError(7, 0, -1)
	EndSwitch

	Return SetError(6, 0, -1)
EndFunc   ;==>_ArrayRESearch
