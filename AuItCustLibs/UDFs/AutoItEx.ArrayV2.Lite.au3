#include-once

;-------------------------------------------------------------------------------
; Name .........: ArrayV2
;-------------------------------------------------------------------------------
Func ArrayV2(Const ByRef $var)
	$var_out = $var
	If Not IsArray($var) Then
		Dim $var_out[2] = [1, $var]
	EndIf
	Return $var_out
EndFunc   ;==>ArrayV2

;-------------------------------------------------------------------------------
; Name .........: _ArrayCreateV2
; Description ..: Create an array where the first index is the items count
; Return .......:
;-------------------------------------------------------------------------------
Func _ArrayCreateV2($v_0 = 0, $v_1 = 0, $v_2 = 0, $v_3 = 0, $v_4 = 0, $v_5 = 0, $v_6 = 0, $v_7 = 0, $v_8 = 0, $v_9 = 0, $v_10 = 0, $v_11 = 0, $v_12 = 0, $v_13 = 0, $v_14 = 0, $v_15 = 0, $v_16 = 0, $v_17 = 0, $v_18 = 0, $v_19 = 0, $v_20 = 0)
	Local $av_Array[22] = [@NumParams, $v_0, $v_1, $v_2, $v_3, $v_4, $v_5, $v_6, $v_7, $v_8, $v_9, $v_10, $v_11, $v_12, $v_13, $v_14, $v_15, $v_16, $v_17, $v_18, $v_19, $v_20]
	ReDim $av_Array[@NumParams + 1]
	Return $av_Array
EndFunc   ;==>_ArrayCreateV2

;-------------------------------------------------------------------------------
; Name .........: _ArrayAddV2
; Description ..: Add an item in the array and update the items count
; Return .......: Success: Returns 1 and the modified array.
;                 Failure: Returns a 1-element array with the value added to it.
;                 @Error: 0 = No error.
;                         1 = $avArray isn't an array.
;                         2 = $avArray isn't a V2 array ($avArray[0] = cout).
; History ......: OCT 12, 2010 - Updated to handle the case when the variable
;                   given in parameter is not an array
;-------------------------------------------------------------------------------
Func _ArrayAddV2(ByRef $avArray, $sValue)
	If Not IsArray($avArray) Then
		Dim $avArray[2] = [1, $sValue]
		SetError(1)
		Return 1
	ElseIf Not IsInt($avArray[0]) Then
		SetError(2)
		Return 0
	Else
		ReDim $avArray[UBound($avArray) + 1]
		$avArray[UBound($avArray) - 1] = $sValue
		$avArray[0] += 1
		SetError(0)
		Return 1
	EndIf
EndFunc   ;==>_ArrayAddV2
