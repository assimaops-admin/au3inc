#include-once

Const $REGEXP_CHARS_LIST = StringSplit("\()[]*+?{}:.^|-", "")

;-------------------------------------------------------------------------------
; Name .........: S2RE (S2REv1)
;-------------------------------------------------------------------------------
Func S2RE($string)
	For $i = 1 To $REGEXP_CHARS_LIST[0]
		$string = StringReplace($string, $REGEXP_CHARS_LIST[$i], "\" & $REGEXP_CHARS_LIST[$i])
	Next
	Return $string
EndFunc   ;==>S2RE

;-------------------------------------------------------------------------------
; Name .........: AU3_RE_WINTITLE_FIX
; WORKAROUND ...: DO NOT FINISH ON CLOSING SQUARE BRACE!
;-------------------------------------------------------------------------------
Func AU3_RE_WINTITLE_FIX($regexp_string)
	If StringRight($regexp_string, 2) = "\]" Then
		$regexp_string = StringTrimRight($regexp_string, 2) & "\x" & Hex(Asc("]"), 2)
	EndIf
	Return $regexp_string
EndFunc   ;==>AU3_RE_WINTITLE_FIX

;-------------------------------------------------------------------------------
; Name .........: REGEXP_WIN_TITLE
; WORKAROUND ...: DO NOT FINISH ON CLOSING SQUARE BRACE!
;-------------------------------------------------------------------------------
Func REGEXP_WIN_TITLE($regexp_string)
	$regexp_string = AU3_RE_WINTITLE_FIX($regexp_string)
	Return "[REGEXPTITLE:" & $regexp_string & "]"
EndFunc   ;==>REGEXP_WIN_TITLE

;-------------------------------------------------------------------------------
; Name .........: LOOSE_WIN_TITLE
;-------------------------------------------------------------------------------
Func LOOSE_WIN_TITLE($string)
	$esc_string = S2RE($string)
	Return REGEXP_WIN_TITLE($esc_string)
EndFunc   ;==>LOOSE_WIN_TITLE

;-------------------------------------------------------------------------------
; Name .........: STRICT_WIN_TITLE
; Testing ......:
;
;    # include <Array.au3>
;    $list = WinList(STRICT_WIN_TITLE("C:\AutoTestingV2\inc\_.au3 - SciTE [2 of 5]"))
;    _ArrayDisplay($list)
;
;-------------------------------------------------------------------------------
Func STRICT_WIN_TITLE($string)
	$esc_string = S2RE($string)
	Return REGEXP_WIN_TITLE("\A" & $esc_string & "\Z")
EndFunc   ;==>STRICT_WIN_TITLE
