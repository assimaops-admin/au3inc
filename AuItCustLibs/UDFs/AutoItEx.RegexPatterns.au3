#include-once

Const $FILEPATH_CHARS_CLASS = '[^/\*\?"<>\|]'
Const $FILENAME_CHARS_CLASS = '[^/\*\?"<>\|\:\\]'

; Characters such as ';', '&', '%' and '^' are actually allowed,
; but we recommend avoiding them. You may enforce this by
; using the pattern below instead.

Const $FILEPATH_CHARS_CLASS_VX = '[^&;%\^/\*\?"<>\|]'
Const $FILENAME_CHARS_CLASS_VX = '[^&;%\^/\*\?"<>\|\:\\]'

; Other characters that are allowed but that may cause problems
; sometimes:
;
;   - parenthesis ()
;   - single quote '
;   - comma ,
;
