#include-once

;-------------------------------------------------------------------------------
; Name .........: ProgramFilesDir32
; Source .......:
;
;	; PROGRAM_FILES_DIR_32 SNIPPET -----------------------------
;	If Not IsDeclared("PROGRAM_FILES_DIR_32") Then
;		Assign("PROGRAM_FILES_DIR_32", @ProgramFilesDir, 2)
;		If @AutoItX64 Then Assign("PROGRAM_FILES_DIR_32", @ProgramFilesDir & " (x86)", 2)
;	EndIf
;	; -----------------------------------------------------------
;
;-------------------------------------------------------------------------------
Func ProgramFilesDir32()
	; In most cases the AutoIT macro returns exactly what we want
	; (regardless of the system we're on)
	$ProgramFilesDir32 = @ProgramFilesDir
	; But when running native 64 bit AutoIt on a 64 bit system,
	; we've been provided the path to the 64 bit programs
	; => Fix that
	If @AutoItX64 Then
		$ProgramFilesDir32 &= " (x86)"
	EndIf
	Return $ProgramFilesDir32
EndFunc   ;==>ProgramFilesDir32
