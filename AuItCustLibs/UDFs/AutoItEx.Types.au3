#include-once

;-------------------------------------------------------------------------------
; Name .........: Array
;-------------------------------------------------------------------------------
Func Array(Const ByRef $var)
	$var_out = $var
	If Not IsArray($var) Then
		Dim $var_out[1] = [$var]
	EndIf
	Return $var_out
EndFunc   ;==>Array

;-------------------------------------------------------------------------------
; Name .........: IsBoolEx
;-------------------------------------------------------------------------------
Func IsBoolEx($varname)
	Return IsBool($varname) Or _
			(IsInt($varname) And ($varname = 1) Or ($varname = 0))
EndFunc   ;==>IsBoolEx
