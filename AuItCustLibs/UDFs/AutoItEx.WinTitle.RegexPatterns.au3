#include-once

;-------------------------------------------------------------------------------
; Name .........: WINDOW_COMPACT_PROP_PATTERN
; Testing ......:
;
;    ; Sample:
;    ; ------
;    # include <AuItCustLibs\StringV2.au3>
;    $title = "[CLASS:SomeBlabla::Clsn; INSTANCE:2]"
;    ConsoleWrite(StringRegExpSingleEx($title, WINDOW_COMPACT_PROP_PATTERN("CLASS"))&@CRLF)
;    ConsoleWrite(StringRegExpSingleEx($title, WINDOW_COMPACT_PROP_PATTERN("INSTANCE"))&@CRLF)
;
;    ; Output:
;    ; ------
;    ; SomeBlabla::Clsn
;    ; 2
;
;-------------------------------------------------------------------------------
Func WINDOW_COMPACT_PROP_PATTERN($PROP)
	Return "\h*\[.*" & $PROP & "\h*\:\h*([^\h\;\]]*)"
EndFunc   ;==>WINDOW_COMPACT_PROP_PATTERN
