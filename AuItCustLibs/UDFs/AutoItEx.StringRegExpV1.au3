#include-once

;-------------------------------------------------------------------------------
; Name .........: StringRegExpV1
; Description ..: Wrapper for StringRegExpV2 with support for simple array output
; Return .......: Array
; History ......: OCT 17, 2016 - Copied from StringRegExpReplaceV2
;-------------------------------------------------------------------------------
Func StringRegExpV1($string, $pattern)
	$retArray = ""
	$array = StringRegExp($string, $pattern, 4)
	For $i = 0 To UBound($array) - 1
		$match = $array[$i]
		$match = $match[0]
		If Not IsArray($retArray) Then
			Dim $retArray[1]
			$retArray[0] = $match
		Else
			ReDim $retArray[UBound($retArray) + 1]
			$retArray[UBound($retArray) - 1] = $match
		EndIf
	Next
	Return $retArray
EndFunc   ;==>StringRegExpFlat
