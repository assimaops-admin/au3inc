#include-once

;-------------------------------------------------------------------------------
; Name .........: ModV2
;-------------------------------------------------------------------------------
Func ModV2($dividend, $divisor, $shift = 0)
	$res = Mod($dividend, $divisor + $shift)
	If $res < 0 Then
		$res += $divisor + $shift
	EndIf
	Return $res
EndFunc   ;==>ModV2

;-------------------------------------------------------------------------------
; Name .........: Min
;-------------------------------------------------------------------------------
Func Min($val1, $val2)
	If $val1 < $val2 Then
		Return $val1
	Else
		Return $val2
	EndIf
EndFunc   ;==>Min

;-------------------------------------------------------------------------------
; Name .........: Max
;-------------------------------------------------------------------------------
Func Max($val1, $val2)
	If $val1 > $val2 Then
		Return $val1
	Else
		Return $val2
	EndIf
EndFunc   ;==>Max

;-------------------------------------------------------------------------------
; Name .........: IsFlagSet
;-------------------------------------------------------------------------------
Func IsFlagSet($s, $flg)
	If BitAND($s, $flg) = $flg Then
		Return True
	Else
		Return False
	EndIf
EndFunc   ;==>IsFlagSet

;-------------------------------------------------------------------------------
; Name .........: IsParallelValue
;-------------------------------------------------------------------------------
Func IsParallelValue($a, $b)
	Return ((($a) And ($b)) Or ((Not ($a)) And (Not ($b))))
EndFunc   ;==>IsParallelValue

;-------------------------------------------------------------------------------
; Name .........: QuestionMark (Ternary Operator)
;-------------------------------------------------------------------------------
Func QuestionMark($cond, $val, $alternative)
	If $cond Then
		Return $val
	Else
		Return $alternative
	EndIf
EndFunc   ;==>QuestionMark
