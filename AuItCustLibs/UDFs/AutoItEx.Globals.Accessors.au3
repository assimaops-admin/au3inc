#include-once

;-------------------------------------------------------------------------------
; Name .........: Set_
;-------------------------------------------------------------------------------
Func Set_($varname, $val = 1)
	Return Assign($varname, $val, 2)
EndFunc   ;==>Set_

;-------------------------------------------------------------------------------
; Name .........: Get_
;-------------------------------------------------------------------------------
Func Get_($varname, $default = "")
	If IsDeclared($varname) Then $default = Eval($varname)
	Return $default
EndFunc   ;==>Get_

;-------------------------------------------------------------------------------
; Name .........: IsSet
;-------------------------------------------------------------------------------
Func IsSet($varname)
	Local $isVarSet = 0
	If IsDeclared($varname) Then
		$varval = Eval($varname)
		$isVarSet = (IsString($varval) And ($varval <> "")) Or ($varval <> 0)
	EndIf
	Return $isVarSet
EndFunc   ;==>IsSet
