#include-once

;-------------------------------------------------------------------------------
; Name .........: StringRegExpReplaceEscChars
; Description ..: Utility for 'StringRegExpReplaceV2'
; Return .......: String
; Testing ......: Call 'StringRegExpReplaceEscChars_Harness()'
; History ......: OCT 7, 2010 - Created
;-------------------------------------------------------------------------------
Func StringRegExpReplaceEscChars($str, $cs, $cr)

	; REPLACEMENTS AT THE BEGINING OF THE STRING
	; Replace the '\r' character in the following string:
	; "\r\nstring starts with a new line."
	$str = StringRegExpReplace($str, "(\A(\\\\)*)\\" & $cs, "\1" & $cr)

	; REPLACEMENTS IN THE MIDDLE OF THE STRING
	; 1/ [^\\] -> do not replace anything in the following string
	;           "talking about \\r which is a special char"
	; 2/ (\\\\) -> keep double backlashes "\\" together
	; 3/ Note: The second filter needs to be applied twice for the
	;    cases when there are multiple time special characters in a row.
	$str = StringRegExpReplace($str, "([^\\](\\\\)*)\\" & $cs, "\1" & $cr)
	$str = StringRegExpReplace($str, "([^\\](\\\\)*)\\" & $cs, "\1" & $cr)

	Return $str

EndFunc   ;==>StringRegExpReplaceEscChars

;-------------------------------------------------------------------------------
; Name .........: StringRegExpReplaceEscChars_Harness
; Description ..: Test harness
; Return .......: n/a
; History ......: OCT 7, 2010 - Created
;-------------------------------------------------------------------------------
;StringRegExpReplaceEscChars_Harness()
Func StringRegExpReplaceEscChars_Harness()
	$cs = "r"
	$cr = "[CR]"

	Dim $test_data[10] = _
			[ _
			"\r", $cr _
			, "\\", "\\" _
			, "\\r", "\\r" _
			, "\\\r", "\\" & $cr _
			, "\r\\", $cr & "\\" _
			, "\\\\\r", "\\\\" & $cr _
			, "\r\r\r\r\r", $cr & $cr & $cr & $cr & $cr _
			, "bla\rbla", "bla" & $cr & "bla" _
			, "bla\r\r", "bla" & $cr & $cr _
			, "bla\r\\\r", "bla" & $cr & "\\" & $cr _
			]

	For $i = 0 To UBound($test_data) - 2 Step 2
		$log = StringFormat("Test %2d: ", $i / 2 + 1)
		$input = $test_data[$i]
		$expect = $test_data[$i + 1]
		$output = StringRegExpReplaceEscChars($input, $cs, $cr)
		If $output = $expect Then
			$pre = "+"
			$log &= "Success."
		Else
			$pre = "!"
			$log &= "Failure."
			$log &= " $input=" & $input
			$log &= " $expect=" & $expect
			$log &= " $output=" & $output
		EndIf
		ConsoleWrite($pre & " " & $log & @CRLF)
	Next
EndFunc   ;==>StringRegExpReplaceEscChars_Harness
