#include-once

;-------------------------------------------------------------------------------
; Name .........: Quote (aka. QuotedV2, EnsureQuotedIfNecessary)
; Description ..: Quote only if contains spaces - Easier for writing generic
;                 code using command line.
; History ......: FEV 2016 - Convert to string first.
;                 FEV 2016 - Quote also if string is empty.
; Return .......: string with or without surrounding quotes
;-------------------------------------------------------------------------------
Func Quote($szPath, $delim = '"')
	$szPath = "" & $szPath
	If StringInStr($szPath, " ") Or ($szPath = "") Then
		Return '"' & $szPath & '"'
	Else
		Return $szPath
	EndIf
EndFunc   ;==>Quote

;-------------------------------------------------------------------------------
; Name .........: Quote (aka. QuotedV2, EnsureQuotedIfNecessary)
; Description ..: Quote only if contains spaces - Easier for writing generic
;                 code using command line.
; History ......: FEV 2016 - Convert to string first.
;                 FEV 2016 - Quote also if string is empty.
; Return .......: string with or without surrounding quotes
;-------------------------------------------------------------------------------
Func QuoteCautious($szPath, $delim = '"')
	$szPath = "" & $szPath
	If StringInStr($szPath, " ") Or ($szPath = "") Or StringInStr($szPath, "~") Then
		Return '"' & $szPath & '"'
	Else
		Return $szPath
	EndIf
EndFunc   ;==>QuoteCautious

;-------------------------------------------------------------------------------
; Name .........: UnQuote (aka UnQuotedV2)
; Description ..: Trim first and last characters if they are double quotes.
; Return .......: String without surrounding quotes.
; Remarks ......: Should have been quoted with Quote() function.
;-------------------------------------------------------------------------------
Func UnQuote($szPath)
	If StringStartsWith($szPath, '"') And StringEndsWith($szPath, '"') Then
		Return StringTrimRight(StringTrimLeft($szPath, 1), 1)
	Else
		Return $szPath
	EndIf
EndFunc   ;==>UnQuote

;-------------------------------------------------------------------------------
; Name .........: StringEquals
; Description ..: Negation of built-in function
; Parameters ...: cf. StringCompare
; Remarks ......: ("bla" = 0) return True because (StringToInt("bla") = 1)
;                 whereas StringEquals("bla", 0) returns False (0 -> "0")
; Return .......: Boolean True if string equals
;-------------------------------------------------------------------------------
Func StringEquals($string1, $string2, $casesense = 0)
	Return Not StringCompare($string1, $string2, $casesense)
EndFunc   ;==>StringEquals

;-------------------------------------------------------------------------------
; Name .........: StringStartsWith
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func StringStartsWith($string, $begin, $casesense = 0)
	If IsString($begin) Then
		Return StringStartsWithV1($string, $begin, $casesense)
	ElseIf IsArray($begin) Then
		For $i = 1 To $begin[0]
			$ret = StringStartsWithV1($string, $begin[$i], $casesense)
			If $ret Then Return $i
		Next
		Return 0
	EndIf
EndFunc   ;==>StringStartsWith

;-------------------------------------------------------------------------------
; Name .........: StringStartsWithV1
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func StringStartsWithV1($string, $begin, $casesense = 0)
	Return Not StringCompare(StringLeft($string, StringLen($begin)), $begin, $casesense)
EndFunc   ;==>StringStartsWithV1

;-------------------------------------------------------------------------------
; Name .........: StringEndsWithV1
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func StringEndsWithV1($string, $end, $casesense = 0)
	Return Not StringCompare(StringRight($string, StringLen($end)), $end, $casesense)
EndFunc   ;==>StringEndsWithV1

;-------------------------------------------------------------------------------
; Name .........: StringEndsWith
; Description ..: not case sensitive by default
; Return .......:
;-------------------------------------------------------------------------------
Func StringEndsWith($string, $end, $casesense = 0)
	If IsString($end) Then
		Return StringEndsWithV1($string, $end, $casesense)
	ElseIf IsArray($end) Then
		For $i = 1 To $end[0]
			$ret = StringEndsWithV1($string, $end[$i], $casesense)
			If $ret Then Return $i
		Next
		Return 0
	EndIf
EndFunc   ;==>StringEndsWith
