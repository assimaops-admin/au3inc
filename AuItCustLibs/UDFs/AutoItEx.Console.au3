#include-once

;-------------------------------------------------------------------------------
; Name .........: ConsoleWriteLine
;-------------------------------------------------------------------------------
Func ConsoleWriteLine($text)
	ConsoleWrite($text & @CRLF)
EndFunc   ;==>ConsoleWriteLine
