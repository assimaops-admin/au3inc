#include-once

;; START - EMBEDDED SAMPLE UNIT TESTING COMMENT ;;
;; case [1]
;~ # include <CustLibs\AbcQa\ScriptingTools.au3>
;; case [2]
;~ # include <CoreUAFLibs\UDFs\Pause.au3>
;; case [3]
;~ # include <CoreUAFLibs\UDFs\Pause.au3>
;~ # include <CoreUAFLibs\UDFs\HotkeyToText.au3>
;; common test-case code
;~ Pause()
;; -- EMBEDDED SAMPLE UNIT TESTING COMMENT - END ;;

;-------------------------------------------------------------------------------
; Name .........: Pause
;-------------------------------------------------------------------------------
Func Pause($txtMsg = "", $otherHotkey = "{F9}")
	; if PauseV3 is defined, call PauseV3 instead!
	$ExpectedUDF = "PauseV3"
	Call($ExpectedUDF, $otherHotkey, $txtMsg)
	If (@error = 0xDEAD) And (@extended = 0xBEEF) Then
		; if _Pause is defined, call _Pause instead!
		$ExpectedUDF = "_Pause"
		Call($ExpectedUDF)
		If (@error = 0xDEAD) And (@extended = 0xBEEF) Then
			; otherwise simply display a message box
			MsgBox(0, @ScriptName & " is paused", $txtMsg & @CRLF & "Click OK to continue...")
		EndIf
	EndIf
EndFunc   ;==>Pause
