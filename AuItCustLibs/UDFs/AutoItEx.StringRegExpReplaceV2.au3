#include-once
#include ".\AutoItEx.StringRegExpReplaceEscChars.au3"

;-------------------------------------------------------------------------------
; Name .........: StringRegExpReplaceV2
; Description ..: Wrapper for StringRegExpReplace with support for the '\n'
;                 and '\r' special characters in the replacement pattern.
; Return .......: String
; History ......: SEP 30, 2010 - Created stub in a will to translate the
;                   'PHP Version' of the 'FilterFileLines' wrapper.
;                 OCT 7, 2010 - Implemented as a wrapper for
;                   'StringRegExpReplace' + bug fix for multiple
;                   backslashes preceding the '\r' or '\n' characters.
;-------------------------------------------------------------------------------
Func StringRegExpReplaceV2($input, $search, $replace, $count = 0)

	; Prepare replacement pattern for AutoIt
	$replace = StringRegExpReplaceEscChars($replace, "r", @CR)
	$replace = StringRegExpReplaceEscChars($replace, "n", @LF)

	; Perform replacements
	$output = StringRegExpReplace($input, $search, $replace, $count)
	$error = @error
	$replacements_count = @extended

	; Return
	Return SetError($error, $replacements_count, $output)

EndFunc   ;==>StringRegExpReplaceV2
