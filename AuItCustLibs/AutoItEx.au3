#NoIncTidy
#include-once
#include ".\UDFs\AutoItEx.Arithmetic.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.ArrayV2.Lite.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.Console.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.Globals.Accessors.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.Macros.ProgramFilesDir32.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.Pause.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.RegexPatterns.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.String.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.Types.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.WinTitle.RegexPatterns.au3" ; <<< EMBEDDED_INCLUDE
#include ".\UDFs\AutoItEx.WinTitle.StringToRegex.au3" ; <<< EMBEDDED_INCLUDE
