#include-once
#include <BaseUAFLibs\StdBaseLib\ScreenCaptureV2.au3>
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>
#include ".\ScrShots.Private.au3"

;-------------------------------------------------------------------------------
; Name .........: ScrShots_WndCtrlShot [EXPORTED]
; Category .....: Control screenshots
; Description ..: take a control screenshot
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
$_g_ScrShots_WndCtrlShot_Count = 0
Func ScrShots_WndCtrlShot($screenshotNickname, $window = Default)
	$screenshotType = 2
	If $window = Default Then $window = "[ACTIVE]"
	$PREFIX_LETTER = "W"
	ScrShots_PrivateFeatureInit($screenshotType)
	$_g_ScrShots_WndCtrlShot_Count += 1
	$screenshotNickname = StringFormat("%s%04d-%s", $PREFIX_LETTER, $_g_ScrShots_WndCtrlShot_Count, $screenshotNickname)
	$wndHandle = WinGetHandleFwd($window)
	If $wndHandle = "" Then Return ""
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname, $screenshotType)
	_ScreenCapture_CaptureWndV2($screenshotPath, $wndHandle)
	Return $screenshotPath
EndFunc   ;==>ScrShots_WndCtrlShot

;-------------------------------------------------------------------------------
; Name .........: ScrShots_AreaCtrlShot [EXPORTED]
; Category .....: Control screenshots
; Description ..: take a control screenshot
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
$_g_ScrShots_AreaCtrlShot_Count = 0
Func ScrShots_AreaCtrlShot($screenshotNickname, $right = $STANDARD_WIDTH_V2, $bottom = $STANDARD_HEIGHT_V2, $left = 0, $top = 0)
	$screenshotType = 2
	$PREFIX_LETTER = "A"
	ScrShots_PrivateFeatureInit($screenshotType)
	$_g_ScrShots_AreaCtrlShot_Count += 1
	$screenshotNickname = StringFormat("%s%04d-%s", $PREFIX_LETTER, $_g_ScrShots_AreaCtrlShot_Count, $screenshotNickname)
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname, $screenshotType)
	_ScreenCapture_Capture($screenshotPath, $left, $top, $right, $bottom)
	Return $screenshotPath
EndFunc   ;==>ScrShots_AreaCtrlShot

;-------------------------------------------------------------------------------
; Name .........: ScrShots_FullCtrlShot [EXPORTED]
; Category .....: Control screenshots
; Description ..: take a control screenshot
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
$_g_ScrShots_FullCtrlShot_Count = 0
Func ScrShots_FullCtrlShot($screenshotNickname)
	$screenshotType = 2
	$PREFIX_LETTER = "F"
	ScrShots_PrivateFeatureInit($screenshotType)
	$_g_ScrShots_FullCtrlShot_Count += 1
	$screenshotNickname = StringFormat("%s%04d-%s", $PREFIX_LETTER, $_g_ScrShots_FullCtrlShot_Count, $screenshotNickname)
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname, $screenshotType)
	_ScreenCapture_Capture($screenshotPath)
	Return $screenshotPath
EndFunc   ;==>ScrShots_FullCtrlShot
