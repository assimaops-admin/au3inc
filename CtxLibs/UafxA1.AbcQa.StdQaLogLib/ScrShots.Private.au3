#include-once
#include <CtxConf\StdQaLogConstants.au3>

Global $g_scrShotsOutDir
Global $g_AUTOMATION_SCREENSHOTS_DIR
Global $g_AUTOMATION_CTRLSCRSHOTS_DIR

;-------------------------------------------------------------------------------
; Name .........: ScrShots_PrivateFeatureInit [PRIVATE]
; Category .....: Private
; Description ..:
; Return .......: n/a
; History ......: JAN 21, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_PrivateFeatureInit($scrShotType = 0)
	Global $g_scrShotsOutDir = $AUTOMATION_OUT_DIRECTORY
	If IsDeclared("AutoTestingV2._RELEASE152_") Then
		$g_scrShotsOutDir = $AUTOMATION_LOG_DIRECTORY
	EndIf
	Global $g_AUTOMATION_SCREENSHOTS_DIR = $g_scrShotsOutDir & "\Screenshots"
	Global $g_AUTOMATION_CTRLSCRSHOTS_DIR = $g_scrShotsOutDir & "\CtrlScrShots"
	Switch $scrShotType
		Case 0
			If Not FileExists($g_AUTOMATION_SCREENSHOTS_DIR) Then DirCreate($g_AUTOMATION_SCREENSHOTS_DIR)
		Case 2
			If Not FileExists($g_AUTOMATION_CTRLSCRSHOTS_DIR) Then DirCreate($g_AUTOMATION_CTRLSCRSHOTS_DIR)
		Case Default
	EndSwitch
EndFunc   ;==>ScrShots_PrivateFeatureInit

;-------------------------------------------------------------------------------
; Name .........: ScrShots_PrivateGetFilename [PRIVATE]
; Category .....: Private
; Description ..:
; Return .......: File path (string)
; History ......: JAN 21, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_PrivateGetFilename($screenshotNickname, $scrShotType = 0)
	$ext = ".png" ; << used to be ".jpg" at the beginning
	If $scrShotType = 1 Then $ext = ".test" & $ext
	If $scrShotType = 2 Then $ext = ".ctls" & $ext
	$dir = $g_AUTOMATION_SCREENSHOTS_DIR
	If $scrShotType = 1 Then $dir = $g_AUTOMATION_CTRLSCRSHOTS_DIR
	Return $dir & "\" & $screenshotNickname & $ext
EndFunc   ;==>ScrShots_PrivateGetFilename
