#include-once
;----------------------------------------------------------------------------------------------
;                                 Module description
;----------------------------------------------------------------------------------------------
;
; There are two sets of functions here:
;
;   1/ ScrShots_FullTestShotVx, ScrShots_AreaTestShotVx, ScrShots_WndTestShotVx
;       are used to take screenshots stored in directory
;       'V:\AutoTestingV2.Data\TMO-AAA-NNN\Automation\Screenshots' and analysed during reporting
;
;   2/ and ScrShots_FullCtrlShotVx, ScrShots_AreaCtrlShotVx, ScrShots_WndCtrlShotVx
;       The screenshots will then appear in 'V:\AtV2.Data\TMO-AAA-NNN\Automation\CtrlScrShots'
;       but will not be processed during the reporting because 'CtrlScrShots'
;       is listed in 'C:\AutoTestingV2\ReportingVx\UnrelevantFiles.dat'
;
;             NOTE: Using ScrShots_WndXxxxShotVx or ScrShots_AreaXxxxShotVx
;             rather than ScrShots_FullXxxxShotVx
;             reduces the storage required for taking the screenshots
;                + Removes uninsteresting areas for testing
;
; Regarding the Ctrl screenshots, they are automatically indexed to make life easier
; but the index works by type of function ( ie. Wnd, Area, Full )
;   eg. A0001-TriggerCapture.ctls.png, A0002-TriggerCapture.ctls.png, etc.
; the prefix_letter being F, A or W depending whether the screenshot is a Full screen,
; just the regular area ( like the one used by WinSetPosStandard() ) or a window only.
; The suffix is .ctls.png - in case one would have forgot it was a special purpose screenshot
;
; History: MAR 31, 2015 - AutoTestingV2._RELEASE152_ - Add a level of indirection
;          Screenshots are now a subdirectory of 'Logging'.
;          'V:\AutoTestingV2.Data\TMO-AAA-NNN\Automation\Logging\Screenshots'
;
;----------------------------------------------------------------------------------------------
#include <AuItMiscUDFs\MouseMoveFarAway.au3> ; <<< EMBEDDED_INCLUDE
#include <BaseUAFLibs\StdBaseLib\ScreenCaptureV2.au3>
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>
#include ".\ScrShots.Private.au3"

;-------------------------------------------------------------------------------
; Name .........: ScrShots_WndTestShotV1 [EXPORTED]
; Category .....: Testing screenshots
; Description ..: hide the cursor for the screenshots
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_WndTestShotV1($screenshotNickname, $window = Default, $fCursor = True)
	$funcVersion = 1
	Return ScrShots_WndTestShotVx($funcVersion, $screenshotNickname, $window, $fCursor)
EndFunc   ;==>ScrShots_WndTestShotV1

;-------------------------------------------------------------------------------
; Name .........: ScrShots_WndTestShotV2 [EXPORTED]
; Category .....: Testing screenshots
; Description ..: hide the cursor for the screenshots
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_WndTestShotV2($screenshotNickname, $window = Default, $fCursor = True)
	$funcVersion = 2
	Return ScrShots_WndTestShotVx($funcVersion, $screenshotNickname, $window, $fCursor)
EndFunc   ;==>ScrShots_WndTestShotV2

;-------------------------------------------------------------------------------
; Name .........: ScrShots_WndTestShotVx [SUB]
; Category .....: Testing screenshots
; Description ..: hide the cursor for the screenshots
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_WndTestShotVx($funcVersion, $screenshotNickname, $window = Default, $fCursor = True)
	If $window = Default Then $window = "[ACTIVE]"

	; REF. MouseMoveFarAwaySnippet1
	$aOriginalCursorPos = MouseGetPos()
	If Not $fCursor Then MouseMove(5000, 5000, 1)

	; REF. MouseMoveFarAwaySnippetX, CALLBACK-BASED IMPLEMENTATION
	$screenshotPath = ScrShots_PrivateWndTestShotVx($funcVersion, $screenshotNickname, $window)

	; REF. MouseMoveFarAwaySnippet2
	If Not $fCursor Then MouseMove($aOriginalCursorPos[0], $aOriginalCursorPos[1], 1)

	Return $screenshotPath
EndFunc   ;==>ScrShots_WndTestShotVx

;-------------------------------------------------------------------------------
; Name .........: ScrShots_AreaTestShotV1 [EXPORTED]
; Category .....: Testing screenshots
; Description ..:
; Return .......: n/a
; History ......: JAN 21, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_AreaTestShotV1($screenshotNickname, $right = $STANDARD_WIDTH_V2, $bottom = $STANDARD_HEIGHT_V2, $left = 0, $top = 0)
	ScrShots_PrivateFeatureInit()
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname)
	_ScreenCapture_Capture($screenshotPath, $left, $top, $right, $bottom)
	Return $screenshotPath
EndFunc   ;==>ScrShots_AreaTestShotV1

;-------------------------------------------------------------------------------
; Name .........: ScrShots_AreaTestShotV2 [EXPORTED]
; Category .....: Testing screenshots
; Description ..:
; Return .......: n/a
; History ......: JAN 21, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_AreaTestShotV2($screenshotNickname, $right = $STANDARD_WIDTH_V2, $bottom = $STANDARD_HEIGHT_V2, $left = 0, $top = 0)
	ScrShots_PrivateFeatureInit()
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname)
	_ScreenCapture_CaptureV2($screenshotPath, $left, $top, $right, $bottom)
	Return $screenshotPath
EndFunc   ;==>ScrShots_AreaTestShotV2

;-------------------------------------------------------------------------------
; Name .........: ScrShots_PrivateWndTestShotVx [PRIVATE]
; Category .....: Private
; Description ..:
; Return .......: n/a
; History ......: JAN 20, 2009 - Created
;-------------------------------------------------------------------------------
Func ScrShots_PrivateWndTestShotVx($funcVersion, $screenshotNickname, $window = Default)
	If $window = Default Then $window = "[ACTIVE]"
	ScrShots_PrivateFeatureInit()
	If IsDeclared("AutoTestingV2._RELEASE152_") Then
		; use the default behaviour
		$wndHandle = WinGetHandle($window)
	Else
		; use forward compatibility wrapper
		$wndHandle = WinGetHandleFwd($window)
	EndIf
	$wndTitle = WinGetTitle($wndHandle)
	If $wndHandle = "" Then Return ""
	$screenshotPath = ScrShots_PrivateGetFilename($screenshotNickname)
	If $funcVersion = 1 Then
		_ScreenCapture_CaptureWnd($screenshotPath, $wndHandle)
	Else ; $funcVersion = 2
		_ScreenCapture_CaptureWndV2($screenshotPath, $wndHandle)
	EndIf
	Return $screenshotPath
EndFunc   ;==>ScrShots_PrivateWndTestShotVx
