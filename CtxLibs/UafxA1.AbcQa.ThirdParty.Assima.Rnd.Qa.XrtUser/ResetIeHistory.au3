#include-once
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa.XrtUser.ThirdPartyImpl\ThirdPartyImpl.au3"

Const $CLEAR_URL_HISTORY_ENV_EXE = "msvcr.stt\ClearURLHistory.exe"

;-------------------------------------------------------------------------------
; Name .........: ResetIeHistory
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ResetIeHistory()
	; Reset Internet Explorer's History
	Return RunWait($CLEAR_URL_HISTORY_ENV_EXE, "", @SW_HIDE)
EndFunc   ;==>ResetIeHistory
