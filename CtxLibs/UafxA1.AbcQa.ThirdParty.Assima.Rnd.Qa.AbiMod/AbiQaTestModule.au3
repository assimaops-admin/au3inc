#include-once
#include <Constants.au3>
#include <NamedPipes.au3>
#include <BaseUAFLibs\UDFs\FuncTimer.au3>
#include <BaseUAFLibs\UDFsx\NamedPipesHelper.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include ".\AbiQaTestModule.ChannelSettings.au3"
#include ".\AbiQaTestModule.Private.au3"
#include ".\AbiQaTestModule.Params.au3"


; ===============================================================================================================================
; Script tweaking
; ===============================================================================================================================
Global $RESET_CHANNEL_SETTINGS_XML_FILE = 1

; ===============================================================================================================================
; Script debugging
; ===============================================================================================================================
Global $DISABLE_ALL_API_LEVEL_OPTS = 0
Global $DISABLE_APP_LISTENER_AUDIT = 0
Global $DISABLE_APP_LISTENER_AUTOTEST = 0
Global $DISABLE_APP_LISTENER_USER_CREDENTIALS = 0
Global $DISABLE_APP_LISTENER_AUTO_RESET_SETTINGS = 0

; ===============================================================================================================================
; Script default parameters
; ===============================================================================================================================
Global Const $BUILD_DIRNAME = "7.08.02-20150219_210034"

Global Const $VIMAGO_SERVER_URL = 'http://708.vimago.assima.net'
;Global Const $VIMAGO_SERVER_URL = 'http://vanilla.708.default.vamigo.assimaqa.local'
;Global Const $VIMAGO_SERVER_URL = 'http://bianca-pc-w7'
Global Const $VIMAGO_CONTAINER_ID = '1007' ; LocalizedItemId=129 (PROJECT!) -- http://vanilla.708.default.vamigo.assimaqa.local/Workspace/Project/Default.aspx?LocalizedItemId=129
;Global Const $VIMAGO_CONTAINER_ID = '130' ; LocalizedItemId=130 (FOLDER!) -- http://vanilla.708.default.vamigo.assimaqa.local/Library/Folder/Default.aspx?LocalizedItemId=130

Global Const $CHILD_PROCESS_USERNAME = "admin@assima.net"
Global Const $CHILD_PROCESS_PASSWORD = "adminAssima"
;Global Const $CHILD_PROCESS_USERNAME = "contrib@abi.qa"
;Global Const $CHILD_PROCESS_PASSWORD = "contribAbi"
;Global Const $CHILD_PROCESS_USERNAME = "default@abi.qa"
;Global Const $CHILD_PROCESS_PASSWORD = "defaultAbi"

; ===============================================================================================================================
; Assima artefact-related constants
; ===============================================================================================================================
Global Const $CHILD_PROCESS_EXE_DIRE = "L:\AIS\Regular_7_08\" & $BUILD_DIRNAME & "\JavaLoader.ops2\Runtime_Performance"
;Global Const $CHILD_PROCESS_EXE_DIRE = @UserProfileDir & "\Assima_Performance_Deployer"
;Global Const $CHILD_PROCESS_EXE_DIRE = "."
Global Const $CHILD_PROCESS_EXE_NAME = "AppListener.exe"
Global Const $CHILD_PROCESS_EXE_FLAG = $STDOUT_CHILD

; ===============================================================================================================================
; Technical constants
; ===============================================================================================================================
Global Const $NAMED_PIPE_ACCESS_READ = 0 ; - The flow of data in the pipe goes from client to server only (inbound)
Global Const $NAMED_PIPE_ACCESS_WRITE = 1 ; - The flow of data in the pipe goes from server to client only (outbound)
Global Const $NAMED_PIPE_ACCESS_RW = 2 ; - The pipe is bi-directional (duplex)
Global Const $NAMED_PIPE_FLAGS = 1 ; - If you attempt to create multiple instances of a pipe with this flag, creation of the first instance succeeds, but creation of the next instance fails.

Global Const $EXIT_CODE_NAMED_PIPE_CREATE_FAILED = 1
Global Const $EXIT_CODE_NAMED_PIPE_CONNECT_FAILED = 2

Global Const $PIPE_STD_PREFIX = "\\.\pipe\"
Global Const $PIPE_NAME_FOR_CMDS = $PIPE_STD_PREFIX & 'AssimaABIAutoTestCmds'
Global Const $PIPE_NAME_FOR_INFO = $PIPE_STD_PREFIX & 'AssimaABIAutoTestInfo'

; ===============================================================================================================================
; Global variables
; ===============================================================================================================================
Global $g_AQTM_appListenerPID = 0
Global $g_AQTM_hPipeForInfo = -1
Global $g_AQTM_hPipeForCmds = -1
Global $g_AQTM_notifsCount = 0
Global $g_AQTM_lastNotif = ""
Global $g_AQTM_CleanTerminateWasCalled = 0


; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_Start
; ===============================================================================================================================
Func AbiQaTestModule_Start($auditFileOut = "")

	AbiQaTestModule_CreatePipes()
	AbiQaTestModule_LaunchAppListener($auditFileOut)
	AbiQaTestModule_WaitForAppListener()

EndFunc   ;==>AbiQaTestModule_Start

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_Terminate (CLEAN TERMINATE)
; ===============================================================================================================================
Func AbiQaTestModule_Terminate()

	Global $g_AQTM_appListenerPID
	Global $g_AQTM_CleanTerminateWasCalled = 1
	Local Const $TestedProcessUserName = "AppListener"
	If Not $g_AQTM_appListenerPID Then
		; STDMSG20150318.1
		Trace("WARNING: " & $TestedProcessUserName & " process was never launched!")
		Return False
	ElseIf Not ProcessExists($g_AQTM_appListenerPID) Then
		; STDMSG20150318.2
		Trace("WARNING: Previously launched " & $TestedProcessUserName & " process was not found. It may have crashed!")
		Return False
	Else
		Trace('Closing AppListener process...')
		AbiQaTestModule_SendAbiCommand("CLOSE_APPLISTENER")
		$waitForCloseMaxTime = 15
		If Not ProcessWaitClose($g_AQTM_appListenerPID, $waitForCloseMaxTime) Then
			$errStr = "AppListener did not close after CLOSE_APPLISTENER was sent (waited " & $waitForCloseMaxTime & "s)"
			; ErrorSub("Here, " & $errStr & "...")
			Trace("ERROR: " & $errStr & "!")
			; Will be closed more aggressively...
			;   >>> See AbiQaTestModule_OnExit()
		EndIf
	EndIf
	AbiQaTestModule_OnExit()
	Return True

EndFunc   ;==>AbiQaTestModule_Terminate

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_OnExit (DIRTY TERMINATE)
; ===============================================================================================================================
Func AbiQaTestModule_OnExit()
	Global $g_AQTM_appListenerPID
	Global $g_AQTM_CleanTerminateWasCalled
	; TODO: Check $g_AQTM_CleanTerminateWasCalled ?
	If ProcessExists($g_AQTM_appListenerPID) Then
		Trace("WARNING: AppListener process found. It was not closed properly!")
		; Kill the monitor first
		$monitorPID = ProcessExists("AppListenerMonitor.exe")
		If $monitorPID <> 0 Then
			ProcessClose($monitorPID)
			ProcessWaitClose($monitorPID)
		EndIf
		; Kill the AppListener process
		ProcessClose($g_AQTM_appListenerPID)
		ProcessWaitClose($g_AQTM_appListenerPID)
	EndIf
EndFunc   ;==>AbiQaTestModule_OnExit

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_CreatePipes
; ===============================================================================================================================
Func AbiQaTestModule_CreatePipes()

	; Create pipes
	; ============
	;
	;
	;       / \       IMPORTANT: The option 'PIPE_TYPE_MESSAGE' is doing the sequencing for us.     / \
	;      / ! \       All we have to do is to remove the ';' separator in between messages.       / ! \
	;      -----                                                                                   -----
	;
	;
	; Trace preview
	; -------------
	;
	; Local $aResult = DllCall("kernel32.dll", "handle", "CreateNamedPipeW", "wstr", $sName, "dword", $iOpenMode, "dword", $iPipeMode, "dword", $iMaxInst, _
	; 	"dword", $iOutBufSize, "dword", $iInpBufSize, "dword", $iDefTimeout, "ptr", $pSecurity)
	;
	;       $sName = \\.\pipe\AssimaABIAutoTestCmds
	;       $iOpenMode = 00080002
	;       $iPipeMode = 00000006
	;       $iMaxInst = 25
	;       $iOutBufSize = 4096
	;       $iInpBufSize = 4096
	;       $iDefTimeout = 5000
	;       $pSecurity = 0
	;       $g_AQTM_hPipeForCmds = 0x00000000000001F0
	;
	;       $sName = \\.\pipe\AssimaABIAutoTestInfo
	;       $iOpenMode = 00080001
	;       $iPipeMode = 00000006
	;       $iMaxInst = 25
	;       $iOutBufSize = 4096
	;       $iInpBufSize = 4096
	;       $iDefTimeout = 5000
	;       $pSecurity = 0
	;       $g_AQTM_hPipeForInfo = 0x00000000000001E0
	;
	; Open mode
	; ---------
	;
	; - PIPE_ACCESS_INBOUND - 0x00000001
	; - PIPE_ACCESS_OUTBOUND - 0x00000002
	; - FILE_FLAG_FIRST_PIPE_INSTANCE - 0x00080000
	;    - If you attempt to create multiple instances of a pipe with this flag, creation of the first instance succeeds, but creation of the next instance fails with ERROR_ACCESS_DENIED.
	;
	; Pipe mode
	; ---------
	;
	; - PIPE_TYPE_MESSAGE - 0x00000004
	; - PIPE_READMODE_MESSAGE - 0x00000002
	;
	$g_AQTM_hPipeForCmds = _NamedPipes_CreateNamedPipe($PIPE_NAME_FOR_CMDS, $NAMED_PIPE_ACCESS_WRITE, $NAMED_PIPE_FLAGS, 0, 1, 1, 1, 1)
	$g_AQTM_hPipeForInfo = _NamedPipes_CreateNamedPipe($PIPE_NAME_FOR_INFO, $NAMED_PIPE_ACCESS_READ, $NAMED_PIPE_FLAGS, 0, 1, 1, 1, 1)
	If ($g_AQTM_hPipeForCmds = -1) Or ($g_AQTM_hPipeForInfo = -1) Then
		ConsoleWrite("!Error: _NamedPipes_CreateNamedPipeV2() failed. " & @CRLF)
		Exit $EXIT_CODE_NAMED_PIPE_CREATE_FAILED
	EndIf

EndFunc   ;==>AbiQaTestModule_CreatePipes

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_LaunchAppListener
; ===============================================================================================================================
Func AbiQaTestModule_LaunchAppListener($auditFileOut = "")

	; 0- Hardcoded values
	$childProcessExeDire = $CHILD_PROCESS_EXE_DIRE
	$vimagoServerUrl = $VIMAGO_SERVER_URL
	$vimagoContainerId = $VIMAGO_CONTAINER_ID
	$vimagoContainerId = $VIMAGO_CONTAINER_ID
	$vimagoServerUserName = $CHILD_PROCESS_USERNAME
	$vimagoServerUserPass = $CHILD_PROCESS_PASSWORD

	; 1- Scripted values
	If Not $DISABLE_ALL_API_LEVEL_OPTS Then

		If $g_AQTM_AppListenerDir <> "" Then $childProcessExeDire = $g_AQTM_AppListenerDir
		If $g_AQTM_AppListenerRes <> "" Then $vimagoContainerId = $g_AQTM_AppListenerRes
		If $g_AQTM_AppListenerSrv <> "" Then $vimagoServerUrl = $g_AQTM_AppListenerSrv
		If $g_AQTM_AppListenerUsrName <> "" Then $vimagoServerUserName = $g_AQTM_AppListenerUsrName
		If $g_AQTM_AppListenerUsrPass <> "" Then $vimagoServerUserPass = $g_AQTM_AppListenerUsrPass

	EndIf

	; 2- Runtime values
	; TODO: GetCmdLineOptn("--target", "--server", etc.) x 3  +  TAKE PRECENDENCE

	If $RESET_CHANNEL_SETTINGS_XML_FILE Then
		AbiQaTestModule_WriteChannelSettingsFile($childProcessExeDire, $vimagoServerUrl, $vimagoContainerId)
	EndIf

	$childProcessExeOpts = ""
	If Not $DISABLE_APP_LISTENER_AUDIT Then
		$childProcessExeOpts = "/audit"
	EndIf
	If Not $DISABLE_APP_LISTENER_AUTOTEST Then
		$childProcessExeOpts &= " /autotest"
	EndIf
	If Not $DISABLE_APP_LISTENER_AUTO_RESET_SETTINGS Then
		$childProcessExeOpts &= " /resetsettings"
	EndIf
	If Not $DISABLE_APP_LISTENER_USER_CREDENTIALS Then
		If $vimagoServerUserName <> "" Then $childProcessExeOpts &= " /username:" & $vimagoServerUserName
		If $vimagoServerUserName <> "" Then $childProcessExeOpts &= " /userpassword:" & $vimagoServerUserPass
	EndIf

	If $auditFileOut <> "" Then
		$childProcessExeOpts &= " /auditfile:" & '"' & $auditFileOut & '"'
	EndIf

	$childProcessExePath = $childProcessExeDire & "\" & $CHILD_PROCESS_EXE_NAME
	$childProcessExeComm = $childProcessExePath & " " & $childProcessExeOpts

	ConsoleWrite('[' & $childProcessExeComm & ']...' & @CRLF)
	$g_AQTM_appListenerPID = Run($childProcessExeComm, $childProcessExeDire, $CHILD_PROCESS_EXE_FLAG)
	ConsoleWrite('$g_AQTM_appListenerPID = ' & $g_AQTM_appListenerPID & @CRLF)

EndFunc   ;==>AbiQaTestModule_LaunchAppListener

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_WaitForAppListener
; ===============================================================================================================================
Func AbiQaTestModule_WaitForAppListener()

	AbiQaTestModule_WaitForConnectVx($g_AQTM_hPipeForInfo)
	AbiQaTestModule_WaitForConnectVx($g_AQTM_hPipeForCmds)

EndFunc   ;==>AbiQaTestModule_WaitForAppListener

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_WaitForConnectVx
; ===============================================================================================================================
Func AbiQaTestModule_WaitForConnectVx($hPipeToWaitFor)
	ConsoleWrite("Waiting for child process to connect to pipe..." & @CRLF)
	_NamedPipes_ConnectNamedPipe($hPipeToWaitFor)
	ConsoleWrite("... Child connected. OK." & @CRLF)
EndFunc   ;==>AbiQaTestModule_WaitForConnectVx

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_WriteChannelSettingsFile
; ===============================================================================================================================
Func AbiQaTestModule_WriteChannelSettingsFile($exeLocation, $serverBaseUrl, $serverProjectId)
	$fp = $exeLocation & "\" & "ChannelSettings.xml"
	ValTrace($fp, '$fp')


	; The default mode when writing text is ANSI
	; 128 = Use Unicode UTF8 (with BOM) reading and writing mode. Reading does not override existing BOM.
	; 256 = Use Unicode UTF8 (without BOM) reading and writing mode.
	;
	;Const $CHANNEL_SETTINGS_WRITE_ENCODING = 0 ; ANSI, ORIGINAL VERSION, _AV160711
	Const $CHANNEL_SETTINGS_WRITE_ENCODING = 256 ; +++ UTF8 (without BOM) +++ JUL 11, 2016

	$writeEncoding = $CHANNEL_SETTINGS_WRITE_ENCODING
	; $fc = AbiQaTestModule_GetChannelSettingsFileContent_AV160711($serverBaseUrl, $serverProjectId)
	$fc = AbiQaTestModule_GetChannelSettingsFileContent($serverBaseUrl, $serverProjectId, $writeEncoding)
	ValTrace($fc, '$fc')
	$hf = FileOpen($fp, 2 + $writeEncoding) ; Write mode (erase previous contents)
	FileWrite($hf, $fc)
	FileClose($hf)

EndFunc   ;==>AbiQaTestModule_WriteChannelSettingsFile

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_SelfTimedReceiveAbiInfo
; ===============================================================================================================================
Func AbiQaTestModule_SelfTimedReceiveAbiInfo($timeoutValue)
	Local $timerObject = FuncTimerCreate($timeoutValue)
	Local $retNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
	Local $errorCode = @error
	Local $extendedErrorCode = @extended
	Return SetError($errorCode, $extendedErrorCode, $retNotif)
EndFunc   ;==>AbiQaTestModule_SelfTimedReceiveAbiInfo

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_TimedReceiveAbiInfo
; ===============================================================================================================================
Func AbiQaTestModule_TimedReceiveAbiInfo(ByRef $timerObject, $isFlushMode = False)

	Local Const $FUNCT_TIMER_CYCLE_DURATION = 100 ; << to adjust depending on context, or make it a parameter

	Local $hPipeToReadFrom = $g_AQTM_hPipeForInfo
	Local $errorCode = 0
	Local $extendedErrorCode = 0
	Local $isVerboseTrace = Not $isFlushMode

	If $isVerboseTrace Then
		Trace('Peeking into the pipe, waiting for the next notification...')
	EndIf
	Local $aPipeDataZERO = ""
	While $aPipeDataZERO = ""
		Sleep($FUNCT_TIMER_CYCLE_DURATION)
		;Trace($funcname & " is peeking into the pipe...")
		$aPipeData = _NamedPipes_PeekNamedPipe($hPipeToReadFrom)
		;ConsoleWrite(@LF & "Pipe Server:: Data: """ & $aPipeData[0] & """" & ", " & $aPipeData[1] & ", " & $aPipeData[2] & ", " & $aPipeData[3] & @LF)
		; Until $aPipeData[0] == "DIE"
		$aPipeDataZERO = $aPipeData[0]
		If FuncTimerIsElapsed($timerObject) Then
			$errorCode = 1
			$extendedErrorCode = 1
			ExitLoop
		EndIf
	WEnd

	If Not $errorCode Then
		If $isVerboseTrace Then
			Trace('AppListener just posted a notification, ' & FuncTimerGetTimeInSec($timerObject) & 's after we started the timer.')
		EndIf
	Else
		If $isVerboseTrace Then
			Trace('Timer elapsed. We were expecting a (specific) notification within ' & FuncTimerGetTimeout($timerObject) & 's after starting the timer.')
		EndIf
	EndIf

	If $extendedErrorCode Then
		Return SetError($errorCode, $extendedErrorCode, "")
	Else
		Return AbiQaTestModule_ReceiveAbiInfo()
	EndIf

EndFunc   ;==>AbiQaTestModule_TimedReceiveAbiInfo

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_ReceiveAbiInfo
; ===============================================================================================================================
Func AbiQaTestModule_ReceiveAbiInfo($isFlushMode = False)

	Local $hPipeToReadFrom = $g_AQTM_hPipeForInfo
	Local $sNotification = ""
	Local $isVerboseTrace = Not $isFlushMode

	Trace("Reading the next notification from pipe...")
	$sNotification = NamedPipesHelper_GetUnicodeText($hPipeToReadFrom)
	$sNotification = StringTrimRight($sNotification, 1) ; remove the ';' character. -- REF. 150320.17
	Trace(AbiQaTestModule_PrivatePrettyLogString("<", $sNotification))

	If $sNotification <> "" Then
		$g_AQTM_notifsCount += 1
		AbiQaTestModulePrivate_RecordAbiInfo($sNotification)
	EndIf

	$g_AQTM_lastNotif = $sNotification

	Return $sNotification

EndFunc   ;==>AbiQaTestModule_ReceiveAbiInfo

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_GetLastAbiInfo
; ===============================================================================================================================
Func AbiQaTestModule_GetLastAbiInfo()
	Return $g_AQTM_lastNotif
EndFunc   ;==>AbiQaTestModule_GetLastAbiInfo

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_SendAbiCommand
; ===============================================================================================================================
Func AbiQaTestModule_SendAbiCommand($sCommand)

	$hPipeToWriteTo = $g_AQTM_hPipeForCmds
	Trace("Writing next command to pipe...")
	Trace(AbiQaTestModule_PrivatePrettyLogString(">", $sCommand))
	$sCommand &= ";" ; add the ';' character. -- REF. 150320.17
	If NamedPipesHelper_SetUnicodeText($hPipeToWriteTo, $sCommand) Then
		Trace("... Command written. OK.")
	Else
		Trace("... Failed to write command.")
	EndIf

EndFunc   ;==>AbiQaTestModule_SendAbiCommand

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_PrivatePrettyLogString
; ===============================================================================================================================
Func AbiQaTestModule_PrivatePrettyLogString($prefixChar, $sCmdOrNotif, $suffixChar = Default)
	Return "_" & @CRLF & AbiQaTestModule_PrivatePrettyPrintString($prefixChar, $sCmdOrNotif, $suffixChar) & @CRLF
EndFunc   ;==>AbiQaTestModule_PrivatePrettyLogString

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_PrivatePrettyPrintString
; --
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString(">", "SOME_ABI_COMMAND") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("<", "SOME_ABI_RESPONSE") & @CRLF)
; --
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("+", "SOME_ABI_DATA_A") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("!", "SOME_ABI_DATA_B") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("-", "SOME_ABI_DATA_C") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("|", "SOME_ABI_DATA_D") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString(">", "SOME_ABI_DATA_E", "<") & @CRLF)
; ConsoleWrite(AbiQaTestModule_PrivatePrettyPrintString("<", "SOME_ABI_DATA_F", ">") & @CRLF)
; ===============================================================================================================================
Func AbiQaTestModule_PrivatePrettyPrintString($prefixChar, $sCmdOrNotif, $suffixChar = Default)
	If ($suffixChar = Default) Or ($suffixChar = "") Then $suffixChar = $prefixChar
	$____dottedLineStr____ = _StringRepeat("-", StringLen($sCmdOrNotif))
	$c_ = $prefixChar
	$_c = $suffixChar
	$outputString = ""
	$outputString &= " /" & $____dottedLineStr____ & "\ " & @CRLF
	$outputString &= $c_ & " " & $sCmdOrNotif & " " & $_c & @CRLF
	$outputString &= " \" & $____dottedLineStr____ & "/ "
	Return $outputString
EndFunc   ;==>AbiQaTestModule_PrivatePrettyPrintString
