#include-once

; Meimoen+Wayne
;Const $cst_AbiQaTestModule_fedAuth_160711 = "77u/77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U2VjdXJpdHlDb250ZXh0VG9rZW4gcDE6SWQ9Il84NTc5NDY5Zi04YTI5LTQ4ZTEtYjcxNy0wNmIxZmUzYTFlNWQtMDY4RkUzRTU5QzhDRjczNDIyQUQyODI1QThDMkQ1OUUiIHhtbG5zOnAxPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIiB4bWxucz0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3Mtc3gvd3Mtc2VjdXJlY29udmVyc2F0aW9uLzIwMDUxMiI+PElkZW50aWZpZXI+dXJuOnV1aWQ6ZTZjMjJkZDUtODc1ZC00MDU5LTg4ZTYtMTVkZTYzOTRlM2FlPC9JZGVudGlmaWVyPjxDb29raWUgeG1sbnM9Imh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwNi8wNS9zZWN1cml0eSI+QVFBQUFOQ01uZDhCRmRFUmpIb0F3RS9DbCtzQkFBQUEvL0JyMUxWNEQweVdESTk4cHRZeWF3QUFBQUFDQUFBQUFBQVFaZ0FBQUFFQUFDQUFBQUNudllyQkFiOEJtTVUyZ0VBUW9hdm5Rekt5V21BOHNxRCtWbTFqRjc0VHBRQUFBQUFPZ0FBQUFBSUFBQ0FBQUFCeEtJeTViU2lERldwY3J6Q0lvdHUwOC95MFhJNTdLWkVCL1RrNHRMUGZwbUFEQUFBVzN0MjFaU0NuNUlwVVl1T0xNVCswaitLU1ZTdDBvVk8vc1paRDIrQXB0TU9Qdk5PN2c1T1VRblYxQ1NxYkRyNnJVSk94NjJqOWREZWxTSTRyQjIxNDRaSFdGeFlNR3RlNlB5NXBnSENPa0JtbVlFeEREeG5RV0dFU0t6UmttZzltSUtrT0hIM1VPc2NUaHlTR0NxU2sxT2ZSOVh3RVd2dUtwSHZYamYwdjR5Qkxtd3o2cFMzNWJtcWNRSGs4VnhFMHhMQ2ZJOTRVRG5zWjBEcHVraFNnWTB4SE1TN2JGTkV6MTlpRTNhajZHbWZmVlVidHFoTUVpNVEvc1oyK0xXdnBqcDlWY0R4M3RkVDZXZU1NM3g2Wm8ybExYNlArWTluaWc3RlNma2c4SDJVTTUwYUdla09rWmxybllhUW5yMkZxTzRxTUlFYU9kMnp1dkYvRnFEZ2syV3dqVTlqMW5RdWJsanIvODZvbVFDUGo1Z0ZLQktOdlFEemVqQjdNRzlacm5vQWpaYk03cDRTMkZVN3RweXBQQVBxZUJEcEwxK1dvMU16VFRlakhTcVpoUktiaVhqdkVWbGdHc2xiRXpNWXY3RUIwWGo0UXMvZEtGN09VMkJ1UloyVmxMdXBEMDFQeXFTZDNPdGQ0Y0ZmQ2N2ZytJek8rNk1SaU9uRDZNdWw1VE1GZVZFMTZxSk16YnBsWXVnMXRwUDU0VWd2RUFCZ1ZTNFMrVVptUE1neVBVWDFsbVdGOGt5TFRDbVFBWHJJWmI2bE9XRzc2UzhQQUkzMktOZjVrb0tFRXBoVVBkUStJYU5CRFpxU3VkTDJ4TGJ2MTR1bGdDRkRoTUppUlg0b2YxaVJIUUg3WEdDRzRXdVBRS2tna2ZWeVZlVDRwbmNHcXZvUFJHWDVjcXFzOFpaUDAyT01pNG45L3lXdlFtbFlPZmtObTRIVysrM29tZlJuS2VUVXlSd1lFdzVwYURhT3dzWDJVVllVb2ljUnJzbTBpWmM0dHdwMkMxSjdZVE13djhheTFNdXIvUVlqbzMzUzRUcVBnR2ZiMVR5NlkrMXNOdk8zOUtQQ2hvN1RWUnp6M24zbWlWMSs2VnBoYmZYTU1TV1VBUWJSNHkwSnUveTNSV0RtQ1JNT0ZCalZFcjhx"
;Const $cst_AbiQaTestModule_fedAuth1_160711 = "TXNCMVh3b2VrQzRlWGxkMGNkaStsdkVnWFBFbFMxZTk2UUxvTEhVOXhjU2JZZ2NCSmswN1RTdDNiQUF0a2VBT0l1Tk1JNitGMVNNcEFMNDFPM0F5TUtlL1VaeExBVk5YWjlHN3lFNVcyRndtK2NRcGJobEhCUER0eTJDM3Y5Q2RHa0lHd001c3pha1U5elBpRzhCVG9rM01rTmF5a3NvWThiajhncFdCSS9IVDgyTlZGaXl5aDBwd0FieGN2YWh0VnBlb1Z4enlndHM1bWFkOVlQbVE3WDJjYWdvVHcwNjBrWk9CVmxwM0VaM0Q2Z2prcnJyUVc4UFpMTkpiYkVNOTFlUXNVczZpbGhhb0xIZjhHN3U4VGxUQmpncGFpTkNKQUFBQUFsRVF1Q1pYZ1Uwc1VZakltajRrSngrTTZNQkovUXE4N0FyeEpYSFdCMUF4UDZGZ3ZYWTBLNVJ2dUFSR2l2OC9BVHd2WkhGVlNGamxQVjZha2d6anN0UT09PC9Db29raWU+PC9TZWN1cml0eUNvbnRleHRUb2tlbj4="

; VAMIGO-ABI-A1
Const $cst_AbiQaTestModule_fedAuth_160711 = "77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U2VjdXJpdHlDb250ZXh0VG9rZW4gcDE6SWQ9Il8xZmI3OWFiYS0yNDQzLTQ5ZjUtOTE5ZC0xZjExMTMxYmEzM2MtQTU0QUFGODhEMUZDRTRGMDg5MkRERDJDMTExMzlDNzciIHhtbG5zOnAxPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktdXRpbGl0eS0xLjAueHNkIiB4bWxucz0iaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3Mtc3gvd3Mtc2VjdXJlY29udmVyc2F0aW9uLzIwMDUxMiI+PElkZW50aWZpZXI+dXJuOnV1aWQ6MTY4OGRjMmQtMzdkOS00OWQ0LWJmNGMtOGY2MGMxMGY3YmUyPC9JZGVudGlmaWVyPjxDb29raWUgeG1sbnM9Imh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwNi8wNS9zZWN1cml0eSI+QVFBQUFOQ01uZDhCRmRFUmpIb0F3RS9DbCtzQkFBQUF5eDM4SDdnZmwwR1IyeWZENkhQQW1BQUFBQUFDQUFBQUFBQVFaZ0FBQUFFQUFDQUFBQUJaUTl4SERQM3o2L0tCKzRNYTROL1BURlIvVkJKQ09RV1ZPa05lYUZ6VGR3QUFBQUFPZ0FBQUFBSUFBQ0FBQUFDQkhLNWpQMlp1UkRnRnkyd29iSHdNcDR6cmpqdXVaV01ySlVTOVdnWmtZUUFFQUFDZldaU3A0cTJRbTREM25ka05aalF2OVBXZWxNdHNyK0JwQkd0UTNodVZjeE16M2hVMk9nZ3ZLZ0g3d25pMWpSdExvRHd0by9PZS9yV2FMUjB3WWx4ckliYTNEUHZaSXEwNnh5U0lHenVJRHJlSDNPeE9YZGMyOTIxdngycDdmQTVKVXYvOU9XRGltdUx0V0ZGelJ2c1pOWXhGMFVtN3FLRFVaU2UwOU9YdkFqT1ZMNVdTVm5VeGVTNWltTDU3MTlIWFNJTC9QOGF5cHZsS3hZRFZCTGNqWGR3N0xhRjBHK1dXeTFaY0E5Nk1VMGhYUGRuVEFDdWIvVFFMcTEwSFpSWCsyY3I4ckpPRkNObTQ4TFdMaVdIZjBNMDNNSnBZdFN3S1NDUHZEZUQ3UFg3OUY2TWgyMy9UeUtRYXBwbG1nMkNva0w1Zjd2UzdBVjd4NS9NajQyUFBWWWJab2I3ZXJuaVlxTWl3MHM0ZjA3aU1DOUtjWE1yVXZCRXhnRGQ2SktsajBJUmhtMVdXT0EwbHdjY0JQc3VreU10azIybGhsRC9HSHU2b1lPdFc1cFlGdFpGOGp6K3krTThqdHNObXllT0M3ZWZkSjBSbnZLcnBxaHk4OS9ITWQ5TDVIZjJkc0NBVXMzSVlsTUs2Rjlqa0NETXVuMDFGdTBUTTVCazJhSDFjN2FHOTdyZnBjNUpGeVozWDc2V3NZRWtFNFA0eGlDWGVEUWNMWlBCVyszdXFZcW1OOHBubHVYcmNrcUd0WTI0Vk5zVUwyVmFUVEcyZVRWeDNkOW5BS3JJQUIxSnljUGdmQ3NKSlRKYk9LMVVQQU1aT0R3QjJoZ3pqYWQ3RHUwa0I3Q3BuRDV3UGlBUGZ1dTUvaDNwb1hsTTZpNlFXaTYwRU9zWGJCY3VQemNaYitDZnpGb2FLMWZYU1NNZkYvbEliTDBHM1o0VldKTUMvM1JrS092aVk3dEl0V05pMUNKRjBhVmtTVkEwcCtqVnkxdFVlZUpvMXd4b2N1UDNGeWdvWk1QWWVLQWRMWTh1ellId3UzcnBzZUVwSE1wMHFBNUgrekJaRmRibHNwZThUV0FBQ09OMlpxaFBhY3RzK1dZOWRrQTZsakJZMXB0MDRKUml5alBqSndiaGdlbnQzdkgz"
Const $cst_AbiQaTestModule_fedAuth1_160711 = "UDNOR3RuYzhlR3BQeVIvekswWUozb2dNVkRYTTRJeTNHWDJDV1RpOFFxemR2OHB3czEvL24zK3JYU3RXdFBFUTlud3Bta24xVlJkVkR4UW9LLzQ0RlpDOEg5T28yZUZnckJoVjR5eTh2blFXczlDTWNnSUd4Vm1rOU5BZ3R2ejF4emR3Um4wdkhtNEo4a1hKM1dFOXJQVy9jeGVJdENHNTdSdzd0ZjAzT0tIU24ycEl6ME9nRk92elFtUDJmOWJZWjZuSGswU1NxL3dzRFF2L3Y4UlgxQit1Nlc1OFhrQW9ZbHMxMXloU3R4TlA3ZHZGZXJnanVaWHpsc09NNElyZUE1KzFlbzRwUVhYVzdKNGJiVXlFcHowVUpxMnFOV1NUR1RwQmxEcGlZckp5OTRXN0ZpdmsrcnRwbzZQRlh4Nk9NNE1vNjFaOU1zSnpoaWNTRUc5a05ISWFrMEFWU1dUK3JQbXdJS1ZqL3k1aTdZMTgxVHNSNTg3a1VsMG1GbVRNV2dYZm9vdWM4SytiTGlJUU9xZjlCUHA2d2NJMWt2L2VNelZHZ2I1TkdrVVN0K1NOcmV2VzVMOUdKUWNndGIyKzREb1dIRDVqNU9HVU9yeGRIT3llekFXUVRwLzFxQlI3N2NxL3g1MjhneTZ4NU5EVy9adXlRUUFBQUFPQTFqeGhNUGpCL1BkcEZCbTF0bnZGODIwR3JwUC9MOW9hek8wL29TeG5OTlA4WEJFRkdCNnJDMkhvNVluanBnMXZXdmJKRVFXUGpnZ1RNa2F6K0NlMD08L0Nvb2tpZT48L1NlY3VyaXR5Q29udGV4dFRva2VuPg=="


; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_GetChannelSettingsFileContent_AV160711
; ===============================================================================================================================
Func AbiQaTestModule_GetChannelSettingsFileContent_AV160711($serverBaseUrl, $serverProjectId)
	; FIX 160712-
	If StringRight($serverBaseUrl, 1) <> '/' Then
		$serverBaseUrl &= '/'
	EndIf
	; ORIGINAL CODE
	Return _
			'<?xml version="1.0" ?>' & _
			@CRLF & '<ChannelSettings>' & _
			@CRLF & '	<Server>' & _
			@CRLF & '		<Location>' & $serverBaseUrl & '</Location>' & _
			@CRLF & '		<ParentId>' & $serverProjectId & '</ParentId>' & _
			@CRLF & '	</Server>' & _
			@CRLF & '	<Client>' & _
			@CRLF & '		<Applications/>' & _
			@CRLF & '		<UserInterfaceSettings>' & _
			@CRLF & '			<LanguageThreeLetterCode>ENG</LanguageThreeLetterCode>' & _
			@CRLF & '			<LanguageId>1</LanguageId>' & _
			@CRLF & '		</UserInterfaceSettings>' & _
			@CRLF & '	</Client>' & _
			@CRLF & '</ChannelSettings>' & _
			''
EndFunc   ;==>AbiQaTestModule_GetChannelSettingsFileContent_AV160711

; #FUNCTION# ====================================================================================================================
; Name...........: AbiQaTestModule_GetChannelSettingsFileContent
; History........: ORIGINALLY - Designed to be written in ANSI
;                    with UserInterfaceSettings set to ENG.
;                    and with 2 parameters :
;                      * Server > Location
;                      * and Server > ParentId
;                  JUL 11, 2016 - Added code for UTF-8 handling (probably without BOM)
;                    + comments regarding Protection, ProductSerial and LicenseServer
;                        (hope it's retrieved automatically from the server, we shouldn't have to set this)
;                    + fedAuth and fedAuth1 settings
; ===============================================================================================================================
Func AbiQaTestModule_GetChannelSettingsFileContent($serverBaseUrl, $serverProjectId, $writeEncoding = 0)
	; FIX 160712-
	If StringRight($serverBaseUrl, 1) <> '/' Then
		$serverBaseUrl &= '/'
	EndIf
	; ORIGINAL CODE
	$documentDeclaratn = '<?xml version="1.0" ?>'
	If $writeEncoding = 0 Then $documentDeclaratn = '<?xml version="1.0" ?>'
	If $writeEncoding = 128 Then $documentDeclaratn = '<?xml version="1.0" encoding="UTF-8"?>' ; with BOM
	If $writeEncoding = 256 Then $documentDeclaratn = '<?xml version="1.0" encoding="UTF-8"?>' ; without BOM
	; 		@CRLF & '<ChannelSettings>' & _
	Return _
			$documentDeclaratn & _
			@CRLF & '<ChannelSettings>' & _
			@CRLF & '	<Protection>' & _
			@CRLF & '		<ProductSerial>0022A8BC-B2F3DD99-D8A17412-3D64035F</ProductSerial>' & _
			@CRLF & '		<LicenseServer>http://licenses.assimaqa.local:100/Services/Licensing.svc</LicenseServer>' & _
			@CRLF & '	</Protection>' & _
			@CRLF & '	<Server>' & _
			@CRLF & '		<Location>' & $serverBaseUrl & '/' & '</Location>' & _
			@CRLF & '		<ParentId>' & $serverProjectId & '</ParentId>' & _
			@CRLF & '	</Server>' & _
			@CRLF & '	<Client>' & _
			@CRLF & '		<Applications/>' & _
			@CRLF & '		<UserInterfaceSettings>' & _
			@CRLF & '			<LanguageThreeLetterCode>ENG</LanguageThreeLetterCode>' & _
			@CRLF & '			<LanguageId>1</LanguageId>' & _
			@CRLF & '		</UserInterfaceSettings>' & _
			@CRLF & '	</Client>' & _
			@CRLF & '	<Security>' & _
			@CRLF & '		<FedAuth>' & $cst_AbiQaTestModule_fedAuth_160711 & '/' & '</FedAuth>' & _
			@CRLF & '		<FedAuth1>' & $cst_AbiQaTestModule_fedAuth1_160711 & '</FedAuth1>' & _
			@CRLF & '	</Security>' & _
			@CRLF & '</ChannelSettings>' & _
			''
EndFunc   ;==>AbiQaTestModule_GetChannelSettingsFileContent
