#include-once
#include <AuItMiscUDFs\GlobalPseudoArray.au3>

Const $AQTM_NOTIFS_GPAE_PREFIX = "gpaeAbiNotifs" ; Global Pseudo-Array Element

; ===============================================================================================================================
; AbiQaTestModulePrivate_RecordAbiInfo
; ===============================================================================================================================
Func AbiQaTestModulePrivate_RecordAbiInfo($sNotification)
	Return GlobalPseudoArray_AddElement($AQTM_NOTIFS_GPAE_PREFIX, $sNotification)
EndFunc   ;==>AbiQaTestModulePrivate_RecordAbiInfo

; ===============================================================================================================================
; AbiQaTestModulePrivate_RememberAbiInfo
; ===============================================================================================================================
Func AbiQaTestModulePrivate_RememberAbiInfo($infoIndex)
	Return GlobalPseudoArrayPrivate_GetElement($AQTM_NOTIFS_GPAE_PREFIX, $infoIndex)
EndFunc   ;==>AbiQaTestModulePrivate_RememberAbiInfo
