#include-once
; PATH ENVIRONMENT VARIABLE
; Used to be PRE-PROVISIONED
; Currently DYNAMICALY CONFGURED
Const $UAFX_LEGACY2_DIRPATH = "C:\AutoTestingV2" ; <<< DEPRECATED
Const $UAFX_RUNTIME_DIRPATH = "C:\Progra~n\AutoIt3\UafxRt"
Const $UAFX_WORKING_DIRPATH = @WorkingDir
; File system names declaration
Const $THIRD_PARTY_IMPL_DIRNAME_2 = "bin" ; <<< DEPRECATED
Const $THIRD_PARTY_IMPL_DIRNAME = "ThirdPartyImpl"
; Paths declaration
; Old location: C:\AutoTestingV2\bin
; New location: C:\AutoTestingVx.Ctrl\UserData\Suites\WebQa699v01\ThirdPartyImpl
; Alt location: C:\Progra~n\AutoIt3\UafxRt\ThirdPartyImpl
Const $THIRD_PARTY_IMPL_DIRPATH_LEGACY2 = $UAFX_LEGACY2_DIRPATH & "\" & "bin" ; <<< DEPRECATED
Const $THIRD_PARTY_IMPL_DIRPATH_RUNTIME = $UAFX_RUNTIME_DIRPATH & "\" & $THIRD_PARTY_IMPL_DIRNAME
Const $THIRD_PARTY_IMPL_DIRPATH_WORKING = $UAFX_WORKING_DIRPATH & "\" & $THIRD_PARTY_IMPL_DIRNAME
; Dynamic setup #2
If FileExists($THIRD_PARTY_IMPL_DIRPATH_WORKING) Then
	$envPathVal = EnvGet("PATH")
	$envPathVal = EnvSet("PATH", $THIRD_PARTY_IMPL_DIRPATH_WORKING & ";" & $envPathVal)
EndIf
; Dynamic setup #3
If FileExists($THIRD_PARTY_IMPL_DIRPATH_RUNTIME) Then
	$envPathVal = EnvGet("PATH")
	$envPathVal = EnvSet("PATH", $THIRD_PARTY_IMPL_DIRPATH_RUNTIME & ";" & $envPathVal)
EndIf
