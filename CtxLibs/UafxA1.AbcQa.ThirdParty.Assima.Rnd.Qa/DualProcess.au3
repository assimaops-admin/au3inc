#include-once
#include <CoreUAFLibs\StdCoreLib\TokenWnd.au3>
#include ".\TimeoutManagement.au3"
#include ".\OutputsManagement.au3"
;#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd\GetValueFromIniFiles.au3"


Const $INTERRUPT_HOTKEY = "^!+i"

; self monitoring processes
Const $OPTION_SELF_MONITOR = "--self-monitor=" ; REF.PS/20101222_HARDINTEROP
Const $OPTION_SELF_TIMEOUT = "--self-timeout="
Const $OPTION_SELF_SESSION = "--self-session="

; auxilliary process for self-monitoring
Global $_DualProcess_isEnabled = 0
Global $_DualProcess_nPidMonitored = 0
Global $_DualProcess_MonitorTimeoutCallback = ""

; callbacks
$AFTER_TEST_MONITOR_LAUNCH = "" ; ................... Call inside of the first process (after fork) [REF.PS/20100608_221554]
$TEST_MONITOR_STARTUP_PROC = "" ; ........................ Call at the begining of the dual process [REF.PS/20100608_221558]
$TEST_MONITOR_SHUTDOWN_PROC = "" ; ... Call before closing the dual process (whether timeout or not) [REF.PS/20100608_222009]


;-------------------------------------------------------------------------------
; Name .........: DualProcess_Enable
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func DualProcess_Enable($sSessionId)
	$_DualProcess_isEnabled = 1
EndFunc   ;==>DualProcess_Enable

;-------------------------------------------------------------------------------
; Name .........: DualProcess_IsEnabled
; Description ..: is module using duality for timeout monitoring
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_IsEnabled()
	Return $_DualProcess_isEnabled
EndFunc   ;==>DualProcess_IsEnabled

;-------------------------------------------------------------------------------
; Name .........: DualProcess_IsThisMonitor
; Description ..: is module using duality for timeout monitoring
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_IsThisMonitor()
	Return GetCmdLineOptnIndex($OPTION_SELF_MONITOR)
EndFunc   ;==>DualProcess_IsThisMonitor

;-------------------------------------------------------------------------------
; Name .........: DualProcess_SinkHere
; Description ..: this is meant to be used inside of the self monitored module
; Parameters ...: Callbacks
;                   - Func MonitorTimeoutCallback($nPidMonitored, $nTimeout)
; Return .......: Monitored process:
;                   - PID of the monitor
;                 Monitoring process
;                   - never returns, this is an exit.
;-------------------------------------------------------------------------------
;
;
Func DualProcess_SinkHere($sSessionId, $MonitorTimeoutCallback, $nTimeout = 0)
	Local $monitorCmdLineByRef
	Return DualProcess_SinkHereWithCmdLine($sSessionId, $MonitorTimeoutCallback, $nTimeout, $monitorCmdLineByRef)
EndFunc   ;==>DualProcess_SinkHere
;
;
Func DualProcess_SinkHereWithCmdLine($sSessionId, $MonitorTimeoutCallback, $nTimeout, ByRef $monitorCmdLineByRef)
	$_DualProcess_isEnabled = 1

	; re-lauch yourself as a timeout monitor?
	If DualProcess_IsEnabled() And Not DualProcess_IsThisMonitor() Then

		; #TrayIcon
		TraySetIcon("Shell32.dll", 246) ; the player's icon = green play

		; DISABLED FEB 2, 2016
		; This piece of code is deprecated, the timeout should be set directly in the modules
		; or in the master template of the given test-module
		; -----
		; ; read the timeout from the configuration files?
		; if ($nTimeout = Default) or ($nTimeout = 0) Then
		; 	$nTimeout = ReadTimeoutFromIniFiles()
		; EndIf
		; -----

		; command line builder
		$NewCmdLine = Quote(@ScriptFullPath)
		; copy the original parameters
		$NewCmdLine &= CmdLineOriginalParamsCopy()
		; add new parameters
		;
		; See 'DualProcess_GetParamsFromCmdLine',
		; for info about how it is being read.
		; (below in this file)
		;
		$NewCmdLine &= " "
		$NewCmdLine &= PrepareCmdLineOptn($OPTION_SELF_MONITOR, String(@AutoItPID))
		If $nTimeout <> Default Then
			$NewCmdLine &= " "
			$NewCmdLine &= PrepareCmdLineOptn($OPTION_SELF_TIMEOUT, String($nTimeout))
		EndIf
		$NewCmdLine &= " "
		$NewCmdLine &= PrepareCmdLineOptn($OPTION_SELF_SESSION, $sSessionId)

		If $NewCmdLine <> "" Then
			ConsoleWrite("[" & @AutoItExe & " " & $NewCmdLine & "]... #ConsoleOnly" & @CRLF)
			$monitorCmdLineByRef = @AutoItExe & " " & $NewCmdLine
			$launchRet = Run(@AutoItExe & " " & $NewCmdLine, "", @SW_HIDE)
			If $AFTER_TEST_MONITOR_LAUNCH <> "" Then Call($AFTER_TEST_MONITOR_LAUNCH)
			Return $launchRet
		Else
			MsgBox(0, "Fatal error", "Process monitor not launched")
			Exit 1
		EndIf

	Else

		; #TrayIcon
		Opt("TrayIconHide", 1) ; the monitor's icon = none
		; TraySetIcon("C:\Windows\System32\Pifmgr.dll", 39) ; the monitor's icon = dynamite ; #EnableToDebug

		Local $nPidMonitored = 0
		If DualProcess_GetParamsFromCmdLine($nPidMonitored, $nTimeout) Then
			If $TEST_MONITOR_STARTUP_PROC <> "" Then Call($TEST_MONITOR_STARTUP_PROC)
			$_DualProcess_nPidMonitored = $nPidMonitored
			$_DualProcess_MonitorTimeoutCallback = $MonitorTimeoutCallback
			DualProcess_SetHotKeyForInterrupt()
			; FYI, $_TestMonitor_OnTimeout = "KillTestModuleProc"
			; => $MonitorTimeoutCallback = "KillTestModuleProc"
			; => $_DualProcess_MonitorTimeoutCallback = "KillTestModuleProc"
			; whereas $TEST_MONITOR_SHUTDOWN_PROC = "TestMonitorShutdownProcV2"
			Exit DualProcess_TimeoutMonitoringProc($nPidMonitored, $nTimeout, $MonitorTimeoutCallback) ; the dual process never goes pass this point (REF.PS/20101222_151835) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			MsgBox(0, "Fatal error", "bad command line '" & $CmdLineRaw & "'")
			Exit 1
		EndIf
	EndIf
EndFunc   ;==>DualProcess_SinkHereWithCmdLine

;-------------------------------------------------------------------------------
; Name .........: DualProcess_GetParamsFromCmdLine
; Description ..:
; Parameters ...:
; Return .......: formatted string
;-------------------------------------------------------------------------------
Func DualProcess_GetParamsFromCmdLine(ByRef $pid, ByRef $nTimeout)
	$bResult = False
	$pid_ = GetCmdLineOptnAsInt($OPTION_SELF_MONITOR)
	$nTimeout_ = GetCmdLineOptnAsInt($OPTION_SELF_TIMEOUT)

	; #TrayIcon
	; Remark : this will not show because the item is hidden.
	; TraySetToolTip("Timeout value: " & $nTimeout_) ; #EnableToDebug

	; Optional parameter
	; ("if not provided, use my own then", says the Dual Process)
	If IsInt($nTimeout_) Then
		$nTimeout = $nTimeout_
	EndIf

	; Compulsory parameter
	; ("if not provided, no use out of me", says the Dual Process)
	; => Return false if the PID was not provided,
	If Not IsInt($pid_) Then
		$bResult = False
	Else
		$pid = $pid_
		$bResult = True
	EndIf
	Return $bResult
EndFunc   ;==>DualProcess_GetParamsFromCmdLine

;-------------------------------------------------------------------------------
; Name .........: DualProcess_TimeoutMonitoringProc
; Description ..: timeout monitoring; if timeout occurs:
;                   - take screenshots,
;                   - dump list of opened windows,
;                   - dump list of opened processes,
;                   - terminate the automation process and log events
; Note .........: The process being terminated is the automated monitor, NOT the
;                   tested application; the tested app. will be closed when the
;                   next test-module will be launched.
; Parameters ...: Callbacks
;                   - Func MonitorTimeoutCallback($nPidMonitored, $nTimeout)
; Return .......: never returns, this is an exit.
;-------------------------------------------------------------------------------
Func DualProcess_TimeoutMonitoringProc($nPidMonitored, $nTimeout, $MonitorTimeoutCallback)

	$isClosed = ProcessWaitClose($nPidMonitored, $nTimeout) ; <<< OLD CODE

	; #EmpowerDualProcess
	; NEW CODE PROPOSAL BELOW
	; THIS SHOULD ALLOW RECEPTION OF KEYSTROKES AND EVENTS
	; $poll_delay_in_sec = 500
	; $isClosed = Not ProcessExists($nPidMonitored)
	; While Not $isClosed
	; 	Sleep($poll_delay_in_sec)
	;	; Do things here ?
	;	; (...)
	; 	$isClosed = Not ProcessExists($nPidMonitored)
	; WEnd

	if (Not $isClosed) And StringInStr(DualProces_GetStatus(), "Saving automation data...") Then ; REF.PS/20101222_145004 - HARDCODED INTEROP
		TimeoutManagement_DUAL_AutomationLog(1, "INFO.: HARDCODED INTEROP 'Saving automation data...' ACK.")
		; wait a couple more minutes
		$wait_min = 3
		TimeoutManagement_DUAL_AutomationLog(1, "INFO.: Wait up to '" & $wait_min & "' more minutes...")
		$startTimer = TimerInit()
		If ProcessWaitClose($nPidMonitored, $wait_min * 60) Then
			$diffTimer = TimerDiff($startTimer)
			$diffTimer /= 1000
			$diffInfoMsg = "INFO.: Module's extra operating time = " & StringFormat("%.2f", $diffTimer) & "s"
			; New logging
			TimeoutManagement_DUAL_AutomationLog(1, $diffInfoMsg)
			; Old logging
			$simpleLocalLogs = HostInfo_GetSimpleLocalLogDir_EnsureExists()
			$theSimpleLocalLog = $simpleLocalLogs & "\AutoIt.DualProcess.Log.txt"
			FileWriteLine($theSimpleLocalLog, "[" & @YEAR & "-" & @MON & "-" & @MDAY & "--" & @HOUR & "-" & @MIN & "-" & @SEC & "]")
			FileWriteLine($theSimpleLocalLog, $CmdLineRaw)
			FileWriteLine($theSimpleLocalLog, $diffInfoMsg)
		Else
			$isClosed = 1
			$msg = ">>> DUAL >>> " & "WARN.: " & $wait_min & " minutes for the test-module to save data." _
					 & @CRLF & "@ScriptName = " & @ScriptName _
					 & @CRLF & "(Closing.)" _
					 & ""
			Warning($msg)
		EndIf
	EndIf
	$monitorExitCode = (Not $isClosed)
	Return DualProcess_DualExitSink($TIMEOUT_99_DUALPROC_TAG, $nPidMonitored, $monitorExitCode) ; NO MORE LOGGING PASSED THIS POINT!!

EndFunc   ;==>DualProcess_TimeoutMonitoringProc

;-------------------------------------------------------------------------------
; Name .........: DualProcess_SetHotKeyForInterrupt
; Description ..:
; Note .........:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_SetHotKeyForInterrupt()
	Return HotKeySet($INTERRUPT_HOTKEY, "DualProcess_InterruptCallback")
EndFunc   ;==>DualProcess_SetHotKeyForInterrupt

;-------------------------------------------------------------------------------
; Name .........: DualProcess_InterruptCallback
; Description ..:
; Note .........:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_InterruptCallback()
	HotKeySet($INTERRUPT_HOTKEY)
	Exit DualProcess_DualExitSink($TIMEOUT_51_OPERATOR_TAG, $_DualProcess_nPidMonitored) ; NO MORE LOGGING PASSED THIS POINT!!
EndFunc   ;==>DualProcess_InterruptCallback

;-------------------------------------------------------------------------------
; Name .........: DualProcess_FuncLevelSelfTimeoutExitCode
; Description ..:
; Note .........:
; Parameters ...: Typical $selfExitCode = 777 ; $AU3_EXIT_LOCAL_TIMEOUT
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_FuncLevelSelfTimeoutExitCode($selfExitCode)
	Return DualProcess_SelfExitSink($TIMEOUT_52_FUNCTION_TAG, @AutoItPID, $selfExitCode) ; NO MORE LOGGING PASSED THIS POINT!!
EndFunc   ;==>DualProcess_FuncLevelSelfTimeoutExitCode

;-------------------------------------------------------------------------------
; Name .........: DualProcess_SelfExitSink
; Description ..:
; Note .........:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_SelfExitSink($timeoutType, $nPidMonitored, $exitCode = 0)
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- INIT '" & $timeoutType & "' TIMEOUT.")
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- APPLY-ON %CurrentProcess%.")
	$logString = ""
	If Not TimeoutManagement_OnTimeoutDirV0($timeoutType, $exitCode, $logString) Then
		; ALREADY TIMEOUT IN PROGRESS ?! HOW IS THAT POSSIBLE ?!!
		; AT THE TIME OF WRITING -- FEB 4, 2016 -- THERE IS NO OTHER TIMEOUT AT A LOWER LEVEL THAN THE 'FUNCTION' LEVEL.
		TimeoutManagement_NORMAL_AutomationLog("INFO.: Another interruption was already raised (DualProcess_SelfExitSink)...")
		TimeoutManagement_NORMAL_AutomationLog("WARNING: ANOTHER TIMEOUT PROCESS IS ALREADY IN PROGRESS...") ; <<< ???
		TimeoutManagement_NORMAL_AutomationLog("WARNING: AT THIS POINT IN THE CODE, IT DOES NOT MAKE SENSE,") ; <<< ???
		TimeoutManagement_NORMAL_AutomationLog("WARNING: THERE IS NO OTHER TIMEOUT AT A LOWER LEVEL THAN THE 'FUNCTION' LEVEL,") ; <<< ???
		TimeoutManagement_NORMAL_AutomationLog("WARNING: AND THE DUAL PROCESS IS UNLIKELY TO INTERFEER WITH $WM_CLOSE MESSAGING.") ; <<< ???
		TimeoutManagement_NORMAL_AutomationLog(@CRLF & @CRLF & $logString & @CRLF)
		TimeoutManagement_NORMAL_AutomationLog("CANCEL '" & $timeoutType & "' TIMEOUT.") ; <<< ???
	Else
		; HERE, NOTHING TO DO, THE CLEANUP CODE WILL BE AUTOMATICALLY CALLED UPON EXIT
	EndIf

	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #A1...")
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B3...")
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B2...")
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B1...")

	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- SCHEDULE RETURN '" & $exitCode & "' EXIT CODE.")
	TimeoutManagement_NORMAL_AutomationLog("INTERRUPT- FINALIZE")

	Return $exitCode

EndFunc   ;==>DualProcess_SelfExitSink

;-------------------------------------------------------------------------------
; Name .........: DualProcess_DualExitSink
; Description ..:
; Note .........:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_DualExitSink($timeoutType, $nPidMonitored, $exitCode = 0)

	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- INIT '" & $timeoutType & "' TIMEOUT.")
	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- APPLY-ON '" & $nPidMonitored & "' PROCESS.")

	;;; ASSERTION ;;;
	$isTimeout = ProcessExists($nPidMonitored)
	Assert(IsParallelValue($isTimeout, $exitCode), "IsParallelValue($isTimeout, $exitCode)")
	If Not IsParallelValue($isTimeout, $exitCode) Then
		$exitCode = 1
		TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- INIT '" & $timeoutType & "' TIMEOUT.")
		TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- APPLY-ON '" & $nPidMonitored & "' PROCESS.")
		TimeoutManagement_DUAL_AutomationLog($exitCode, "CRITICAL ERROR: INTERNAL ASSERTION FAILED.")
	EndIf

	;;; PROCEDURE ;;;
	If Not $isTimeout Then
		TimeoutManagement_DUAL_AutomationLog($exitCode, "INFO.: The (eventual) timeout is not relevant because the monitored process does not exist anymore.")
		TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- DISABLE PROCESS KILLING.")
	Else
		;;; LOGGING ;;;
		$logString = ""
		If Not TimeoutManagement_OnTimeoutDirV0($timeoutType, $exitCode, $logString) Then
			; Already timeout in progress, probably triggered by a custom, timed 'wait' function call
			TimeoutManagement_DUAL_AutomationLog($exitCode, "INFO.: Another interruption was already raised (DualProcess_DualExitSink)...")
			TimeoutManagement_DUAL_AutomationLog($exitCode, "WARNING: ANOTHER TIMEOUT PROCESS IS ALREADY IN PROGRESS...") ; <<< ???
			TimeoutManagement_DUAL_AutomationLog($exitCode, "WARNING: AT THIS POINT IN THE CODE, IT CAN ONLY BE A 'CUSTOM TIMED WAIT' FUNCTION CALL") ; <<< ???
			TimeoutManagement_DUAL_AutomationLog($exitCode, @CRLF & @CRLF & $logString & @CRLF)
			TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- DISABLE CLEANUP PROCEDURE #A1.")
		Else
			;;; PROCESSING ;;;
			TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- TRIGGER CLEANUP PROCEDURE #A1...")
			Call($_DualProcess_MonitorTimeoutCallback, $nPidMonitored) ; Typical: 'KillTestModuleProc'
			TimeoutManagement_DUAL_AutomationLog($exitCode, "INFO.: The test-module was terminated.")
		EndIf
	EndIf

	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B3...")
	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B2...")
	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- RESCHEDULE CLEANUP PROCEDURE #B1...")

	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- SCHEDULE RETURN '" & $exitCode & "' EXIT CODE.")
	TimeoutManagement_DUAL_AutomationLog($exitCode, "INTERRUPT- FINALIZE")

	If $TEST_MONITOR_SHUTDOWN_PROC <> "" Then Call($TEST_MONITOR_SHUTDOWN_PROC)
	OutputsManagement_OnMonitorExit( _
			GetCmdLineOptn($OPTION_SELF_SESSION) _
			, GetCmdLineOptn($OPTION_UPLOAD_OUTPUTS) _
			, GetCmdLineOptn($OPTION_SUPER_SESSION) _
			, GetCmdLineOptn($OPTION_TEST_MODULE_ID) _
			)
	; NO MORE LOGGING PASSED THIS POINT!!
	Return $exitCode

EndFunc   ;==>DualProcess_DualExitSink

;-------------------------------------------------------------------------------
; Name .........: DualProcess_SetStatus
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_SetStatus($status)
	$title = "DualProcess." & @ScriptName & ".Status"
	Return TokenWnd_Inst($title, $status)
EndFunc   ;==>DualProcess_SetStatus

;-------------------------------------------------------------------------------
; Name .........: DualProces_GetStatus
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProces_GetStatus()
	$title = "DualProcess." & @ScriptName & ".Status"
	Return TokenWnd_GetValue($title)
EndFunc   ;==>DualProces_GetStatus

;-------------------------------------------------------------------------------
; Name .........: DualProcess_SoftKill
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_SoftKill()
	TimeoutManagement_DUAL_AutomationLog(1, "KILL --SOFT")
	$title = "DualProcess." & @ScriptName & ".Status"
	$hWnd = WinGetHandle($title)
	If $hWnd <> "" Then WinClose($hWnd)
EndFunc   ;==>DualProcess_SoftKill

;-------------------------------------------------------------------------------
; Name .........: DualProcess_HardKill
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_HardKill($nPidMonitored)
	TimeoutManagement_DUAL_AutomationLog(1, "KILL --HARD")
	; Close the process being monitored
	; REF.PS/20091104_162442: SCRIPT/MONITOR PROCESS SYNCHRONISATION
	ProcessClose($nPidMonitored)
	ProcessWaitClose($nPidMonitored)
EndFunc   ;==>DualProcess_HardKill

;-------------------------------------------------------------------------------
; Name .........: DualProcess_FancyKill
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func DualProcess_FancyKill($nPidMonitored, $waitCloseTime)
	DualProcess_SoftKill()
	ProcessWaitClose($nPidMonitored, $waitCloseTime)
	If ProcessExists($nPidMonitored) Then
		DualProcess_HardKill($nPidMonitored)
	EndIf
EndFunc   ;==>DualProcess_FancyKill
