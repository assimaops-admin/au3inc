#include-once

; See also:
; #include <UAFXA1\StdQaLogLib\StdQaLogConstants.au3>
; #include <UAFXA1\AbcQa.ThirdParty.Assima.Rnd\AssimaConstants.au3>

; #include "..\StdQaLogLib\StdQaLogConstants.au3" -- BEFORE JUL 11, 2016
#include <CtxConf\StdQaLogConstants.au3> ; -- AFTER JUL 11, 2016

; enumerate files extensions
Const $FILE_EXTENSION_TXT = "txt"
Const $FILE_EXTENSION_TEXT = "text"
Const $FILE_EXTENSION_THE = "the"
Const $FILE_EXTENSION_SIM = "sim"
Const $FILE_EXTENSION_TUT = "tut"
Const $FILE_EXTENSION_THT = "tht"
Const $FILE_EXTENSION_DIC = "dic"
Const $FILE_EXTENSION_XML = "xml"
Const $FILE_EXTENSION_HTM = "htm"
Const $FILE_EXTENSION_HTML = "html"
Const $FILE_EXTENSION_LOG = "log"
Const $FILE_EXTENSION_DB = "db"
Const $FILE_EXTENSION_SQL = "sql"
Const $FILE_EXTENSION_LIST = "list"

; enumerate filetypes flags
; NOTE: Bit operations are performed as 32-bit integers.
Const $fFILETYPE_UNKNOWN = 0
Const $fFILETYPE_TEXT = BitShift(1, -1)
Const $fFILETYPE_ASSIMA_THES = BitShift(1, -2)
Const $fFILETYPE_ASSIMA_SIM = BitShift(1, -3)
Const $fFILETYPE_ASSIMA_TUT = BitShift(1, -4)
Const $fFILETYPE_ASSIMA_PROJ = BitShift(1, -5)
Const $fFILETYPE_ASSIMA_DUMP = BitShift(1, -6)
Const $fFILETYPE_ASSIMA_DICT = BitShift(1, -7)
Const $fFILETYPE_XML = BitShift(1, -8)
Const $fFILETYPE_HTML = BitShift(1, -9)
Const $fFILETYPE_LOG = BitShift(1, -10)
Const $fFILETYPE_SQLITE_DB = BitShift(1, -11)
Const $fFILETYPE_LIST = BitShift(1, -12)

; enumerate filetypes
Const $eFILETYPE_ASSIMA_SIMULATION = BitAND($fFILETYPE_ASSIMA_THES, $fFILETYPE_ASSIMA_SIM)
Const $eFILETYPE_ASSIMA_TUTORIAL = BitAND($fFILETYPE_ASSIMA_THES, $fFILETYPE_ASSIMA_TUT)
Const $eFILETYPE_ASSIMA_PROJECT = BitAND($fFILETYPE_ASSIMA_THES, $fFILETYPE_ASSIMA_PROJ)
Const $eFILETYPE_ASSIMA_DICTIONARY = BitAND($fFILETYPE_ASSIMA_THES, $fFILETYPE_ASSIMA_DICT)

; protocols
Const $WINDOW_PROTOCOL = "hwnd://"

; tutorials QA-testing
Dim Const $RESULT_XML_TAGS[3] = ["Result", "EndResult", "LaunchResult"]

; Scenario 1, Test Modules 1,2,3, etc.
Const $DEFAULT_PROJECT_NAME = "TEST BUILD"

; command line parameters
Const $OPTION_TEST_MODULE_ID = "--tmguid="
Const $OPTION_SUPER_SESSION = "--super="
Const $OPTION_MOVIE_SCRSHOTS = "--movie="
Const $OPTION_META_SESSION = "--meta="
Const $OPTION_RESET_VISIBLE = "--reset="
Const $OPTION_UPLOAD_OUTPUTS = "--upload="
Const $OPTION_MODULES_LIST = "--list="
Const $OPTION_THOROUGH_TESTING = "--thorough="
Const $OPTION_INPUT_DATA_PATH = "--inputs="
