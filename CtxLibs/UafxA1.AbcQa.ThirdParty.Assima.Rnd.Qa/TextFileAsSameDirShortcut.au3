#include-once
#include <File.au3>
#include <AuItCustLibs\StringV2.au3>
#include <CtxConf\HostInfo.Lite.Repositories.au3>

;-------------------------------------------------------------------------------
; Name .........: TextFileAsSameDirShortcutHacked
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TextFileAsSameDirShortcutHacked($directoryOrFile_in)
	$directoryOrFile_followed = TextFileAsSameDirShortcutMain($directoryOrFile_in)
	$directoryOrFile_followed = HostInfo_ReplaceByCloserRepository($directoryOrFile_followed)
	Return $directoryOrFile_followed
EndFunc   ;==>TextFileAsSameDirShortcutHacked

;-------------------------------------------------------------------------------
; Name .........: TextFileAsSameDirShortcutMain
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TextFileAsSameDirShortcutMain($directoryOrFile_in)
	$directoryOrFile_followed = $directoryOrFile_in
	If FileExists($directoryOrFile_in) And Not StringInStr(FileGetAttrib($directoryOrFile_in), "D") Then
		; get the name of the folder
		$szEntry = FileReadLine($directoryOrFile_in)
		$szEntry = StringReplaceLeftChars($szEntry, $STRING_HORIZONTAL_WHITESPACE_CHARS, "")
		$szEntry = StringReplaceRightChars($szEntry, $STRING_HORIZONTAL_WHITESPACE_CHARS, "")
		; compose the path
		$directoryOrFile_followed = TextFileAsSameDirShortcutSub($directoryOrFile_in, $szEntry)
	EndIf
	Return $directoryOrFile_followed
EndFunc   ;==>TextFileAsSameDirShortcutMain

;-------------------------------------------------------------------------------
; Name .........: TextFileAsSameDirShortcutSub
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TextFileAsSameDirShortcutSub($directoryOrFile_in, $szEntry)
	$directoryOrFile_followed = $directoryOrFile_in
	If FileExists($directoryOrFile_in) And Not StringInStr(FileGetAttrib($directoryOrFile_in), "D") Then
		; get the parent directory
		Dim $szDrive = "", $szDir = "", $szFName = "", $szExt = ""
		_PathSplit($directoryOrFile_in, $szDrive, $szDir, $szFName, $szExt)
		$szParentDirectory = $szDrive & $szDir
		$szParentDirectory = StringReplaceRightChars($szParentDirectory, "\", "")
		; compose the path
		If $szEntry = "" Then
			; undefined
			; return original path
		ElseIf FileExists($szParentDirectory & "\" & $szEntry) Then
			; return amended path
			$directoryOrFile_followed = $szParentDirectory & "\" & $szEntry
		Else
			; invalid
			; return original path
		EndIf
	EndIf
	Return $directoryOrFile_followed
EndFunc   ;==>TextFileAsSameDirShortcutSub

;-------------------------------------------------------------------------------
; Name .........: TextFileAsSameDirShortcutFwd
; Description ..: Reapply the link onto a sub-file (string replacement)
; Return .......: n/a
;-------------------------------------------------------------------------------
Func TextFileAsSameDirShortcutFwd($subEntry_in, $directoryOrFile_in, $directoryOrFile_followed)
	$subEntry_followed = $subEntry_in
	If $directoryOrFile_followed <> $directoryOrFile_in Then
		$subEntry_followed = StringReplace($subEntry_in, $directoryOrFile_in, $directoryOrFile_followed)
	EndIf
	Return $subEntry_followed
EndFunc   ;==>TextFileAsSameDirShortcutFwd
