#include-once
#include <Process.au3>
#include <AuItCustLibs\AutoItEx.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include <CtxLibs\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa.XrtUser\ResetIeHistory.au3>

;-------------------------------------------------------------------------------
; Name .........: ResetVisibleEnvironment
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ResetVisibleEnvironment()
	$isResetEnv = EnvGet("TEST_MODULES_RESET_VISIBLE_ENVIRONMENT")
	If Not ($isResetEnv = "0") Then
		ResetVisibleWindows()
		ResetVisibleProcesses()
	EndIf
EndFunc   ;==>ResetVisibleEnvironment

;-------------------------------------------------------------------------------
; Name .........: CloseUnwantedWindows
; Description ..:
; Return .......:
; Note .........:
; Parameters ...:
;-------------------------------------------------------------------------------
Func CloseUnwantedWindows()

	; netlogons
	Dim $logon_servers[4] = ["As-crm01", "Dcalion", "Dclondon02", "Gcserver"]
	For $server In $logon_servers
		WinClose("\\" & $server & "\NETLOGON")
		WinClose("\\" & $server & "\NETLOGON on " & $server)
		WinClose("netlogon on " & $server)
	Next

	; other windows
	WinClose("WiFi Station Utility")
	WinClose("Scheduled Tasks")

EndFunc   ;==>CloseUnwantedWindows

;-------------------------------------------------------------------------------
; Name .........: ResetModalMessagesInCaseOfCrash
; Description ..:
; Return .......:
; Note .........:
; Parameters ...:
;-------------------------------------------------------------------------------
Func ResetModalMessagesInCaseOfCrash($szTargetPath = "")

	While ResetModalMessageInCaseOfCrash($szTargetPath)
		; NOP
	WEnd

EndFunc   ;==>ResetModalMessagesInCaseOfCrash

;-------------------------------------------------------------------------------
; Name .........: ResetModalMessageInCaseOfCrash
; Description ..:
; Return .......: 'true' if a modal error message was found
; Note .........: There is a similar code in 'XxxCommon_EnsureProcessNotRunning'
; Parameters ...:
;-------------------------------------------------------------------------------
Func ResetModalMessageInCaseOfCrash($szTargetPath = "")

	; prepare return value
	$foundModalError = False

	; split the file names and paths
	$szAppName = ""
	$szAppNameWithoutExt = ""
	If $szTargetPath <> "" Then
		$szAppName = GetFilenameFromPath($szTargetPath)
		$szAppNameWithoutExt = GetNameFromPath($szTargetPath)
	EndIf

	; list the 3 types of crashes
	Dim $titles_searched[3] = [ _
			"Visual Studio Just-In-Time Debugger" _
			, $szAppName & " - Application Error" _
			, $szAppNameWithoutExt _
			]

	; search those windows
	For $title_searched In $titles_searched
		;ConsoleWrite("$title_searched = "&$title_searched &@CRLF)
		$hWnd = WinGetHandle(STRICT_WIN_TITLE($title_searched))
		if ($title_searched <> "") and ($hWnd <> "") Then
			$foundModalError = True
			If Not WinActive($hWnd) Then
				WinActivate($hWnd)
				WinWaitActive($hWnd)
			EndIf
			Send("{ENTER}")
			Sleep(5000)
		EndIf
		If $foundModalError Then
			ExitLoop
		EndIf
	Next

	; tell the caller whether an error message was found or not
	Return $foundModalError

EndFunc   ;==>ResetModalMessageInCaseOfCrash

;-------------------------------------------------------------------------------
; Name .........: ResetClipboard
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ResetClipboard()
	; Put some content in the clipboard
	ClipPut("[CLIPBOARD_DUMMY_CONTENT]")
EndFunc   ;==>ResetClipboard

;-------------------------------------------------------------------------------
; Name .........: ResetVisibleWindows
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ResetVisibleWindows()

	; Close all the open windows
	$var = WinList()
	For $i = 1 To $var[0][0]

		$wname = $var[$i][0]
		$hWnd = $var[$i][1]
		$pname = _ProcessGetName(WinGetProcess($hWnd))

		$noCaseSensFlag = 2 ; StringCompare
		$winIsVisibleFlag = 2 ; WinGetState

		; visible?
		If Not BitAND(WinGetState($hWnd), $winIsVisibleFlag) Then
			ContinueLoop
		EndIf

		; interaction
		If $wname = "Start" And $pname = "explorer.exe" Then
			; allowed
			; NOTE: closing this triggers shutdown wizard
			; => NOP
		ElseIf $wname = "Program Manager" Then
			; allowed
			; => NOP
		ElseIf StringInStr($wname, @ScriptFullPath) Then
			; allowed for testing
			; => NOP
		ElseIf $wname = "Server Manager" Then
			; requires admin rights :S
			WinClose($hWnd)
		ElseIf StringCompare($wname, "cmd.exe", $noCaseSensFlag) = 0 _
				Or StringCompare($wname, "c:\windows\system32\cmd.exe", $noCaseSensFlag) = 0 _
				Or StringCompare($wname, "Windows PowerShell", $noCaseSensFlag) = 0 _
				Or StringCompare($wname, "administrator: c:\windows\system32\cmd.exe", $noCaseSensFlag) = 0 _
				Or StringCompare($wname, "Administrator: Windows PowerShell", $noCaseSensFlag) = 0 _
				Then
			; minimise
			WinSetState($hWnd, "", @SW_MINIMIZE)
		ElseIf StringCompare($wname, "EasyPHP", $noCaseSensFlag) = 0 _
				Or StringInStr($pname, "vmware", $noCaseSensFlag) _
				Or StringCompare($pname, "vmplayer.exe", $noCaseSensFlag) = 0 _
				Then
			; minimise
			WinSetState($hWnd, "", @SW_MINIMIZE)
		ElseIf $wname <> "" Then
			; close
			WinClose($hWnd)
		EndIf

	Next

EndFunc   ;==>ResetVisibleWindows

;-------------------------------------------------------------------------------
; Name .........: ResetVisibleProcesses
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ResetVisibleProcesses()
	$pid = ProcessExists("winepss.exe")
	If $pid Then ProcessClose($pid)
EndFunc   ;==>ResetVisibleProcesses
