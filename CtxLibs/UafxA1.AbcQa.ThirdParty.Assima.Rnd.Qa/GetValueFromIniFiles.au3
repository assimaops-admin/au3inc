#include-once
#include <AuItCustLibs\ArrayV2.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd\AssimaConstants.au3"
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd\MinorProfile.au3"

;-------------------------------------------------------------------------------
; Name .........: SearchConfigurationFiles
; Description ..: allows to search for a few configuration files at a time
; Return .......: array of files found
;                   - first element of the array is the number of files found
;-------------------------------------------------------------------------------
Func SearchConfigurationFiles($fns, $szSubFolderName = "")
	Local $afn
	If IsArray($fns) Then
		$afn = $fns
	Else
		Dim $afn[1] = [$fns]
	EndIf
	$asFileList = _ArrayCreate(0)
	$indirCount = 0
	While True
		For $i = 0 To UBound($afn) - 1
			$szFilename = $afn[$i]
			;	ConsoleWrite($szFilename & @CRLF) ;### Debug Console
			If Not SearchConfigurationFiles_Sub($asFileList, @WorkingDir, $szFilename, $indirCount, $szSubFolderName) Then
				Return $asFileList
			EndIf
		Next
		$indirCount += 1
	WEnd
EndFunc   ;==>SearchConfigurationFiles

;-------------------------------------------------------------------------------
; Name .........: SearchConfigurationFiles_Sub
; Description ..: private sub-procedure
; Return .......: none
;-------------------------------------------------------------------------------

Func SearchConfigurationFiles_CondAdd(ByRef $asFileList, $szConfigurationFileAbs)
	; Conditional Add
	$szConfigurationFileAbs = NormalizePath($szConfigurationFileAbs)
	;	ConsoleWrite(@TAB & @TAB & @TAB & @TAB & @TAB & $szConfigurationFileAbs) ;### Debug Console
	; Do not insert duplicates or non-existing files
	If FileExists($szConfigurationFileAbs) _
			And (_ArraySearch($asFileList, $szConfigurationFileAbs, 1) = -1) Then
		$_dgbLineLength = 60 - StringLen($szConfigurationFileAbs)
		$repeatVal = 0
		If $_dgbLineLength > 0 Then $repeatVal = $_dgbLineLength
		$_dgbLineFillin = " " & _StringRepeat(".", $repeatVal) & " "
		;	ConsoleWrite($_dgbLineFillin & "ADDED OK." & @CRLF) ;### Debug Console
		_ArrayAdd($asFileList, $szConfigurationFileAbs)
		$asFileList[0] += 1
	Else
		; n/a
		;	ConsoleWrite(@CRLF) ;### Debug Console
	EndIf
EndFunc   ;==>SearchConfigurationFiles_CondAdd

Func SearchConfigurationFiles_Sub(ByRef $asFileList, $szRootDir, $szFilename, $indirLevel, $szSubFolderName)

	$szIndirection = ""

	For $i = 1 To $indirLevel
		$szIndirection = "..\" & $szIndirection
	Next

	$szTestPath = NormalizePath($szRootDir & "\" & $szIndirection)
	If @error Or Not FileExists($szTestPath) Then Return False

	SearchConfigurationFiles_CondAdd($asFileList, $szRootDir & "\" & $szIndirection & "\" & $szFilename)

	If $szSubFolderName <> "" Then
		SearchConfigurationFiles_CondAdd($asFileList, $szRootDir & "\" & $szIndirection & "\" & $szSubFolderName & "\" & $szFilename)
	EndIf

	Return True

EndFunc   ;==>SearchConfigurationFiles_Sub

;-------------------------------------------------------------------------------
; Name .........: __GetValueFromIniFiles_Sub
; Description ..: Try file  [DEVPC]Platform(Profile608).ini
;                   then    Platform(Profile608).ini
;                   then    [DEVPC]Platform.ini
;                   then    Platform.ini
; History ......: Used to be Platform.Profile.608.Local.DEVPC.ini
;                   then    Platform.Profile.608.ini
;                   then    Platform.Local.DEVPC.ini
;                   then    Platform.ini
;                 DEC 17, 2010 - Host config now has priority over profile.
;                 DEC 17, 2010 - Added Similar behaviour for section names.
;                 DEC 17, 2010 - Added Similar behaviour for keys names.
;                 DEC 21, 2010 - (Rollback) Profile config has priority over
;                                host config.
;                 JAN 5, 2011  - Host has priority over config regarding the
;                                ini filenames.
;                 FEB 8, 2013  - File names deep in the hierarchy have prio
;                                over files located closer to the root
; Testing ......:
;
; ; DIRECT TEST
; 		FileChangeDir("C:\AutoTestingV2\Suites\AtsQa600v01")
; 		Local $foundin
; 		;$foundval = __GetValueFromIniFiles_Sub($foundin, "BLA", "MOUARF")
; 		$foundval = __GetValueFromIniFiles_Sub($foundin, "Directory", "DfltATS")
; 		ConsoleWrite('$foundval = ' & $foundval & @CRLF)
; 		ConsoleWrite('$foundin = ' & $foundin & @CRLF)
;
; Return .......:
;-------------------------------------------------------------------------------

Func __GetValueFromIniFiles_LoadEnv($inifn, ByRef $aVars, ByRef $aVals)
	$aSection = IniReadSection($inifn, "ENVIRONMENT")
	If (Not @error) And $aSection[0][0] Then
		$aVarsSub = _ArrayExtractColumn($aSection, 0, 1)
		$aValsSub = _ArrayExtractColumn($aSection, 1, 1)
		If Not IsArray($aVars) Then
			$aVars = $aVarsSub
			$aVals = $aValsSub
		ElseIf $aSection[0][0] > 0 Then
			; Remove duplicates from the bottom
			;+;_ArrayDisplay($aVarsSub, $inifn)
			For $i = UBound($aVarsSub) - 1 To 0 Step -1
				;ConsoleWrite('$aVarsSub[$i] = ' & $aVarsSub[$i] & @crlf)
				If _ArraySearch($aVars, $aVarsSub[$i]) <> -1 Then
					_ArrayDelete($aVarsSub, $i)
					_ArrayDelete($aValsSub, $i)
				EndIf
			Next
			; Concatenate arrays
			_ArrayConcatenate($aVars, $aVarsSub)
			_ArrayConcatenate($aVals, $aValsSub)
		EndIf
		;+;_ArrayDisplay($aVars, $inifn)
	EndIf
EndFunc   ;==>__GetValueFromIniFiles_LoadEnv

Func __GetValueFromIniFiles_ApplyEnv($str, Const ByRef $aVars, Const ByRef $aVals)
	If Not IsArray($aVars) Then Return $str
	For $i = 0 To UBound($aVars) - 1
		$str = StringReplace($str, "[%" & $aVars[$i] & "%]", $aVals[$i])
	Next
	Return $str
EndFunc   ;==>__GetValueFromIniFiles_ApplyEnv

Func __GetValueFromIniFiles_Sub(ByRef $szIniFile_ret, $szSection, $szKey, $default = "")
	;$funcTrace = FuncTrace("__GetValueFromIniFiles_Sub", $szIniFile_ret, $szSection, $szKey, $default)
	; extract the value
	$minorProfile = MinorProfile_Get()
	Dim $aIniFilenames[4] = _
			["(" & @ComputerName & ")Platform(Profile" & $minorProfile & ").ini" _
			, "(" & @ComputerName & ")Platform.ini" _
			, "Platform(Profile" & $minorProfile & ").ini" _
			, "Platform.ini" _
			]
	Dim $aSectionNames[4] = _
			["(" & @ComputerName & ")" & $szSection & "(Profile" & $minorProfile & ")" _
			, $szSection & "(Profile" & $minorProfile & ")" _
			, "(" & @ComputerName & ")" & $szSection _
			, $szSection _
			]
	Dim $aKeysNames[4] = _
			["(" & @ComputerName & ")" & $szKey & "(Profile" & $minorProfile & ")" _
			, $szKey & "(Profile" & $minorProfile & ")" _
			, "(" & @ComputerName & ")" & $szKey _
			, $szKey _
			]

	Const $szSecondGenerationIniFiles = "Platform(Overrides).ini"

	Local $aFirstGenerationIniFiles = ""
	Local $aOverridingIniFiles = ""
	Local $aAllIniFiles2013 = ""

	Local $aEnvSectionVars = "", $aEnvSectionVals = ""

	Local $szOverrideSearch = "", $szOverrideReplace = ""

	; Search all first and second generation configuration files
	$aFirstGenerationIniFiles = SearchConfigurationFiles($aIniFilenames)
	;+;_ArrayDisplay($aFirstGenerationIniFiles, "$aFirstGenerationIniFiles")
	$aOverridingIniFiles = SearchConfigurationFiles($szSecondGenerationIniFiles)
	;+;_ArrayDisplay($aOverridingIniFiles, "$aOverridingIniFiles")
	; + Gather all configuration files into one single array
	$aAllIniFiles2013 = $aOverridingIniFiles
	If Not IsArray($aAllIniFiles2013) Then
		$aAllIniFiles2013 = $aFirstGenerationIniFiles
	Else
		_ArrayConcatenate($aAllIniFiles2013, $aFirstGenerationIniFiles, 1)
		$aAllIniFiles2013[0] += $aFirstGenerationIniFiles[0]
	EndIf

	;+;_ArrayDisplay($aAllIniFiles2013, "$aAllIniFiles2013")

	; Load environment variables
	For $k = 1 To $aAllIniFiles2013[0]
		__GetValueFromIniFiles_LoadEnv($aAllIniFiles2013[$k], $aEnvSectionVars, $aEnvSectionVals)
	Next

	;+;_ArrayDisplayTwo($aEnvSectionVars, $aEnvSectionVals, "$aEnvSectionVars and $aEnvSectionVals")

	; Search candidates for overrides
	For $j = 1 To $aOverridingIniFiles[0]

		$szOverridesIniFile = $aOverridingIniFiles[$j]

		; Get policies list
		$szOverridePolicies = IniRead($szOverridesIniFile, "COMPUTERNAME=" & StringUpper(@ComputerName), "(Default)", "")

		; Search for applicables policies
		If $szOverridePolicies <> "" Then
			$aOverridePolicies = StringSplit($szOverridePolicies, ";", 2)
			For $overridePolicy In $aOverridePolicies
				$sections = IniRead($szOverridesIniFile, "COMPUTERNAME=" & StringUpper(@ComputerName), "SECTIONS(" & $overridePolicy & ")", "")
				$sections = __GetValueFromIniFiles_ApplyEnv($sections, $aEnvSectionVars, $aEnvSectionVals)
				If ($sections <> "") And StringRegExp($szSection, $sections) Then
					$entries = IniRead($szOverridesIniFile, "COMPUTERNAME=" & StringUpper(@ComputerName), "ENTRIES(" & $overridePolicy & ")", "")
					$entries = __GetValueFromIniFiles_ApplyEnv($entries, $aEnvSectionVars, $aEnvSectionVals)
					If ($entries <> "") And StringRegExp($szKey, $entries) Then

						$search = IniRead($szOverridesIniFile, "COMPUTERNAME=" & StringUpper(@ComputerName), "SEARCH(" & $overridePolicy & ")", "")
						$search = __GetValueFromIniFiles_ApplyEnv($search, $aEnvSectionVars, $aEnvSectionVals)
						$szOverrideSearch = $search

						$replace = IniRead($szOverridesIniFile, "COMPUTERNAME=" & StringUpper(@ComputerName), "REPLACE(" & $overridePolicy & ")", "")
						$replace = __GetValueFromIniFiles_ApplyEnv($replace, $aEnvSectionVars, $aEnvSectionVals)
						$szOverrideReplace = $replace

					EndIf
				EndIf
			Next
		EndIf

	Next

	For $d = 1 To 1
		For $i = 1 To $aFirstGenerationIniFiles[0]
			$szIniFile = $aFirstGenerationIniFiles[$i]
			For $szSectionName In $aSectionNames
				For $szKeyName In $aKeysNames
					;Trace("Searching '"&$szKeyName&"'...")
					;Trace("       in '"&$szSectionName&"'...")
					;Trace("       in '"&$szIniFile&"'...")
					$szValue = IniRead($szIniFile, $szSectionName, $szKeyName, "<<<[[[VALUE_NOT_FOUND-#1]]]>>>")
					If $szValue <> "<<<[[[VALUE_NOT_FOUND-#1]]]>>>" Then
						$szIniFile_ret = $szIniFile ; ByRef
						$szValue_ret = $szValue
						$szValue_ret = StringReplace($szValue_ret, "[%LOWER_CASE_COMPUTER_NAME%]", StringLower(@ComputerName))
						$szValue_ret = StringRegExpReplace($szValue_ret, $szOverrideSearch, $szOverrideReplace)
						;Trace("Found value '"&$szValue_ret&"' in '"&$szIniFile_ret&"'.")
						Return $szValue_ret
					EndIf
				Next
			Next
		Next
	Next
	;RetTrace($default, $funcTrace)
	Return $default
EndFunc   ;==>__GetValueFromIniFiles_Sub

;-------------------------------------------------------------------------------
; Name .........: GetValueFromIniFiles
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetValueFromIniFiles($szSection, $szKey, $default = "")
	$szIniFile = ""
	Return __GetValueFromIniFiles_Sub($szIniFile, $szSection, $szKey, $default)
EndFunc   ;==>GetValueFromIniFiles

;-------------------------------------------------------------------------------
; Name .........: GetPathFromIniFiles
; Description ..: like __GetValueFromIniFiles_Sub but replace relative paths
; Return .......:
;-------------------------------------------------------------------------------
Func GetPathFromIniFiles($szSection, $szKey, $default = "")
	$ret = $default
	$szIniFile = ""
	; extract the value
	$szValue = __GetValueFromIniFiles_Sub($szIniFile, $szSection, $szKey, "<<<[[[VALUE_NOT_FOUND-#2]]]>>>")
	;	ConsoleWrite('$szValue = ' & $szValue & @CRLF) ;### Debug Console
	If $szValue <> "<<<[[[VALUE_NOT_FOUND-#2]]]>>>" Then
		If Not StringStartsWith($szValue, ".") Or _
				$szKey = "ExtList" Then
			; NOP: this is an absolute path
			$ret = $szValue
		Else
			; modify relative path
			$szDrive = ""
			$szDir = ""
			$szFName = ""
			$szExt = ""
			_PathSplit($szIniFile, $szDrive, $szDir, $szFName, $szExt)

			; create absolute path
			$szExePath = $szDrive & $szDir & $szValue
			;	ConsoleWrite('$szExePath = ' & $szExePath & @CRLF) ;### Debug Console

			; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
			$ret = NormalizePath($szExePath, False, False)
		EndIf
	EndIf
	Return $ret
EndFunc   ;==>GetPathFromIniFiles

;-------------------------------------------------------------------------------
; Name .........: MyManualDebug
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func MyManualDebug($text, $title = "MyConfig")
	;$simpleLocalLogs = HostInfo_GetSimpleLocalLogDir_EnsureExists()
	;FileWrite($simpleLocalLogs & "\AutoIt.ManualDebug." & $title & ".Log.txt", $text)
EndFunc   ;==>MyManualDebug

;-------------------------------------------------------------------------------
; Name .........: GetFltPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFltPath($szInputFilePath, $default = "")
	Const $SECTION = "FileFiltersV1"
	$ret = $default
	$strList = GetValueFromIniFiles($SECTION, "ExtList")
	MyManualDebug('$strList = ' & $strList & @CRLF)
	If $strList <> "" Then
		$aList = StringSplit($strList, ";")
		For $i = 1 To $aList[0]
			$fileExt = $aList[$i]
			MyManualDebug('$fileExt = ' & $fileExt & @CRLF)
			If StringEndsWith($szInputFilePath, $fileExt) Then
				$tmp = GetPathFromIniFiles($SECTION, $fileExt)
				If $tmp <> "" Then
					$ret = $tmp
					ExitLoop
				EndIf
			EndIf
		Next
	EndIf
	Return $ret
EndFunc   ;==>GetFltPath

;-------------------------------------------------------------------------------
; Name .........: GetExePath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetExePath($szKey, $default = "")
	Return GetPathFromIniFiles("Executable", $szKey, $default)
EndFunc   ;==>GetExePath

;-------------------------------------------------------------------------------
; Name .........: GetBatchPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetBatchPath($szKey, $default = "")
	Return GetPathFromIniFiles("Batch", $szKey, $default)
EndFunc   ;==>GetBatchPath

;-------------------------------------------------------------------------------
; Name .........: GetGuiProp
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetGuiProp($szKey, $default = "")
	Return GetPathFromIniFiles("GUIProperties", $szKey, $default)
EndFunc   ;==>GetGuiProp

;-------------------------------------------------------------------------------
; Name .........: GetDirPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetDirPath($szKey, $default = "", $bReplaceAllDrivesByUNC = False, $bReplaceMappedDrivesByUNC = True, $rootDirForRelativePaths = True)
	$szDirPath = GetPathFromIniFiles("Directory", $szKey, $default)
	Return NormalizePath($szDirPath, $bReplaceAllDrivesByUNC, $bReplaceMappedDrivesByUNC, $rootDirForRelativePaths)
EndFunc   ;==>GetDirPath

;-------------------------------------------------------------------------------
; Name .........: GetFilename
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFilename($szKey, $default = "")
	Return GetPathFromIniFiles("Filename", $szKey, $default)
EndFunc   ;==>GetFilename

;-------------------------------------------------------------------------------
; Name .........: GetFolder
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFolder($szKey, $default = "")
	Return GetPathFromIniFiles("Folder", $szKey, $default)
EndFunc   ;==>GetFolder

;-------------------------------------------------------------------------------
; Name .........: GetFilePath
; Description ..:
; Return .......:
; Testing ......:
;   DIRECT TEST:
; 		FileChangeDir("C:\AutoTestingV2\Suites\AtsQa600v01")
; 		$tst = GetFilePath("TestModulesList")
; 		ConsoleWrite('$tst = ' & $tst & @CRLF)
;-------------------------------------------------------------------------------
Func GetFilePath($szFileKey, $default = "")
	Return GetPathFromIniFiles("File", $szFileKey, $default)
EndFunc   ;==>GetFilePath

;-------------------------------------------------------------------------------
; Name .........: GetCmdPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetCmdPath($szKey, $default = "")
	Const $SEARCH_PATTERN = "\%\[(\w+)\[(\w+)\]\]\%"
	$command = GetPathFromIniFiles("Command", $szKey, $default)
	$found = StringRegExp($command, $SEARCH_PATTERN, 2)
	If Not @error Then
		$component = GetPathFromIniFiles($found[1], $found[2], $default) ;GetExePath(),GetFilePath(),etc.
		$component = StringReplace($component, "\", "\\")
		$command = StringRegExpReplace($command, $SEARCH_PATTERN, $component)
	EndIf
	Return $command
EndFunc   ;==>GetCmdPath

;-------------------------------------------------------------------------------
; Name .........: ComposeFilePath
; Description ..: use directory and filename...
; Return .......:
;-------------------------------------------------------------------------------
Func ComposeFilePath($szDirectoryKey, $szFilenameKey)
	If FileExists($szDirectoryKey) Then
		$szDirectory = $szDirectoryKey
	Else
		$szDirectory = GetDirPath($szDirectoryKey)
	EndIf
	$szFilename = GetFilename($szFilenameKey)
	If $szDirectory <> "" And $szFilename <> "" Then
		Return $szDirectory & "\" & $szFilename
	Else
		Return ""
	EndIf
EndFunc   ;==>ComposeFilePath

;-------------------------------------------------------------------------------
; Name .........: GetFolderPath
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func GetFolderPath($szDirectoryKey, $szFolderKey)
	$szDirectory = GetDirPath($szDirectoryKey)
	$szFolder = GetFolder($szFolderKey)
	If $szDirectory <> "" And $szFolder <> "" Then
		Return GatherDirectoryAndName($szDirectory, $szFolder)
	Else
		Return ""
	EndIf
EndFunc   ;==>GetFolderPath

;-------------------------------------------------------------------------------
; Name .........: GetTxt()
; Description ..: read a file and return the string
; Return .......: none
;-------------------------------------------------------------------------------
Func GetTxt($filename)
	$directory = GetDirPath("TextResources")
	Return FileRead($directory & $filename)
EndFunc   ;==>GetTxt
