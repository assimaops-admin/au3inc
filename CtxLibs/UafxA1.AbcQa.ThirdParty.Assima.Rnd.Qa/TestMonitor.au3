#include-once
#include <CoreUAFLibs\StdCoreLib\DummyDate.au3>
#include ".\DualProcess.au3"

Global $_TestMonitor_Timeout

$_TestMonitor_OnTimeout = "KillTestModuleProc"
$_TestMonitor_SafeCleanupProc = ""
$_TestMonitor_MonitorPID = 0 ; REF.PS/20101222_HARDINTEROP


; ------------------
;     DEFAULTS
; ------------------

$AFTER_TEST_MONITOR_LAUNCH = "AfterTestMonitorLaunchV2"
$TEST_MONITOR_STARTUP_PROC = "TestMonitorStartupProcV2"
$TEST_MONITOR_SHUTDOWN_PROC = "TestMonitorShutdownProcV2"


; ------------------
;      METHODS
; ------------------

Func TestMonitor_SetTimeout($timeout) ; in seconds
	If IsString($timeout) And StringIsInt($timeout) Then $timeout = Int($timeout)
	If Not IsInt($timeout) Then FatalError("not IsInt($timeout)")
	Global $_TestMonitor_Timeout = $timeout
EndFunc   ;==>TestMonitor_SetTimeout

Func TestMonitor_GetTimeout() ; in seconds
	Return $_TestMonitor_Timeout
EndFunc   ;==>TestMonitor_GetTimeout

Func TestMonitor_IsTimeoutSet()
	Return IsDeclared("_TestMonitor_Timeout") And ($_TestMonitor_Timeout <> Default)
EndFunc   ;==>TestMonitor_IsTimeoutSet

Func TestMonitor_SetDate($year, $month, $day)
	DummyDate_SetDate($year, $month, $day)
EndFunc   ;==>TestMonitor_SetDate

; this callback should not contain any 'wait' instruction!
Func TestMonitor_SetSafeCleanupProc($callback = "")
	If Not IsString($callback) Then FatalError("not IsString($callback)")
	$_TestMonitor_SafeCleanupProc = $callback
EndFunc   ;==>TestMonitor_SetSafeCleanupProc

Func TestMonitor_GetMonitorPID($sSessionId)
	Return $_TestMonitor_MonitorPID
EndFunc   ;==>TestMonitor_GetMonitorPID

Func TestMonitor_IsThisMonitor()
	Return DualProcess_IsThisMonitor()
EndFunc   ;==>TestMonitor_IsThisMonitor

Func TestMonitor_SinkHere($sSessionId)
	FuncTrace("TestMonitor_SinkHere", $sSessionId)
	DualProcess_Enable($sSessionId)
	$_TestMonitor_MonitorPID = DualProcess_SinkHere($sSessionId, $_TestMonitor_OnTimeout, $_TestMonitor_Timeout) ; the dual process never goes pass this point (REF.PS/20101222_151835)
	Return $_TestMonitor_MonitorPID
EndFunc   ;==>TestMonitor_SinkHere

Func TestMonitor_SinkHereWithCmdLine($sSessionId, ByRef $monitorCmdLineByRef)
	FuncTrace("TestMonitor_SinkHereWithCmdLine", $sSessionId)
	DualProcess_Enable($sSessionId)
	$_TestMonitor_MonitorPID = DualProcess_SinkHereWithCmdLine($sSessionId, $_TestMonitor_OnTimeout, $_TestMonitor_Timeout, $monitorCmdLineByRef) ; the dual process never goes pass this point (REF.PS/20101222_151835)
	Return $_TestMonitor_MonitorPID
EndFunc   ;==>TestMonitor_SinkHereWithCmdLine


; ------------------
;     PROCS
; ------------------

Enum $TESTMONSTAT_LAUNCH = 1, $TESTMONSTAT_DOWNLOAD, $TESTMONSTAT_SETUP, $TESTMONSTAT_AUTOMATE, $TESTMONSTAT_TEARDOWN, $TESTMONSTAT_SAVE, $TESTMONSTAT_ALLDONE

Func TestMonitor_GetStatusFromId($statusID)
	$status = ""
	Switch $statusID
		Case $TESTMONSTAT_LAUNCH
			$status = "Launching timeout monitor..."
		Case $TESTMONSTAT_DOWNLOAD
			$status = "Downloading target runtime..."
		Case $TESTMONSTAT_SETUP
			$status = "Setting up the environment..."
		Case $TESTMONSTAT_AUTOMATE
			$status = "Automating windows..."
		Case $TESTMONSTAT_TEARDOWN
			$status = "Tearing down the environment and exiting..."
		Case $TESTMONSTAT_SAVE
			$status = "Saving automation data..." ; REF.PS/20101222_145004 - HARDCODED INTEROP
		Case $TESTMONSTAT_ALLDONE
			$status = "Automation data saved."
	EndSwitch
	Assert($status <> "", '$status <> ""')
	Return $status
EndFunc   ;==>TestMonitor_GetStatusFromId

Func TestMonitor_Broadcast($statusID)
	DualProcess_SetStatus(TestMonitor_GetStatusFromId($statusID))
EndFunc   ;==>TestMonitor_Broadcast


; ------------------
;     CALLBACKS
; ------------------

;-------------------------------------------------------------------------------
; Name .........: AfterTestMonitorLaunchV2 [CALLBACK] [REF.PS/20100608_221554]
; Description ..: Call inside of the first process (after fork)
;                   -> Wait a little for the dual process to initialize
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func AfterTestMonitorLaunchV2()
	; TODO: Wait for event rather than fixed timer!
	Sleep(3000)
	TraySetIcon("Shell32.dll", 246)
EndFunc   ;==>AfterTestMonitorLaunchV2

;-------------------------------------------------------------------------------
; Name .........: TestMonitorStartupProcV2 [CALLBACK] [REF.PS/20100608_221558]
; Description ..: Call at the begining of the dual process
;                   -> Set the new system time
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func TestMonitorStartupProcV2()
	If DummyDate_IsInUse() Then DummyDateStartupProc()
EndFunc   ;==>TestMonitorStartupProcV2

;-------------------------------------------------------------------------------
; Name .........: TestMonitorShutdownProcV2 [CALLBACK] [REF.PS/20100608_222009]
; Description ..: Call before closing the dual process (whether timeout or not)
;                   -> Call 'safe' cleanup callback
;                    (this callback should not contain any 'wait' instruction!)
;                   -> Restore the real date/time
;                   -> Restore the windows time service ('Horloge Windows')
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func TestMonitorShutdownProcV2()
	If $_TestMonitor_SafeCleanupProc <> "" Then Call($_TestMonitor_SafeCleanupProc)
	If DummyDate_IsInUse() Then DummyDateShutdownProc()
EndFunc   ;==>TestMonitorShutdownProcV2

;-------------------------------------------------------------------------------
; Name .........: KillTestModuleProc [CALLBACK]
; Description ..: This is an implementation of the callback known as
;                   - Func MonitorTimeoutCallback($nPidMonitored, $nTimeout)
;                 It used to be called __AtmSession_LogTimeout (AutoTestingV1)
; Return .......: n/a
; History ......: JUN 8, 2010 - Integrated in AutoTestingV2
;-------------------------------------------------------------------------------
Func KillTestModuleProc($nPidMonitored)
	DualProcess_FancyKill($nPidMonitored, 180)
EndFunc   ;==>KillTestModuleProc
