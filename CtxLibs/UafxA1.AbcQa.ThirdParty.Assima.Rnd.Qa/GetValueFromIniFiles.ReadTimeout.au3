#include-once
#include ".\GetValueFromIniFiles.au3"

;-------------------------------------------------------------------------------
; Name .........: ReadTimeoutFromIniFiles
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReadTimeoutFromIniFiles()
	$nTimeout = 0
	$szTimeout = GetValueFromIniFiles("SelfTimeout", @ScriptName)
	If $szTimeout <> "" And StringIsInt($szTimeout) Then
		$nTimeout = Int($szTimeout)
	Else
		$nTimeout = 180 ; 600 = wait for 10 minutes
	EndIf
	Return $nTimeout
EndFunc   ;==>ReadTimeoutFromIniFiles
