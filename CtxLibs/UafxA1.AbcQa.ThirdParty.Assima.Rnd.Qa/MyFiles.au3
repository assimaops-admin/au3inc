#include-once
#include <AuItCustLibs\StringV2.au3>
#include ".\GetValueFromIniFiles.au3"


;-------------------------------------------------------------------------------
; Name .........: FileIsNotRelevant
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileIsNotRelevant($szFilename)
	$bIsFileUnrelevant = False
	; check filenames
	If Not $bIsFileUnrelevant Then
		$szFilterFile = GetPathFromIniFiles("ReportingCfgInfo", "UnrelevantFiles")
		If $szFilterFile <> "" Then
			Dim $aFilters
			_FileReadToArray($szFilterFile, $aFilters)
			For $szExtensionsFilter In $aFilters
				If $szExtensionsFilter <> "" And StringEndsWith($szFilename, $szExtensionsFilter) Then
					$bIsFileUnrelevant = True
					ExitLoop
				EndIf
			Next
		EndIf
	EndIf
	; check extensions
	If Not $bIsFileUnrelevant Then
		$szFilterFile = GetPathFromIniFiles("ReportingCfgInfo", "UnrelevantFilesExtensions")
		If $szFilterFile <> "" Then
			Dim $aFilters
			_FileReadToArray($szFilterFile, $aFilters)
			For $szExtensionsFilter In $aFilters
				If $szExtensionsFilter <> "" And StringEndsWith($szFilename, $szExtensionsFilter) Then
					$bIsFileUnrelevant = True
					ExitLoop
				EndIf
			Next
		EndIf
	EndIf
	Return $bIsFileUnrelevant
EndFunc   ;==>FileIsNotRelevant

;-------------------------------------------------------------------------------
; Name .........: FileIsAssimaThesaurus
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileIsAssimaThesaurus($szPath)

	$szPath_short = FileGetShortNameV2($szPath)

	; exception
	; cannot be a thesaurus if doesn't exist or if it is a directory
	if (Not FileExists($szPath_short)) Or IsDirectory($szPath_short, 1) Then
		Return False
	EndIf

	; exception
	; do not process binary .idb files
	Const $BINARY_IDB_HEADER_BYTES = "IDBF"
	If StringEndsWith($szPath, ".idb") Then
		; version 1 - binary idbs did exist only in APS
		; -> if StringInStr($szPath, "PerformanceData") then
		; version 2 - now we have binary idbs even when performing ATS testing
		$handle = FileOpen($szPath_short, 16)
		$header = FileRead($handle, StringLen($BINARY_IDB_HEADER_BYTES))
		FileClose($handle)
		If $header = $BINARY_IDB_HEADER_BYTES Then
			Return False
		EndIf
	EndIf

	; check the file extension
	$szExt = GetExtensionFromPath($szPath)
	If $szExt = ".the" Or $szExt = ".tut" Or $szExt = ".sim" Or $szExt = ".dic" Or $szExt = ".syn" Or $szExt = ".scr" Or $szExt = ".idb" Then
		Return True
	EndIf

	; can't be a tutorial otherwise
	Return False

EndFunc   ;==>FileIsAssimaThesaurus

;-------------------------------------------------------------------------------
; Name .........: FileIsAssimaTutorial
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileIsAssimaTutorial($szPath)

	; check the file extension
	$szExt = GetExtensionFromPath($szPath)
	If $szExt = ".tut" Or StringEndsWith($szPath, "_t.the") Then
		Return True
	EndIf

	; can't be a tutorial otherwise
	Return False

EndFunc   ;==>FileIsAssimaTutorial

;-------------------------------------------------------------------------------
; Name .........: FileIsAssimaSimulation
; Description ..:
; Return .......:
; History ......: NOV 30, 2009 - Created
;-------------------------------------------------------------------------------
Func FileIsAssimaSimulation($szPath)

	; check the file extension
	$szExt = GetExtensionFromPath($szPath)
	If $szExt = ".sim" Or StringEndsWith($szPath, "_s.the") Then
		Return True
	EndIf

	; can't be a simulation otherwise
	Return False

EndFunc   ;==>FileIsAssimaSimulation

;-------------------------------------------------------------------------------
; Name .........: FileIsAssimaDictionary
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileIsAssimaDictionary($szPath)

	; cannot be a dictionary if not a thesaurus
	If Not FileIsAssimaThesaurus($szPath) Then
		Return False
	EndIf

	; check the file extension
	$szExt = GetExtensionFromPath($szPath)
	If $szExt = ".dic" Then
		Return True
	EndIf

	; can't be a tutorial otherwise
	Return False

EndFunc   ;==>FileIsAssimaDictionary

;-------------------------------------------------------------------------------
; Name .........: FileIsDisabledThroughAuxFile
; Description ..: Check if a file "somefile.ext.disable_for(TagName)" exists
; Return .......: boolean specifying whether file should be ignored
;-------------------------------------------------------------------------------
Func FileIsDisabledThroughAuxFile($szEntryPath, $szTag = "")
	If $szTag <> "" Then
		Return FileExists($szEntryPath & ".disable_for(" & $szTag & ")")
	Else
		return (FileFindFirstFile($szEntryPath & ".disable_for(*)") <> -1)
	EndIf
EndFunc   ;==>FileIsDisabledThroughAuxFile

;-------------------------------------------------------------------------------
; Name .........: FileIsLineSortable
; Description ..:
; Return .......: BOOL
;-------------------------------------------------------------------------------
Func FileIsLineSortable($szFile)
	$testModuleGuid = EnvGet("@TestModuleProc")
	$isLineSortableCandidate = StringInStr($testModuleGuid, "MultipleProcess") _
			Or StringInStr($testModuleGuid, "GrabbingSbl80")
	Return $isLineSortableCandidate _
			And StringEndsWith($szFile, "test_lesson.sim.tht.filtered")
EndFunc   ;==>FileIsLineSortable
