#include-once
#include <ScreenCapture.au3>
#include <BaseUAFLibs\StdBaseLib\CmdLine.au3>
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>
#include ".\AssimaQaConstants.au3"
#include ".\AutomationLog.au3"
#include ".\ResetDesktopEnvironment.au3"

; dependancy
; $AUTOMATION_TIMEOUT_DIRECTORY
; CtxConf\StdQaLogConstants.au3:11
; Const $AUTOMATION_TIMEOUT_DIRECTORY = $AUTOMATION_TMP_DIRECTORY & "\AutomationData" & "\Logging" & "\Timeout"

; constants
Const $TIMEOUT_TYPE_FILENAME = "TimeoutType.txt"
Const $TIMEOUT_CODE_FILENAME = "TimeoutCode.txt"

; constants BIS
Const $TIMEOUT_99_DUALPROC_TAG = "DUALPROC" ; for public function ## -- NEW NAME SEP 26, 2016
Const $TIMEOUT_99_STANDARD_TAG = "STANDARD" ; for public function ## -- DEPRECATED NAME
Const $TIMEOUT_52_FUNCTION_TAG = "FUNCTION" ; for public function
Const $TIMEOUT_51_OPERATOR_TAG = "OPERATOR" ; for public function
Const $TIMEOUT_11_WORKMSTR_TAG = "WORKMSTR" ; for EXPORTED function


;;;;;;;;;;;;;;;;;;;;
;;    PRIVATE
;;;;;;;;;;;;;;;;;;;;

Func TimeoutManagement_PRIVATE_GetTimeoutTypeTag($timeoutDirectory)

	; otherise proceed
	$filePath = $timeoutDirectory & "\" & $TIMEOUT_TYPE_FILENAME
	If Not FileExists($filePath) Then Return ""
	Return FileRead($filePath)

EndFunc   ;==>TimeoutManagement_PRIVATE_GetTimeoutTypeTag

Func TimeoutManagement_PRIVATE_GetTimeoutTypeCode($timeoutDirectory)

	; otherise proceed
	$filePath = $timeoutDirectory & "\" & $TIMEOUT_CODE_FILENAME
	If Not FileExists($filePath) Then Return ""
	Return FileRead($filePath)

EndFunc   ;==>TimeoutManagement_PRIVATE_GetTimeoutTypeCode

Func TimeoutManagement_PRIVATE_CreateDirNow($timeoutDirectory, ByRef $logString)

	FuncTrace("TimeoutManagement_PRIVATE_CreateDirNow", $timeoutDirectory)
	$logString &= "TimeoutManagement_PRIVATE_CreateDirNow" & " " & $timeoutDirectory & @CRLF

	$dirShouldNotExist = True
	$createDirectory = False

	;;; CONTRACTS V2+ ;;;

	If $dirShouldNotExist Then
		If FileExists($timeoutDirectory) Then Return False
		$createDirectory = True
	EndIf

	;;; CONTRACTS Vx ;;;

	If $createDirectory Then
		FuncTrace("DirCreate", $timeoutDirectory)
		$logString &= "DirCreate" & " " & $timeoutDirectory & @CRLF
		If Not DirCreate($timeoutDirectory) Then Return False
	EndIf

	Return True

EndFunc   ;==>TimeoutManagement_PRIVATE_CreateDirNow

Func TimeoutManagement_PRIVATE_SetTimeoutType($timeoutDirectory, $tag, $cde, ByRef $logString)

	FuncTrace("TimeoutManagement_PRIVATE_SetTimeoutType", $timeoutDirectory, $tag, $cde)
	$logString &= "TimeoutManagement_PRIVATE_SetTimeoutType" & " " & $timeoutDirectory & " " & $tag & " " & $cde & @CRLF

	#cs DISABLED SEP 26, 2016 -- RESPONSIBILITY OF CONTRACT TRANSFERRED AT TimeoutManagement_OnTimeoutDirV3
	-- TimeoutManagement_OnTimeoutDirV3 and THIS FUNCTION consider that the Timeout Directory exists.

		; standard locations (REF.PS/20110113_121735)
		If Not FileExists($AUTOMATION_LOG_DIRECTORY) Then Return False
		; standard locations (REF.PS/20110113_121735)
		If FileExists($AUTOMATION_TIMEOUT_DIRECTORY) Then Return False
	#ce
	#region ALTERNATIVE SEP 26, 2016
	; standard locations (REF.PS/20110113_121735 +20160926_155927)
	If Not FileExists($timeoutDirectory) Then Return False
	#endregion

	; otherise proceed
	$filePath1 = $timeoutDirectory & "\" & $TIMEOUT_TYPE_FILENAME
	$filePath2 = $timeoutDirectory & "\" & $TIMEOUT_CODE_FILENAME

	ValTrace($filePath1, "$filePath1")
	ValTrace($filePath2, "$filePath2")

	$logString &= ValTraceStr($filePath1, "$filePath1") & @CRLF
	$logString &= ValTraceStr($filePath2, "$filePath2") & @CRLF

	; usage 1 - add info
	If $tag <> "" Then
		$desc = "usage 1 - add info"
		Trace($desc)
		$logString &= $desc & @CRLF

		If FileExists($filePath1) Then Return False ; <<< CONTRACT
		If FileExists($filePath2) Then Return False ; <<< CONTRACT

		FileWrite($filePath1, $tag)
		FileWrite($filePath2, $cde)
	EndIf

	; usage 2 - delete info, if any
	If $tag = "" Then

		$desc = "usage 2 - delete info, if any"
		Trace($desc)
		$logString &= $desc & @CRLF
		If FileExists($filePath1) Then FileDelete($filePath1) ; <<< OPTIONAL
		If FileExists($filePath2) Then FileDelete($filePath2) ; <<< OPTIONAL

	EndIf

	Return True

EndFunc   ;==>TimeoutManagement_PRIVATE_SetTimeoutType

Func TimeoutManagement__PRIVATE__AutomationLog($szLineMidFix, $szTextLine)
	; standard locations (REF.PS/20110113_121735)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) Then Return
	; otherise proceed
	$filePath = $AUTOMATION_LOG_DIRECTORY & "\" & $AUTOMATION_LOG_FILENAME
	; write last line
	EasyLogWrite($filePath, LogServices_TranslateEntryForLogFile($szLineMidFix & $szTextLine))
EndFunc   ;==>TimeoutManagement__PRIVATE__AutomationLog

Func TimeoutManagement_NORMAL_AutomationLog($szTextLine)
	$EMPTY_STRING = ""
	TimeoutManagement__PRIVATE__AutomationLog($EMPTY_STRING, $szTextLine)
EndFunc   ;==>TimeoutManagement_NORMAL_AutomationLog

Func TimeoutManagement_DUAL_AutomationLog($exitCode, $szTextLine)
	$SPECIAL_PREFIX = ">>> DUAL >>> "
	If $exitCode Then TimeoutManagement__PRIVATE__AutomationLog($SPECIAL_PREFIX, $szTextLine)
EndFunc   ;==>TimeoutManagement_DUAL_Automation

Func TimeoutManagement_ANY_AutomationLog($szTextLine)
	$SPECIAL_PREFIX = ">>> ANY >>> "
	TimeoutManagement__PRIVATE__AutomationLog($SPECIAL_PREFIX, $szTextLine)
EndFunc   ;==>TimeoutManagement_DUAL_AutomationLog

;;;;;;;;;;;;;;;;;;;;
;;    PRIVATE
;;;;;;;;;;;;;;;;;;;;

Func TimeoutManagement_PRIVATE_SaveLogReports($timeoutDirectory, ByRef $logString)

	FuncTrace("TimeoutManagement_PRIVATE_SaveLogReports", $timeoutDirectory)
	$logString &= "TimeoutManagement_PRIVATE_SaveLogReports" & " " & $timeoutDirectory

	;;; CONTRACT ;;;

	If Not FileExists($timeoutDirectory) Then Return False

	;;; SETTINGS ;;;

	$szDumpPath = $timeoutDirectory & "\TimeoutEnvDump_v1.xml"
	$szScreenshotPath = $timeoutDirectory & "\TimeoutPreview.png"

	; If a BOM for either UTF-16 or UTF-32 is present, that encoding is used.
	; Otherwise UTF-8 is the encoding. (The BOM for UTF-8 is optional)
	;   2 = Write mode (erase previous contents)
	; 128 = Use Unicode UTF8 (with BOM)
	$hDumpFile = FileOpen($szDumpPath, 2 + 128)
	FileWriteLine($hDumpFile, '<?xml version="1.0" encoding="UTF-8"?>')
	FileWriteLine($hDumpFile, "<EnvDump>")


	;;; WINDOWS LIST ;;;

	$aWinList = WinList()
	FileWriteLine($hDumpFile, @TAB & "<WinList>")
	For $i = 1 To $aWinList[0][0]
		FileWriteLine($hDumpFile, @TAB & @TAB & "<Window>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<Title><![CDATA[" & $aWinList[$i][0] & "]]></Title>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<Classname>" & _WinAPI_GetClassName($aWinList[$i][1]) & "</Classname>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<IsVisible>" & WinIsVisible($aWinList[$i][1]) & "</IsVisible>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<Handle>" & $aWinList[$i][1] & "</Handle>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<ProcessName>" & _ProcessGetName(WinGetProcess($aWinList[$i][1])) & "</ProcessName>")
		FileWriteLine($hDumpFile, @TAB & @TAB & "</Window>")
	Next
	FileWriteLine($hDumpFile, @TAB & "</WinList>")


	;;; PROCESSES LIST ;;;

	$aProcList = ProcessList()
	FileWriteLine($hDumpFile, @TAB & "<ProcessList>")
	For $i = 1 To $aProcList[0][0]
		FileWriteLine($hDumpFile, @TAB & @TAB & "<Process>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<Name><![CDATA[" & $aProcList[$i][0] & "]]></Name>")
		FileWriteLine($hDumpFile, @TAB & @TAB & @TAB & "<PID>" & $aProcList[$i][1] & "</PID>")
		FileWriteLine($hDumpFile, @TAB & @TAB & "</Process>")
	Next
	FileWriteLine($hDumpFile, @TAB & "</ProcessList>")

	FileWriteLine($hDumpFile, "</EnvDump>")
	FileClose($hDumpFile)


	;;; SCREENSHOT ;;;

	_ScreenCapture_Capture($szScreenshotPath)

	Return True

EndFunc   ;==>TimeoutManagement_PRIVATE_SaveLogReports

Func TimeoutManagement_PRIVATE_OptionalCleanDesktop(ByRef $logString)

	FuncTrace("TimeoutManagement_PRIVATE_OptionalCleanDesktop")
	$logString &= "TimeoutManagement_PRIVATE_OptionalCleanDesktop" & @CRLF

	;;; CLEAN ALL ;;;
	; For non-assisted testing sessions, clear the desktop a bit further
	; + prevent locked files and disturbances etc.
	$isDeskResetEnabled = _
			GetCmdLineOptn($OPTION_UPLOAD_OUTPUTS) _
			Or GetCmdLineOptn($OPTION_RESET_VISIBLE) _
			Or (EnvGet("TEST_MODULES_RESET_VISIBLE_ENVIRONMENT") = "1")
	If $isDeskResetEnabled Then
		FuncTrace("ResetVisibleEnvironment")
		$logString &= "ResetVisibleEnvironment" & @CRLF
		ResetVisibleEnvironment()
	EndIf

	Return True

EndFunc   ;==>TimeoutManagement_PRIVATE_OptionalCleanDesktop

;;;;;;;;;;;;;;;;;;;;
;;    EXPORTED
;;;;;;;;;;;;;;;;;;;;

Func TimeoutManagement_OnTimeoutDirVx($timeoutDirectory, ByRef $logString, $timeoutType = $TIMEOUT_11_WORKMSTR_TAG, $timeoutCode = 999)

	FuncTrace("TimeoutManagement_OnTimeoutDirVx", $timeoutDirectory)
	$logString &= "TimeoutManagement_OnTimeoutDirVx" & " " & $timeoutDirectory & @CRLF

	; the output directory should not exist
	If Not TimeoutManagement_PRIVATE_CreateDirNow($timeoutDirectory, $logString) Then Return False

	; set type
	$setTypeRet = TimeoutManagement_PRIVATE_SetTimeoutType($timeoutDirectory, $timeoutType, $timeoutCode, $logString)
	Trace($logString)
	If Not $setTypeRet Then Return False

	; save data
	If Not TimeoutManagement_PRIVATE_SaveLogReports($timeoutDirectory, $logString) Then Return False

	; automation
	Return TimeoutManagement_PRIVATE_OptionalCleanDesktop($logString)

EndFunc   ;==>TimeoutManagement_OnTimeoutDirV3

Func TimeoutManagement_OnTimeoutDirV0($timeoutType, $timeoutCode, ByRef $logString)

	$timeoutDirectory = $AUTOMATION_TIMEOUT_DIRECTORY
	Return TimeoutManagement_OnTimeoutDirVx($timeoutDirectory, $logString, $timeoutType, $timeoutCode)

EndFunc   ;==>TimeoutManagement_OnTimeout

Func TimeoutManagement_OnTimeoutDirV9($timeoutDirectory)

	$logString = ""
	$timeoutType = $TIMEOUT_11_WORKMSTR_TAG
	$timeoutCode = 999
	Return TimeoutManagement_OnTimeoutDirVx($timeoutDirectory, $logString, $timeoutType, $timeoutCode)

EndFunc   ;==>TimeoutManagement_OnTimeout
