#include-once
#include <CoreUAFLibs\StdCoreLib\LocalLog.au3>
#include ".\AutomationLog.au3"
#include ".\ResetDesktopEnvironment.au3"
#include ".\MyFiles.Callbacks.au3"
#include ".\OutputsManagement.au3"

;-------------------------------------------------------------------------------
; Name .........: AutomationData_Init
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func AutomationData_Init($szTextLine)
	ConsoleWrite(@CRLF & "-----" & @CRLF)
	FuncTrace("AutomationData_Init", $szTextLine)

	; For non-assisted testing sessions, clear the desktop a bit further
	; + prevent locked files and disturbances etc.
	$isDeskResetEnabled = _
			GetCmdLineOptn($OPTION_UPLOAD_OUTPUTS) _
			Or GetCmdLineOptn($OPTION_RESET_VISIBLE) _
			Or (EnvGet("TEST_MODULES_RESET_VISIBLE_ENVIRONMENT") = "1")
	If $isDeskResetEnabled Then
		FuncTrace("ResetVisibleEnvironment")
		ResetVisibleEnvironment()
	EndIf

	; is there anything already stored there
	If FileExists($AUTOMATION_OUT_DIRECTORY) Then
		FileBackupOnSite($AUTOMATION_OUT_DIRECTORY)
	EndIf

	; exception: the previous content could not be removed
	If FileExists($AUTOMATION_OUT_DIRECTORY) Then
		FatalError("Log file '" & $AUTOMATION_OUT_DIRECTORY & "' already/still exists.")
	EndIf

	DirCreate($AUTOMATION_OUT_DIRECTORY)
	DirCreate($AUTOMATION_LOG_DIRECTORY)
	FileWriteLine($AUTOMATION_OUT_DIRECTORY & "\.out.init", 1)
	Return LocalLog_Init($AUTOMATION_LOG_DIRECTORY & "\" & $AUTOMATION_LOG_FILENAME, $szTextLine)

EndFunc   ;==>AutomationData_Init

;-------------------------------------------------------------------------------
; Name .........: AutomationData_SaveWorkspace
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func AutomationData_SaveWorkspace( _
		$szRepositoryPathSrc _
		, $szTestModule _
		, $bCheckRepositoryContent _
		, ByRef $SavedAutomationData _
		, $isEntryToSkipWhenComparingRepositoriesCallback = "" _
		, $bUpload = False _
		)

	RecursivelyListContentOfDirectory_SetDefaultCallback($isEntryToSkipWhenComparingRepositoriesCallback)

	FuncTrace("AutomationData_SaveWorkspace", _
			$szRepositoryPathSrc _
			, $szTestModule _
			, $bCheckRepositoryContent _
			, $SavedAutomationData _
			, $isEntryToSkipWhenComparingRepositoriesCallback = "" _
			, $bUpload _
			)

	$szRepositoryNickname = GetNameFromPath($szRepositoryPathSrc)

	; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
	$szAutomationDataPath = OutputsManagement_PrepareWorkspaceDestination($bUpload)

	; OUTPUTS TRANSFERS [REF. D368352F]
	; -> Here the Workspace is saved in the intermediate location
	Trace("Saving automation data resulting from testing to '" & $szAutomationDataPath & "'")
	If FileExists($szRepositoryPathSrc) Then
		$szRepositoryPathDest = $szAutomationDataPath & "\Workspace\" & $szRepositoryNickname
		If Not FSEMove($szRepositoryPathSrc, $szRepositoryPathDest) Then
			$szMsg = "Failed to move data from '" & $szRepositoryPathSrc & "' to '" & $szRepositoryPathDest & "'."
			FatalError($szMsg)
		Else
			$SavedAutomationData = $szAutomationDataPath
		EndIf

	EndIf

	Return 0
EndFunc   ;==>AutomationData_SaveWorkspace


; TODO: Wrap LocalLog_Term in AutomationData_Term
