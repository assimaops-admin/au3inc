#include-once
#include <BaseUAFLibs\StdBaseLib\CmdLine.au3>
#include <CorpUAFLibs\UDFs\GetOSLang.au3>
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\HawkInc.Quants.au3"
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd\BuildInfo.au3"
#include ".\AssimaQaConstants.au3"
#include ".\TextFileAsSameDirShortcut.au3"

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_GetTempWS
; Description ..: Get name.
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_GetTempWS()
	$sSessionId = EnsureSessionId()
	$workspaceDir = $AUTOMATION_TMP_DIRECTORY
	$workspaceDir &= "\"
	$workspaceDir &= HawkQuants_GetTmoName($sSessionId) & ".Workspace"
	If Not FileExists($workspaceDir) Then DirCreate($workspaceDir)
	Return $workspaceDir
EndFunc   ;==>OutputsManagement_GetTempWS

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_SaveTempWS
; Category .....: Testing phases
; Description ..: n/a
; Return .......: n/a
;-------------------------------------------------------------------------------
Func OutputsManagement_SaveTempWS()
	FuncTrace("OutputsManagement_SaveTempWS")
	$src = OutputsManagement_GetTempWS()
	$dest = $AUTOMATION_OUT_DIRECTORY & "\Workspace"
	DirCreate($AUTOMATION_OUT_DIRECTORY)
	$ret = DirMove($src, $dest, 1)
	Return $ret
EndFunc   ;==>OutputsManagement_SaveTempWS

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_EnsureTMODirectory
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_EnsureTMODirectory($sSessionId, $bUpload = False)
	$szResultRepositoryPath = HawkQuants_GetTmoPath($sSessionId, $bUpload)
	If Not FileExists($szResultRepositoryPath) Then
		If Not DirCreate($szResultRepositoryPath) Then
			$errorMsg = ""
			$errorMsg &= @CRLF & "Failed to create directory '" & $szResultRepositoryPath & "'."
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "  " & "($szResultRepositoryPath = '" & $szResultRepositoryPath & "')"
			$errorMsg &= @CRLF & "  " & "(FileExists($szResultRepositoryPath) = '" & FileExists($szResultRepositoryPath) & "')"
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "Remember that destination must exist, that you must have the rights"
			$errorMsg &= @CRLF & "to write in it, and that the computer name must be registered in"
			$errorMsg &= @CRLF & "'O:\.drive_mapping\atv2_ops_root_o' if O: drive is used."
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "  " & "FileExists('C:\')" & " = " & FileExists('C:\')
			$errorMsg &= @CRLF & "  " & "FileExists('V:\')" & " = " & FileExists('V:\')
			$errorMsg &= @CRLF & "  " & "FileExists('Q:\')" & " = " & FileExists('Q:\')
			$errorMsg &= @CRLF & "  " & "FileExists('O:\')" & " = " & FileExists('O:\')
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "  " & "FileExists('C:\AutoTestingV2.Data')" & " = " & FileExists('C:\AutoTestingV2.Data')
			$errorMsg &= @CRLF & "  " & "FileExists('V:\AutoTestingV2.Data')" & " = " & FileExists('V:\AutoTestingV2.Data')
			$errorMsg &= @CRLF & "  " & "FileExists('Q:\AutoTestingV2.Data')" & " = " & FileExists('Q:\AutoTestingV2.Data')
			$errorMsg &= @CRLF & "  " & "FileExists('O:\AutoTestingV2.Data')" & " = " & FileExists('O:\AutoTestingV2.Data')
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "  " & "FileRead('O:\.drive_mapping\atv2_ops_root_o')"
			$errorMsg &= @CRLF & "  " & FileRead('O:\.drive_mapping\atv2_ops_root_o')
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "Additionally, please verify the content of the following files:"
			$errorMsg &= @CRLF & "- '%SystemDrive%\AutoTestingVx.Conf\.SiteConfig\DefaultSiteConf.ini'"
			$errorMsg &= @CRLF & "- '%SystemDrive%\AutoTestingVx.Ctrl\.SiteConfig\DefaultSiteConf.ini'"
			$errorMsg &= @CRLF
			$errorMsg &= @CRLF & "In the meantime, current output data will remain on the local disk..."
			$errorMsg &= @CRLF
			FatalError($errorMsg)
		EndIf
	EndIf
	Return $szResultRepositoryPath
EndFunc   ;==>OutputsManagement_EnsureTMODirectory

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_SetTMOInfo
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_SetTMOInfo($sSessionId, $section, $key, $value)
	IniWrite($AUTOMATION_TMP_DIRECTORY & "\TMOInfo-" & @ComputerName & "-" & $sSessionId & ".ini", $section, $key, $value)
EndFunc   ;==>OutputsManagement_SetTMOInfo

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_GetTMOInfo
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_GetTMOInfo($sSessionId, $section, $key)
	Return IniRead($AUTOMATION_TMP_DIRECTORY & "\TMOInfo-" & @ComputerName & "-" & $sSessionId & ".ini", $section, $key, "")
EndFunc   ;==>OutputsManagement_GetTMOInfo

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_GatherSystemInfo
; Description ..: Prepare the TMOInfo.ini file
; Parameters ...: $szTargetDir - where the tested executable is
; Return .......: n/a
;-------------------------------------------------------------------------------
Func OutputsManagement_GatherSystemInfo($sSessionId, $szTargetDirEx = "", $szSuperSessionId = "", $szScriptParams = "", $monitorCmdLine = "")
	; Save information about this test-run (System, etc.)
	OutputsManagement_StoreAllInfoAside_Part1($sSessionId, $szTargetDirEx, $szSuperSessionId, $szScriptParams)
	; Record the PID of the dual process,
	; data from the command line and data from global variables
	OutputsManagement_StoreAllInfoAside_Part2($sSessionId, $monitorCmdLine)
EndFunc   ;==>OutputsManagement_GatherSystemInfo

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_StoreAllInfoAside_Part1
; Description ..: Prepare the INI file
; Parameters ...: $szTargetDir - where the tested executable is
; Return .......: n/a
;-------------------------------------------------------------------------------
Func OutputsManagement_StoreAllInfoAside_Part1($sSessionId, $szTargetDirEx = "", $szSuperSessionId = "", $szScriptParams = "")
	; standard locations (REF.PS/20110113_121735)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) Then Return

	$szTargetDir = ""
	If $szTargetDirEx <> "" Then
		$szTargetDir = TextFileAsSameDirShortcutHacked($szTargetDirEx)
	EndIf

	$buildUTID = ""
	If $szTargetDir <> "" Then
		$buildUTID = BuildInfo_GetBuildUTIDFromPath($szTargetDir)
	EndIf

	$szTestModuleName = GetModuleName()
	$szTestModuleGuid = GetCmdLineOptn($OPTION_TEST_MODULE_ID)
	If $szTestModuleGuid = "" Then $szTestModuleGuid = $szTestModuleName

	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "MinorProfile", MinorProfile_Get())
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "SuperSessionId", $szSuperSessionId)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "SessionId", $sSessionId)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "ComputerName", @ComputerName)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TargetDirEx", $szTargetDirEx)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TargetDir", $szTargetDir)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "BuildUTID", $buildUTID)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TestModule", $szTestModuleName) ; This field will become DEPRECATED after some time. REF.PS/20150127_10
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TestModuleName", $szTestModuleName)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TestModuleGuid", $szTestModuleGuid)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "TestModulePath", @ScriptFullPath)
	OutputsManagement_SetTMOInfo($sSessionId, "AutomationInfo", "ScriptParams", $szScriptParams)

	; YYYY/MM/DD HH:MM:SS
	OutputsManagement_SetTMOInfo($sSessionId, "LocalTimeInfo", "Date1", _NowCalc())

	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "DesktopHeight", @UserName)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "OSType", @OSType)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "OSVersion", @OSVersion)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "OSLang", @OSLang)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "OSLangFull", GetOSLangFull())
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "OSServicePack", @OSServicePack)
	;OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "ProcessorArch", @ProcessorArch);DISABLE FOR AUTOIT 6.3.6.1 UPGRADE
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "DesktopHeight", @DesktopHeight)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "DesktopWidth", @DesktopWidth)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "DesktopDepth", @DesktopDepth)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "DesktopRefresh", @DesktopRefresh)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "LogonDomain", @LogonDomain)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "AutoItVersion", @AutoItVersion)
	OutputsManagement_SetTMOInfo($sSessionId, "SystemInfo", "AutoItExe", @AutoItExe)

EndFunc   ;==>OutputsManagement_StoreAllInfoAside_Part1

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_StoreAllInfoAside_Part2
; Description ..: Prepare the INI file
; Parameters ...: $szTargetDir - where the tested executable is
; Return .......: n/a
;-------------------------------------------------------------------------------
Func OutputsManagement_StoreAllInfoAside_Part2($sSessionId, $monitorCmdLine = "")
	; standard locations (REF.PS/20110113_121735)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) Then Return

	OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "ThisPID", @AutoItPID)

	; REF.PS/20101222_HARDINTEROP
	OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "ThisCmdLine", $CmdLineRaw)
	If GetCmdLineOptnIndex("--self-monitor=") Then
		OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "TestModulePID", GetCmdLineOptn("--self-monitor="))
	EndIf

	; Info regarding the dual process
	OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "MonitorCmdLine", $monitorCmdLine)
	If IsDeclared("_TestMonitor_MonitorPID") Then
		OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "MonitorPID", Eval("_TestMonitor_MonitorPID"))
	EndIf

	OutputsManagement_SetTMOInfo($sSessionId, "ProcessInfo", "@CurrentDirectory", @WorkingDir)

EndFunc   ;==>OutputsManagement_StoreAllInfoAside_Part2

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_PrepareWorkspaceDestination
; Description ..:
; Parameters ...:   $bUpload - means that we will upload the results on the
;                 server otherwise the data is kept on the local computer
; Return .......: string (path)
; Note .........:   No need for '$szTestModuleName' because we don't use this
;                 function during record and we can afford to call directly
;                 'GetModuleName()'
;-------------------------------------------------------------------------------
Func OutputsManagement_PrepareWorkspaceDestination($bUpload = False)

	; get the destination path
	$szResultRepositoryPath = HawkQuants_GetTmoPath($g_szSessionId, $bUpload)
	If FileExists($szResultRepositoryPath) Then
		; JAN 27, 2015: Disabled the code below. Who says it's an error to have this directory already?!
		; FatalError("The TMO directory '" & $szResultRepositoryPath & "' already exists.")
	Else
		DirCreate($szResultRepositoryPath)
	EndIf

	; get the session's ID
	$sSessionId = GetCmdLineOptn($OPTION_SUPER_SESSION, "")
	If $sSessionId = "" Then $sSessionId = $g_szSessionId

	; Create destination directory
	DirCreate($szResultRepositoryPath & "\Automation\Workspace")
	Return $szResultRepositoryPath & "\Automation"

EndFunc   ;==>OutputsManagement_PrepareWorkspaceDestination

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_FlushTMOInfo
; Description ..:
; Parameters ...:
; Return .......: Path to the resulting repository
;-------------------------------------------------------------------------------
Func OutputsManagement_FlushTMOInfo($sSessionId, $bUpload = False)
	; standard locations (REF.PS/20110113_121735.2 #FlushFunctions#)
	$szResultRepositoryPath = HawkQuants_GetTmoPath($sSessionId, $bUpload)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) _
			And Not FileExists($szResultRepositoryPath) Then Return

	; YYYY/MM/DD HH:MM:SS
	$Date1 = OutputsManagement_GetTMOInfo($sSessionId, "LocalTimeInfo", "Date1")
	If $Date1 <> "" Then
		$Date2 = _NowCalc()
		$DateDiffInSec = _DateDiff('s', $Date1, $Date2)
		$DateDiffInMin = _DateDiff('n', $Date1, $Date2)
		$DateDiffInHrs = _DateDiff('h', $Date1, $Date2)
		OutputsManagement_SetTMOInfo($sSessionId, "LocalTimeInfo", "Date2", $Date2)
		OutputsManagement_SetTMOInfo($sSessionId, "LocalTimeInfo", "DateDiffInSec", $DateDiffInSec)
		OutputsManagement_SetTMOInfo($sSessionId, "LocalTimeInfo", "DateDiffInMin", $DateDiffInMin)
		OutputsManagement_SetTMOInfo($sSessionId, "LocalTimeInfo", "DateDiffInHrs", $DateDiffInHrs)
	EndIf

	$szResultRepositoryPath = OutputsManagement_EnsureTMODirectory($sSessionId, $bUpload)
	$src = $AUTOMATION_TMP_DIRECTORY & "\TMOInfo-" & @ComputerName & "-" & $sSessionId & ".ini"
	$dest = $szResultRepositoryPath & "\TMOInfo.ini"
	If FileExists($src) And Not FileMove($src, $dest) Then
		$msgWarn = "Failed to move automation info file." _
				 & @CRLF & "(src=" & $src & ")" _
				 & @CRLF & "(dest=" & $dest & ")"
		Warning($msgWarn)
		If @ComputerName = "PERSES" Then MsgBox(0, @ScriptName & " - WARNING", $msgWarn)
	EndIf
	Return $szResultRepositoryPath
EndFunc   ;==>OutputsManagement_FlushTMOInfo

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_FlushAutomationData
; Description ..: Copy remaining data... Note that we use 'DirMoveV2' because
;                 we don't want our directory to be copied INSIDE of the
;                 existing one.
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_FlushAutomationData($sSessionId, $bUpload = False)
	; standard locations (REF.PS/20110113_121735.2 #FlushFunctions#)
	$szResultRepositoryPath = HawkQuants_GetTmoPath($sSessionId, $bUpload)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) _
			And Not FileExists($szResultRepositoryPath) Then Return

	$ret = 1
	$szResultRepositoryPath = OutputsManagement_EnsureTMODirectory($sSessionId, $bUpload)
	$src = $AUTOMATION_OUT_DIRECTORY
	$dest = $szResultRepositoryPath & "\Automation"

	; OUTPUTS TRANSFERS [REF. D368352F]
	; -> Give it another go.
	; WARNING: IT IS A BIT CONFUSING TO HAVE TWO PLACES WHERE THE UPLOAD CAN TAKE PLACE!
	If FileExists($src) Then
		$ret = DirMoveV2($src, $dest, 1)
		If Not $ret Then
			Warning("Failed to move automation data directory." _
					 & @CRLF & "(src=" & $src & ")" _
					 & @CRLF & "(dest=" & $dest & ")" _
					)
		EndIf
	EndIf

	Return $ret

EndFunc   ;==>OutputsManagement_FlushAutomationData

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_FlushStaticScripts
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_FlushStaticScripts($sSessionId, $bUpload = False)
	; standard locations (REF.PS/20110113_121735.2 #FlushFunctions#)
	$szResultRepositoryPath = HawkQuants_GetTmoPath($sSessionId, $bUpload)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) _
			And Not FileExists($szResultRepositoryPath) Then Return
	Return HawkQuants_StaticScripts($sSessionId, $bUpload)
EndFunc   ;==>OutputsManagement_FlushStaticScripts

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_FlushOutputsRegister
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func OutputsManagement_FlushOutputsRegister($sSessionId, $bUpload = False, $superSessionId = "", $testModuleGuid = "")
	; standard locations (REF.PS/20110113_121735.2 #FlushFunctions#)
	$szResultRepositoryPath = HawkQuants_GetTmoPath($sSessionId, $bUpload)
	If Not FileExists($AUTOMATION_LOG_DIRECTORY) _
			And Not FileExists($szResultRepositoryPath) Then Return
	Return HawkQuants_RegisterTmoFull($sSessionId, $bUpload, $superSessionId, $testModuleGuid)
EndFunc   ;==>OutputsManagement_FlushOutputsRegister

;-------------------------------------------------------------------------------
; Name .........: OutputsManagement_OnMonitorExit
; Description ..:
; Parameters ...:
; Return .......: Path to the resulting repository
; Note .........:
;-------------------------------------------------------------------------------
Func OutputsManagement_OnMonitorExit($sSessionId, $bUpload = False, $superSessionId = "", $testModuleGuid = "")
	$TMODir = OutputsManagement_FlushTMOInfo($sSessionId, $bUpload)
	OutputsManagement_FlushOutputsRegister($sSessionId, $bUpload, $superSessionId, $testModuleGuid)
	OutputsManagement_FlushAutomationData($sSessionId, $bUpload)
	; OutputsManagement_FlushStaticScripts($sSessionId, $bUpload) ; DISABLED -- MAR 8, 2015
	Return $TMODir
EndFunc   ;==>OutputsManagement_OnMonitorExit
