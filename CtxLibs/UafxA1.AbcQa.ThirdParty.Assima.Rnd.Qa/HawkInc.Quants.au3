#include-once
#include <CoreUAFLibs\StdCoreLib\SessionId.au3>
#include <CtxConf\HostInfo.Lite.au3>
#include ".\HawkInc.au3"

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_GetTmoName
; Description ..: Get name.
; Parameters ...:
; Return .......: FEB 11, 2011 - Created
;-------------------------------------------------------------------------------
Func HawkQuants_GetTmoName($sSessionId = Default)
	If $sSessionId = Default Then $sSessionId = EnsureSessionId()
	Return "TMO-" & @ComputerName & "-" & $sSessionId
EndFunc   ;==>HawkQuants_GetTmoName

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_GetTmoPath
; Description ..: Get path
; Parameters ...:
; Return .......:
; History ......: FEB 11, 2011 - Created from 'OutputsManagement_GetTMODirectory'
;-------------------------------------------------------------------------------
Func HawkQuants_GetTmoPath($sSessionId = Default, $bUpload = Default)
	If $sSessionId = Default Then $sSessionId = EnsureSessionId()
	If $bUpload = Default Then $bUpload = False
	$dataStor = HostInfo_GetClosestDataDir($bUpload)
	$tmoDirName = HawkQuants_GetTmoName($sSessionId)
	Return $dataStor & "\" & $tmoDirName
EndFunc   ;==>HawkQuants_GetTmoPath

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_StaticScripts
; Description ..:
; Parameters ...:
; Return .......: FEB 11, 2011 - Created
;-------------------------------------------------------------------------------
Func HawkQuants_StaticScripts($sSessionId, $bUpload = Default, $hard = 0)
	If $bUpload = Default Then $bUpload = False
	$dataStor = HostInfo_GetClosestDataDir($bUpload)
	$tmoDirName = HawkQuants_GetTmoName($sSessionId)
	Return Hawk_StaticScripts($dataStor & "\" & $tmoDirName, $hard)
EndFunc   ;==>HawkQuants_StaticScripts

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_RegisterTmoBase
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func HawkQuants_RegisterTmoBase($sSessionId, $bUpload = Default)
	If $bUpload = Default Then $bUpload = False
	$dataStor = HostInfo_GetClosestDataDir($bUpload)
	$tmoDirName = HawkQuants_GetTmoName($sSessionId)
	Return Hawk_RegisterTmoBase($dataStor, $tmoDirName)
EndFunc   ;==>HawkQuants_RegisterTmoBase

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_RegisterSuperSessionInfo
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func HawkQuants_RegisterSuperSessionInfo($superSessionId, $key, $value, $bUpload = Default)
	If $bUpload = Default Then $bUpload = False
	$dataStor = HostInfo_GetClosestDataDir($bUpload)
	Return Hawk_RegisterSuperSessionInfo($dataStor, $superSessionId, $key, $value)
EndFunc   ;==>HawkQuants_RegisterSuperSessionInfo

;-------------------------------------------------------------------------------
; Name .........: HawkQuants_RegisterTmoFull
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func HawkQuants_RegisterTmoFull($sSessionId, $bUpload = Default, $superSessionId = "", $testModuleGuid = "")
	If $bUpload = Default Then $bUpload = False
	$dataStor = HostInfo_GetClosestDataDir($bUpload)
	$tmoDirName = HawkQuants_GetTmoName($sSessionId)
	Return Hawk_RegisterTmoFull($dataStor, $tmoDirName, $superSessionId, $testModuleGuid)
EndFunc   ;==>HawkQuants_RegisterTmoFull
