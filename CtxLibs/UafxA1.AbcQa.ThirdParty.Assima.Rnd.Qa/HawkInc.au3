#include-once
#include <CoreUAFLibs\StdCoreLib\FileRegister.au3>
#include <CorpUAFLibs\StdThirdPartyExLib\FileVx.au3>
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd\MinorProfile.au3"

;-------------------------------------------------------------------------------
; Name .........: Hawk_StaticScripts
; Remarks ......: [CALLBACK REF.PS/20110211_133100]
; Description ..:
; Parameters ...:
; Return .......: SEP 27, 2010 - Synthesis
;-------------------------------------------------------------------------------
Func Hawk_StaticScripts($tmo_dir, $hard = 0)
	$rewrite_if_exists = $hard
	Return Hawk_WriteAllQuickScripts($tmo_dir & "\QuickHawk", '..', $rewrite_if_exists)
EndFunc   ;==>Hawk_StaticScripts

;-------------------------------------------------------------------------------
; Name .........: Hawk_WriteAllQuickScripts
; Remarks ......: SUB CALL to [CALLBACK REF.PS/20110211_133100]
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_WriteAllQuickScripts($quickhawk_location, $working_dir = "", $rewrite_if_exists = 0)

	$ret = 1

	Const $LATEST_QUICKHAWK_VERSION_ID = 4
	Const $LATEST_QUICKHAWK_VERSION_FILE = $quickhawk_location & "\version.txt"

	; exception
	; already up-to-date?
	$currentVersion = FileRead($LATEST_QUICKHAWK_VERSION_FILE)
	If $currentVersion = $LATEST_QUICKHAWK_VERSION_ID Then Return $ret

	; changed scripts content?
	$rewrite_if_exists = 0

	; swap enabled
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-transfer-this", 'Transferring', 'hawk.au3 --bold transfer this', $working_dir, $rewrite_if_exists)

	; multi-target
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-approve-this--local", 'Approving', 'hawk.au3 --bold approve this --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-approve-this--upload", 'Approving', 'hawk.au3 --bold approve this --upload', $working_dir, $rewrite_if_exists)

	; standard
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-report-this--local", 'Reporting', 'hawk.au3 --bold report this --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-flag-this-failure--local", 'Flagging', 'hawk.au3 --bold flag this failure --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-flag-this-forced--local", 'Flagging', 'hawk.au3 --bold flag this forced --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-flag-this-reset--local", 'Flagging', 'hawk.au3 --bold flag this reset --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-flag-this-success--local", 'Flagging', 'hawk.au3 --bold flag this success --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-clean-this--local", 'Cleaning', 'hawk.au3 --bold clean this --local', $working_dir, $rewrite_if_exists)
	Hawk_WriteQuickScript($quickhawk_location, "hawk-bold-delete-this--local", 'Deleting', 'hawk.au3 --bold delete this --local', $working_dir, $rewrite_if_exists)

	If FileExists($LATEST_QUICKHAWK_VERSION_FILE) Then FileDelete($LATEST_QUICKHAWK_VERSION_FILE)
	FileWrite($LATEST_QUICKHAWK_VERSION_FILE, $LATEST_QUICKHAWK_VERSION_ID)

	Return $ret

EndFunc   ;==>Hawk_WriteAllQuickScripts

;-------------------------------------------------------------------------------
; Name .........: Hawk_WriteQuickScript
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_WriteQuickScript($quickhawk_location, $scr_name, $op_name, $cmd_line_part, $working_dir = "", $rewrite_if_exists = 0)
	$cmd_line_au3 = $cmd_line_part
	$cmd_line_php = $cmd_line_part
	; command line extension
	$cmd_line_au3 &= " --parent=""&'""'&@ScriptFullPath&'""'&"""
	$cmd_line_php &= " --parent="".'""'.$_SERVER[""argv""][0].'""'."""
	; command line prefixes
	If $op_name <> "" Then
		$cmd_line_au3 = "echo " & $op_name & "... & " & $cmd_line_au3
		$cmd_line_php = "echo " & $op_name & "... & " & $cmd_line_php
	EndIf
	If $working_dir <> "" Then
		$cmd_line_au3 = "cd " & $working_dir & " & " & $cmd_line_au3
		$cmd_line_php = "cd " & $working_dir & " & " & $cmd_line_php
	EndIf
	; command line suffixes
	If $op_name <> "" Then
		$cmd_line_au3 &= " & echo  " & $op_name & " done."
		$cmd_line_php &= " & echo  " & $op_name & " done."
	EndIf
	; create directory
	If Not FileExists($quickhawk_location) Then DirCreate($quickhawk_location)
	; write file 1
	$file = $quickhawk_location & "\" & $scr_name & ".au3"
	If FileExists($file) And $rewrite_if_exists Then
		FSEDelete($file)
	EndIf
	If Not FileExists($file) Then
		FileWriteLine($file, 'RunWait(@ComSpec & " /C " & "' & $cmd_line_au3 & '")')
	EndIf
	; write file 2
	$file = $quickhawk_location & "\" & $scr_name & ".php"
	If FileExists($file) And $rewrite_if_exists Then
		FSEDelete($file)
	EndIf
	If Not FileExists($file) Then
		FileWriteLine($file, '<?php set_time_limit(0); echo shell_exec("' & $cmd_line_php & '"); ?>')
	EndIf
EndFunc   ;==>Hawk_WriteQuickScript

;-------------------------------------------------------------------------------
; Name .........: Hawk_GetMainIndex
; Remarks ......:
; Description ..:
; Parameters ...: $atvx_ext_location - "AtvX.Data" or "AtvX.Refs" directory
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_GetMainIndex($atvx_ext_location)
	$indexFile = ""
	If StringInStr($atvx_ext_location, ".Refs") Then
		$indexFile = $atvx_ext_location & "\All_Refs.txt"
	ElseIf StringInStr($atvx_ext_location, ".Data") Then
		$indexDir = $atvx_ext_location & "\_SSDB_\All_TMOs"
		$indexFile = $atvx_ext_location & "\_SSDB_\All_TMOs\index.txt"
		If Not FileExists($indexDir) Then DirCreate($indexDir)
	EndIf
	Return $indexFile
EndFunc   ;==>Hawk_GetMainIndex

;-------------------------------------------------------------------------------
; Name .........: Hawk_IsTmoRegistered
; Remarks ......:
; Description ..:
; Parameters ...: $atvx_ext_location - "AtvX.Data" or "AtvX.Refs" directory
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_IsTmoRegistered($atvx_ext_location, $tmoDirName)
	$ret = 0
	$indexFile = Hawk_GetMainIndex($atvx_ext_location)
	If $indexFile <> "" Then
		$ret = FileRegisterIsEntry($indexFile, $tmoDirName, 0)
	EndIf
	Return $ret
EndFunc   ;==>Hawk_IsTmoRegistered

;-------------------------------------------------------------------------------
; Name .........: Hawk_RegisterTmoBase
; Remarks ......:
; Description ..:
; Parameters ...: $atvx_ext_location - "AtvX.Data" or "AtvX.Refs" directory
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_RegisterTmoBase($atvx_data_location, $tmoDirName)
	$indexFile = Hawk_GetMainIndex($atvx_data_location)
	ValTrace($indexFile, "$indexFile")
	ValTrace($tmoDirName, "$tmoDirName")
	Return FileRegisterWriteLine($indexFile, $tmoDirName)
EndFunc   ;==>Hawk_RegisterTmoBase

;-------------------------------------------------------------------------------
; Name .........: Hawk_RegisterBuild
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_RegisterBuild($atvx_data_location, $buidCid)

	If $buidCid = "" Then Return ; Cid = Custom Identifier (eg. "201501-13-08-4354")

	; ---------------------
	;    N-th register
	; ---------------------

	; Create parent directory
	$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Builds"
	If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)

	; Get nickname
	$nickname = $buidCid

	; Add module to Index
	$indexFile = $ssdb_subdir & "\index.txt"
	If Not FileExists($indexFile) _
			Or Not StringInStr(FileRead($indexFile), $nickname & @CRLF) Then
		FileWriteLine($indexFile, $nickname)
	EndIf

	Return 1
EndFunc   ;==>Hawk_RegisterBuild

;-------------------------------------------------------------------------------
; Name .........: Hawk_RegisterBuildInfo
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_RegisterBuildInfo($atvx_data_location, $buidCid, $key, $data, $ini_section_name = Default)
	If $buidCid <> "" Then
		; Create parent directory
		$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Builds"
		If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)
		; Reference session in super-session
		IniWrite($ssdb_subdir & "\" & $buidCid & ".ini", $ini_section_name, $key, $data)
	EndIf
	Return 1
EndFunc   ;==>Hawk_RegisterBuildInfo

;-------------------------------------------------------------------------------
; Name .........: Hawk_RegisterSuperSessionInfo
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_RegisterSuperSessionInfo($atvx_data_location, $ssid, $key, $data, $ini_section_name = Default)
	If $ssid <> "" Then
		; Create parent directory
		$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Super_Sessions"
		If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)
		; Reference session in super-session
		IniWrite($ssdb_subdir & "\" & $ssid & ".ini", $ini_section_name, $key, $data)
	EndIf
	Return 1
EndFunc   ;==>Hawk_RegisterSuperSessionInfo

;-------------------------------------------------------------------------------
; Name .........: Hawk_RegisterTmoFull
; Remarks ......:
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func Hawk_RegisterTmoFull($atvx_data_location, $tmoDirName, $ssid = "", $tmguid = "")

	Hawk_RegisterTmoBase($atvx_data_location, $tmoDirName)

	; ---------------------
	;    Second register
	; ---------------------

	; Create parent directory
	$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Test_Modules"
	If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)

	; Get nickname
	If $tmguid = "" Then
		$tmguid = StringTrimRight(@ScriptName, StringLen(".au3"))
	EndIf

	; Add module to Index
	$indexFile = $ssdb_subdir & "\index.txt"
	If Not FileExists($indexFile) _
			Or Not StringInStr(FileRead($indexFile), $tmguid & @CRLF) Then
		FileWriteLine($indexFile, $tmguid)
	EndIf

	; Reference session in test-module executions group
	FileWriteLine($ssdb_subdir & "\" & $tmguid & ".txt", $tmoDirName)

	; ---------------------
	;    Third register
	; ---------------------

	If $ssid <> "" Then
		; Create parent directory
		$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Super_Sessions"
		If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)
		; Add super-session to Index
		$indexFile = $ssdb_subdir & "\index.txt"
		If Not FileExists($indexFile) _
				Or Not StringInStr(FileRead($indexFile), $ssid & @CRLF) Then
			FileWriteLine($indexFile, $ssid)
		EndIf
		; Reference session in super-session
		FileWriteLine($ssdb_subdir & "\" & $ssid & ".txt", $tmoDirName)
	EndIf

	; ---------------------
	;    Fourth register
	; ---------------------

	; Create parent directory
	$ssdb_subdir = $atvx_data_location & "\_SSDB_\All_Profiles"
	If Not FileExists($ssdb_subdir) Then DirCreate($ssdb_subdir)

	; Get nickname
	$profileGuid = MinorProfile_Get()

	; Add profile to Index
	$indexFile = $ssdb_subdir & "\index.txt"
	If Not FileExists($indexFile) _
			Or Not StringInStr(FileRead($indexFile), $profileGuid & @CRLF) Then
		FileWriteLine($indexFile, $profileGuid)
	EndIf

	; Reference session in profile
	FileWriteLine($ssdb_subdir & "\" & $profileGuid & ".txt", $tmoDirName)

	Return 1
EndFunc   ;==>Hawk_RegisterTmoFull
