#include-once
#include <AuItCustLibs\StringV2.au3>

;-------------------------------------------------------------------------------
; Name .........: FileIsSystemFile
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func FileIsSystemFile($szFilename)
	$bSkip = False
	Select
		; source safe files
		Case StringInStr($szFilename, "vssver")
			$bSkip = True
		Case StringEndsWith($szFilename, ".scc")
			$bSkip = True

			; comparator files
			; REF.PS/E848B3BE
		Case StringEndsWith($szFilename, ".filtered")
			$bSkip = True

			; comparator files
			; REF.PS/E848B3BE
		Case StringEndsWith($szFilename, ".lsorted")
			$bSkip = True

			; assima expert mode files
			; REF.PS/BE115753
		Case StringEndsWith($szFilename, ".tht")
			$bSkip = True

			; sqlite database dumps
			; REF.PS/BE115753
		Case StringEndsWith($szFilename, ".dump.sql")
			$bSkip = True

			; alternative outputs
			; REF.PS/BE115753
		Case StringRegExp($szFilename, ".altout\d\d\Z")
			$bSkip = True

			; other files
		Case Else
			$bSkip = False
	EndSelect
	Return $bSkip
EndFunc   ;==>FileIsSystemFile
