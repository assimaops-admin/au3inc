#include-once
#include ".\MyFiles.au3"
#include ".\MyFiles.FileIsSystemFile.au3"

;-------------------------------------------------------------------------------
; Name .........: RecursivelyListContentOfDirectory_SetDefaultCallback
; Description ..:
; Return .......:
; WARNING ......: Requires function 'IsEntryToSkipDefaultCallback'.
;-------------------------------------------------------------------------------
Func RecursivelyListContentOfDirectory_SetDefaultCallback(ByRef $isEntryToSkipCallback)
	If $isEntryToSkipCallback = "" Then $isEntryToSkipCallback = "IsEntryToSkipDefaultCallback"
EndFunc   ;==>RecursivelyListContentOfDirectory_SetDefaultCallback

;-------------------------------------------------------------------------------
; Name .........: IsEntryToSkipDefaultCallback
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
; History ......: JUL 7, 2010 - Added 2 other parameters
;                              ($entryName and $entryLocation)
;-------------------------------------------------------------------------------
Func IsEntryToSkipDefaultCallback($szEntryPath, $entryName, $entryLocation)
	$bSkip = False
	Select
		Case FileIsSystemFile($entryName)
			$bSkip = True
		Case FileIsNotRelevant($entryName)
			$bSkip = True
		Case Else
			$bSkip = False
	EndSelect
	Return $bSkip
EndFunc   ;==>IsEntryToSkipDefaultCallback

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotTutOrDir
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func SkipIfNotTutOrDir($szEntryPath, $entryName, $entryLocation)
	Return SkipIfNotTutOrDirEx($szEntryPath)
EndFunc   ;==>SkipIfNotTutOrDir

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotSimOrDir
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func SkipIfNotSimOrDir($szEntryPath, $entryName, $entryLocation)
	; don't skip directories
	If IsDirectory($szEntryPath) Then Return False
	; skip files that are not tutorials
	If Not FileIsAssimaSimulation($szEntryPath) Then Return True
	; don't skip anything else: all the rest are valid simulations
	Return False
EndFunc   ;==>SkipIfNotSimOrDir

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotTutOrDirOrDisabled
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func SkipIfNotTutOrDirOrDisabled($szEntryPath, $entryName, $entryLocation)
	Return SkipIfNotTutOrDirEx($szEntryPath, True)
EndFunc   ;==>SkipIfNotTutOrDirOrDisabled

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotAssimaThesOrDir
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func SkipIfNotAssimaThesOrDir($szEntryPath, $entryName, $entryLocation)
	If IsDirectory($szEntryPath) Then Return False
	Return Not FileIsAssimaThesaurus($szEntryPath)
EndFunc   ;==>SkipIfNotAssimaThesOrDir

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotDictOrDir
; Description ..: Implements callback-type "REF.PS/B757E73E"
; Return .......:
;-------------------------------------------------------------------------------
Func SkipIfNotDictOrDir($szEntryPath, $entryName, $entryLocation)
	If IsDirectory($szEntryPath) Then Return False
	Return Not FileIsAssimaDictionary($szEntryPath)
EndFunc   ;==>SkipIfNotDictOrDir

;-------------------------------------------------------------------------------
; Name .........: SkipIfNotTutOrDirEx
; Description ..: Gather subroutines 'SkipIfNotTutOrDir' and
;                   'SkipIfNotTutOrDirOrDisabled'
; Return .......:
;-------------------------------------------------------------------------------
$SkipIfNotTutOrDirExDeclared = 1
Func SkipIfNotTutOrDirEx($szEntryPath = "", $checkIfDisabled = False)
	;FuncTrace("SkipIfNotTutOrDirEx", $szEntryPath, $checkIfDisabled)
	; don't skip directories
	If IsDirectory($szEntryPath) Then Return False
	; skip files that are not tutorials
	If Not FileIsAssimaTutorial($szEntryPath) Then Return True
	;ConsoleWrite($szEntryPath & " is a tutorial..." & @CRLF)
	; skip files that are disabled tutorials
	If $checkIfDisabled And FileIsDisabledThroughAuxFile($szEntryPath) Then Return True
	; don't skip anything else: all the rest are valid tutorials
	Return False
EndFunc   ;==>SkipIfNotTutOrDirEx
