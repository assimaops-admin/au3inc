#include-once
;__________________________________
;
; MAIN BATCH LOG FILE
;
; NMAKE : fatal error U1077: 'src\..\host\w32\makensis.EXE' : return code '0x1'
;__________________________________

Const $ROOT_LOGFILE_ERR_PATTERN = "ROOT_LOGFILE_ERR_PATTERN_UNUSED"

;__________________________________
;
; COMPILATION LOG FILE
;
; File: "..\..\TestRelease\extddxcare.dll" -> no files found.
; File: "..\..\TestRelease64\extddxcare.dll" -> no files found.
;__________________________________

Const $COMPILE_LOGFILE_ERR_PATTERN = " -> no files found."

;__________________________________
;
; OTHER BATCH LOG FILES
;
; ?????????????????????????????
; ?????????????????????????????
; ?????????????????????????????
;__________________________________

Const $DEFAULT_LOGFILE_ERR_PATTERN = " error "
; Const $ALTERN_LOGFILE_ERR_PATTERN_1 = " error C" ; UNUSED
; Const $ALTERN_LOGFILE_ERR_PATTERN_2 = " error U" ; UNUSED

;__________________________________
;
; VISUAL STUDIO HTML LOG FILES
;
; We can't look for "error PRJ" because this seems to be a generic text in the BuildLog.htm files.
;
; This file also says things like "0 error(s), 2 warning(s)", so beware...
; => use pattern " error(s)" conditionally,
; only if errors were found using pattern " error LNK".
;__________________________________

Const $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER = "HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER"
; Const $HTML_LOGFILE_ERR_PATTERN_EXT_1 = " error PRJ" ; UNUSED (See comment above)
Const $HTML_LOGFILE_ERR_PATTERN_EXT_2 = " error LNK"
Const $HTML_LOGFILE_ERR_PATTERN_EXT_X = " error(s)"
