#include-once

; See also:
; #include <UAFXA1\StdQaLogLib\StdQaLogConstants.au3>
; #include <UAFXA1\AbcQa.ThirdParty.Assima.Rnd.Qa\AssimaQaConstants.au3>

Const $ATV2_TIMESTAMP_PATTERN = "(\d\d\d\d\d\d\-\d\d\-\d\d\-\d\d\d\d)"

; command line parameters
Const $OPTION_TARGET_RUNTIME = "--target="
