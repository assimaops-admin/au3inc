;===============================================================================
; Module ...........: MinorProfile.au3
; Description ......: Information about the test-bench
; Cross-reference ..: [REF 2010-12-21-134510] See BuildInfo.au3, HostInfo.au3,
;                       TestSuiteInfo.au3 and MinorProfile.au3.
; Author ...........: Patryk Szczepankiewicz (pszczepa at gmail dot com)
; History ..........: DEC 21, 2010 - Created or added this header
;===============================================================================
#include-once

#include <AuItCustLibs\StringV2.au3>

Const $SCSV_LOCKED_PROFILES = "607;608;610;612;613;614;615;616;617;618;619;620" ; semi-colon separated values
Const $WIP_FINAL_MINOR_PROFILE = 621
Const $WIP_ALPHA_MINOR_PROFILE = 622
Const $WIP_INTERNAL_MINOR_PROFILE = 600

; Ordered
Const $SCSV_WIP_MINOR_PROFILES_ORD = $WIP_FINAL_MINOR_PROFILE & ";" & $WIP_ALPHA_MINOR_PROFILE & ";" & $WIP_INTERNAL_MINOR_PROFILE
; Reverse order
Const $SCSV_WIP_MINOR_PROFILES_REV = $WIP_INTERNAL_MINOR_PROFILE & ";" & $WIP_ALPHA_MINOR_PROFILE & ";" & $WIP_FINAL_MINOR_PROFILE

Const $SCSV_ALL_SUPPORTED_MINOR_PROFILES = $SCSV_LOCKED_PROFILES & ";" & $SCSV_WIP_MINOR_PROFILES_ORD
Const $ALL_SUPPORTED_MINOR_PROFILES_ARRAY = StringSplit($SCSV_ALL_SUPPORTED_MINOR_PROFILES, ";", 2)
Const $RECOMMENDED_MINOR_PROFILE = $ALL_SUPPORTED_MINOR_PROFILES_ARRAY[UBound($ALL_SUPPORTED_MINOR_PROFILES_ARRAY) - 1]

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_IsSupported
; Description ..:
; Parameters ...:
; Return .......: String with the infeered profile name - Empty string otherwise
; History ......: DEC 21, 2010 - Created
;-------------------------------------------------------------------------------
Func MinorProfile_IsSupported($profile)
	$isDefault = ($profile = $WIP_INTERNAL_MINOR_PROFILE)
	$isEmpty = (IsString($profile) And ($profile = ""))
	$isListed = 0
	For $supportedProfile In $ALL_SUPPORTED_MINOR_PROFILES_ARRAY
		If $profile = $supportedProfile Then
			$isListed = 1
			ExitLoop
		EndIf
	Next
	Return $isEmpty Or $isDefault Or $isListed
EndFunc   ;==>MinorProfile_IsSupported

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_Get
; Description ..:
; Return .......: String - Profile name
;-------------------------------------------------------------------------------
Func MinorProfile_Get()
	Return MinorProfile_Ensure(EnvGet("@MinorProfile"))
EndFunc   ;==>MinorProfile_Get

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_Set
; Description ..:
; Return .......: String - Profile name
;-------------------------------------------------------------------------------
Func MinorProfile_Set($profile = "")
	Return EnvSet("@MinorProfile", MinorProfile_Ensure($profile))
EndFunc   ;==>MinorProfile_Set

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_Ensure
; Description ..:
; Return .......: String - Profile name
;-------------------------------------------------------------------------------
Func MinorProfile_Ensure($profile)
	If $profile = "" Then $profile = $WIP_INTERNAL_MINOR_PROFILE
	Return $profile
EndFunc   ;==>MinorProfile_Ensure

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_Compare
; Description ..:
; Remarks ......: See 'NightlyThreshold' for other function related to profiles
; Return .......: 1, 0 or -1
;-------------------------------------------------------------------------------
Func MinorProfile_Compare($profile1, $profile2)

	$profile1 = MinorProfile_Ensure($profile1)
	$profile2 = MinorProfile_Ensure($profile2)

	If $profile2 = $WIP_INTERNAL_MINOR_PROFILE _
			And $profile1 = $WIP_INTERNAL_MINOR_PROFILE Then
		Return 0
	ElseIf $profile1 = $WIP_INTERNAL_MINOR_PROFILE Then
		Return 1
	ElseIf $profile2 = $WIP_INTERNAL_MINOR_PROFILE Then
		Return -1
	Else
		$sub = $profile1 - $profile2
		Return $sub / Abs($sub)
	EndIf

EndFunc   ;==>MinorProfile_Compare

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_IsEqualOrAbove
; Description ..:
; Return .......: boolean - True if the profile belongs to recent branch.
;-------------------------------------------------------------------------------
Func MinorProfile_IsEqualOrAbove($ref_profile, $tested_profile = Default)
	$ret = 1 ; Empty profile is the latest profile
	If $tested_profile = Default Then $tested_profile = MinorProfile_Get()
	If $tested_profile <> $WIP_INTERNAL_MINOR_PROFILE Then
		$ret = ($tested_profile >= $ref_profile)
	EndIf
	Return $ret
EndFunc   ;==>MinorProfile_IsEqualOrAbove

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_IsStrictlyBelow
; Description ..:
; Return .......: boolean - True if the profile belongs to older branch.
;-------------------------------------------------------------------------------
Func MinorProfile_IsStrictlyBelow($ref_profile, $tested_profile = Default)
	Return Not MinorProfile_IsEqualOrAbove($ref_profile, $tested_profile)
EndFunc   ;==>MinorProfile_IsStrictlyBelow

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_GetFromAnyStringWithBuildPath
; Description ..: Extract the minor profile info from a string that contains
;                 the builds directory path.
; Remark .......: Can be used with $CmdLineRaw as a parameter (sometimes)
; Return .......: Either of those strings "600", "610", ....
;-------------------------------------------------------------------------------
Func MinorProfile_GetFromAnyStringWithBuildPath($stringWithBuildPath)
	$profile = ""
	If StringInStr($stringWithBuildPath, "\Daily_builds_InternalDevelopment\") Then
		$profile = "600"
	ElseIf StringInStr($stringWithBuildPath, "\Regular_InternalDevelopment\") Then
		$profile = "600"
	Else
		$part = StringRegExpSingleEx($stringWithBuildPath, "\\Daily_builds_(\d_\d\d)\\")
		If $part = "" Then
			$part = StringRegExpSingleEx($stringWithBuildPath, "\\Regular_(\d_\d\d)\\")
		EndIf
		If $part <> "" Then
			$a = StringSplit($part, "")
			$profile = $a[1] & $a[3] & $a[4]
		EndIf
	EndIf
	Return $profile
EndFunc   ;==>MinorProfile_GetFromAnyStringWithBuildPath

;-------------------------------------------------------------------------------
; Name .........: MinorProfile_ToBuildsContainerName200x
; Description ..:
; Remark .......:
; Return .......: A string such as "Daily_builds_6_12", etc.
;-------------------------------------------------------------------------------
Func MinorProfile_ToBuildsContainerName200x($minorProfile)
	;
	; WARNING:
	; THIS PIECE OF CODE IS NOT COMPATIBLE WITH \\VSSERVER02\VERSIONS$\AIS\
	;   -- IS_VIMAGO_BLDSYS_OCT2013_SET
	;
	$subPath = ""
	If $minorProfile = 600 Then
		$subPath = "Daily_builds_InternalDevelopment"
	Else
		$major = StringLeft($minorProfile, 1)
		$minor = StringRight($minorProfile, 2)
		$subPath = "Daily_builds_" & $major & "_" & $minor
	EndIf
	Return $subPath
EndFunc   ;==>MinorProfile_ToBuildsContainerName200x
