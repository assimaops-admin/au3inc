;===============================================================================
; Module ...........: BuildInfo.au3
; Description ......: Information about the test-bench
; Cross-reference ..: [REF 2010-12-21-134510] See BuildInfo.au3, HostInfo.au3,
;                       TestSuiteInfo.au3 and MinorProfile.au3.
; Author ...........: Patryk Szczepankiewicz (pszczepa at gmail dot com)
; History ..........: DEC 21, 2010 - Created or added this header
;===============================================================================
#include-once
#include <AuItCustLibs\AutoItEx.au3>
#include <AuItCustLibs\ArrayV2.au3>
#include <BaseUAFLibs\UDFsx\FileGetTimeV2.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>
#include <CoreUAFLibs\UDFs\StringSplitExcerpt.au3>
#include <CorpUAFLibs\UDFs\HTMLMisc.au3>
#include ".\MinorProfile.au3"
#include ".\BuildInfo.Const.au3"

Const $TESTRELEASE_ARTEFACTS_SUBDIR = "TestRelease"
; Const $IS_VIMAGO_BLDSYS_OCT2013_SET = 1 ; REMOVED JUL 26, 2016 - AS USELESS/DEPRECATED
; Set_("IS_BUILD_MANAGER_MODULE_TESTING") ; VERBOSE AND WITH MESSAGE BOXES - FOR TESTING

Const $BUILD_NUMBER_PATTERN_V3 = "(\.\d\d\d\d)\z" ; Match only at the end of the string
Const $BUILD_NUMBER_PATTERN_V2 = "(\.\d\d\d\d)[^\d]"
Const $BUILD_NUMBER_PATTERN_V1 = "(\.\d\d\d\d)"
Const $BUILD_UTID_PATTERN = "(\w+\.\d\d\.\d\d\d\d.\d\d\d\d\d\d-\d\d-\d\d-\d\d\d\d)"
Const $PRODUCT_UTID_PATTERN = "(\w+\.\d\d\.)"

Const $BUILD_STATUS_SUCCESS_CODE = 0
Const $BUILD_STATUS_GENERAL_ERROR_CODE = 1
Const $BUILD_STATUS_MISSING_LOG_FILE_ERROR_CODE = 2
Const $BUILD_STATUS_EMPTY_LOG_FILE_ERROR_CODE = 3

Const $BM_FAILED_BUILD_PATTERN = "FAILED|NOTFOUND"
Const $BM_NON_NIGHTLY_BUILD_PATTERN = "MIDDAY|Last_Build"
Const $BM_NON_SUCCESSFUL_NIGHLTY_PATTERN = $BM_FAILED_BUILD_PATTERN & "|" & $BM_NON_NIGHTLY_BUILD_PATTERN
Const $BM_FIVER_BUILD_PATTERN = "[^05]\Z"
Const $BM_FAILED_UNIT_TESTS_PATTERN = "UNIT_TESTS_FAILED"


;
; Build log info:
;     1/ file name
; Hunk info:
;     1/ excerpt,
;     2/ pos,
;     3/ hunk start line index (ake. marker),
;     4/ glob splits (aka. displayed lines count)
;     and 5/ searched string
;
Const $_HUNKINFO_COL_SHIFT_EXCERPT = 1
Const $_HUNKINFO_COL_SHIFT_POS = 2
Const $_HUNKINFO_COL_SHIFT_MARK = 3
Const $_HUNKINFO_COL_SHIFT_SPLITS = 4
Const $_HUNKINFO_COL_SHIFT_SEARCHED = 5
;
Const $_HUNKINFO_COLS_COUNT = $_HUNKINFO_COL_SHIFT_SEARCHED ; << COMPUTED
Const $_BUILDINFO_COLS_COUNT = 1 + (2 * $_HUNKINFO_COLS_COUNT) ; << COMPUTED

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetExePathV1
; Description ..: Get path to executable file
; Parameters ...: build directory path and executable name
; Return .......: String - Path to the executable file
;                 - or - empty string upon failure
;-------------------------------------------------------------------------------
Func BuildInfo_GetExePathV1($suite, $studio)
	FuncTrace("BuildInfo_GetExePathV1", $suite, $studio)
	Return $suite & "\" & $studio
EndFunc   ;==>BuildInfo_GetExePathV1

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetExePathV2
; Description ..: Get path to executable file
; Parameters ...: build directory path and executable name
; Return .......: String - Path to the executable file
;                 - or - empty string upon failure
;-------------------------------------------------------------------------------
Func BuildInfo_GetExePathV2($suite, $studio)
	FuncTrace("BuildInfo_GetExePathV2", $suite, $studio)
	Return $suite & "\" & $TESTRELEASE_ARTEFACTS_SUBDIR & "\" & $studio
EndFunc   ;==>BuildInfo_GetExePathV2

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetLogFilePath
; Description ..: Get the log file path
; Parameters ...: Path to the build directory (no 'TestRelease' please!)
; Return .......: String - Path to the log file
;                 - or - empty string upon failure
;-------------------------------------------------------------------------------
Func BuildInfo_GetLogFilePath($buildPath)
	Local $logFileName = "", $logFilePath = ""
	BuildInfo_GetLogFileNameAndPath($buildPath, $logFileName, $logFilePath)
	Return $logFilePath
EndFunc   ;==>BuildInfo_GetLogFilePath

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetLogFileNameAndPath
; Description ..: Get the log file path
; Parameters ...: Path to the build directory (no 'TestRelease' please!)
; Return .......: String - Path to the log file
;                 - or - empty string upon failure
; Testing ......:
;  ; DIRECT TEST -- REF.PS/20130131_202458
; 		;$testBuildDir = "V:\AIS\DAILY_~1\7.02.2123"
; 		;$testBuildDir = "L:\AIS\DAILY_~1\7.02.2123"
; 		;$testBuildDir = "\\storage0x\versions$\AIS\DAILY_~1\7.02.2123"
; 		$logFilePath = BuildInfo_GetLogFilePath($testBuildDir)
; 		;ConsoleWrite('---> $logFilePath = ' & $logFilePath & @CRLF)
;-------------------------------------------------------------------------------
Func BuildInfo_GetLogFileNameAndPath($buildPath, ByRef $logFileName, ByRef $logFilePath)
	FuncTrace("BuildInfo_GetLogFileNameAndPath", $buildPath)
	$isFound = False
	$names = _ArrayCreate("logWebsite.txt", "log.txt", "logMisys.txt", "logLawson.txt")
	$subdirs = _ArrayCreate("Logs", "")
	For $nm In $names
		For $sd In $subdirs
			If $sd = "" Then
				$fp = $buildPath & "\" & $nm
			Else
				$fp = $buildPath & "\" & $sd & "\" & $nm
			EndIf
			$isFound = FileExists($fp)
			;ConsoleWrite('---> $fp = ' & $fp & @CRLF)
			;ConsoleWrite('---> $isFound = ' & $isFound & @CRLF)
			If $isFound Then
				$logFileName = $nm
				$logFilePath = $fp
				ExitLoop
			EndIf
		Next
	Next
	Return $isFound
EndFunc   ;==>BuildInfo_GetLogFileNameAndPath

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetBuildTime
; Description ..: Get the timestamp of the log file.
; Parameters ...: $format [optional] - To specify type of return
;                     - 0 = return an array (default)
;                     - 1 = return a string YYYYMMDDHHMMSS
;                     - String = custom format (eg. "%04d%02d%02d-%02d%02d%02d")
; Return .......: Array or String depending on requested format
;                - or - Interger '0' upon failure.
;-------------------------------------------------------------------------------
Func BuildInfo_GetBuildTime($buildPath, $format = 0)
	FuncTrace("BuildInfo_GetBuildTime", $buildPath, $format)
	$file = BuildInfo_GetLogFilePath($buildPath)
	If $file = "" Then Return 0
	Return FileGetTimeV2($file, 0, $format) ; << WARNING: FORMAT FORWARDED
EndFunc   ;==>BuildInfo_GetBuildTime

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_IsBuildUTID
; Description ..:
; Return .......: Boolean
;-------------------------------------------------------------------------------
Func BuildInfo_IsBuildUTID($string)
	FuncTrace("BuildInfo_IsBuildUTID", $string)
	$buildUTID = StringRegExpSingleEx($string, $BUILD_UTID_PATTERN)
	Return $string = $buildUTID
EndFunc   ;==>BuildInfo_IsBuildUTID

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetProductName
; Category .....: BUILD PATHS
; Description ..: n/a
; Return .......: n/a
; WARNING ......: The name can either be "AIS" or "Vimago"
;                 - different from Acronym which always is "AIS"
; WARNING ......: There are differences of implementation between
;                 BuildInfo_GetProductName and BuildManager_GetProductName
;-------------------------------------------------------------------------------
Func BuildInfo_GetProductName($buildPath)
	$productNameRet = BuildInfo__Private__GetProductAcronym($buildPath)
	If StringInStr($buildPath, "\Vimago\") Then $productNameRet = "Vimago"
	RetTrace($productNameRet, "BuildInfo_GetProductName")
	Return $productNameRet
EndFunc   ;==>BuildInfo_GetProductName

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetProductAcronym
; Description ..: '$buildPath' must contain the tag of the product.
; Return .......: Either of those strings "ATS", "APS", "ACMS", "MLS", "AMS"....
; WARNING ......: "Vimago" acronym is converted to "AIS"
;-------------------------------------------------------------------------------
Func BuildInfo_GetProductAcronym($buildPath)
	$retVal = BuildInfo__Private__GetProductAcronym($buildPath)
	RetTrace($retVal, "BuildInfo_GetProductAcronym")
	Return $retVal
EndFunc   ;==>BuildInfo_GetProductAcronym
Func BuildInfo__Private__GetProductAcronym($buildPath)
	$retVal = ""
	$acronyms = _ArrayCreateV2("AIS", "ATS", "APS", "ACMS", "MLS", "AMS", "ESB", "ACC", "ASLS", "Vimago")
	Dim $indexes[UBound($acronyms)] = [$acronyms[0]]
	For $i = 1 To $acronyms[0]
		If StringInStr($buildPath, $acronyms[$i]) Then
			$indexes[$i] = StringInStr($buildPath, $acronyms[$i])
		EndIf
	Next
	$iMinIndex = 0
	For $i = 1 To $indexes[0]
		If $indexes[$i] And ((Not $iMinIndex) Or ($indexes[$i] < $indexes[$iMinIndex])) Then
			$iMinIndex = $i
		EndIf
	Next
	If Not $iMinIndex Then
		FatalError("Path '" & $buildPath & "' does not contain information about the product (or this product is not listed).")
	EndIf
	$retVal = $acronyms[$iMinIndex]
	; ----------------------------------------
	; Hack for new version - OCT 1, 2014
	If $retVal = "Vimago" Then $retVal = "AIS"
	; ----------------------------------------
	Return $retVal
EndFunc   ;==>BuildInfo__Private__GetProductAcronym

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetMinorProfile
; Description ..: '$buildPath' must contain the standard branch name.
; Return .......: Either of those strings "600", "610", ....
;-------------------------------------------------------------------------------
Func BuildInfo_GetMinorProfile($buildPath)
	Return MinorProfile_GetFromAnyStringWithBuildPath($buildPath)
EndFunc   ;==>BuildInfo_GetMinorProfile

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetFullProfile
; Description ..: '$buildPath' must contain the tag of the product.
; Return .......: Either of those strings "Ats600", "Aps610", ....
;-------------------------------------------------------------------------------
Func BuildInfo_GetFullProfile($buildPath)
	$productAcronym = BuildInfo_GetProductAcronym($buildPath)
	$productAcronym = StringLeft($productAcronym, 1) & StringLower(StringTrimLeft($productAcronym, 1))
	Return $productAcronym & BuildInfo_GetMinorProfile($buildPath)
EndFunc   ;==>BuildInfo_GetFullProfile

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetProductUTIDFromBuildUTID
; Description ..:
; Return .......: String
;-------------------------------------------------------------------------------
Func BuildInfo_GetProductUTIDFromBuildUTID($buildUTID)
	FuncTrace("BuildInfo_GetProductUTIDFromBuildUTID", $buildUTID)
	$product = StringRegExpSingleEx($buildUTID, $PRODUCT_UTID_PATTERN)
	; remove the trailing dot
	Return StringTrimRight($product, 1)
EndFunc   ;==>BuildInfo_GetProductUTIDFromBuildUTID

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetBuildUTIDFromPath
; Description ..:
; Return .......: String
;-------------------------------------------------------------------------------
Func BuildInfo_GetBuildUTIDFromPath($buildPath)
	FuncTrace("BuildInfo_GetBuildUTIDFromPath", $buildPath)

	$ret = ""

	; read the build number on the directory
	$build_number = StringRegExpSingleEx($buildPath, $BUILD_NUMBER_PATTERN_V2)
	If $build_number = "" Then $build_number = StringRegExpSingleEx($buildPath, $BUILD_NUMBER_PATTERN_V3)

	; remove the 'dot' separator
	If $build_number <> "" Then $build_number = StringTrimLeft($build_number, 1)

	; read the build number inscribed within an executable file
	$executable = BuildInfo_FindFirstExecutable($buildPath)
	$build_number_bis = ""
	If $executable = "" Then
		; no executable found
		; TODO: notification/warning?
	Else
		$version = FileGetVersion($executable)
		$build_number_bis = StringRegExpSingleEx($version, $BUILD_NUMBER_PATTERN_V1)
	EndIf

	; remove the 'dot' separator
	If $build_number_bis <> "" Then $build_number_bis = StringTrimLeft($build_number_bis, 1)

	; check for absurdities
	; -> check normal/tolerated cases first
	If ($build_number <> "") _
			And ($build_number_bis <> "") _
			And ( _
			($build_number = $build_number_bis) _
			Or (($build_number = $build_number_bis + 1) And StringRegExp($buildPath, $BM_NON_SUCCESSFUL_NIGHLTY_PATTERN)) _
			) _
			Then
		; OK
	Else
		; not even for the sake of dummy testing?!
		If Not StringInStr(@ScriptFullPath, "SelfQa") Then
			; THAT IS BAD THEN
			FatalError("BuildInfo_GetBuildUTID assertion failed." _
					 & @CRLF & "build_number=" & $build_number & " and build_number_bis=" & $build_number_bis)
		EndIf
	EndIf

	; using a renammed build directory?
	If $build_number = "" Then $build_number = $build_number_bis

	; get the timestamp
	$modif_date = BuildInfo_GetBuildTime($buildPath, $eFileGetTimeFormat_1)

	; main prefix
	$product_prefix = "ATS.06"
	If FileExists($buildPath & "\PerformanceStudio.exe") Or FileExists($buildPath & "\TestRelease\PerformanceStudio.exe") Then
		$product_prefix = "APS.06"
	EndIf

	; return the final ID
	If $build_number <> "" And $modif_date <> "" Then
		$ret = $product_prefix & "." & $build_number & "." & $modif_date
	EndIf
	Return $ret
EndFunc   ;==>BuildInfo_GetBuildUTIDFromPath

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_FindFirstExecutable
; Description ..:
; Return .......: String - Path to executable
;-------------------------------------------------------------------------------
Func BuildInfo_FindFirstExecutable($buildPath)
	FuncTrace("BuildInfo_FindFirstExecutable", $buildPath)
	$executable_path = ""
	; try the first location
	$search = FileFindFirstFile($buildPath & "\*.exe")
	If $search = -1 Then
		; try another location
		$buildPath = $buildPath & "\" & $TESTRELEASE_ARTEFACTS_SUBDIR
		$search = FileFindFirstFile($buildPath & "\*.exe")
	EndIf
	; was the search successful?
	; -> prepare the return value then
	If $search <> -1 Then
		$executable_path = $buildPath & "\" & FileFindNextFile($search)
		FileClose($search)
	EndIf
	; return whatever we found - or not
	Return $executable_path
EndFunc   ;==>BuildInfo_FindFirstExecutable

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetBuildStatus -- AKA. BuildInfo_GetErrCodeFromLogs
; Description ..:
; Parameters ...: $excerpt_size - Number of lines to extract before and after
;                      the word error
; Return .......: 2-dimensions array
;                 $status[0][0] - error code - '0' if build compiled successfully
;                 $status[0][1] - excerpts count ('n')
;                 $status[n][0] - file path 'n'
;                 $status[n][1] - excerpt
;                 $status[n][2] - offset (for the search pattern)
;                 $status[n][3] - split marker (typically the file line number)
;                 $status[n][4] - splits count (typically the number of lines)
; Remarks ......: Regarding the search pattern (REF.PS/20100920_151257),
;                 we put a space character before and after the word "error".
;
;        1/ Usually the string is
;
;             ": error C1234"
;             "fatal error C1234",
;             "fatal error U1077", etc.
;
;        2/ Not to be mistaken with
;
;             "triggeronerror "
;             'Errors.cpp'
;             "you may find errors "
;             'Task "error" skipped'...
;
;        3/ .NET compilation contains some errors the we have to ignore:
;
;             "error PRJ0019"
;
;        4/ 'LogTests.txt' contains multiple types of indications. First,
;            let's see the static ones:
;
;             4.a/ "Failed                "
;                  -- eg. "Failed                Assima(...)DefaultField"
;                  (NOTE the 16 {SPACE} characters!)
;             4.b/ "Test Run Failed."
;
;           Second, there are some patterns that could be expressed using
;           regular expressions:
;
;             4.i/ "Failed     1"
;             4.ii/ "1558/1559 test(s) Passed, 1 Failed"
;
;         4-BIS/ "LogTests.trx" contains strings such as
;                   outcome="Failed"
;
;         5/ Recent log files also end with the following messages
;
;             5.a/ "<== All processes ended successfully ==>" (logUpgrade.txt, SetupLog.txt)
;
;             5.b.i/ "<== All actions completed successfully. ==>" (SetupLog.txt)
;             5.b.ii/ "<== At least one action has failed. ==>" (SetupLog.txt)
;
;             -- NOTE: the messages describing actions status comes
;             after the one describing the processes
;
; Testing ......:
;
;  ; SAMPLE BUILD PATHS
;
; 		$testBuildDir = "C:\Cache\_samples\AIS700-7.03.2138_UNIT_TESTS_NOTFOUND_20130220.1"
; 		$testBuildDir = "L:\ASLS\Daily_builds_InternalDevelopment\1.00.2054_COMPILATION_FAILED_20130412"
; 		$testBuildDir = "L:\AIS\Daily_builds_InternalDevelopment\7.05.2259_WEBSITE_COMPILATION_FAILED_20130701"
; 		$testBuildDir = "C:\Cache\_samples\AIS700-7.06.2383-20131128_013726_UNIT_TESTS_FAILED"
; 		$testBuildDir = "V:\ATS\Daily_builds_6_25\6.25.10.3374-20180222_090502"
; 		$testBuildDir = "D:\Cache\storage01-versions$\ATS\Daily_builds_6_25\6.25.27.3374-20180620_152724"
; 		$testBuildDir = "D:\Cache\storage01-versions$\ATS\Daily_builds_6_25\6.25.26.3374-20180619_132850_COMPILATION_FAILED_20180619"
; 		$testBuildDir = "V:\ATS\Daily_builds_6_25\6.25.26.3374-20180619_132850_COMPILATION_FAILED_20180619"
; 		$testBuildDir = "V:\ATS\Daily_builds_InternalDevelopment\6.26.3530_COMPILATION_FAILED_20180628"
;
;  ; DIRECT TEST -- REF.PS/20130131_202458
;
; 		$status = BuildInfo_GetBuildStatus($testBuildDir)
; 		_ArrayDisplay($status)
;
;
;  ; MASTER TEST -- REF.PS/20130131_202458
;
;  		$status = BuildInfo_GetBuildStatus($testBuildDir)
;  		$body = BuildInfo_FormatBuildStatus($status, 1)
;		# include <File.au3>
;  		$tmpf = _TempFile(Default, "~", ".html")
;  		;$tmpf = _TempFile("C:\Users\Pino\AppData\Local\Temp", "~", ".html")
;  		FileWrite($tmpf, $body)
;  		ShellExecute($tmpf)
;
;
;  ; NETWORK TEST -- REF.PS/20180702_111841
;
;		Set_("IS_BUILD_MANAGER_MODULE_TESTING")
;		EnvSet("PATH", "C:\AutoTestingV2\bin;" & EnvGet("PATH"))
;		# include <ExtdCtxPrivMods\AssimaAdminMods\BuildManagerInc.au3>
;		BuildManager_CheckBuildStatusDlgSub($testBuildDir, BuildInfo_GetFullProfile($testBuildDir))
;
;
; History ......: AUG 18, 2010 - Created
;                 JUN 22, 2018 - Using Log_Regular\compile.txt and co.
;-------------------------------------------------------------------------------
Func BuildInfo_GetBuildStatus($buildPath)
	FuncTrace("BuildInfo_GetBuildStatus", $buildPath)

	$status = BuildInfo_PrivateLogInfoArrayCreate()

	Local $LOGTESTS_FILENAME = "logTests.txt"
	Local $IS_VIMAGO_BLDSYS_OCT2013_APPLIES = 0
	$productAcronym = BuildInfo_GetProductAcronym($buildPath)
	If (($productAcronym = "AIS") Or ($productAcronym = "Vimago")) Then
		$LOGTESTS_FILENAME = "logTests.trx"
		$IS_VIMAGO_BLDSYS_OCT2013_APPLIES = 1
	EndIf

	; _____________________________
	;
	; LOADER, WEBSITE, RAPIDLEARNING, SERVICES, DEPLOYMENT, INSTALLATION, UPGRADE
	; _____________________________

	;Const $RAPIDLEARNING_LOGFILE_ERR_PATTERN_LOCAL = $DEFAULT_LOGFILE_ERR_PATTERN
	;Const $SERVICES_LOGFILE_ERR_PATTERN_LOCAL = $DEFAULT_LOGFILE_ERR_PATTERN
	Const $DEPLOYMENT_LOGFILE_ERR_PATTERN_LOCAL = "error MSB"
	;Const $INSTALLATION_LOGFILE_ERR_PATTERN_LOCAL = $DEFAULT_LOGFILE_ERR_PATTERN
	Const $UPGRADE_LOGFILE_ERR_PATTERN_LOCAL = "has failed"
	; _____________________________
	;
	; AND TESTS -- REF.PS/20130702_154540+0200
	; _____________________________

	Const $UNITTEST_LOGFILE_NOTFOUND_PATTERN_LOCAL = '" not found'
	Const $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL_1 = "Failed                "
	Const $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL_2 = '" outcome="Failed" '
	Const $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL = QuestionMark($IS_VIMAGO_BLDSYS_OCT2013_APPLIES, $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL_2, $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL_1)
	Const $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL_1 = "Test Run Failed."
	Const $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL_2 = '<ResultSummary outcome="Failed">'
	Const $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL = QuestionMark($IS_VIMAGO_BLDSYS_OCT2013_APPLIES, $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL_2, $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL_1)
	Const $UNITTEST_LOGFILE_ERR_EXCERPT_LINES_COUNT = QuestionMark($IS_VIMAGO_BLDSYS_OCT2013_APPLIES, 50, 15)
	Const $UNITTEST_LOGFILE_ERR_EXCERPT_LINES_BIAS = QuestionMark($IS_VIMAGO_BLDSYS_OCT2013_APPLIES, $UNITTEST_LOGFILE_ERR_EXCERPT_LINES_COUNT / 2 - 2, 0)
	; _____________________________

	; Get the name of the main log file
	Local $main_log_file_name = "", $main_log_file_path = ""
	BuildInfo_GetLogFileNameAndPath($buildPath, $main_log_file_name, $main_log_file_path)

	; Create the initial array
	Const $MAX_LOGFILES_COUNT = 9
	Local $cLogs = $MAX_LOGFILES_COUNT
	Dim $logsearch_filenames[$cLogs + 1], _
			$logsearch_patterns[$cLogs + 1], _
			$logsearch_global_patterns[$cLogs + 1], _
			$logsearch_excerpts_lines_counts[$cLogs + 1], _
			$logsearch_excerpts_lines_bias[$cLogs + 1]

	; Fill the appropriate fields of the default array
	$logsearch_filenames[0] = $cLogs
	$logsearch_patterns[0] = $cLogs
	$logsearch_global_patterns[0] = $cLogs
	$logsearch_excerpts_lines_counts[0] = $cLogs
	$logsearch_excerpts_lines_bias[0] = $cLogs

	; Enter the information about the main log file
	For $itmp = 1 To $cLogs
		$logsearch_patterns[$itmp] = $DEFAULT_LOGFILE_ERR_PATTERN
		$logsearch_global_patterns[$itmp] = ""
		$logsearch_excerpts_lines_counts[$itmp] = $DEFAULT_EXCERPT_LINES_COUNT
		$logsearch_excerpts_lines_bias[$itmp] = 0
	Next

	; Check the status of the main log file
	If $main_log_file_name = "" Then

		; exception: missing log file
		BuildInfo_PrivateLogInfoArraySetError($status, $BUILD_STATUS_MISSING_LOG_FILE_ERROR_CODE)
		Return $status

	ElseIf FileGetSize($main_log_file_path) = 0 Then

		; exception: missing log file
		BuildInfo_PrivateLogInfoArraySetError($status, $BUILD_STATUS_EMPTY_LOG_FILE_ERROR_CODE)
		Return $status

	ElseIf $main_log_file_name = "log.txt" _
			Or $main_log_file_name = "logMisys.txt" _
			Or $main_log_file_name = "logLawson.txt" _
			Then

		; WARNING: Shrink the array if $cLogs < $MAX_LOGFILES_COUNT
		$cLogs = $MAX_LOGFILES_COUNT ; = 9
		;~ ReDim $logsearch_filenames[$cLogs + 1], $logsearch_patterns[$cLogs + 1], $logsearch_global_patterns[$cLogs + 1], $logsearch_excerpts_lines_counts[$cLogs + 1], $logsearch_excerpts_lines_bias[$cLogs + 1]
		;~ $logsearch_filenames[0] = $cLogs
		;~ $logsearch_patterns[0] = $cLogs
		;~ $logsearch_global_patterns[0] = $cLogs
		;~ $logsearch_excerpts_lines_counts[0] = $cLogs
		;~ $logsearch_excerpts_lines_bias[0] = $cLogs

		$iLog = 1
		$logsearch_filenames[$iLog] = $main_log_file_name
		$logsearch_patterns[$iLog] = $ROOT_LOGFILE_ERR_PATTERN

		$iLog = 2
		$logsearch_filenames[$iLog] = "Logs_Regular\compile.txt"
		$logsearch_patterns[$iLog] = $COMPILE_LOGFILE_ERR_PATTERN

		$iLog = 3
		$logsearch_filenames[$iLog] = "Logs_Vimago\compile.txt"
		$logsearch_patterns[$iLog] = $COMPILE_LOGFILE_ERR_PATTERN

		$iLog = 4
		$logsearch_filenames[$iLog] = "Logs_x64\compile.txt"
		$logsearch_patterns[$iLog] = $COMPILE_LOGFILE_ERR_PATTERN

		$iLog = 5
		$logsearch_filenames[$iLog] = "Logs_Regular\BuildLog.htm"
		$logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER

		;
		; THE FILES BELOW DO NOT EXIST
		;  BUT LET THEM BE... FOR NOW.
		;

		$iLog = 6
		$logsearch_filenames[$iLog] = "BuildLog2.htm"
		$logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER

		$iLog = 7
		$logsearch_filenames[$iLog] = "BuildLog4.htm"
		$logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER

		$iLog = 8
		$logsearch_filenames[$iLog] = "BuildLogVimago2.htm"
		$logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER

		$iLog = $cLogs
		$logsearch_filenames[$iLog] = "BuildLogVimago4.htm"
		$logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER

	ElseIf $main_log_file_name = "logWebsite.txt" Then

		; WARNING: Shrink the array if $cLogs < $MAX_LOGFILES_COUNT
		$cLogs = $MAX_LOGFILES_COUNT ; = 9
		;~ ReDim $logsearch_filenames[$cLogs + 1], $logsearch_patterns[$cLogs + 1], $logsearch_global_patterns[$cLogs + 1], $logsearch_excerpts_lines_counts[$cLogs + 1], $logsearch_excerpts_lines_bias[$cLogs + 1]
		;~ $logsearch_filenames[0] = $cLogs
		;~ $logsearch_patterns[0] = $cLogs
		;~ $logsearch_global_patterns[0] = $cLogs
		;~ $logsearch_excerpts_lines_counts[0] = $cLogs
		;~ $logsearch_excerpts_lines_bias[0] = $cLogs

		$iLog = 1
		$logsearch_filenames[$iLog] = $main_log_file_name

		$iLog = 2
		$logsearch_filenames[$iLog] = "logLoader.txt"

		$iLog = 3
		$logsearch_filenames[$iLog] = "logRapidLearning.txt"

		$iLog = 4
		$logsearch_filenames[$iLog] = "logServices.txt"

		$iLog = 5
		$logsearch_filenames[$iLog] = "logDeployment.txt"
		$logsearch_patterns[$iLog] = $DEPLOYMENT_LOGFILE_ERR_PATTERN_LOCAL

		$iLog = 6
		$logsearch_filenames[$iLog] = "logInstallation.txt"

		$iLog = 7
		$logsearch_filenames[$iLog] = "logUpgrade.txt"
		$logsearch_patterns[$iLog] = $UPGRADE_LOGFILE_ERR_PATTERN_LOCAL

		$iLog = 8
		$logsearch_filenames[$iLog] = $LOGTESTS_FILENAME
		$logsearch_patterns[$iLog] = $UNITTEST_LOGFILE_NOTFOUND_PATTERN_LOCAL

		$iLog = $cLogs
		$logsearch_filenames[$iLog] = $LOGTESTS_FILENAME
		$logsearch_patterns[$iLog] = $UNITTEST_LOGFILE_ERR_PATTERN_LOCAL
		$logsearch_global_patterns[$iLog] = $UNITTEST_LOGFILE_ERR_PATTERN_GLOBAL
		$logsearch_excerpts_lines_counts[$iLog] = $UNITTEST_LOGFILE_ERR_EXCERPT_LINES_COUNT
		$logsearch_excerpts_lines_bias[$iLog] = $UNITTEST_LOGFILE_ERR_EXCERPT_LINES_BIAS

	Else
		FatalError("Log file '" & $main_log_file_name & "' not supported!")
	EndIf

	; do the first search
	$iLog = 1
	$other_log_file_path = $main_log_file_path
	Trace("BuildInfo_PrivateLogInfoArrayFill -- CALL " & $iLog)
	$searchedPos = BuildInfo_PrivateLogInfoArrayFill($status, $logsearch_patterns[$iLog], $other_log_file_path, $logsearch_global_patterns[$iLog], $logsearch_excerpts_lines_counts[$iLog], $logsearch_excerpts_lines_bias[$iLog])
	Trace("$searchedPos = " & $searchedPos)

	; do the next searches (if any)
	If $logsearch_filenames[0] > 1 Then
		For $iLog = 2 To $logsearch_filenames[0]
			; build path to file
			$other_log_file_path = StringReplace($main_log_file_path, $main_log_file_name, $logsearch_filenames[$iLog])
			If Not FileExists($other_log_file_path) Then ContinueLoop
			; check the content and store info
			Trace("BuildInfo_PrivateLogInfoArrayFill -- CALL #" & $iLog)
			If $logsearch_patterns[$iLog] = $HTML_LOGFILE_ERR_PATTERN_PLACEHOLDER Then
				;
				; SPECIAL FOR HTML-based LOG FILES
				;
				Local $searchErrorCount = False
				$searchedPos = BuildInfo_PrivateLogInfoArrayFill($status, $HTML_LOGFILE_ERR_PATTERN_EXT_2, $other_log_file_path, $logsearch_global_patterns[$iLog], $logsearch_excerpts_lines_counts[$iLog], $logsearch_excerpts_lines_bias[$iLog])
				If $searchedPos Then $searchErrorCount = True
				If $searchErrorCount Then
					$searchedPos = BuildInfo_PrivateLogInfoArrayFill($status, $HTML_LOGFILE_ERR_PATTERN_EXT_X, $other_log_file_path, $logsearch_global_patterns[$iLog], $logsearch_excerpts_lines_counts[$iLog], $logsearch_excerpts_lines_bias[$iLog])
				EndIf
			Else
				;
				; DEFAULT CASE FOR TEXT-based LOG FILES
				;
				$searchedPos = BuildInfo_PrivateLogInfoArrayFill($status, $logsearch_patterns[$iLog], $other_log_file_path, $logsearch_global_patterns[$iLog], $logsearch_excerpts_lines_counts[$iLog], $logsearch_excerpts_lines_bias[$iLog])
			EndIf
			Trace("$searchedPos = " & $searchedPos)
		Next
	EndIf

	Trace("BuildInfo_GetBuildStatus -- RETURN")
	Return $status

EndFunc   ;==>BuildInfo_GetBuildStatus

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_PrivateLogInfoArrayCreate
; History ......: JAN 27, 2011 - Created/Refactored
;-------------------------------------------------------------------------------
Func BuildInfo_PrivateLogInfoArrayCreate()
	Dim $status[1][$_BUILDINFO_COLS_COUNT] = [[$BUILD_STATUS_SUCCESS_CODE]]
	Return $status
EndFunc   ;==>BuildInfo_PrivateLogInfoArrayCreate

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_PrivateLogInfoArraySetError
; History ......: JAN 27, 2011 - Created/Refactored
;-------------------------------------------------------------------------------
Func BuildInfo_PrivateLogInfoArraySetError(ByRef $status, $error_code)
	$status[0][0] = $error_code
EndFunc   ;==>BuildInfo_PrivateLogInfoArraySetError

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_PrivateLogInfoArrayFill
; Testing ......:
; ; MASTER TEST -- REF.PS/20130131_202458
;		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2121_UNIT_TESTS_FAILED_20130130"
;		$status = BuildInfo_GetBuildStatus($testBuildDir)
;		_ArrayDisplay($status)
;
;  ; DIRECT TEST -- REF.PS/20130131_202458
;
; 		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2121_UNIT_TESTS_FAILED_20130130"
; 		$buildLogFile = $testBuildDir & "\logTests.txt"
; 		$mainSearchPatt = "Failed                "
; 		$2ndSearchPatt = "Test Run Failed."
;
; 		$testBuildDir = "V:\AIS\DAILY_~1\7.03.2127_LOADER_COMPILATION_FAILED_20130207"
; 		$testBuildDir = "V:\AIS\DAILY_~1\7.03.2128_LOADER_COMPILATION_FAILED_20130207"
; 		$buildLogFile = $testBuildDir & "\logLoader.txt"
; 		$mainSearchPatt = " error "
; 		$2ndSearchPatt = ""
;
; 		$status = BuildInfo_PrivateLogInfoArrayCreate()
; 		BuildInfo_PrivateLogInfoArrayFill($status, $mainSearchPatt, $buildLogFile, $buildLogFile, $2ndSearchPatt)
; 		_ArrayDisplay($status)
;
; History ......: JAN 27, 2011 - Created/Refactored
;-------------------------------------------------------------------------------
Func BuildInfo_PrivateLogInfoArrayFill(ByRef $status, $szLocSearch, $file, $szGlobSearch, $cExcerptLines, $cExcerptBias = 0)

	FuncTrace("BuildInfo_PrivateLogInfoArrayFill", $status, $szLocSearch, $file, $szGlobSearch, $cExcerptLines, $cExcerptBias)

	Local $cFiles = $status[0][1]
	Local $iLocPos = 0, $iGlobPos = 0
	Local $szLocExcerpt = "", $szGlobExcerpt = ""
	Local $iLocMarker = 0, $iGlobMarker = 0
	Local $cLocSplits = 0, $cGlobSplits = 0

	$cSideLines = ($cExcerptLines - 1) / 2

	; WARNING: DIRTY HACK HERE
	;  => DON'T GATHER TOO MUCH INFO FROM GLOBAL SEARCH
	$cGlobalSideLines_HACKED = QuestionMark($cExcerptBias, $cSideLines / 4, 0)
	$cGlobalExcerptBias_HACKED = QuestionMark($cExcerptBias, $cGlobalSideLines_HACKED * 3 / 5, 0)

	; check: file exists?
	If Not FileExists($file) Then Return $iLocPos

	; load the content of the file
	$fileContent = FileRead($file)

	; WARNING: DIRTY HACK HERE
	; -- JUL 14, 2016
	$fileContent = StringReplace($fileContent _
		, "Connection succeeded, however the following error occurred" _
		, "Connection succeeded, however the following.error.occurred" _
		)

	; do the global search
	If $szGlobSearch <> "" Then
		$szGlobExcerpt = StringSplitExcerptByTextV2($fileContent, $szGlobSearch, $iGlobPos, $cGlobalSideLines_HACKED, $cGlobalExcerptBias_HACKED)
		$iGlobMarker = @error
		$cGlobSplits = @extended
	EndIf
	; do the local search
	$szLocExcerpt = StringSplitExcerptByTextV2($fileContent, $szLocSearch, $iLocPos, $cSideLines, $cExcerptBias)
	$iLocMarker = @error
	$cLocSplits = @extended
	; anything found?
	If $szLocExcerpt <> "" Then
		; prepare outputs
		$cFiles += 1
		ReDim $status[$cFiles + 1][$_BUILDINFO_COLS_COUNT]
		$status[0][1] = $cFiles

		$status[$cFiles][0] = $file

		$iColShift = 0 * $_HUNKINFO_COLS_COUNT

		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_EXCERPT] = $szLocExcerpt
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_POS] = $iLocPos
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_MARK] = $iLocMarker
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_SPLITS] = $cLocSplits
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_SEARCHED] = $szLocSearch

		$iColShift = 1 * $_HUNKINFO_COLS_COUNT

		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_EXCERPT] = $szGlobExcerpt
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_POS] = $iGlobPos
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_MARK] = $iGlobMarker
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_SPLITS] = $cGlobSplits
		$status[$cFiles][$iColShift + $_HUNKINFO_COL_SHIFT_SEARCHED] = $szGlobSearch

		#cs
			ConsoleWrite('[' & $cFiles & ']' & @CRLF)
			ConsoleWrite('$file = ' & $file & @CRLF)

			ConsoleWrite('$szLocExcerpt = ' & $szLocExcerpt & @CRLF)
			ConsoleWrite('$iLocPos = ' & $iLocPos & @CRLF)
			ConsoleWrite('$iLocMarker = ' & $iLocMarker & @CRLF)
			ConsoleWrite('$cLocSplits = ' & $cLocSplits & @CRLF)
			ConsoleWrite('$szLocSearch = ' & $szLocSearch & @CRLF)

			ConsoleWrite('$szGlobExcerpt = ' & $szGlobExcerpt & @CRLF)
			ConsoleWrite('$iGlobPos = ' & $iGlobPos & @CRLF)
			ConsoleWrite('$iGlobMarker = ' & $iGlobMarker & @CRLF)
			ConsoleWrite('$cGlobSplits = ' & $cGlobSplits & @CRLF)
			ConsoleWrite('$szGlobSearch = ' & $szGlobSearch & @CRLF)
		#ce

		Trace("BuildInfo_PrivateLogInfoArraySetError")
		BuildInfo_PrivateLogInfoArraySetError($status, $BUILD_STATUS_GENERAL_ERROR_CODE)
	EndIf

	Trace("BuildInfo_PrivateLogInfoArrayFill -- RETURN")
	Return $iLocPos

EndFunc   ;==>BuildInfo_PrivateLogInfoArrayFill

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_IsRecordableBuild
; Description ..: APS is not compiled if ATS failed
;                 + Cannot record builds that do not have a genuine logfile
; Parameters ...:
; Return .......:
; History ......: MAY 19, 2011 - Created
;-------------------------------------------------------------------------------
Func BuildInfo_IsRecordableBuild($buildPath)
	$funcTrace = FuncTrace("BuildInfo_IsRecordableBuild", $buildPath)
	$ret = True
	$isFake = StringInStr($buildPath, "\APS\") _
			And StringInStr($buildPath, "_ATS_COMPILATION_FAILED")
	If $isFake Then $ret = False
	If BuildInfo_GetLogFilePath($buildPath) = "" Then $ret = False
	Return RetTrace($ret, $funcTrace)
EndFunc   ;==>BuildInfo_IsRecordableBuild

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_FormatBuildStatus
; Description ..: Process array returned by 'BuildInfo_GetBuildStatus'
; Parameters ...: $status - multi-dimensional array returned by
;                          'BuildInfo_GetBuildStatus'
;                 $format - '0' for text standard format
;                           '1' for html standard format
; Testing ......:
; ; DIRECT TEST / SPECIFIC -- REF.PS/20130131_202458
;		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2121_UNIT_TESTS_FAILED_20130130"
;		$format = 1 ; !!!
;		$status = BuildInfo_PrivateLogInfoArrayCreate()
;		BuildInfo_PrivateLogInfoArrayFill($status, "Failed                ", $testBuildDir & "\logTests.txt", "Test Run Failed.")
;		$body = BuildInfo_FormatBuildStatus($status, $format)
;		$tmpf = _TempFile(Default, "~", ".html")
;		FileWrite($tmpf, $body)
;		ShellExecute($tmpf)
;
;  ; DIRECT TEST / SPECIFIC -- REF.PS/20130131_202458
; 		$testBuildDir = "V:\AIS\DAILY_~1\7.03.2128_LOADER_COMPILATION_FAILED_20130207"
;  		$buildLogFile = $testBuildDir & "\logLoader.txt"
;  		$mainSearchPatt = " error "
;  		$2ndSearchPatt = ""
; 		$format = 1 ; !!!
; 		$status = BuildInfo_PrivateLogInfoArrayCreate()
;  		BuildInfo_PrivateLogInfoArrayFill($status, $mainSearchPatt, $buildLogFile, $buildLogFile, $2ndSearchPatt)
; 		$body = BuildInfo_FormatBuildStatus($status, $format)
; 		$tmpf = _TempFile(Default, "~", ".html")
; 		FileWrite($tmpf, $body)
; 		ShellExecute($tmpf)
;
; ; DIRECT TEST / GENERAL -- REF.PS/20130131_202458
; 		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2121_UNIT_TESTS_FAILED_20130130"
; 		$status = BuildInfo_GetBuildStatus($testBuildDir)
; 		$body = BuildInfo_FormatBuildStatus($status, 1)
; 		$tmpf = _TempFile(Default, "~", ".html")
; 		FileWrite($tmpf, $body)
; 		ShellExecute($tmpf)
; Return .......: Formatted string
; History ......: AUG 18, 2010 - Created
;-------------------------------------------------------------------------------
Func BuildInfo_FormatBuildStatus($status, $format = 0)
	FuncTrace("BuildInfo_FormatBuildStatus", $status, $format)

	$msg = ""

	$cFiles = $status[0][1]

	; exception: no error
	If $status[0][0] = $BUILD_STATUS_SUCCESS_CODE Then
		Return $msg
	EndIf

	; exception: missing log file
	If $status[0][0] = $BUILD_STATUS_MISSING_LOG_FILE_ERROR_CODE Then
		$msg &= "Missing compilation info (no 'log.txt'/'logSomething.txt' file found)."
		Return $msg
	EndIf

	; exception: missing log file
	If $status[0][0] = $BUILD_STATUS_EMPTY_LOG_FILE_ERROR_CODE Then
		$msg &= "Missing compilation info (file 'log.txt'/'logSomething.txt' is empty)."
		Return $msg
	EndIf

	; here, generic error
	Assert($status[0][0] = $BUILD_STATUS_GENERAL_ERROR_CODE _
			, "$status[0][0]=$BUILD_STATUS_GENERAL_ERROR_CODE")
	;  |
	;  |
	;  V
	; exception: missging further info
	If $cFiles = 0 Then
		$msg &= "Generic error. Missing further information."
		Return $msg
	EndIf
	;  |
	;  |
	;  V
	; general case: format array
	For $iFile = 1 To $cFiles

		Local $msgFile = ""
		Local $msgLoc = "", $msgGlob = ""

		$iLocMarker = $status[$iFile][(0 * $_HUNKINFO_COLS_COUNT) + $_HUNKINFO_COL_SHIFT_MARK]
		$iGlobMarker = $status[$iFile][(1 * $_HUNKINFO_COLS_COUNT) + $_HUNKINFO_COL_SHIFT_MARK]

		$iHunk = 0
		$msgLoc = BuildInfo_FormatBuildStatusSub($status, $iFile, $iHunk, $format)

		If $iGlobMarker Then
			$iHunk = 1
			$msgGlob = BuildInfo_FormatBuildStatusSub($status, $iFile, $iHunk, $format)
		EndIf

		; concatenate hunks
		$msgFile = $msgLoc & $msgGlob
		If $iGlobMarker < $iLocMarker Then $msgFile = $msgGlob & $msgLoc

		; append to message
		If $format = 1 Then $msg &= @CRLF & '<br />'
		If $format = 1 Then $msg &= @CRLF & '<div class="file-dflt">'
		If $format = 1 Then $msg &= @CRLF & '<div class="file-ttl1">'
		$msg &= "File: " & $status[$iFile][0]
		If $format = 1 Then $msg &= @CRLF & '</div>'
		If $format = 1 Then $msg &= @CRLF & '<br />'
		$msg &= $msgFile
		If $format = 1 Then $msg &= @CRLF & '</div>'

	Next

	$smsg = ""
	If $format = 1 Then
		$smsg &= @CRLF
		$smsg &= HTMLMisc_InlineCssStyleString() & @CRLF
	EndIf

	Return $smsg & $msg
EndFunc   ;==>BuildInfo_FormatBuildStatus

Func BuildInfo_FormatBuildStatusSub($status, $iFile, $iHunk, $format = 0)

	$submsg = ""

	$iColShift = $iHunk * $_HUNKINFO_COLS_COUNT

	; Header section (format inspired by GIT):
	; Show index of the first line + shown lines count
	; @ -116,10 +116,18 @ Func BuildManager_IsTodayBuild($fullProfile, $bldDirName)
	$firstLineDisplayedAny = $status[$iFile][$iColShift + $_HUNKINFO_COL_SHIFT_MARK]
	$countLinesDisplayedAny = $status[$iFile][$iColShift + $_HUNKINFO_COL_SHIFT_SPLITS]

	; Content section (ie. excerpt)
	$excerptAny = $status[$iFile][$iColShift + $_HUNKINFO_COL_SHIFT_EXCERPT]
	If $format = 1 Then
		; Encode all sensitive characters
		$excerptAny = HTMLSpecialChars($excerptAny)
		$searchedAny = HTMLSpecialChars($status[$iFile][$iColShift + $_HUNKINFO_COL_SHIFT_SEARCHED])
		$excerptAny = StringReplace($excerptAny, $searchedAny, '<span class="tag-positive">' & $searchedAny & '</span>')
	EndIf

	If $format = 1 Then $submsg &= @CRLF & '<div class="hunk-dflt">'

	If $format = 1 Then $submsg &= @CRLF & '<div class="hunk-ttl1">'
	$submsg &= @CRLF & StringFormat('@ %s,%s @', $firstLineDisplayedAny, $countLinesDisplayedAny)
	If $format = 1 Then $submsg &= '</div>' & @CRLF & '<div class="hunk-data">'
	$submsg &= $excerptAny
	If $format = 1 Then $submsg &= '</div>'

	If $format = 1 Then $submsg &= @CRLF & '</div>'

	Return $submsg

EndFunc   ;==>BuildInfo_FormatBuildStatusSub

;-------------------------------------------------------------------------------
; Name .........: BuildInfo_GetCoreErrCode
; Description ..: Do we have the core executables available?
; Parameters ...:
; Return .......: Error-code - Integer ('0' = Success)
; Testing ......:
; ; DIRECT TEST -- REF.PS/20130131_202458
; 		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2121_UNIT_TESTS_FAILED_20130130"
; 		$testBuildDir = "L:\AIS\DAILY_~1\7.02.2123"
;		$testBuildDir = "V:\AIS\DAILY_~1\7.02.2123"
; 		$err_code = BuildInfo_GetCoreErrCode($testBuildDir)
; 		;ConsoleWrite('---> $err_code = ' & $err_code & @CRLF)
; History ......: AUG 18, 2010 - Created
;-------------------------------------------------------------------------------
Func BuildInfo_GetCoreErrCode($buildPath)
	FuncTrace("BuildInfo_GetCoreErrCode", $buildPath)
	$err_code = 1
	If Not BuildInfo_IsRecordableBuild($buildPath) Then Return $err_code
	$productAcronym = BuildInfo_GetProductAcronym($buildPath)

	; Old AIS test results
	Local $lastLogFile = "logTests.txt"
	; AIS "Data integration tests" results
	If (($productAcronym = "AIS") Or ($productAcronym = "Vimago")) Then ; $IS_VIMAGO_BLDSYS_OCT2013_SET
		$lastLogFile = "logTests.trx"
	EndIf
	; Vimago - Build-related "Data integration tests" are disabled in the recent versions (Vimago based on Framework + an orgy of services)
	;   - WARNING: "logUpgrade.txt.zip" does not exist either!
	; If BuildInfo_GetProductName($buildPath) = "Vimago" Then $lastLogFile = "logUpgrade.txt.zip"
	If BuildInfo_GetProductName($buildPath) = "Vimago" Then $lastLogFile = "logInstallation.txt.zip"

	;ConsoleWrite('---> $productAcronym = ' & $productAcronym & @CRLF)
	Switch $productAcronym
		Case "ATS"
			Trace("Case ATS!")
			Trace("  -> Check that TrainingStudio.exe and Simulator.exe exist.")
			If FileExists($buildPath & "\" & $TESTRELEASE_ARTEFACTS_SUBDIR & "\TrainingStudio.exe") And FileExists($buildPath & "\TestRelease\Simulator.exe") Then
				$err_code = 0
			EndIf
		Case "APS"
			Trace("Case APS!")
			Trace("  -> Check that PerformanceStudio.exe exists...")
			If FileExists($buildPath & "\" & $TESTRELEASE_ARTEFACTS_SUBDIR & "\PerformanceStudio.exe") Then
				$err_code = 0
			EndIf
		Case "Vimago"
			ContinueCase
		Case "AIS"
			Trace("Case AIS!")
			Trace("  -> Check that " & $lastLogFile & " exist.")
			If FileExists($buildPath & "\" & $lastLogFile) Then
				$err_code = 0
			EndIf
		Case Else
			$status = BuildInfo_GetBuildStatus($buildPath)
			$err_code = $status[0][0]
	EndSwitch
	RetTrace($err_code, "BuildInfo_GetCoreErrCode")
	Return $err_code
EndFunc   ;==>BuildInfo_GetCoreErrCode
