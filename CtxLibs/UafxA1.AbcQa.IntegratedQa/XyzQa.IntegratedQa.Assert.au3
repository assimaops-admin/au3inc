#include-once
#include ".\AbcQa.au3" ; <<< _AbcQa_Assert

; TODO: Print references to console if a given parameter is passed to this script

; REFERENCES:
; ConsoleWrite("C:\Progra~2\AutoIt3\Include\Debug.au3:63:Func _Assert($sCondition, $bExit = True, $nCode = 0x7FFFFFFF, $sLine = @ScriptLineNumber, Const $curerr = @error, Const $curext = @extended)" & @CRLF)
; ConsoleWrite("C:\Progra~2\AutoIt3\Include\CoreUAFLibs\StdCoreLib\XcptSystem.au3:42:Func Assert($test, $sTest, $nErrorCode = 1, $sScriptPath = @ScriptFullPath, $sLine = @ScriptLineNumber)" & @CRLF)
; ConsoleWrite("C:\Progra~2\AutoIt3\Include\CoreUAFLibs\StdCoreLib\LocalLog.au3:37:	Assert($g_szLogPath = "", '$g_szLogPath = ""')" & @CRLF)
; ConsoleWrite("C:\Progra~2\AutoIt3\Include\CustLibs\TEMPAPI160.IBMPLib\TEMPAPI160_IterBasedMemPerfQa_LIBRARY.au3:130:Func TEMPAPI160_IterBasedMemPerfQa_ASSERT($isCond, $szAddErrorMsgB = "", $szAddErrorMsgA = "", $isSilent = False, $scriptLineNumber = @ScriptLineNumber, $scriptPath = @ScriptFullPath)" & @CRLF)
; ConsoleWrite("AbiQa.Generic.au3:284:			Trace(""This isn't what we expected (ASSERTION FAILED)."")" & @CRLF)
; ConsoleWrite("C:\Progra~2\AutoIt3\Include\CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3:8:Func Trace($txt)" & @CRLF)
; ConsoleWrite("ExtdLibs\ThirdParty.Assima.Rnd.Qa\BuildManager.au3:399:;		#include <ExtdCtxPrivMods\LegacyDevMods\ExtdLibs\UDFs\AutoItQuickTestv1.au3>" & @CRLF)

; + #include <UAFXA1\TEMPAPI160.IBMPLib\TEMPAPI160_IterBasedMemPerfQa_LIBRARY.au3>

;		If $actualNotif <> $expectedNotif Then
;			Trace("This isn't what we expected (ASSERTION FAILED).")
;			Trace("Exit " & $AU3_EXIT_ASSERT & "...")
;			Exit $AU3_EXIT_ASSERT
;		EndIf

; ThirdParty.Assima.Rnd.Qa\BuildManager.au3:407:;		QuickTest_ExpectVarVal($isNightly, True, "$isNightly")
; ThirdParty.Assima.Rnd.Qa\BuildManager.au3:412:;		QuickTest_ExpectVarVal($isNightly, False, "$isNightly")
; ThirdParty.Assima.Rnd.Qa\BuildManager.au3:417:;		QuickTest_ExpectVarVal($isNightly, True, "$isNightly")
; ThirdParty.Assima.Rnd.Qa\BuildManager.au3:422:;		QuickTest_ExpectVarVal($isNightly, False, "$isNightly")
; UDFs\AutoItQuickTestv1.au3:29:	Return QuickTest_ExpectVarVal($val, $expected)
; UDFs\AutoItQuickTestv1.au3:32:Func QuickTest_ExpectVarVal($val, $expected, $varname = "n/a")
; UDFs\AutoItQuickTestv1.au3:63:EndFunc   ;==>QuickTest_ExpectVarVal
; UDFs\AutoItQuickTestv1.au3:105:	QuickTest_ExpectVarVal(1, 1)
; UDFs\AutoItQuickTestv1.au3:106:	QuickTest_ExpectVarVal(1, 2)

; (+)
; Tests.Self\LooseUDFs.test\StringV2-test.au3:38:QuickTest_ExpectVarValWpr(MLRE_ExtractParagraphFromFullLineWpr($sample000, $linepa0), $barepa0)

Func _Xyz_Assert($cond, $text = "")
; 	If Not $cond Then
; 		; Trace("This isn't what we expected (ASSERTION FAILED).")
; 		; Trace("Exit " & $AU3_EXIT_ASSERT & "...")
; 		Exit $AU3_EXIT_ASSERT
; 	EndIf
	Return _Abc_Assert($cond, $text)
EndFunc
