#include-once
#include <CtxConf\StdQaLogInitSettings.au3>

$g_Au3_isLoggingEnabled = False

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Opt_EnableLogging
; Description ...: Calling this function will enable creation of the Automation.log file, as well as screenshots taking, etc.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......: Can't use #OnAutoItStartRegister => causes cryptic error with Date.au3 not knowing what $tagFILETIME is, or sth...
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Opt_EnableLogging($shouldEnableLogging = True)
	; PARAMETER NOT IMPLEMENTED -- CAN'T BE DISABLED ONCE IT WAS ENABLED
	$g_Au3_isLoggingEnabled = $shouldEnableLogging
	StdQaLog_Opt_BindLogServices___OnAutoItStartCallback()
EndFunc   ;==>_Au3_Opt_EnableLogging

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_IsLoggingEnabled
; Description ...: PRIVATE
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_IsLoggingEnabled()
	Return $g_Au3_isLoggingEnabled
EndFunc   ;==>_Au3_IsLoggingEnabled
