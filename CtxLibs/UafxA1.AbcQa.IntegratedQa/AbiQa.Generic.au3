#include-once
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa.AbiMod\AbiQaTestModule.au3"
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\DualProcess.au3"
#include ".\AbcQa.ExitCodes.au3"
#include ".\Au3Qa.IntegratedQa.Services.au3"

; Constants
Const $ABIQA_DEFAULT_WAIT_TIMEOUT = 0 ; wait forever
; Const $ABIQA_DEFAULT_WAIT_TIMEOUT = 90 ; 90 seconds = 1.5 minutes

; Global variables
Global $g_ABIQA_DefaultWaitTimeout = $ABIQA_DEFAULT_WAIT_TIMEOUT
Global $g_ABIQA_IsExitUponTimeout = False


; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Command
; Description ...: Sends a command to AppListener.exe. See the AppListener documentation for format info.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Command($text)
	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	AbiQaTestModule_SendAbiCommand($text)
EndFunc   ;==>_Abi_Command

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetAppListenerDir
; Description ...: eg. @UserProfileDir & "\Assima_Performance_Deployer"
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetAppListenerDir($localDirPath)
	Global $g_AQTM_AppListenerDir = $localDirPath
EndFunc   ;==>_Abi_Opt_SetAppListenerDir

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetAppListenerRes
; Description ...: Library > Folder > LocalisedResourceID. Can be extracted from the Vimago URL. Can also be the Project's ID
;                  if you run the script in Contributor-mode.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetAppListenerRes($serverFolderId)
	Global $g_AQTM_AppListenerRes = $serverFolderId
EndFunc   ;==>_Abi_Opt_SetAppListenerRes

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetAppListenerSrv
; Description ...: eg. "http://708.vimago.assima.net"
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetAppListenerSrv($serverUrl)
	Global $g_AQTM_AppListenerSrv = $serverUrl
	ValTrace($g_AQTM_AppListenerSrv, "$g_AQTM_AppListenerSrv")
EndFunc   ;==>_Abi_Opt_SetAppListenerSrv

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetAppListenerUsr
; Description ...: eg. "default@abi.qa", "defaultAbi"
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetAppListenerUsr($userName, $userPassword)
	Global $g_AQTM_AppListenerUsrName = $userName
	Global $g_AQTM_AppListenerUsrPass = $userPassword
EndFunc   ;==>_Abi_Opt_SetAppListenerUsr

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetDefaultWaitTimeout
; Description ...: Set the default timeout when _Abi_Wait functions are called. By default we would wait forever (and get
;                  interrupted only by the global script timeout).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetDefaultWaitTimeout($defaultTimeout)
	Global $g_ABIQA_DefaultWaitTimeout = $defaultTimeout
EndFunc   ;==>_Abi_Opt_SetDefaultWaitTimeout

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_Opt_SetExitUponTimeout
; Description ...: NOT IMPLEMENTED
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_Opt_SetExitUponTimeout($isExitUponTimeout = True)
	Global $g_ABIQA_IsExitUponTimeout = $isExitUponTimeout
EndFunc   ;==>_Abi_Opt_SetExitUponTimeout

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_TestModule_Start
; Description ...: This will launch AppListener according to options, start logging and timeout monitoring.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_TestModule_Start()
	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	FuncTrace("_Abi_TestModule_Start")
	Local $auditFileOut = ""
	If _Au3_IsLoggingEnabled() Then
		DirCreate($AUTOMATION_THIRDPARTY_DIRECTORY)
		$auditFileOut = $AUTOMATION_THIRDPARTY_DIRECTORY & "\AppListenerLog.txt"
	EndIf

	;; SNIPPET REF. 150925.2252 ---
	;; WARNING : THIS PIECE OF CODE WILL SPECIFICALLY WORK WITH 'upgrade.default'
	;; - TestModulesLauncher\BareTestModulesLauncher.bat
	;; - TestModulesLauncher\BareTestModulesLauncher.bat.au3
	;; ---
	;; REMARK:
	;; We also have the choice of the file to probe, some files are specific to some versions,
	;; for instance C:\Inetpub\wwwroot\upgrade.default\Services\Windows.Service\Assima.Hosts.Windows.exe
	;;  is available in 709+ only
	;; whereas C:\Progra~2\Assima\Assima Vimago - Instance 'upgrade.default'\Assima.Setup.exe
	;;  is available in 708, 709+
	;; ---
	;; REMARK:
	;; The file version that we get out of the files that are installed have to be filtered/converted
	;;  in order to match for format used by the build directory names
	;; runtimeVersion_CLEAN = 7.00.00.7.08.21.2589-20150925_200001
	;; runtimeVersion_CLEAN = 7.08.21.2589-20150925_200001
	;; runtimeVersion_CLEAN = 7.08.21-20150925_200001
	;;
	;; runtimeVersion_CLEAN = 7.00.00.7.99.0.2933-20150925_200001
	;; runtimeVersion_CLEAN = 7.99.0.2933-20150925_200001
	;;
	;; ---
	Const $APPLICATION_PATH = @UserProfileDir & "\Assima_Performance_Deployer\AppListener.exe"
	Const $WEBINSTANCE_PATH = "C:\Progra~2\Assima\Assima Vimago - Instance 'upgrade.default'\Assima.Setup.exe"
	Const $FILE_META_PROP = "ProductVersion"

	$applicationVersion_BARE = FileGetVersion($APPLICATION_PATH, $FILE_META_PROP)
	$webinstanceVersion_BARE = FileGetVersion($WEBINSTANCE_PATH, $FILE_META_PROP)

	ValTrace($webinstanceVersion_BARE, "(LOCAL!) (USING?) webinstanceVersion_BARE" & "@" & @ComputerName)
	ValTrace($applicationVersion_BARE, "(USING!) (USING!) applicationVersion_BARE" & "@" & @ComputerName)

	Return AbiQaTestModule_Start($auditFileOut)
EndFunc   ;==>_Abi_TestModule_Start

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_TestModule_Terminate
; Description ...: Invoking this function is equivalent to saying that the test has passed. This will close AppListener
;                  and stop logging.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_TestModule_Terminate()
	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	FuncTrace("_Abi_TestModule_Terminate")
	_Abi_FlushAllAcks()
	Return AbiQaTestModule_Terminate()
EndFunc   ;==>_Abi_TestModule_Terminate

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_WaitNextAck
; Description ...: Expect an immediate and exact acknowledgement from AppListener.exe. The expected ACK has to be the next one
;                  without any other in the meantime. If a different ACK is received, the script will exit with a non-nul return
;                  value (ie. FAILED). ($maxWaitTime in seconds.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_WaitNextAck($expectedNotif, $maxWaitTime = Default, $isExitUponTimeout = Default, $isMultiSearch = False)

	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	Global $g_ABIQA_AreAbiWaitFunctionsActive
	If Not $g_ABIQA_AreAbiWaitFunctionsActive Then Return

	$isMultiSearchSeparator = ";"
	Local $expectedNotifAsStr = $expectedNotif
	Dim $expectedNotifAsArray[1] = [$expectedNotif]
	If IsArray($expectedNotif) Then $expectedNotifAsStr = _ArrayToString($expectedNotif, $isMultiSearchSeparator)
	FuncTrace("_Abi_WaitNextAck", $expectedNotifAsStr, $maxWaitTime)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_EXPECT_PREFIX__ & $expectedNotifAsStr, $MAXIMAL_VERBOSITY_REQUIRED)
	If IsArray($expectedNotif) Then $expectedNotifAsArray = $expectedNotif
	; If Not IsArray($expectedNotif) Then NOP
	If (Not IsArray($expectedNotif)) And $isMultiSearch Then $expectedNotifAsArray = StringSplit($expectedNotif, $isMultiSearchSeparator)


	Global $g_ABIQA_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_ABIQA_DefaultWaitTimeout

	Global $g_ABIQA_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_ABIQA_IsExitUponTimeout

	Local $timerObject = FuncTimerCreate($maxWaitTime)
	Local $actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
	Local $errorCode = @error
	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	; check for timeout
	If $errorCode Then
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	; check for assertion
	If UBound($expectedNotifAsArray) = 1 Then
		If $actualNotif <> $expectedNotif Then
			Trace("This isn't what we expected (ASSERTION FAILED).")
			Trace("Exit " & $AU3_EXIT_ASSERT & "...")
			Exit $AU3_EXIT_ASSERT
		EndIf
	Else
		; process as unordered list
		While UBound($expectedNotifAsArray) >= 1
			$asi = _ArraySearch($expectedNotifAsArray, $actualNotif)
			If $asi <> -1 Then
				Trace("    - matched an expected item in the unordered list.")
				_ArrayDelete($expectedNotifAsArray, $asi)
			Else
				Trace("This isn't what we expected (ASSERTION FAILED).")
				Trace("Exit " & $AU3_EXIT_ASSERT & "...")
				Exit $AU3_EXIT_ASSERT
			EndIf
			; retrieve a new notification
			$actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
			$errorCode = @error
			_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
			; check for timeout
			If $errorCode Then
				If $isExitUponTimeout Then
					Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
				Else
					_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
					Return False
				EndIf
			EndIf
		WEnd
	EndIf

	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX & $__MOVIE_SCREENSHOT_RETURN_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return True

EndFunc   ;==>_Abi_WaitNextAck

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_WaitUntilAck
; Description ...: Expect an immediate, regex-matched, acknowledgement from AppListener.exe. The expected ACK has to be the next
;                  one without any other in the meantime. If a different ACK is received, the script will exit with a non-nul
;                  return value (ie. FAILED). ($maxWaitTime in seconds.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_WaitUntilAck($expectedNotif, $maxWaitTime = Default, $isExitUponTimeout = Default)

	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	Global $g_ABIQA_AreAbiWaitFunctionsActive
	If Not $g_ABIQA_AreAbiWaitFunctionsActive Then Return
	FuncTrace("_Abi_WaitUntilAck", $expectedNotif, $maxWaitTime)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_EXPECT_PREFIX__ & $expectedNotif, $MAXIMAL_VERBOSITY_REQUIRED)

	Global $g_ABIQA_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_ABIQA_DefaultWaitTimeout

	Global $g_ABIQA_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_ABIQA_IsExitUponTimeout

	Local $timerObject = FuncTimerCreate($maxWaitTime)
	Local $actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
	Local $errorCode = @error
	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	; keep retrieving
	While ($actualNotif <> $expectedNotif) And (Not $errorCode)
		$actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
		$errorCode = @error
		_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	WEnd
	; check for timeout
	If $errorCode Then
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	; check for assertion
	If $actualNotif <> $expectedNotif Then
		Trace("This isn't what we expected (ASSERTION FAILED).")
		Trace("Exit " & $AU3_EXIT_ASSERT & "...")
		Exit $AU3_EXIT_ASSERT
	EndIf

	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX & $__MOVIE_SCREENSHOT_RETURN_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return True

EndFunc   ;==>_Abi_WaitUntilAck

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_WaitNextRegexAck
; Description ...: Wait for an acknowledgement from AppListener.exe. All notifications that do no match our wait criteria are
;                  ignored and discarded until the right one is received. ($maxWaitTime in seconds.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_WaitNextRegexAck($expectedNotif, $maxWaitTime = Default, $isExitUponTimeout = Default, $isMultiSearch = False)

	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	Global $g_ABIQA_AreAbiWaitFunctionsActive
	If Not $g_ABIQA_AreAbiWaitFunctionsActive Then Return

	$isMultiSearchSeparator = ";"
	Local $expectedNotifAsStr = $expectedNotif
	Dim $expectedNotifAsArray[1] = [$expectedNotif]
	If IsArray($expectedNotif) Then $expectedNotifAsStr = _ArrayToString($expectedNotif, ";")
	FuncTrace("_Abi_WaitNextRegexAck", $expectedNotifAsStr, $maxWaitTime)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_EXPECT_PREFIX__ & $expectedNotifAsStr, $MAXIMAL_VERBOSITY_REQUIRED)
	If IsArray($expectedNotif) Then $expectedNotifAsArray = $expectedNotif
	; If Not IsArray($expectedNotif) Then NOP
	If (Not IsArray($expectedNotif)) And $isMultiSearch Then $expectedNotifAsArray = StringSplit($expectedNotif, $isMultiSearchSeparator)


	Global $g_ABIQA_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_ABIQA_DefaultWaitTimeout

	Global $g_ABIQA_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_ABIQA_IsExitUponTimeout

	Local $timerObject = FuncTimerCreate($maxWaitTime)
	Local $actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
	Local $errorCode = @error
	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	; check for timeout
	If $errorCode Then
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	; check for assertion
	If UBound($expectedNotifAsArray) = 1 Then
		If Not StringRegExp($actualNotif, $expectedNotif) Then
			Trace("This isn't what we expected (ASSERTION FAILED).")
			Trace("Exit " & $AU3_EXIT_ASSERT & "...")
			Exit $AU3_EXIT_ASSERT
		EndIf
	Else
		; process as unordered list
		While UBound($expectedNotifAsArray) >= 1
			$asi = _ArrayRESearch($expectedNotifAsArray, $actualNotif)
			If $asi <> -1 Then
				Trace("    - matched an expected item in the unordered list.")
				_ArrayDelete($expectedNotifAsArray, $asi)
			Else
				Trace("This isn't what we expected (ASSERTION FAILED).")
				Trace("Exit " & $AU3_EXIT_ASSERT & "...")
				Exit $AU3_EXIT_ASSERT
			EndIf
			; retrieve a new notification
			$actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
			$errorCode = @error
			_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
			; check for timeout
			If $errorCode Then
				If $isExitUponTimeout Then
					Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
				Else
					_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
					Return False
				EndIf
			EndIf
		WEnd
	EndIf

	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX & $__MOVIE_SCREENSHOT_RETURN_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return True

EndFunc   ;==>_Abi_WaitNextRegexAck

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_WaitUntilRegexAck
; Description ...: Wait for an acknowledgement from AppListener.exe. All notifications that do no match our wait criteria are
;                  ignored and discarded until the right one is received. ($maxWaitTime in seconds.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_WaitUntilRegexAck($expectedNotif, $maxWaitTime = Default, $isExitUponTimeout = Default)

	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	Global $g_ABIQA_AreAbiWaitFunctionsActive
	If Not $g_ABIQA_AreAbiWaitFunctionsActive Then Return
	FuncTrace("_Abi_WaitUntilRegexAck", $expectedNotif, $maxWaitTime)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_EXPECT_PREFIX__ & $expectedNotif, $MAXIMAL_VERBOSITY_REQUIRED)

	Global $g_ABIQA_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_ABIQA_DefaultWaitTimeout

	Global $g_ABIQA_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_ABIQA_IsExitUponTimeout

	Local $timerObject = FuncTimerCreate($maxWaitTime)
	Local $actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
	Local $errorCode = @error
	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	; keep retrieving
	While Not StringRegExp($actualNotif, $expectedNotif) And (Not $errorCode)
		$actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject)
		$errorCode = @error
		_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	WEnd
	; check for timeout
	If $errorCode Then
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	; check for assertion
	If Not StringRegExp($actualNotif, $expectedNotif) Then
		Trace("This isn't what we expected (ASSERTION FAILED).")
		Trace("Exit " & $AU3_EXIT_ASSERT & "...")
		Exit $AU3_EXIT_ASSERT
	EndIf

	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX & $__MOVIE_SCREENSHOT_RETURN_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return True

EndFunc   ;==>_Abi_WaitUntilRegexAck

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_FlushAllAcks
; Description ...: Flush all the remaining notifications in the pipe.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_FlushAllAcks($maxWaitTime = 1)

	$ret = False
	$isFlushModeTRUE = True

	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	FuncTrace("_Abi_FlushAllAcks", $maxWaitTime)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)

	Local $timerObject = FuncTimerCreate($maxWaitTime)
	Local $actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject, $isFlushModeTRUE)
	Local $errorCode = @error
	_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_FLUSH_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	$ret = Not $errorCode
	While (Not $errorCode)
		$actualNotif = AbiQaTestModule_TimedReceiveAbiInfo($timerObject, $isFlushModeTRUE)
		$errorCode = @error
		_Au3_INTSRVCS_MovieScreenshotNow($MOVIE_SCREENSHOT_FLUSH_PREFIX__ & $actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	WEnd

	_Au3_INTSRVCS_MovieScreenshotNow($actualNotif & $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX & $__MOVIE_SCREENSHOT_RETURN_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return $ret

EndFunc   ;==>_Abi_FlushAllAcks

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_HACK_FlushAcksAfterEveryAction
; Description ...: Flush all the remaining notifications in the pipe after each Click, Send, DblClick, etc. Useful when writing new script.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_HACK_FlushAcksAfterEveryAction($timeoutValue = 5)
	Global $g_ABIQA_AppListenerEnabled
	If Not $g_ABIQA_AppListenerEnabled Then Return
	Global $g_ABIQA_IsFlushAfterEveryAction
	Global $g_ABIQA_AreAbiWaitFunctionsActive
	$oldSetting = $g_ABIQA_IsFlushAfterEveryAction
	$g_ABIQA_IsFlushAfterEveryAction = $timeoutValue
	$g_ABIQA_AreAbiWaitFunctionsActive = Not $g_ABIQA_IsFlushAfterEveryAction
	Return $oldSetting
EndFunc   ;==>_Abi_HACK_FlushAcksAfterEveryAction
