#include-once
#include ".\Au3Qa.Generic.au3"
#include ".\Au3Qa.IntegratedQa.Automation.au3"

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_EnterTransact
; Description ...: Enter the $TCode in the combo and validate.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_EnterTransact($tCode)
	_Au3_WinActivateOnly("SAP Easy Access")
	_Au3_Scenario("Type '" & $tCode & "' in the transaction code field...")
	_Au3_Send($tCode)
	_Sap_PressEnter()
EndFunc   ;==>_Sap_EnterTransact

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_PressEnter
; Description ...: Press the ENTER key.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_PressEnter()
	_Au3_Scenario("Click the Enter button...")
	_Au3_Send("{ENTER}")
EndFunc   ;==>_Sap_PressEnter

; #FUNCTION# ====================================================================================================================
; Name...........: _Sap_HACK_DisableSapLogonLaunch
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Sap_HACK_DisableSapLogonLaunch($val = 1)
	$g_SAP_SapLogonLaunchDisabled = $val
EndFunc   ;==>_Sap_HACK_DisableSapLogonLaunch
