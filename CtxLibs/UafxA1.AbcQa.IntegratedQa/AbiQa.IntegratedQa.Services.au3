#include-once
#include ".\AbiQa.Generic.au3"

$g_ABIQA_IsFlushAfterEveryAction = False
$g_ABIQA_AreAbiWaitFunctionsActive = True

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_INTSRVCS_FlushAfterEveryAction
; Description ...: PRIVATE - NOT SUPPORTED
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_INTSRVCS_FlushAfterEveryAction()
	If Not $g_ABIQA_IsFlushAfterEveryAction Then Return
	ConsoleWrite("_Abi_INTSRVCS_FlushAfterEveryAction()..." & @CRLF)
	_Abi_FlushAllAcks($g_ABIQA_IsFlushAfterEveryAction)
EndFunc   ;==>_Abi_INTSRVCS_FlushAfterEveryAction
