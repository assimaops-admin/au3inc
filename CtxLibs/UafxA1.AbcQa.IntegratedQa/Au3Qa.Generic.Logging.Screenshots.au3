#include-once
#include "..\UafxA1.AbcQa.StdQaLogLib\ScrShots.au3"
#include ".\Au3Qa.Generic.Logging.au3"


; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_ScreenshotWindow
; Description ...: Take a screenshot of the current active window. By default the mouse cursor is moved temporarily out of the way to (0,0).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_ScreenshotWindow($screenshotNickname, $window = Default, $fCursor = True)
	If Not _Au3_IsLoggingEnabled() Then Return
	$funcVersion = 1
	Return ScrShots_WndTestShotVx($funcVersion, $screenshotNickname, $window, $fCursor)
EndFunc   ;==>_Au3_ScreenshotWindow

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_ScreenshotLayeredWindow
; Description ...: Take a screenshot of the current active window. By default the mouse cursor is moved temporarily out of the way to (0,0). With enabled support for LAYERED windows.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_ScreenshotLayeredWindow($screenshotNickname, $window = Default, $fCursor = True)
	If Not _Au3_IsLoggingEnabled() Then Return
	$funcVersion = 2
	Return ScrShots_WndTestShotVx($funcVersion, $screenshotNickname, $window, $fCursor)
EndFunc   ;==>_Au3_ScreenshotLayeredWindow

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_ScreenshotArea
; Description ...: Take a screenshot of a given area of the screen.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_ScreenshotArea($screenshotNickname, $right = $STANDARD_WIDTH_V2, $bottom = $STANDARD_HEIGHT_V2, $left = 0, $top = 0)
	If Not _Au3_IsLoggingEnabled() Then Return
	Return ScrShots_AreaTestShotV2($screenshotNickname, $right, $bottom, $left, $top)
EndFunc   ;==>_Au3_ScreenshotArea
