#include-once
#include <CoreUAFLibs\StdCoreLib\AutomationShorthand.au3>
#include ".\AbiQa.IntegratedQa.Services.au3"
#include ".\Au3Qa.IntegratedQa.Services.au3"

; Constants
Const $AU3TM_DEFAULT_WAIT_TIMEOUT = 0 ; wait forever
; Const $AU3TM_DEFAULT_WAIT_TIMEOUT = 90 ; 1.5 minutes

; Globals
Global $g_AU3TM_DefaultWaitTimeout = $AU3TM_DEFAULT_WAIT_TIMEOUT
Global $g_AU3TM_IsExitUponTimeout = False

; Private
Global $g_AU3TM_LastCloseWin = ""

; TODO:
;  - _Au3_DoubleClick(50, 318)
;  - _Au3_WinMoveToOriginCoord

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Click
; Description ...: Click at the chosen coordinates. (WARNING: coords. can be relative to window or absolute ; see AutoItSetOption function, MouseCoordMode setting.) (This is a standard wrapper for the AutoIt3 'MouseClick(left, ...)' function.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Click($x, $y)
	$MINIMAL_VERBOSITY_REQUIRED = 2
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotTitle("CLICK" & " " & $x & " " & $y & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Click($x, $y, "", "_Au3_INTSRVCS_MovieScreenshotCallback")
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_Click

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_DblClick
; Description ...: Perform a double-click at the chosen coordinates. (WARNING: coords. can be relative to window or absolute ; see AutoItSetOption function, MouseCoordMode setting.) (This is a standard wrapper for the AutoIt3 'MouseClick(left, ...)' function.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_DblClick($x, $y)
	$MINIMAL_VERBOSITY_REQUIRED = 2
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotTitle("DBLCLICK" & " " & $x & " " & $y & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	DblClick($x, $y, "", "_Au3_INTSRVCS_MovieScreenshotCallback")
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_DblClick

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_RightClick
; Description ...: Perform a double-click at the chosen coordinates. (WARNING: coords. can be relative to window or absolute ; see AutoItSetOption function, MouseCoordMode setting.) (This is a standard wrapper for the AutoIt3 'MouseClick(left, ...)' function.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_RightClick($x, $y)
	$MINIMAL_VERBOSITY_REQUIRED = 2
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotTitle("RIGHTCLICK" & " " & $x & " " & $y & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	RightClick($x, $y, "", "_Au3_INTSRVCS_MovieScreenshotCallback")
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_RightClick

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_DragDrop
; Description ...: Perform a double-click at the chosen coordinates. (WARNING: coords. can be relative to window or absolute ; see AutoItSetOption function, MouseCoordMode setting.) (This is a standard wrapper for the AutoIt3 'MouseClick(left, ...)' function.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_DragDrop($x, $y, $x2, $y2)
	$MINIMAL_VERBOSITY_REQUIRED = 2
	$MAXIMAL_VERBOSITY_REQUIRED = 3
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotTitle("DRAGDROP" & " " & $x & " " & $y & " " & $x2 & " " & $y2 & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Drag($x, $y, $x2, $y2, "", "_Au3_INTSRVCS_MovieScreenshotCallback")
	_Au3_INTSRVCS_MovieScreenshotNow("DRAGDROP END" & " " & $x & " " & $y & " " & $x2 & " " & $y2 & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_DragDrop

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Send
; Description ...: Simulate keyboard typing. (This is a standard wrapper for the AutoIt3 'Send' function.)
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Send($text)
	$MINIMAL_VERBOSITY_REQUIRED = 2
	$MAXIMAL_VERBOSITY_REQUIRED = 3
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("KEYBOARD" & " " & $text & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	SendV2($text)
	_Au3_INTSRVCS_MovieScreenshotNow("KEYBOARD END" & " " & $text & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_Send

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_HACK_PauseAfterEveryAction
; Description ...: Utility for crafting scripts.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_HACK_PauseAfterEveryAction($hotkey = "^+{Pause}")
	$g_AU3_PauseAfterEveryActionHotkey = $hotkey
EndFunc   ;==>_Au3_HACK_PauseAfterEveryAction

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Opt_EnableMovieScreenshots
; Description ...: Utility for crafting scripts.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Opt_EnableMovieScreenshots($verbosityLevel = 1)
	$g_AU3_EnableMovieScreenshots = $verbosityLevel
EndFunc   ;==>_Au3_Opt_EnableMovieScreenshots

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Opt_SetDefaultWaitTimeout
; Description ...: NOT IMPLEMENTED
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Opt_SetDefaultWaitTimeout($maxTime)
	$g_AU3TM_DefaultWaitTimeout = $maxTime
EndFunc   ;==>_Au3_Opt_SetDefaultWaitTimeout

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Opt_SetExitUponTimeout
; Description ...: NOT IMPLEMENTED
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Opt_SetExitUponTimeout($isExitUponTimeout = True)
	; OPTION NOT IMPLEMENTED
	;   -- BY DEFAULT WE EXIT UPON TIMEOUT
	;   -- THE EXIT CODE FOR INDIVIDUAL TIMEOUT IS $AU3_EXIT_LOCAL_TIMEOUT
EndFunc   ;==>_Au3_Opt_SetExitUponTimeout

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WaitNextWin
; Description ...: Expect a window to appear ( or exist ), activate it (bring it to front) and move it to standard (0,0) position.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WaitNextWin($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
	Return _Au3_PRIVATE_WaitNextWinSub("_Au3_WaitNextWin", $title, $text, $maxWaitTime, $isExitUponTimeout)
EndFunc   ;==>_Au3_WaitNextWin

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WaitNextWinEx
; Description ...: Expect a window to appear ( or exist ), activate it (bring it to front) and move it to standard position and size (0,0,1024,738).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WaitNextWinEx($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
	Return _Au3_PRIVATE_WaitNextWinSub("_Au3_WaitNextWinEx", $title, $text, $maxWaitTime, $isExitUponTimeout)
EndFunc   ;==>_Au3_WaitNextWinEx

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WaitLastClose
; Description ...: Expect a window to appear ( or exist ), activate it (bring it to front) and move it to standard position and size (0,0,1024,738).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WaitLastClose($maxWaitTime = Default, $isExitUponTimeout = Default)
	FuncTrace("_Au3_WaitLastClose", $maxWaitTime, $isExitUponTimeout)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("WAITLASTCLOSE" & " " & $g_AU3TM_LastCloseWin & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)

	If $g_AU3TM_LastCloseWin = "" Then Return True

	Global $g_AU3TM_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_AU3TM_DefaultWaitTimeout

	Global $g_AU3TM_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_AU3TM_IsExitUponTimeout

	If Not WinWaitClose($maxWaitTime, $maxWaitTime) Then
		; Timeout occured
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow("WAITLASTCLOSE TIMEOUT" & " " & $g_AU3TM_LastCloseWin & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	_Au3_INTSRVCS_MovieScreenshotNow("WAITLASTCLOSE END" & " " & $g_AU3TM_LastCloseWin & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	Return True
EndFunc   ;==>_Au3_WaitLastClose

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_PRIVATE_WaitNextWinSub -  PRIVATE
; Description ...: Expect a window to exist ( wait for it ), activate it and move to standard position and size.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_PRIVATE_WaitNextWinSub($funcName, $title, $text, $maxWaitTime, $isExitUponTimeout)
	FuncTrace($funcName, $title, $text, $maxWaitTime, $isExitUponTimeout)
	$FUNCPRINT = StringUpper(StringTrimLeft($funcName, StringLen("_Au3_")))
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow($FUNCPRINT & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)

	Global $g_AU3TM_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_AU3TM_DefaultWaitTimeout

	; SPECIAL - FEB 3, 2016
	Trace("                                                                                   $maxWaitTime = " & $maxWaitTime)


	$timer = TimerInit()

	; SPECIAL - FEB 3, 2016
	Trace("                                                                            _Au3_PRIVATE_WaitNextWinSub.WinWait")

	$hwndFound = WinWait($title, $text, $maxWaitTime)

	; SPECIAL - FEB 3, 2016
	Trace("                                                                                 $hwndFound = " & $hwndFound)
	Trace("                                                                     WinGetTitle($hwndFound) = " & WinGetTitle($hwndFound))

	If Not $hwndFound Then
		; Timeout occured
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow($FUNCPRINT & " " & "TIMEOUT" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	Else
		$g_AU3TM_LastCloseWin = $hwndFound
		$maxRemainingTime = $maxWaitTime
		If IsInt($maxRemainingTime) Then
			$maxRemainingTime = TimerDiff($timer) / 1000
		EndIf
		WinActivate($hwndFound)

		; SPECIAL - FEB 3, 2016
		Trace("                                                                                   $maxRemainingTime = " & $maxRemainingTime)


		; SPECIAL - FEB 3, 2016
		Trace("                                                                            _Au3_PRIVATE_WaitNextWinSub.WinWaitActive")

		$isActive = WinWaitActive($hwndFound, $maxRemainingTime)

		; SPECIAL - FEB 3, 2016
		Trace("                                                                                         $isActive = " & $isActive)

		If Not $isActive Then
			; Timeout occured
			If $isExitUponTimeout Then
				Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
			Else
				_Au3_INTSRVCS_MovieScreenshotNow($FUNCPRINT & " " & "TIMEOUT" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
				Return False
			EndIf
		Else

			; SPECIAL - FEB 3, 2016
			Trace("                                                                            _Au3_PRIVATE_WaitNextWinSub.WinSetPos")

			; Finally
			Sleep(250)
			If $funcName = "_Au3_WaitNextWin" Then
				WinSetPosOrigin($hwndFound)
			Else ; If $funcName = "_Au3_WaitNextWinEx" Then
				WinSetPosStandard($hwndFound)
			EndIf
			Sleep(250)
		EndIf
	EndIf

	; SPECIAL - FEB 3, 2016
	Trace("                                                                            _Au3_PRIVATE_WaitNextWinSub.EndFunc")

	_Au3_INTSRVCS_MovieScreenshotNow($FUNCPRINT & " " & "END" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
	Return True
EndFunc   ;==>_Au3_PRIVATE_WaitNextWinSub

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WinActivateOnly
; Description ...: Activate an existing window (bring it to front).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WinActivateOnly($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
	FuncTrace("_Au3_WinActivateOnly", $title, $text)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("WINACTIVATEONLY" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)

	Global $g_AU3TM_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_AU3TM_DefaultWaitTimeout

	Global $g_AU3TM_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_AU3TM_IsExitUponTimeout

	WinActivate($title, $text)
	$isActive = WinWaitActive($title, $text, $maxWaitTime)
	If Not $isActive Then
		; Timeout occured
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow("WINACTIVATEONLY TIMEOUT" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	_Au3_INTSRVCS_MovieScreenshotNow("WINACTIVATEONLY END" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
	Return True
EndFunc   ;==>_Au3_WinActivateOnly

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WinSetPosStandard
; Description ...: Move the current window to a standard position and size (0,0,1024,738).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......: By default the standard maximized position is 768 x 1024
;                  but we reserve 30 pixels on the Y axis for the taskbar.
;                  + remove 'maximized' attribute (if any)
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WinSetPosStandard($title = "", $text = "")
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("WINSETPOSSTANDARD" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	WinSetPosStandard($title, $text)
	_Au3_INTSRVCS_MovieScreenshotNow("WINSETPOSSTANDARD END" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_WinSetPosStandard

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WinSetPosOrigin
; Description ...: Move the window to the top left corner (0,0).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WinSetPosOrigin($title = "", $text = "")
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("WINSETPOSORIGIN" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)
	WinSetPosOrigin($title, $text)
	_Au3_INTSRVCS_MovieScreenshotNow("WINSETPOSORIGIN END" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
EndFunc   ;==>_Au3_WinSetPosOrigin

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_WaitNextWinSub
; Description ...: Expect a window to appear ( or exist ). Do not take any further action (do not activate, do not move).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_WinWaitOnly($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
	; DEPRECATED - KEPT FOR BACKWARD COMPATIBILITY
	; USE _Au3_WaitNextWinSub INSTEAD
	Return _Au3_WaitNextWinSub($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
EndFunc   ;==>_Au3_WinWaitOnly
Func _Au3_WaitNextWinSub($title, $text = "", $maxWaitTime = Default, $isExitUponTimeout = Default)
	FuncTrace("_Au3_WaitNextWinSub", $title, $text)
	$MINIMAL_VERBOSITY_REQUIRED = 1
	$MAXIMAL_VERBOSITY_REQUIRED = 9
	_Au3_INTSRVCS_MovieScreenshotInc($MINIMAL_VERBOSITY_REQUIRED)
	_Au3_INTSRVCS_MovieScreenshotNow("WAITNEXTWINSUB" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MAXIMAL_VERBOSITY_REQUIRED)

	Global $g_AU3TM_DefaultWaitTimeout
	If $maxWaitTime = Default Then $maxWaitTime = $g_AU3TM_DefaultWaitTimeout

	Global $g_AU3TM_IsExitUponTimeout
	If $isExitUponTimeout = Default Then $isExitUponTimeout = $g_AU3TM_IsExitUponTimeout

	$hwndFound = WinWait($title, $text, $maxWaitTime)
	If Not $hwndFound Then
		; Timeout occured
		If $isExitUponTimeout Then
			Exit DualProcess_FuncLevelSelfTimeoutExitCode($AU3_EXIT_LOCAL_TIMEOUT) ; NO MORE LOGGING PASSED THIS POINT!!
		Else
			_Au3_INTSRVCS_MovieScreenshotNow("WAITNEXTWINSUB TIMEOUT" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
			Return False
		EndIf
	EndIf
	_Au3_INTSRVCS_MovieScreenshotNow("WAITNEXTWINSUB END" & " " & $title & $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX, $MINIMAL_VERBOSITY_REQUIRED)
	_Abi_INTSRVCS_FlushAfterEveryAction()
	_Au3_INTSRVCS_PauseAfterEveryAction()
	Return True
EndFunc   ;==>_Au3_WaitNextWinSub
