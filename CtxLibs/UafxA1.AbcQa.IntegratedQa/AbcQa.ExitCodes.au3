#include-once

; =================
; RESERVED SECTIONS
; =================

; RESERVED : 1-799
; SUPERUSER : 800

; OPERATOR : 801-849

; SUPERUSER : 849-899
; RESERVED : 900-*


; =================
; FRAMEWORK SECTION
; =================

$AU3_EXIT_SUCCESS = 0 ; Automation-wise, reaching 'Exit 0' (or the equivalent)
; is considered as a success. When the process is a 'test', we say that the test
; is negative. (ie. a successful process termination => a negative test.)

; $AU3_EXIT_INTERRUPT_REASON
$AU3_EXIT_INTERRUPT_CLICK = 222 ; close by clicking on exit of the systray.
$AU3_EXIT_INTERRUPT_LOGOFF = 333 ; close by user logoff.
$AU3_EXIT_INTERRUPT_SHUTDOWN = 444 ; close by Windows shutdown.

; CODED INTERRUPTION
$AU3_EXIT_SCRIPT_GLOBAL_TIMEOUT = 555 ; close by dual process (soft kill)

; $AU3_EXIT_ABCQA_REASON
$AU3_EXIT_ASSERT = 666
$AU3_EXIT_LOCAL_TIMEOUT = 777

; ================
; OPERATOR SECTION
; ================

; -----------
;    800
; -----------
; RESERVED FOR IMPLEMETING << CUSTOM >> POSITIVE TESTS

; -----------
;   801-809
; -----------
; RESERVED FOR IMPLEMENTING << CUSTOM >> CALLER AND HOST CONTRACTS CHECKING
; (eg. check cmdline params, check caller contract,
;   check if resources exists, etc.)
; - eg. 801 - bad parameters
; - eg. 802 - bad value/type
; - eg. 803 - data not found
; - ...
; - eg. 806 - bad constraint
; - ...

; -----------
;   810-849
; -----------
; RESERVED FOR IMPLEMENTING ADDITIONAL << CUSTOM >> USER ERRORS
; (eg. ?)

; -----------
;   851-899
; -----------
; RESERVED FOR IMPLEMENTING META-LAYER(S) << CUSTOM >> USER ERRORS
; (eg. custom launcher, custom sub-process, etc.)

; =================
; SUPERUSER SECTION
; =================

; RESERVED FOR IMPLEMENTING INCORPORATED LAYER CALLER AND HOST CONTRACT CHECKING
; (eg. check cmdline params, check caller contract,
;   check if resources exists, etc.)
; 901-909
; - eg. 901 - bad parameters
; - eg. 902 - bad value/type
; - eg. 903 - data not found
; - ...
; - eg. 906 - bad constraint
; - ...

; RESERVED FOR IMPLEMENTING ADDITIONAL INCORPORATED LAYER USER ERRORS
; 910-949

; =================
; FRAMEWORK SECTION
; =================

; UNEXPECTED AND UNEXPLAINED SYSTEM EXCEPTIONS
; (eg. network disruption, out-of-memory, etc.)
$AU3_EXIT_UNKNOWN = 999
