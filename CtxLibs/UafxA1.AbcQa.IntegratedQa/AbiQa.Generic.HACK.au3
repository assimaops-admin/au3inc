#include-once

Global $g_ABIQA_AppListenerEnabled = True

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_HACK_DisableAllAppListener
; Description ...: FOR SCRIPT WRITING ONLY
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_HACK_DisableAllAppListener($isDisabled = True)
	$g_ABIQA_AppListenerEnabled = Not $isDisabled
EndFunc   ;==>_Abi_HACK_DisableAllAppListener
