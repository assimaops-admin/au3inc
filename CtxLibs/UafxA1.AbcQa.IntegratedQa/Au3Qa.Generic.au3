#include-once
#include <CoreUAFLibs\StdCoreLib\AutomationShorthand.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.au3>
#include <CoreUAFLibs\StdCoreLib\WinV2.au3>

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Sleep
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Sleep($duration) ; in milliseconds.
	SleepV2($duration)
EndFunc   ;==>_Au3_Sleep

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Trace
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Trace($traceTxt)
	Trace($traceTxt)
EndFunc   ;==>_Au3_Trace

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Scenario
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Scenario($text)
	Scenario($text)
EndFunc   ;==>_Au3_Scenario
