#NoIncTidy
#include-once
#include <CorpUAFLibs\AutomLib.SAP\SAPGeneric.au3> ; _Sap_RunSapLogon, _Sap_LoginUser, etc.
#include ".\SapQa.IntegratedQa.au3" ; _Sap_EnterTransact, _Sap_PressEnter, etc.
; + # include <ExtdCtxPrivMods\LegacyDevMods\ExtdLibs\ThirdPartyEx\SAPCommon.au3> ; (...)
; + # include <ExtdCtxPrivMods\LegacyDevMods\ExtdLibs\UDFsx\SAPSession.au3> ; (...)
