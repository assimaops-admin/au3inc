#NoIncTidy
#include-once
#include ".\Au3Qa.IntegratedQa.Assert.au3" ; <<< _Au3_Assert
#include ".\Au3Qa.Generic.au3" ; <<< _Au3_Sleep, _Au3_Trace, _Au3_WinSetPosStandard
#include ".\Au3Qa.Generic.Logging.au3" ; <<< _Au3_Opt_EnableLogging
#include ".\Au3Qa.Generic.Logging.Screenshots.au3" ; <<< _Au3_ScreenshotArea, _Au3_ScreenshotWindow
#include ".\Au3Qa.Generic.TestModule.au3" ; <<< _Au3_TestModule_Start, _Au3_TestModule_Terminate, _Au3_Scenario
#include ".\Au3Qa.IntegratedQa.Automation.au3" ; <<< _Au3_Send, _Au3_Click
