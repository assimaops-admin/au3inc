#include-once
#include <CoreUAFLibs\StdCoreLib\SessionId.au3>
#include <CorpUAFLibs\StdCorpLib\TestTarget.au3>
#include <CorpUAFLibs\StdCorpLib\TestThirdPartyTarget.au3>
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\AutomationData.au3"
#include "..\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\TestMonitor.au3"
#include ".\AbcQa.ExitCodes.au3"
#include ".\Au3Qa.Generic.Logging.au3"

$g_AU3_IsNaturalExitReached = 0

; #FUNCTION# ====================================================================================================================
; Name...........: _OnTestModuleInit_SAMPLE_CALLBACK
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _OnTestModuleInit_SAMPLE_CALLBACK() ; <<< eg. _Abi_TestModule_Start (minimal), _OnTestModuleInit (generic)
	; To be implemented at TestSuite level.
	; Sample implementation:
	;    _Abi_TestModule_Start()
	;    _Sap_RunSapLogon()
	;    _Sap_LoginServer()
	;    _Sap_LoginUser(...)
EndFunc   ;==>_OnTestModuleInit_SAMPLE_CALLBACK

; #FUNCTION# ====================================================================================================================
; Name...........: _OnTestModuleExit_SAMPLE_CALLBACK
; Description ...:
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _OnTestModuleExit_SAMPLE_CALLBACK() ; <<< eg. _Abi_TestModule_Terminate (minimal), _OnTestModuleExit (generic)
	; To be implemented at TestSuite level.
	; Sample implementation:
	;    _Sap_CloseSapLogon()
	;    _Abi_TestModule_Terminate()
EndFunc   ;==>_OnTestModuleExit_SAMPLE_CALLBACK

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_Opt_SetTestModuleTimeout
; Description ...: Set the maximum time allowed for executing the script. After this time, the script will exit with a non-nul return value (ie. FAILED).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_Opt_SetTestModuleTimeout($maxTime) ; in seconds
	If Not TestMonitor_IsTimeoutSet() Then TestMonitor_SetTimeout($maxTime)
EndFunc   ;==>_Au3_Opt_SetTestModuleTimeout

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_TestModule_Start
; Description ...: Initialise the environment and the test. Call the test-suite initialisation callback. Store termination callback for later use.
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......: TODO: add a parameter to pass on to the callbacks!
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_TestModule_Start($callbackOnInit = Default, $callbackOnExit = Default)

	$ret = Default

	Assign("AutoTestingV2._RELEASE152_", 1, 2)

	FuncTrace("_Au3_TestModule_Start", $callbackOnInit, $callbackOnExit)

	; REF. #TestModuleInitImplRoot# -- THIS IMPL IS MEANT FOR REUSE (ABI, etc.)

	; In the very first test-suites, the timeout was set using the configuration files.
	; Later we started writing it directly in the test-module file: it's less flexible but more accessible.
	; In some cases we may dynamically add some time for downloading the build. (AtsQa)
	; - or - Compute the timeout in some cases (eg. SimulatorQa)
	; - or - In more rencent version of the test suite, the timeout is directly set in the test-suite's master template,
	; or overwritten in the test-module. ( see _Au3_Opt_SetTestModuleTimeout ).
	; Local, function-based, timeouts should allow not wasting time and using the globa timeout as a security net.

	; Extra settings for test-monitor
	; (see below)

	; Callback TestModule_SafeCleanup is not implemented, here, for the moment,
	; we'll be using _OnTestModuleExit instead. Remark: this callback used to
	; be executed by the dual process after the killing.
	TestMonitor_SetSafeCleanupProc("")

	; Start dual process for controlling global timeout
	Local $monitorCmdLineByRef = ""
	ConsoleWrite("Launch timeout monitor... #ConsoleOnly" & @CRLF)
	TestMonitor_Broadcast($TESTMONSTAT_LAUNCH)
	TestMonitor_SinkHereWithCmdLine(EnsureSessionId(), $monitorCmdLineByRef)

	; REF. #TestModulePostSink#

	; The code below is executed only in the original process...
	; => prepare outputs directory and automation log
	If _Au3_IsLoggingEnabled() Then
		AutomationData_Init("Session initialized.")
		ValTrace(TestMonitor_GetTimeout(), "@SelfTimeout")
		ValTrace(@WorkingDir, "@CurrentDirectory")
		Trace("INTERRUPT- SCHEDULE CLEANUP PROCEDURE #A1...") ; IMPLICIT
		Trace("INTERRUPT- SCHEDULE CLEANUP PROCEDURE #B3...") ; IMPLICIT
		Trace("INTERRUPT- SCHEDULE CLEANUP PROCEDURE #B2...") ; IMPLICIT
		Trace("INTERRUPT- SCHEDULE CLEANUP PROCEDURE #B1...") ; IMPLICIT
	EndIf

	; Our default and last callback, for dealing with the exit value
	; WARNING: REGISTERED FIRST = CALLED LAST
	OnAutoItExitRegister("_Au3_TestModule_ExitFinally")

	; The new cleanup procedure
	; WARNING: REGISTERED LAST = CALLED FIRST
	If ($callbackOnExit <> "") And ($callbackOnExit <> Default) Then
		OnAutoItExitRegister($callbackOnExit)
	EndIf

	; Save information about this test-run (System, etc.)
	Trace("Store system and test-module characteristics...")
	$szTargetDirEx = TestTarget_GetOriginalPath() ; eg. $ORIGINAL_SUITE_PATH, TestTarget_GetOriginalPath(), etc.
	$szScriptParams = TestThirdPartyTarget_GetData() ; eg. $szScriptParams, $REGISTERED_SAMPLES_LIST, $_TestModule_Target2, etc.
	$szSuperSessionId = GetCmdLineOptn($OPTION_SUPER_SESSION, "") ; std
	OutputsManagement_GatherSystemInfo(EnsureSessionId(), $szTargetDirEx, $szSuperSessionId, $szScriptParams, $monitorCmdLineByRef) ; std

	; REF. #TestModulePostTMOIniWrite#

	; Delegate environment setup to the user.
	TestMonitor_Broadcast($TESTMONSTAT_SETUP)
	If ($callbackOnInit <> "") And ($callbackOnInit <> Default) Then
		$ret = Call($callbackOnInit)
	EndIf

	; Finally,
	TestMonitor_Broadcast($TESTMONSTAT_AUTOMATE)

	Return $ret

EndFunc   ;==>_Au3_TestModule_Start

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_TestModule_Terminate
; Description ...: Normal termination of a script. When called, the script will exit with a nul value (ie. PASSED).
;                  Triggers use of the previously stored termination user-callback (see _Au3_TestModule_Start).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_TestModule_Terminate()
	FuncTrace("_Au3_TestModule_Terminate")
	$g_AU3_IsNaturalExitReached = 1
	Exit 0
EndFunc   ;==>_Au3_TestModule_Terminate

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_TestModule_ExitFinally
; Description ...: PRIVATE
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_TestModule_ExitFinally() ; PRIVATE FUNCTION
	FuncTrace("_Au3_TestModule_ExitFinally")
	; AbiQaTestModule_OnExit()
	If $g_AU3_IsNaturalExitReached Then
		; Here, the normal (and successful) final exit site
		; No more of our code should run after this
		$myExitCode = $AU3_EXIT_SUCCESS
		Trace("Session terminated (" & $myExitCode & ").")
		Exit $myExitCode
	Else
		; 0 Natural closing -- SHOULDN'T HAPPEN BY DESIGN
		; 1 close by Exit function. -- should be $AU3_EXIT_SCRIPT_GLOBAL_TIMEOUT
		; 2 close by clicking on exit of the systray. -- $AU3_EXIT_INTERRUPT_REASON
		; 3 close by user logoff. -- $AU3_EXIT_INTERRUPT_REASON
		; 4 close by Windows shutdown. -- $AU3_EXIT_INTERRUPT_REASON
		Switch @exitMethod
			Case 0
				$myExitCode = $AU3_EXIT_UNKNOWN
				Trace("Session terminated (" & $myExitCode & ").")
				Exit $myExitCode
			Case 1
				$myExitCode = @exitCode
				Trace("Session terminated (" & $myExitCode & ").")
				Exit $myExitCode
			Case 2
			Case 3
			Case 4
				$myExitCode = Int("" & @exitCode & @exitCode & @exitCode)
				Trace("Session terminated (" & $myExitCode & ").")
				Exit $myExitCode
			Case Else
				$myExitCode = $AU3_EXIT_UNKNOWN
				Trace("Session terminated (" & $myExitCode & ").")
				Exit $myExitCode
		EndSwitch
	EndIf
EndFunc   ;==>_Au3_TestModule_ExitFinally
