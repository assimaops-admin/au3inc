#include-once
#include <CoreUAFLibs\UDFs\Pause.au3>
#include ".\Au3Qa.Generic.Logging.Screenshots.au3"

$g_AU3_PauseAfterEveryActionHotkey = ""
$g_AU3_EnableMovieScreenshots = 0
$g_AU3_NextTitleForCallback = ""
$g_AU3_NextVerbLvlForCallback = 9
$g_AU3_LastScreenshotIndex = 100 * 1000 * 10 * 1000
$g_AU3_LastScreenshotMajIdx = $g_AU3_LastScreenshotIndex
Const $MAJ_IDX_INCREMENT = 10 * 1000

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_INTSRVCS_PauseAfterEveryAction
; Description ...: PRIVATE
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Au3_INTSRVCS_PauseAfterEveryAction()
	If Not $g_AU3_PauseAfterEveryActionHotkey Then Return
	Trace("_Au3_INTSRVCS_PauseAfterEveryAction()..." & @CRLF)
	_Pause($g_AU3_PauseAfterEveryActionHotkey)
EndFunc   ;==>_Au3_INTSRVCS_PauseAfterEveryAction

; #FUNCTION# ====================================================================================================================
; Name...........: _Au3_INTSRVCS_MovieScreenshot
; Description ...: PRIVATE
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Const $MOVIE_SCREENSHOT_TIMEOUT_PREFIX__ = "TIMEOUT-" & " "
Const $MOVIE_SCREENSHOT_EXPECT_PREFIX__ = "EXPECT-" & " "
Const $MOVIE_SCREENSHOT_FLUSH_PREFIX__ = "FLUSH-" & " "
Const $__MOVIE_SCREENSHOT_INTERCEPT_SUFFIX = " " & "-INTERCEPTED"
Const $__MOVIE_SCREENSHOT_PREVIEW_SUFFIX = " " & "-PREVIEW"
Const $__MOVIE_SCREENSHOT_RETURN_SUFFIX = " " & "-RETURN"
Func _Au3_INTSRVCS_MovieScreenshotInc($verbosityLevelRequired = 9)
	If $verbosityLevelRequired > $g_AU3_EnableMovieScreenshots Then Return
	Trace("_Au3_INTSRVCS_MovieScreenshotInc()...")
	$g_AU3_LastScreenshotMajIdx += $MAJ_IDX_INCREMENT
	$g_AU3_LastScreenshotIndex = $g_AU3_LastScreenshotMajIdx
EndFunc   ;==>_Au3_INTSRVCS_MovieScreenshotInc
Func _Au3_INTSRVCS_MovieScreenshotNow($textDesc, $verbosityLevelRequired = 9)
	If $verbosityLevelRequired > $g_AU3_EnableMovieScreenshots Then Return
	$g_AU3_LastScreenshotIndex += 1
	$currentIndexToUse = $g_AU3_LastScreenshotIndex
	If $verbosityLevelRequired = 3 Then
		$currentIndexToUse = $g_AU3_LastScreenshotMajIdx & ".A"
	EndIf
	If $verbosityLevelRequired = 1 Or $verbosityLevelRequired = 2 Then
		$currentIndexToUse = ($g_AU3_LastScreenshotMajIdx + $MAJ_IDX_INCREMENT - 1)
	EndIf
	$FILENAME_FORBIDDEN_CARACTERS_STRING = '\/:*?"<>|' ; CustLibs\StdLibXXL\MicrosoftConstants.au3:6
	$FILENAME_FORBIDDEN_CARACTERS_AS_ARRAY = StringSplit($FILENAME_FORBIDDEN_CARACTERS_STRING, "")
	$safeTextDesc = $textDesc
	For $i = 1 To $FILENAME_FORBIDDEN_CARACTERS_AS_ARRAY[0]
		$safeTextDesc = StringReplace($safeTextDesc, $FILENAME_FORBIDDEN_CARACTERS_AS_ARRAY[$i], "_")
	Next
	Trace("MOV" & $currentIndexToUse & "- " & $safeTextDesc & "...")
	_Au3_ScreenshotArea("MOV" & $currentIndexToUse & "- " & $safeTextDesc, -1, -1)
EndFunc   ;==>_Au3_INTSRVCS_MovieScreenshotNow
Func _Au3_INTSRVCS_MovieScreenshotTitle($text, $verbosityLevelRequired = 9)
	$g_AU3_NextTitleForCallback = $text
	$g_AU3_NextVerbLvlForCallback = $verbosityLevelRequired
EndFunc   ;==>_Au3_INTSRVCS_MovieScreenshotTitle
Func _Au3_INTSRVCS_MovieScreenshotCallback()
	_Au3_INTSRVCS_MovieScreenshotNow($g_AU3_NextTitleForCallback, $g_AU3_NextVerbLvlForCallback)
EndFunc   ;==>_Au3_INTSRVCS_MovieScreenshotCallback
