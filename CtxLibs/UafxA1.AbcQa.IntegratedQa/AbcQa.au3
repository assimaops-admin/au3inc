#include-once
#include ".\AbcQa.ExitCodes.au3"
Func _Abc_Assert($cond, $text = "")
	If Not $cond Then
		Exit $AU3_EXIT_ASSERT
	EndIf
EndFunc
