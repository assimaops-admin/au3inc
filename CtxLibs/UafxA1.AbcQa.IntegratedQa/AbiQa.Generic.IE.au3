#include-once

$g_AQTM_IsClearCacheEnabled = True

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_HACK_DisableClearIECache
; Description ...: CHANGE THE DEFAULT TEST-SUITE BAHAVIOUR
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_HACK_DisableClearIECache($isDisabled = True)
	$g_AQTM_IsClearCacheEnabled = Not $isDisabled
EndFunc   ;==>_Abi_HACK_DisableClearIECache

; #FUNCTION# ====================================================================================================================
; Name...........: _Abi_ClearIECache
; Description ...: Clear the Internet Explorer cache (deletes Temporary Internet Files Only).
; Syntax.........:
; Parameters ....:
; Return values .:
; Author ........: Patryk Szczepankiewicz
; Modified.......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......:
; ===============================================================================================================================
Func _Abi_ClearIECache()
	If Not $g_AQTM_IsClearCacheEnabled Then Return
	RunWait("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 8")
	Sleep(500)
EndFunc   ;==>_Abi_ClearIECache
