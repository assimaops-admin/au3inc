:: START BOILERPLATE SNIPPET REF.PS/20130722 REF.PS/20150610.2 -------
@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra-n\Git
@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~n\Git
@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~2\Git
@if not exist "%GITROOT%" set GITROOT=%SYSTEMDRIVE%\Progra~1\Git
@set GITROOT_NIX=%GITROOT:\=/%
:: ------------------------------------------- END BOILERPLATE SNIPPET
@if not exist "%GitInstallDir%" set GitInstallDir=%GITROOT%
@if not exist "%GitInstallDir%" echo ERROR: Git installation directory was not found.&pause&exit 1
@set LANG=en
@cd "%~dp0"
@%GitInstallDir%\bin\sh.exe --login
