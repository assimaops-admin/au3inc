#include-once
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include <CtxLibs\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\MyFiles.Callbacks.au3>
#include <ExtdBaseLibs\FilesPncLib\FilesPncConstants.au3>

;-------------------------------------------------------------------------------
; Name .........: DecodeSQLiteDatabaseTo
; Description ..: n/a
; Note .........: Denormalize path when calling commands (REF.PS/40A4213F_1)
; Return .......: The path of the decoded file
;-------------------------------------------------------------------------------
Func DecodeSQLiteDatabaseTo($szFile_in, $szFile_out, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	FuncTrace("DecodeSQLiteDatabaseTo", $szFile_in, $szFile_out, $eUnpacking)

	; file "in" and file "out"
	; NOTE: we don't fully normalize the path
	$szFile_original = NormalizePath($szFile_in, False, False)
	$szFile_decoded = NormalizePath($szFile_out, False, False)

	$szFile_original_shortname = FileGetShortNameV2($szFile_original)
	$szFile_decoded_shortname = FileGetShortNameV2($szFile_decoded)

	; was this file already unpacked before?
	If Not FileExists($szFile_decoded_shortname) Or $eUnpacking <> $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT Then
		; was this file already unpacked before (bis)?
		$last_decoded = FileReadLine($szFile_decoded_shortname & ".origin_info")
		$this_file_date = FileGetTimeV2($szFile_original_shortname, 0, $eFileGetTimeFormat_1)
		If $last_decoded = "" Or $last_decoded <> $this_file_date Then
			; dump thesaurus to text file
			If Not FileExists(@TempDir & "\EasyDumpSQLite.sub.sqlite") Then FileWrite(@TempDir & "\EasyDumpSQLite.sub.sqlite", ".dump" & @CRLF & ".exit" & @CRLF)
			$command_line = ""
			$command_line &= '"' & @ComSpec & '" /c '
			$command_line &= StringFormat('sqlite3.exe "%s" < "%s" > "%s"' _
					, $szFile_original_shortname _
					, @TempDir & "\EasyDumpSQLite.sub.sqlite" _
					, $szFile_decoded_shortname)
			RunAndCheckFileExists($command_line, $szFile_decoded_shortname, $szFile_decoded)
			; record last dump
			FileDelete($szFile_decoded_shortname & ".origin_info")
			FileWriteLine($szFile_decoded_shortname & ".origin_info", $this_file_date)
		EndIf
	EndIf

	Return $szFile_decoded

EndFunc   ;==>DecodeSQLiteDatabaseTo

;-------------------------------------------------------------------------------
; Name .........: DecodeSQLiteDatabase
; Description ..: n/a
; Note .........: Denormalize path when calling commands (REF.PS/40A4213F_1)
; Return .......: The path of the decoded file
;-------------------------------------------------------------------------------
Func DecodeSQLiteDatabase($szFile_in, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	Return DecodeSQLiteDatabaseTo($szFile_in, $szFile_in & ".dump.sql", $eUnpacking)
EndFunc   ;==>DecodeSQLiteDatabase
