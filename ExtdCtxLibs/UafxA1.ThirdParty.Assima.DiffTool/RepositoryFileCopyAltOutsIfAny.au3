#include-once
#include <___.au3>
#include <+UAFCore.au3>

Func RepositoryFileCopyAltOutsIfAny($src, $dest)

	; exec('DummyMsg.au3 RepositoryFileCopyAltOutsIfAny')

	$src_short = FileGetShortNameV2($src)
	$dest_short = FileGetShortNameV2($dest)

	If Not FileExists($src_short) Then
		FatalError("Invalid source file '" & $src_short & "'.")
	EndIf

	; copy the alternative outputs
	$index = 1;
	$altout_src = StringFormat("%s.altout%02d", $src, $index)
	$altout_dest = StringFormat("%s.altout%02d", $dest, $index)
	$altout_src_short = FileGetShortNameV2($altout_src)
	$altout_dest_short = FileGetShortNameV2($altout_dest)
	While FileExists($altout_src_short)
		If Not FileExists($altout_dest_short) Then
			FileCopy($altout_src_short, $altout_dest_short, 9)
		EndIf
		$index += 1;
		$altout_src = StringFormat("%s.altout%02d", $src, $index)
		$altout_dest = StringFormat("%s.altout%02d", $dest, $index)
		$altout_src_short = FileGetShortNameV2($altout_src)
		$altout_dest_short = FileGetShortNameV2($altout_dest)
	WEnd

EndFunc   ;==>RepositoryFileCopyAltOutsIfAny
