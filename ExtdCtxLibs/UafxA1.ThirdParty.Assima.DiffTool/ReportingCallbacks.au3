#include-once
#include <CtxLibs\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\MyFiles.Callbacks.au3>

;-------------------------------------------------------------------------------
; Name .........: ReportingCallbacks_GetIsEntryToSkipCallback
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingCallbacks_GetIsEntryToSkipCallback($szScriptParams)
	$isEntryToSkipWhenComparingRepositoriesCallback = ""
	If StringStartsWith(@ScriptName, "TestModule.Aps") Then
		Return "IsEntryToSkipDefaultCallback"
	EndIf
	If Not StringInStr($szScriptParams, "ThirdParty") Then
		If StringInStr($szScriptParams, "FullCheck") Then
			$isEntryToSkipWhenComparingRepositoriesCallback = "__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_ForFullCheck"
		ElseIf StringInStr($szScriptParams, "GrabbingHtml") Then
			$isEntryToSkipWhenComparingRepositoriesCallback = "__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_Capture"
		ElseIf StringInStr($szScriptParams, "GrabbingSap") Then
			$isEntryToSkipWhenComparingRepositoriesCallback = "__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_Capture"
		Else
			$isEntryToSkipWhenComparingRepositoriesCallback = "__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories"
		EndIf
	EndIf
	Return $isEntryToSkipWhenComparingRepositoriesCallback
EndFunc   ;==>ReportingCallbacks_GetIsEntryToSkipCallback

;-------------------------------------------------------------------------------
; Name .........: __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories($szEntryPath, $entryName, $entryLocation)
	$bSkip = False
	Select
		Case IsEntryToSkipDefaultCallback($szEntryPath, $entryName, $entryLocation)
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".tpl")
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".dll")
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".exe")
			$bSkip = True
		Case StringEndsWith($szEntryPath, "resources.the")
			$bSkip = True
		Case StringEndsWith($szEntryPath, "publish.log")
			$bSkip = True
		Case IsDirectory($szEntryPath) And GetFolderNameFromPath($szEntryPath) = "Templates"
			$bSkip = True
		Case IsDirectory($szEntryPath) And GetFolderNameFromPath($szEntryPath) = "__tmp_screenshots__"
			$bSkip = True
		Case Else
			$bSkip = False
	EndSelect
	Return $bSkip
EndFunc   ;==>__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories

;-------------------------------------------------------------------------------
; Name .........: __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_ForFullCheck
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_ForFullCheck($szEntryPath, $entryName, $entryLocation)
	Return IsEntryToSkipDefaultCallback($szEntryPath, $entryName, $entryLocation)
EndFunc   ;==>__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_ForFullCheck

;-------------------------------------------------------------------------------
; Name .........: __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_Capture
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_Capture($szEntryPath, $entryName, $entryLocation)
	$bSkip = False
	Select
		Case __AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories($szEntryPath, $entryName, $entryLocation)
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".tut")
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".the")
			$bSkip = True
		Case StringEndsWith($szEntryPath, ".dic")
			$bSkip = True
		Case Else
			$bSkip = False
	EndSelect
	Return $bSkip
EndFunc   ;==>__AtsCommon_CallbackIsEntryToSkip_WhenComparingRepositories_Capture
