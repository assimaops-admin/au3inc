#include-once
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include <CoreUAFLibs\StdCoreLib\XcptSystem.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include ".\MyDiffTool.Toolbox.CompareFiles.ThirdParty.au3"

;-------------------------------------------------------------------------------
; Name .........: CompareTextFiles
; Description ..:
; Parameters ...:
; Return .......: String - empty string "" if there is no difference
;                   - or - description of the difference otherwise.
;-------------------------------------------------------------------------------
Func CompareTextFiles($file1, $file2)
	FuncTrace("CompareTextFiles", $file1, $file2)

	$file1_shortname = FileGetShortNameV2($file1)
	$file2_shortname = FileGetShortNameV2($file2)

	; get file encodings
	$iFileEncoding1 = FileGetEncoding($file1_shortname)
	$iFileEncoding2 = FileGetEncoding($file2_shortname)

	; emit a warning for tricky cases
	If $iFileEncoding1 <> $iFileEncoding2 Then
		Warning('File encoding differs' _
				 & ' between file "' & $file1 & '"' _
				 & ' (shortname = ' & $file1_shortname & ', encoding = ' & $iFileEncoding1 & ')' _
				 & ' and file "' & $file2 & '"' _
				 & ' (shortname = ' & $file2_shortname & ', encoding = ' & $iFileEncoding2 & ').' _
				)
	EndIf

	; prepare comparison command line
	$szDiffCmd = StringFormat('cygwin.stt\diff.exe --text "%s" "%s"', $file1_shortname, $file2_shortname)

	; translate encoding info
	$iBinaryEncoding = FileEncodingToBinaryFlag($iFileEncoding1)

	; launch differences
	$resTxt = RunAndReadBinaryStdout($szDiffCmd)
	$resTextLen = StringLen($resTxt)

	; convert binary output to string
	$resTxt = BinaryToStringV2($resTxt, $iBinaryEncoding)

	Return $resTxt

EndFunc   ;==>CompareTextFiles

#cs INLINE TESTING #1
	Const $TEST_DIR = @ScriptDir & "\..\Meta\Tst\Resources\2008\diff"
	Const $TEST_RESULT = CompareTextFiles($TEST_DIR&"\file1.txt", $TEST_DIR&"\file2.txt")
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $TEST_RESULT = ' & $TEST_RESULT & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
#ce

#cs INLINE TESTING #2
	$file1 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\CompareTextFiles\file1.txt"
	$file2 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\CompareTextFiles\file2.txt"
	$out = CompareTextFiles($file1, $file2)
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $out = ' & $out & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
#ce

;-------------------------------------------------------------------------------
; Name .........: CompareBinaryFiles
; Description ..:
; Parameters ...: - $tolerancePercentage
;                      - Percentage of the original size
; Return .......: String - empty string "" if there is no difference
;                   - or - description of the difference otherwise.
;-------------------------------------------------------------------------------
Func CompareBinaryFiles($file1, $file2, $tolerancePercentage = 0)
	FuncTrace("CompareBinaryFiles", $file1, $file2, $tolerancePercentage)
	; NOTE: 'FileGetShortNameV2' is called in the sub-functions
	$resTxt = CompareFilesSizes($file1, $file2, $tolerancePercentage)
	If $resTxt = "" Then
		$resTxt = CompareFilesBinaryContent($file1, $file2, $tolerancePercentage)
	EndIf
	Return $resTxt
EndFunc   ;==>CompareBinaryFiles

;-------------------------------------------------------------------------------
; Name .........: CompareFilesSizes
; Description ..:
; Parameters ...: - $tolerancePercentage
;                      - Percentage of the original size
; Return .......: String - empty string "" if there is no difference
;                   - or - description of the difference otherwise.
;-------------------------------------------------------------------------------
Func CompareFilesSizes($file1, $file2, $tolerancePercentage = 0)
	FuncTrace("CompareFilesSizes", $file1, $file2, $tolerancePercentage)

	$resTxt = ""

	$size1 = FileGetSize(FileGetShortNameV2($file1))
	$size2 = FileGetSize(FileGetShortNameV2($file2))
	$sizeDiff = $size2 - $size1
	$sizePercent = Abs($sizeDiff) * 100 / $size1
	$isDiff = ($sizePercent > $tolerancePercentage)

	If $isDiff Then
		$resTxt = "Files have different sizes."
		$resTxt &= @CRLF & "Size1='" & $size1 & "'"
		$resTxt &= @CRLF & "Size2='" & $size2 & "'"
		$resTxt &= @CRLF & "File1='" & $file1 & "'"
		$resTxt &= @CRLF & "File2='" & $file2 & "'"
		$resTxt &= @CRLF & "SizeDifference='" & $sizeDiff & "'"
	EndIf
	Return $resTxt
EndFunc   ;==>CompareFilesSizes

;-------------------------------------------------------------------------------
; Name .........: CompareFilesBinaryContent
; Description ..:
; Parameters ...: - $tolerancePercentage
;                      - Percentage of the original size
; Return .......: String - empty string "" if there is no difference
;                   - or - description of the difference otherwise.
;-------------------------------------------------------------------------------
Func CompareFilesBinaryContent($file1, $file2, $tolerancePercentage = 0)
	FuncTrace("CompareFilesBinaryContent", $file1, $file2, $tolerancePercentage)

	$resTxt = ""

	$handle = FileOpen(FileGetShortNameV2($file1), 16)
	$content1 = FileRead($handle)
	FileClose($handle)

	$handle = FileOpen(FileGetShortNameV2($file2), 16)
	$content2 = FileRead($handle)
	FileClose($handle)

	$str_diff = StringCompareExV1($content1, $content2)

	If $str_diff Then
		$resTxt = "Files have different binary content."
		$resTxt &= @CRLF & "File1='" & $file1 & "'"
		$resTxt &= @CRLF & "File2='" & $file2 & "'"
	EndIf
	Return $resTxt

EndFunc   ;==>CompareFilesBinaryContent
