#include-once
#include <AuItCustLibs\AutoItEx.au3>

;-------------------------------------------------------------------------------
; Name .........: StringCompareExV1
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func StringCompareExV1($s1, $s2)

	; HARDCODED FILTER

	;$handle = FileOpen(@ScriptDir&"\Res\idbf2011_header.idb", 16)
	;$idbf2011_header = FileRead($handle)
	;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $idbf2011_header = ' & $idbf2011_header & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	;FileClose($handle)
	Const $idbf2011_header = "0x49444246020000000000000000000000040000000000000001000000010000000E00000061007300730069006D00610000000100000000000000060000000E00000041007300730069006D00610000002E00000041007300730069006D0061005F00"

	;$handle = FileOpen(@ScriptDir&"\Res\idbf2011_header_with_date.idb", 16)
	;$idbf2011_header_with_date = FileRead($handle)
	;ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $idbf2011_header_with_date = ' & $idbf2011_header_with_date & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	;FileClose($handle)
	Const $idbf2011_header_with_date = "0x49444246020000000000000000000000040000000000000001000000010000000E00000061007300730069006D00610000000100000000000000060000000E00000041007300730069006D00610000002E00000041007300730069006D0061005F00320030003100310030003400320039005F00310034003500370033003800"

	$s1sub = StringTrimLeft($s1, StringLen($idbf2011_header_with_date))
	$s2sub = StringTrimLeft($s2, StringLen($idbf2011_header_with_date))

	; hardcoded filter
	If StringStartsWith($s1, $idbf2011_header) Then $s1 = $s1sub
	If StringStartsWith($s2, $idbf2011_header) Then $s2 = $s2sub

	Return StringCompare($s1, $s2)

EndFunc   ;==>StringCompareExV1
