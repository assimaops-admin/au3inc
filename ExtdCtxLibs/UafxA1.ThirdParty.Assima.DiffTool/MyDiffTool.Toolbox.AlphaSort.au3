#include-once
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include <CoreUAFLibs\StdCoreLib\LogShorthand.Trace.au3>
#include <ExtdBaseLibs\FilesPncLib\FilesPncConstants.au3>

;-------------------------------------------------------------------------------
; Name .........: EasyLineSortFileTo
; Description ..:
; Note .........:
; Return .......:
; Testing ......:
;
;         $timer = TimerInit()
;         EasyLineSortFile(@DesktopDir&"\test_lesson.sim.tht.filtered")
;         ConsoleWrite((TimerDiff($timer)/1000)&@CRLF)
;         $timer = TimerInit()
;         EasyLineSortFile(@DesktopDir&"\test_lesson.sim.tht.filtered")
;         ConsoleWrite((TimerDiff($timer)/1000)&@CRLF)
;         $timer = TimerInit()
;
;         EasyLineSortFile(@DesktopDir&"\test_lesson.sim.tht.filtered")
;
; History ......: FEB 11, 2011 - Created for multi-process capture testing
;-------------------------------------------------------------------------------
Func EasyLineSortFileTo($szFile_in_longname, $szFile_out_longname, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	FuncTrace("EasyLineSortFileTo", $szFile_in_longname, $szFile_out_longname, $eUnpacking)

	; file "in" and file "out"
	; NOTE: we don't fully normalize the path
	$szFile_original = NormalizePath($szFile_in_longname, False, False)
	$szFile_sorted = NormalizePath($szFile_out_longname, False, False)

	$szFile_original_shortname = FileGetShortNameV2($szFile_original)
	$szFile_sorted_shortname = FileGetShortNameV2($szFile_sorted)

	$iFileEncoding = FileGetEncoding($szFile_original_shortname)

	; was this file already unpacked before?
	If Not FileExists($szFile_sorted_shortname) Or $eUnpacking <> $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT Then
		; was this file already unpacked before (bis)?
		$last_decoded = FileReadLine($szFile_sorted_shortname & ".origin_info")
		$this_file_date = FileGetTimeV2($szFile_original_shortname, 0, $eFileGetTimeFormat_1)
		If $last_decoded = "" Or $last_decoded <> $this_file_date Then

			; pseudo command-line
			$command_line = StringFormat('linesort.exe < "%s" > "%s" /opt:%d', $szFile_original_shortname, $szFile_sorted_shortname, $eUnpacking)
			ConsoleWrite("[" & $command_line & "]..." & @CRLF)

			; get original content
			Dim $aFileContent
			_FileReadToArray($szFile_original_shortname, $aFileContent)

			; sort file lines
			_ArraySort($aFileContent, 1, 1)

			$emptyLinesCount = 0
			While StringRegExp($aFileContent[$aFileContent[0] - $emptyLinesCount], "\A\h*\Z")
				$emptyLinesCount += 1
			WEnd
			$aFileContent[0] -= $emptyLinesCount
			ReDim $aFileContent[$aFileContent[0] + 1] ;; +1!!

			; create the new file
			; REF.PS/E848B3BE
			If FileExists($szFile_sorted_shortname) Then
				FileDelete($szFile_sorted_shortname)
			EndIf
			$hFile_sorted = FileOpen($szFile_sorted_shortname, 1 + $iFileEncoding)
			_FileWriteFromArrayV2($hFile_sorted, $aFileContent, 1)
			FileClose($hFile_sorted)

			; record last sorting
			FileDelete($szFile_sorted_shortname & ".origin_info")
			FileWriteLine($szFile_sorted_shortname & ".origin_info", $this_file_date)

		EndIf
	EndIf

	Return $szFile_sorted

EndFunc   ;==>EasyLineSortFileTo

;-------------------------------------------------------------------------------
; Name .........: EasyLineSortFile
; Description ..:
; Note .........:
; Return .......:
; History ......: FEB 11, 2011 - Created for multi-process capture testing
;-------------------------------------------------------------------------------
Func EasyLineSortFile($szFile_in, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	Return EasyLineSortFileTo($szFile_in, $szFile_in & ".lsorted", $eUnpacking)
EndFunc   ;==>EasyLineSortFile
