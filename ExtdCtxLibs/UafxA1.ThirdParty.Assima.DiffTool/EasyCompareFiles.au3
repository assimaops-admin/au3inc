#include-once
#include <___.au3>
#include <CtxLibs\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\AssimaQaConstants.au3>
#include <ExtdBaseLibs\FilesPncLib\FilterTextFiles.au3>
#include <ExtdBaseLibs\FilesPncLib\RecursivelyListContentOfDirectoryVx.au3>
#include "..\UafxA1.ThirdParty.Assima.AtsFiles\AssimaThes.au3"
#include "..\UafxA1.ThirdParty.SQLite.SQLiteFiles\SQLiteFile.au3"
#include ".\MyDiffTool.Toolbox.CompareFiles.au3" ; CompareBinaryFiles,
#include ".\MyDiffTool.Toolbox.AlphaSort.au3" ; EasyLineSortFile
#include ".\RepositoryFileCopyAltOutsIfAny.au3"

; enumerate test-result types
Enum $RESULT_SUCCESS = 0, $RESULT_ERROR = 1, $RESULT_WARNING = 2

; ascii output prettyfier
Const $GENERIC_COLUMN_SIZE = 35

;-------------------------------------------------------------------------------
; Name .........: EasyCompareFiles
; Description ..:
; Parameters ...:
; Return .......:
;-------------------------------------------------------------------------------
Func EasyCompareFiles(ByRef $szFile1_InAndOut, ByRef $szFile2_InAndOut, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	; Case 1: compare file sizes
	If $eUnpacking = $COMPARATOR_CHECK_RES_SIZE_ONLY Then
		; copy all the alternative outputs back into the new outputs
		; and run the diff tool
		RepositoryFileCopyAltOutsIfAny($szFile1_InAndOut, $szFile2_InAndOut)
		Return CompareFilesExSub($szFile1_InAndOut, $szFile2_InAndOut, "CompareFilesSizes")
	EndIf

	; Case 2: check files contents
	$szResult = ""
	Select

		Case StringEndsWith($szFile1_InAndOut, ".js") _
				Or StringEndsWith($szFile1_InAndOut, ".htm") _
				Or StringEndsWith($szFile1_InAndOut, ".html") _
				Or StringEndsWith($szFile1_InAndOut, ".log") _
				Or StringEndsWith($szFile1_InAndOut, ".txt") _
				Or StringEndsWith($szFile1_InAndOut, ".text") _
				Or StringEndsWith($szFile1_InAndOut, ".list") _
				Or StringEndsWith($szFile1_InAndOut, ".xml") _

			$szResult = CompareFilesEx($szFile1_InAndOut, $szFile2_InAndOut, $eUnpacking)

		Case FileIsAssimaThesaurus($szFile1_InAndOut) _
				And FileIsAssimaThesaurus($szFile2_InAndOut)

			$szResult = CompareFilesEx($szFile1_InAndOut, $szFile2_InAndOut, $eUnpacking, $fFILETYPE_ASSIMA_THES)

		Case StringEndsWith($szFile1_InAndOut, ".db")

			$szResult = CompareFilesEx($szFile1_InAndOut, $szFile2_InAndOut, $eUnpacking, $fFILETYPE_SQLITE_DB)

		Case StringEndsWith($szFile1_InAndOut, ".dtb") _
				Or StringEndsWith($szFile1_InAndOut, ".tpl") _
				Or StringEndsWith($szFile1_InAndOut, ".zip")

			; We already check the content of the archive by unpacking it...
			; -> so just check whether the archive is there
			$unpackedDirectory = $szFile1_InAndOut & ".unpacked"
			If Not FileExists(FileGetShortNameV2($unpackedDirectory)) Then
				$szResult &= "Directory '" & $unpackedDirectory & "' is missing." & @CRLF
			EndIf
			$unpackedDirectory = $szFile2_InAndOut & ".unpacked"
			If Not FileExists(FileGetShortNameV2($unpackedDirectory)) Then
				$szResult &= "Directory '" & $unpackedDirectory & "' is missing." & @CRLF
			EndIf

		Case StringEndsWith($szFile1_InAndOut, ".doc") _
				Or StringEndsWith($szFile1_InAndOut, ".glo")

			; copy all the alternative outputs back into the new outputs
			; and run the diff tool
			RepositoryFileCopyAltOutsIfAny($szFile1_InAndOut, $szFile2_InAndOut)
			$szResult = CompareFilesExSub($szFile1_InAndOut, $szFile2_InAndOut, "CompareFilesSizes")

		Case Else

			; copy all the alternative outputs back into the new outputs
			; and run the diff tool
			RepositoryFileCopyAltOutsIfAny($szFile1_InAndOut, $szFile2_InAndOut)
			$szResult = CompareFilesExSub($szFile1_InAndOut, $szFile2_InAndOut, "CompareBinaryFiles")

	EndSelect
	Return $szResult

EndFunc   ;==>EasyCompareFiles

#cs INLINE TESTING #3
	$file1 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\file1.sim"
	$file2 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\file2.sim"
	$out = EasyCompareFiles($file1, $file2)
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $out = ' & $out & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\*.tht")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\*.origin_info")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\*.filtered")
#ce

#cs INLINE TESTING #4
	$file1 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData1\test.sim"
	$file2 = "C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData2\test.sim"
	$out = EasyCompareFiles($file1, $file2)
	ConsoleWrite('@ Debug(' & @ScriptLineNumber & ') : $out = ' & $out & @crlf & '>Error code: ' & @error & @crlf) ;### Debug Console
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData1\*.tht")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData1\*.origin_info")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData1\*.filtered")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData2\*.tht")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData2\*.origin_info")
	FileDelete("C:\AutoTestingV2\Meta\Tst\Resources\2008\diff\EasyCompareFiles\BigFiles\uGrabData2\*.filtered")
#ce

;-------------------------------------------------------------------------------
; Name .........: CompareFilesEx
; Description ..: Compare two files containing structured text information
; Param ........: $FileType
;                   - integer value made of flags ($fFILETYPE_TEXT,
;                     $fFILETYPE_ASSIMA_THES, etc.)
;                 $szSmartFilter
;                   - set to "" or "default" if you wish to use the
;                     default filters for this type of files (based on $FileType)
;                   - set to 0 if you explicitely do not wish to use any filter
;                     at all
;                   - give name of the filter to use (*)
; Note .........: Denormalize path when calling commands (REF.PS/40A4213F_1)
; Return .......: Comparison output as it is comes from the Cygwin 'Diff' Tool
; (*) TOPIC REF.PS/F40CC619: names are converted into paths using "Path.ini"
;-------------------------------------------------------------------------------
Func CompareFilesEx(ByRef $szFile1_InAndOut, ByRef $szFile2_InAndOut, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT, $FileType = Default)

	FuncTrace("CompareFilesEx", $szFile1_InAndOut, $szFile2_InAndOut, $eUnpacking, $FileType)

	Local $szFile1_final = $szFile1_InAndOut
	Local $szFile2_final = $szFile2_InAndOut

	Local $bDecoded = False
	Local $szFilterFile = ""
	Local $bFiltered = False
	Local $bSorted = False

	; default behaviour
	If $FileType = Default Then $FileType = $fFILETYPE_TEXT

	; security check
	If Not IsInt($FileType) Then
		MsgBox(0, "CompareFilesEx", "Bad parameter $FileType = " & $FileType)
		Return ""
	EndIf

	;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 1: DECODING ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 1: decoding...")

	; decode input file
	$szFile1_decoded = ""
	$szFile2_decoded = ""
	Select
		Case IsFlagSet($FileType, $fFILETYPE_ASSIMA_THES)
			$szFile1_decoded = AssimaThes_DecodeV2($szFile1_final, $eUnpacking)
			$szFile2_decoded = AssimaThes_DecodeV2($szFile2_final, $eUnpacking)
			$bDecoded = True
		Case IsFlagSet($FileType, $fFILETYPE_SQLITE_DB)
			$szFile1_decoded = DecodeSQLiteDatabase($szFile1_final, $eUnpacking)
			$szFile2_decoded = DecodeSQLiteDatabase($szFile2_final, $eUnpacking)
			$bDecoded = True
		Case Else
			; NOP
	EndSelect

	; update names of the files to compare
	If $bDecoded Then
		$szFile1_final = $szFile1_decoded
		$szFile2_final = $szFile2_decoded
	EndIf

	;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 2: FILTERING ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 2: filtering...")

	; get the filter path
	$szFilterFile = GetFltPath($szFile1_final)

	; filter the files
	$szFile1_filtered = ""
	$szFile2_filtered = ""
	If $szFilterFile <> "" Then
		ValTrace(FileGetEncoding(FileGetShortNameV2($szFile1_final)), 'FileGetEncoding("' & $szFile1_final & '")')
		ValTrace(FileGetEncoding(FileGetShortNameV2($szFile2_final)), 'FileGetEncoding("' & $szFile2_final & '")')
		$szFile1_filtered = FilterTextFileLinesToFiltered($szFile1_final, $szFilterFile, $eUnpacking)
		$szFile2_filtered = FilterTextFileLinesToFiltered($szFile2_final, $szFilterFile, $eUnpacking)
		If $szFile1_filtered <> "" And $szFile2_filtered <> "" Then
			$bFiltered = True
		EndIf
	EndIf

	; update names of the files to compare
	If $bFiltered Then
		$szFile1_final = $szFile1_filtered
		$szFile2_final = $szFile2_filtered
	EndIf

	;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 3: SORTING ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 3: sorting...")

	; line-sort the files
	$szFile1_lsorted = ""
	$szFile2_lsorted = ""
	If FileIsLineSortable($szFile1_final) Then
		$szFile1_lsorted = EasyLineSortFile($szFile1_final, $eUnpacking)
		$szFile2_lsorted = EasyLineSortFile($szFile2_final, $eUnpacking)
		If $szFile1_lsorted <> "" And $szFile2_lsorted <> "" Then
			$bSorted = True
		EndIf
	EndIf

	; update names of the files to compare
	If $bSorted Then
		$szFile1_final = $szFile1_lsorted
		$szFile2_final = $szFile2_lsorted
	EndIf

	;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 4: VARIATIONS ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 4: variations...")

	; copy all the alternative outputs back into the new outputs
	RepositoryFileCopyAltOutsIfAny($szFile1_final, $szFile2_final)

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 5: COMPARISONS ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 5: comparisons...")

	; run the diff tool
	$szResult = CompareFilesExSub($szFile1_final, $szFile2_final, "CompareTextFiles")

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; PART 6: CLEANING-UP ;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	Trace(@TAB & @TAB & @TAB & "Part 6: cleaning-up...")

	; delete the filtered files
	If $bSorted And $eUnpacking = $COMPARATOR_DELETE_UNPACKED_CONTENT Then
		FileDelete($szFile1_lsorted)
		FileDelete($szFile2_lsorted)
	EndIf

	; delete the filtered files
	If $bFiltered And $eUnpacking = $COMPARATOR_DELETE_UNPACKED_CONTENT Then
		FileDelete($szFile1_filtered)
		FileDelete($szFile2_filtered)
	EndIf

	; delete the decompressed files
	If $bDecoded And $eUnpacking = $COMPARATOR_DELETE_UNPACKED_CONTENT Then
		FileDelete($szFile1_decoded)
		FileDelete($szFile2_decoded)
	EndIf

	; return names of the unpacked files
	If $eUnpacking <> $COMPARATOR_DELETE_UNPACKED_CONTENT Then
		$szFile1_InAndOut = $szFile1_final
		$szFile2_InAndOut = $szFile2_final
	EndIf

	; return comparison result
	Return $szResult

EndFunc   ;==>CompareFilesEx

;-------------------------------------------------------------------------------
; Name .........: CompareFilesExSub
; Description ..:
; Parameters ...:
; Testing ......:
;
;     $f1='C:\AutoTestingV2.Refs\Bold\600\TestModule.SelfQa.G102.T105#CreateOneFile\Automation\Workspace\file1.txt'
;     $f2='C:\AutoTestingV2.Data\TMO-DEVPC-201012-22-19-3146\Automation\Workspace\file1.txt'
;     CompareFilesExSub($f1, $f2, "CompareTextFiles")
;
; Return .......: String - empty string "" if there is no difference
;                   - or - description of the difference otherwise.
;-------------------------------------------------------------------------------
Func CompareFilesExSub($file1, $file2, $compare_callback)

	$resTxt = CompareBinaryFiles($file1, $file2)
	$isDiff = ($resTxt <> "")

	$altout_index = 1
	$altout_file1 = StringFormat("%s.altout%02d", $file1, $altout_index)
	$altout_file2 = StringFormat("%s.altout%02d", $file2, $altout_index)
	While $isDiff And FileExists($altout_file1)

		; do not compare files if a match was already found
		$resTxt = CompareBinaryFiles($altout_file1, $file2)
		$isDiff = ($resTxt <> "") ; is this a matching file?

		$altout_index += 1
		$altout_file1 = StringFormat("%s.altout%02d", $file2, $altout_index)
		$altout_file2 = StringFormat("%s.altout%02d", $file2, $altout_index)
	WEnd

	If $isDiff <> "" Then
		; launch the appropriate diff tool
		$resTxt = Call($compare_callback, $file1, $file2)
	EndIf

	Return $resTxt

EndFunc   ;==>CompareFilesExSub

;-------------------------------------------------------------------------------
; Name .........: __EasyCompareDirectories_Sub_AddDiffToList
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func __EasyCompareDirectories_Sub_AddDiffToList(ByRef $Differences, $szMsg, $szFile1 = "", $szFile2 = "", $szFilename1 = "", $szFilename2 = "", $szFile1_original = "", $szFile2_original = "")
	If $Differences = "" Then
		Dim $Differences[1][7]
		$Differences[0][0] = $szMsg
		$Differences[0][1] = $szFile1
		$Differences[0][2] = $szFile2
		$Differences[0][3] = $szFilename1
		$Differences[0][4] = $szFilename2
		$Differences[0][5] = $szFile1_original
		$Differences[0][6] = $szFile2_original
	Else
		$nUBound = UBound($Differences)
		ReDim $Differences[$nUBound + 1][7]
		$Differences[$nUBound][0] = $szMsg
		$Differences[$nUBound][1] = $szFile1
		$Differences[$nUBound][2] = $szFile2
		$Differences[$nUBound][3] = $szFilename1
		$Differences[$nUBound][4] = $szFilename2
		$Differences[$nUBound][5] = $szFile1_original
		$Differences[$nUBound][6] = $szFile2_original
	EndIf
EndFunc   ;==>__EasyCompareDirectories_Sub_AddDiffToList

;-------------------------------------------------------------------------------
; Name .........: EasyCompareDirectories
; Description ..:
; Return .......: - integer '0' if error occured
;                 - empty string "" if there is no differences at all
;                 - 2-dimensional array of strings (if there were differences)
;
;                         $Differences[$iDiff][0] = $szMsg
;                         $Differences[$iDiff][1] = $szFile1
;                         $Differences[$iDiff][2] = $szFile2
;                         $Differences[$iDiff][3] = $szFilename1
;                         $Differences[$iDiff][4] = $szFilename2
;
;-------------------------------------------------------------------------------
Func EasyCompareDirectories($szRefRepositoryPath, $szResultRepositoryPath, $isEntryToSkipCallback = "", $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)

	RecursivelyListContentOfDirectory_SetDefaultCallback($isEntryToSkipCallback)

	$Differences = ""

	; check
	If Not FileExists($szRefRepositoryPath) Then
		Warning("Impossible repository analysis." & @CRLF _
				 & "(Invalid path '" & $szRefRepositoryPath & "'.)")
		Return 0
	EndIf
	If Not FileExists($szResultRepositoryPath) Then
		Warning("Impossible repository analysis." & @CRLF _
				 & "(Invalid path '" & $szResultRepositoryPath & "'.)")
		Return 0
	EndIf

	; Normalize paths
	$szRefRepositoryPath = NormalizePath($szRefRepositoryPath)
	$szResultRepositoryPath = NormalizePath($szResultRepositoryPath)

	; list all the files and directories
	; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
	$szRefRepositoryPath_name = GetFolderNameFromPath($szRefRepositoryPath)
	$szResultRepositoryPath_name = GetFolderNameFromPath($szResultRepositoryPath)
	$arrayDirectories1 = _ArrayCreate(1, $szRefRepositoryPath)
	$arrayDirectories2 = _ArrayCreate(1, $szResultRepositoryPath)
	$arrayFilenames1 = _ArrayCreate(0)
	$arrayFilenames2 = _ArrayCreate(0)
	$arrayFolders1 = _ArrayCreate(1, $szRefRepositoryPath_name)
	$arrayFolders2 = _ArrayCreate(1, $szResultRepositoryPath_name)
	$arrayFiles1 = _ArrayCreate(0)
	$arrayFiles2 = _ArrayCreate(0)
	RecursivelyListContentOfDirectoryV2($arrayDirectories1, $arrayFiles1, $arrayFolders1, $arrayFilenames1, $isEntryToSkipCallback, $eUnpacking)
	RecursivelyListContentOfDirectoryV2($arrayDirectories2, $arrayFiles2, $arrayFolders2, $arrayFilenames2, $isEntryToSkipCallback, $eUnpacking)

	; -*-*-*-*-*-*-*-*-*-* ;
	; check there is the same number of files
	If $arrayFilenames1[0] <> $arrayFilenames2[0] Then

		; Write the beggining of the message

		$szMsg = @CRLF
		$szMsg &= "INVALID NUMBER OF FILES:"
		$szMsg &= @CRLF
		$szMsg &= "(" & ($arrayFilenames2[0] - $arrayFilenames1[0]) & " file(s))"
		$szMsg &= @CRLF
		$szMsg &= "- reference contains " & $arrayFilenames1[0] & " file(s),"
		$szMsg &= @CRLF
		$szMsg &= "- this test's result contains " & $arrayFilenames2[0] & " file(s)."


		; What are the extra files?
		; Regarding the code below, note that the discriminant infomation is the NAME of the file
		; ( cf. "_ArraySearch($arrayFilenames1, $arrayFilenames2[$i])" )

		$aResultExtraFiles = _ArrayCreate(0)
		$aResultExtraFilenames = _ArrayCreate(0)
		$aReferenceExtraFiles = _ArrayCreate(0)
		$aReferenceExtraFilenames = _ArrayCreate(0)

		; ArrayTrace($arrayFilenames2, "$arrayFilenames2")
		; ArrayTrace($arrayFiles2, "$arrayFiles2")
		; ArrayTrace($arrayFilenames1, "$arrayFilenames1")
		; ArrayTrace($arrayFiles1, "$arrayFiles1")

		For $i = 1 To $arrayFilenames2[0]
			If _ArraySearch($arrayFilenames1, $arrayFilenames2[$i]) = -1 Then

				$aResultExtraFiles[0] += 1
				_ArrayAdd($aResultExtraFiles, $arrayFiles2[$i])

				$aResultExtraFilenames[0] += 1
				_ArrayAdd($aResultExtraFilenames, $arrayFilenames2[$i])

			EndIf
		Next

		For $i = 1 To $arrayFilenames1[0]
			If _ArraySearch($arrayFilenames2, $arrayFilenames1[$i]) = -1 Then

				$aReferenceExtraFiles[0] += 1
				_ArrayAdd($aReferenceExtraFiles, $arrayFiles1[$i])

				$aReferenceExtraFilenames[0] += 1
				_ArrayAdd($aReferenceExtraFilenames, $arrayFilenames1[$i])

			EndIf
		Next


		; Write the list of extra files (part 1)

		If $aReferenceExtraFilenames[0] Then

			$szMsg &= @CRLF
			$szMsg &= @CRLF
			$szMsg &= "REFERENCE FILE(S) THAT DO NOT MATCH ANY RESULT FILE:"
			$szMsg &= @CRLF
			$szMsg &= "(" & $aReferenceExtraFilenames[0] & " file(s))"

			For $i = 1 To $aReferenceExtraFilenames[0]
				$emptySpace = $GENERIC_COLUMN_SIZE - StringLen(Quote($aReferenceExtraFilenames[$i]))
				$szMsg &= @CRLF
				$szMsg &= "- " & Quote($aReferenceExtraFilenames[$i]) _
						 & _StringRepeat(" ", $emptySpace) _
						 & "(" & Quote($aReferenceExtraFiles[$i]) & ")"
			Next

		EndIf


		; Write the list of extra files (part 2)

		If $aResultExtraFilenames[0] Then

			$szMsg &= @CRLF
			$szMsg &= @CRLF
			$szMsg &= "RESULT FILE(S) THAT DO NOT MATCH ANY REFERENCE FILE:"
			$szMsg &= @CRLF
			$szMsg &= "(" & $aResultExtraFilenames[0] & " file(s))"

			For $i = 1 To $aResultExtraFilenames[0]
				$emptySpace = $GENERIC_COLUMN_SIZE - StringLen(Quote($aResultExtraFilenames[$i]))
				$szMsg &= @CRLF
				$szMsg &= "- " & Quote($aResultExtraFilenames[$i]) _
						 & _StringRepeat(" ", $emptySpace) _
						 & "(" & Quote($aResultExtraFiles[$i]) & ")"
			Next

		EndIf


		; Write full list of files (part 3)

		If (Not $aReferenceExtraFilenames[0]) And (Not $aResultExtraFilenames[0]) Then

			$szMsg &= @CRLF
			$szMsg &= @CRLF
			$szMsg &= "REFERENCE FILE(S):"
			$szMsg &= @CRLF
			$szMsg &= "(" & $arrayFilenames1[0] & " file(s))"

			For $i = 1 To $arrayFilenames1[0]
				$emptySpace = $GENERIC_COLUMN_SIZE - StringLen(Quote($arrayFilenames1[$i]))
				$szMsg &= @CRLF
				$szMsg &= "- " & Quote($arrayFilenames1[$i]) _
						 & _StringRepeat(" ", $emptySpace) _
						 & "(" & Quote($arrayFiles1[$i]) & ")"
			Next

			$szMsg &= @CRLF
			$szMsg &= @CRLF
			$szMsg &= "RESULT FILE(S):"
			$szMsg &= @CRLF
			$szMsg &= "(" & $arrayFilenames2[0] & " file(s))"

			For $i = 1 To $arrayFilenames2[0]
				$emptySpace = $GENERIC_COLUMN_SIZE - StringLen(Quote($arrayFilenames2[$i]))
				$szMsg &= @CRLF
				$szMsg &= "- " & Quote($arrayFilenames2[$i]) _
						 & _StringRepeat(" ", $emptySpace) _
						 & "(" & Quote($arrayFiles2[$i]) & ")"
			Next

		EndIf


		; Record the difference between the Reference and the Result

		$szMsg &= @CRLF
		__EasyCompareDirectories_Sub_AddDiffToList( _
				$Differences _
				, $szMsg _
				, $szRefRepositoryPath _
				, $szResultRepositoryPath _
				, $szRefRepositoryPath_name _
				, $szResultRepositoryPath_name _
				)
	EndIf
	; -*-*-*-*-*-*-*-*-*-* ;


	; -*-*-*-*-*-*-*-*-*-* ;
	; check file names + files sizes + files content
	Dim $standard_left_to_right
	$nFiles = Min($arrayFilenames1[0], $arrayFilenames2[0])
	If $arrayFilenames1[0] <= $arrayFilenames2[0] Then
		$standard_left_to_right = True
		$smaller_array_filenames = $arrayFilenames1
		$bigger_array_filenames = $arrayFilenames2
		$smaller_array_files = $arrayFiles1
		$bigger_array_files = $arrayFiles2
		$smaller_array_root = $szRefRepositoryPath
		$bigger_array_root = $szResultRepositoryPath
	Else
		$standard_left_to_right = False
		$smaller_array_filenames = $arrayFilenames2
		$bigger_array_filenames = $arrayFilenames1
		$smaller_array_files = $arrayFiles2
		$bigger_array_files = $arrayFiles1
		$smaller_array_root = $szResultRepositoryPath
		$bigger_array_root = $szRefRepositoryPath
	EndIf

	ValTrace($smaller_array_root, "$smaller_array_root")
	ValTrace($bigger_array_root, "$bigger_array_root")

	ValTrace($nFiles, "$nFiles")
	ValTrace($smaller_array_root, "$smaller_array_root")

	For $i1 = 1 To $nFiles
		$szFilename1 = $smaller_array_filenames[$i1]
		$szFile1 = $smaller_array_files[$i1]
		$szFile1_RelativeToRoot = StringTrimLeft($szFile1, StringLen($smaller_array_root))
		; search the second file
		$i2 = -1
		$szFilename2 = ""
		$szFile2 = ""
		$bFoundSecondFile = False
		Do
			; FuncTrace("_ArraySearch", $bigger_array_filenames, $szFilename1, $i2+1)
			$i2 = _ArraySearch($bigger_array_filenames, $szFilename1, $i2 + 1)
			If $i2 <> -1 Then
				; ValTrace($i2, "$i2")
				$szFilename2 = $bigger_array_filenames[$i2]
				$szFile2 = $bigger_array_files[$i2]
				$szFile2_RelativeToRoot = StringTrimLeft($szFile2, StringLen($bigger_array_root))

				; did we find the second file?
				$traceMsg = "Comparing two relative paths '" & $szFile1_RelativeToRoot & "' and '" & $szFile2_RelativeToRoot & "' to see if we shall compare the two files..."
				Trace($traceMsg)

				If $szFile2_RelativeToRoot = $szFile1_RelativeToRoot Then
					$bFoundSecondFile = True

					; put 'file 1' on the left hand side
					; and 'file 2' on the right hand side
					If $standard_left_to_right Then
						$szFile_left = $szFile1
						$szFile_right = $szFile2
						$szFilename_left = $szFilename1
						$szFilename_right = $szFilename2
					Else
						$szFile_left = $szFile2
						$szFile_right = $szFile1
						$szFilename_left = $szFilename2
						$szFilename_right = $szFilename1
					EndIf

					; compare the files
					; WARNING: $szFile_left and $szFile_right are passed by reference and may be amended
					$szFile_left_original = $szFile_left
					$szFile_right_original = $szFile_right
					$szMsg = EasyCompareFiles($szFile_left, $szFile_right, $eUnpacking)
					; store the result
					__EasyCompareDirectories_Sub_AddDiffToList( _
							$Differences, $szMsg _
							, $szFile_left, $szFile_right _
							, $szFilename_left, $szFilename_right _
							, $szFile_left_original, $szFile_right_original _
							)
				EndIf
			EndIf
		Until ($i2 = -1) Or ($bFoundSecondFile = True)
	Next
	; -*-*-*-*-*-*-*-*-*-* ;

	;;;_ArrayDisplay($Differences, "$Differences")
	Return $Differences

EndFunc   ;==>EasyCompareDirectories

;-------------------------------------------------------------------------------
; Name .........: ConvertArrayOfDifferencesToChunks
; Description ..:
; Return .......: $RESULT_ERROR, $RESULT_WARNING or $RESULT_SUCCESS
;-------------------------------------------------------------------------------
Func ConvertArrayOfDifferencesToChunks($aDifferences, $chunksDir, $refRoot, $resRoot, $encode4html = 1)

	If Not IsArray($aDifferences) Then
		Return $RESULT_ERROR
	EndIf
	$nResult = $RESULT_SUCCESS

	$cSkippedOnNoDiff = 0
	For $i = 0 To UBound($aDifferences) - 1

		$szMsg = $aDifferences[$i][0]
		$szFile1 = $aDifferences[$i][1]
		$szFile2 = $aDifferences[$i][2]
		$szFilename1 = $aDifferences[$i][3]
		$szFilename2 = $aDifferences[$i][4]
		;$szFile1_original	= $aDifferences[$i][5]
		;$szFile2_original	= $aDifferences[$i][6]

		$szFileSub1 = ""
		$szFileSub2 = ""
		If StringStartsWith($szFile1, $refRoot) Then $szFileSub1 = StringTrimLeft($szFile1, StringLen($refRoot))
		If StringStartsWith($szFile2, $resRoot) Then $szFileSub2 = StringTrimLeft($szFile2, StringLen($resRoot))


		; optimisation:
		; do not log entries when there is no difference with reference
		If $szMsg = "" Then
			$cSkippedOnNoDiff += 1
			ContinueLoop
		EndIf

		; SPECIFIC
		$chunkHandle = FileOpen($chunksDir & $szFileSub2, 10)
		FileWrite($chunkHandle, _
				QuestionMark($encode4html, ToHtml($szMsg), $szMsg) _
				)
		FileClose($chunkHandle)

	Next

	Return $nResult
EndFunc   ;==>ConvertArrayOfDifferencesToChunks

;-------------------------------------------------------------------------------
; Name .........: ConvertArrayOfDifferencesToString
; Description ..:
; Return .......: $RESULT_ERROR, $RESULT_WARNING or $RESULT_SUCCESS
;-------------------------------------------------------------------------------
Func ConvertArrayOfDifferencesToString($aDifferences, ByRef $szGatheredMessages, $refRoot, $resRoot, $encode4html = 1, $indent = "")

	If Not IsArray($aDifferences) Then
		Return $RESULT_ERROR
	EndIf
	$nResult = $RESULT_SUCCESS

	$cSkippedOnNoDiff = 0
	For $i = 0 To UBound($aDifferences) - 1

		$szMsg = $aDifferences[$i][0]
		$szFile1 = $aDifferences[$i][1]
		$szFile2 = $aDifferences[$i][2]
		$szFilename1 = $aDifferences[$i][3]
		$szFilename2 = $aDifferences[$i][4]
		;$szFile1_original	= $aDifferences[$i][5]
		;$szFile2_original	= $aDifferences[$i][6]

		$szFileSub1 = ""
		$szFileSub2 = ""
		If StringStartsWith($szFile1, $refRoot) Then $szFileSub1 = StringTrimLeft($szFile1, StringLen($refRoot))
		If StringStartsWith($szFile2, $resRoot) Then $szFileSub2 = StringTrimLeft($szFile2, StringLen($resRoot))


		; optimisation:
		; do not log entries when there is no difference with reference
		If $szMsg = "" Then
			$cSkippedOnNoDiff += 1
			ContinueLoop
		EndIf

		$szGatheredMessages &= @CRLF & $indent
		$szGatheredMessages &= "<DiffEntry>"

		$szGatheredMessages &= @CRLF & $indent
		$szGatheredMessages &= @TAB
		$szGatheredMessages &= "<DiffNumber>"
		$szGatheredMessages &= ($i - $cSkippedOnNoDiff + 1)
		$szGatheredMessages &= "</DiffNumber>"

		If $szFilename1 = $szFilename2 Then
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileName>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFilename1
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileName>"
		Else
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileName1>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFilename1
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileName1>"
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileName2>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFilename2
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileName2>"
		EndIf

		If $szFileSub1 = $szFileSub2 Then
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileSub>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFileSub1
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileSub>"
		Else
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileSub1>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFileSub1
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileSub1>"
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileSub2>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= $szFileSub2
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileSub2>"
		EndIf

		If $szFile1 <> "" Then
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FilePath1>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= NormalizePath($szFile1, True)
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FilePath1>"
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileDirPath1>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= NormalizePath(GetParentDirectory($szFile1), True)
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileDirPath1>"
		EndIf

		If $szFile2 <> "" Then
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FilePath2>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= NormalizePath($szFile2, True)
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FilePath2>"
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<FileDirPath2>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= NormalizePath(GetParentDirectory($szFile2), True)
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</FileDirPath2>"
		EndIf

		If $szMsg <> "" Then
			$nResult = $RESULT_WARNING
			$szGatheredMessages &= @CRLF & $indent
			$szGatheredMessages &= @TAB
			$szGatheredMessages &= "<DiffDescription>"
			$szGatheredMessages &= "<![CDATA["
			$szGatheredMessages &= QuestionMark($encode4html _
					, ToHtml($szMsg) _
					, $szMsg)
			$szGatheredMessages &= "]]>"
			$szGatheredMessages &= "</DiffDescription>"
		EndIf

		$szGatheredMessages &= @CRLF & $indent
		$szGatheredMessages &= "</DiffEntry>"

	Next

	$szGatheredMessages &= @CRLF

	Return $nResult
EndFunc   ;==>ConvertArrayOfDifferencesToString
