#include-once
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include <CtxLibs\UafxA1.AbcQa.ThirdParty.Assima.Rnd.Qa\MyFiles.Callbacks.au3> ; RecursivelyListContentOfDirectory_SetDefaultCallback
#include <ExtdBaseLibs\FilesPncLib\RecursivelyListContentOfDirectoryVx.au3>

;Const $7ZIP_PATH = "C:\AutoTestingV2\bin"
;Const $7ZIP_EXEC = "7za"
Const $7ZIP_PATH = "C:\Program Files\7-Zip"
Const $7ZIP_EXEC = "7z"

;-------------------------------------------------------------------------------
; Name .........: EasyUnpackRec
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func EasyUnpackRec($szFile, $isEntryToSkipCallback = "", $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	FuncTrace("EasyUnpackRec", $szFile, $isEntryToSkipCallback, $eUnpacking)
	RecursivelyListContentOfDirectory_SetDefaultCallback($isEntryToSkipCallback)
	$recurs = _ArrayCreate(0)
	Return EasyUnpack_Private($szFile, $recurs, $isEntryToSkipCallback, $eUnpacking)
EndFunc   ;==>EasyUnpackRec

;-------------------------------------------------------------------------------
; Name .........: EasyUnpack
; Description ..:
; Return .......: true if anything was unpacked
;-------------------------------------------------------------------------------
Func EasyUnpack($szFile, $isEntryToSkipCallback = "", $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	FuncTrace("EasyUnpack", $szFile, $isEntryToSkipCallback, $eUnpacking)
	RecursivelyListContentOfDirectory_SetDefaultCallback($isEntryToSkipCallback)
	$recurs = ""
	Return EasyUnpack_Private($szFile, $recurs, $isEntryToSkipCallback, $eUnpacking)
EndFunc   ;==>EasyUnpack

;-------------------------------------------------------------------------------
; Name .........: EasyUnpack_Private
; Description ..:
; Return .......: true if anything was unpacked
; WARNING ......: if we use anything else than the
;                   $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT flag then
;                   there might be an ineffciency in the recursive process
;                   (directory unpacked once through rec - and deleted
;                   and unpacked a second time by the calling function)
;-------------------------------------------------------------------------------
Func EasyUnpack_Private($szFile, ByRef $recurs, $isEntryToSkipCallback = "", $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	RecursivelyListContentOfDirectory_SetDefaultCallback($isEntryToSkipCallback)
	If Not IsDirectory($szFile) Then

		; optimistation (well, sort of...)
		; -> don't decompress the same package twice
		If IsArray($recurs) And ($recurs[0] > 0) And (_ArraySearch($recurs, $szFile, 1) <> -1) Then Return True

		$tryUnpack = EasyUnpackFile($szFile, $eUnpacking)
		If $tryUnpack <> "" Then
			If IsArray($recurs) Then
				_ArrayAdd($recurs, $szFile)
				$recurs[0] += 1
				EasyUnpack_Private($tryUnpack, $recurs, $eUnpacking)
				Return True
			EndIf
		EndIf

	Else
		; list all the files and directories
		$arrayDirectories = _ArrayCreate(1, $szFile)
		$arrayFilenames = _ArrayCreate(0)
		$arrayFolders = _ArrayCreate(1, GetFolderNameFromPath($szFile))
		$arrayFiles = _ArrayCreate(0)
		RecursivelyListContentOfDirectoryV3($arrayDirectories, $arrayFiles, $arrayFolders, $arrayFilenames, $isEntryToSkipCallback)
		$isAnything = False
		For $i = 1 To $arrayFiles[0]
			$thatFile = $arrayFiles[$i]

			; optimistation (well, sort of... don't decompress twice the same package)
			If IsArray($recurs) And ($recurs[0] > 0) And (_ArraySearch($recurs, $thatFile, 1) <> -1) Then ContinueLoop

			$tryUnpack = EasyUnpackFile($thatFile, $eUnpacking)
			If $tryUnpack <> "" Then
				If IsArray($recurs) Then
					_ArrayAdd($recurs, $thatFile)
					$recurs[0] += 1
					EasyUnpack_Private($tryUnpack, $recurs, $eUnpacking)
				EndIf
				$isAnything = True
			EndIf

		Next
		Return $isAnything
	EndIf
	Return False
EndFunc   ;==>EasyUnpack_Private

;-------------------------------------------------------------------------------
; Name .........: EasyUnpackFile
; Description ..:
; Notes ........: might need to be split into EasyUnpackFile and
;                   EasyUnpackFileRes (via EasyUnpackFile_Private)
; Return .......: path to unpacked file - or empty string
;                  (NOTE: doesn't say whether unpacking was successful)
;-------------------------------------------------------------------------------
Func EasyUnpackFile($szFile, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT)
	$ret = ""
	$szFile_shortname = FileGetShortNameV2($szFile)
	If (Not FileExists($szFile_shortname)) Or IsDirectory($szFile_shortname) Then
		Return $ret
	Else
		$szFile_unpacked = $szFile & ".unpacked"
		$szFile_unpacked_shortname = FileGetShortNameV2($szFile_unpacked)

		; was this file already unpacked before?
		If Not FileExists($szFile_unpacked_shortname) Or $eUnpacking <> $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT Then

			; was this file already unpacked before (bis)?
			$last_unpacked = FileReadLine($szFile_unpacked_shortname & ".origin_info")
			$this_file_date = FileGetTimeV2($szFile_shortname, 0, $eFileGetTimeFormat_1)
			If $last_unpacked = "" Or $last_unpacked <> $this_file_date Then

				If FileExists($szFile_unpacked_shortname) Then DirRemove($szFile_unpacked_shortname)

				If StringEndsWith($szFile, ".dtb") Or StringEndsWith($szFile, ".tpl") Then
					$command_line = StringFormat('assima.stt\DtbRestore.exe "%s" /outdir:"%s.unpacked"', $szFile_shortname, $szFile_shortname)
					$ret = RunAndCheckFileExists($command_line, $szFile_shortname & ".unpacked", $szFile & ".unpacked")
				ElseIf StringEndsWith($szFile_shortname, ".zip") Then
					; !!! WARNING: WE USE THE 'e' OPTION RATHER THAN THE 'x' ONE !!!
					$command_line = StringFormat($7ZIP_EXEC & ' e "%s" -y -o"%s.unpacked"', $szFile_shortname, $szFile_shortname)
					$ret = RunAndCheckFileExists($command_line, $szFile_shortname & ".unpacked", $szFile & ".unpacked")
				EndIf

				If $ret <> "" Then
					; record last unpacking
					FileDelete($szFile_unpacked_shortname & ".origin_info")
					FileWriteLine($szFile_unpacked_shortname & ".origin_info", $this_file_date)
				EndIf

			EndIf

		EndIf
	EndIf
	Return $ret
EndFunc   ;==>EasyUnpackFile

;-------------------------------------------------------------------------------
; Name .........: Easy7Zip
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func Easy7Zip($fileOrDirSrc)
	Return Easy7ZipTo($fileOrDirSrc, Default)
EndFunc   ;==>Easy7Zip

;-------------------------------------------------------------------------------
; Name .........: Easy7ZipTo
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func Easy7ZipTo($fileOrDirSrc, $fileOrDirDst = "")
	FuncTrace("Easy7ZipDir", $fileOrDirSrc)
	While ( StringEndsWith($fileOrDirSrc, "\") Or StringEndsWith($fileOrDirSrc, "/"))
		$fileOrDirSrc = StringTrimRight($fileOrDirSrc, 1)
	WEnd
	$fileOrDirSrc_shortname = FileGetShortNameV2($fileOrDirSrc)
	$fileOrDirDst_shortname = $fileOrDirSrc_shortname & ".7z"
	If ($fileOrDirDst <> Default) Or ($fileOrDirDst <> "") Then
		$fileOrDirDst_shortname = FileGetShortNameV2($fileOrDirDst)
	EndIf
	$command_line = StringFormat($7ZIP_EXEC & ' a "%s" "%s"', $fileOrDirDst_shortname, $fileOrDirSrc_shortname)
	Return (RunAndCheckFileExists($command_line, $fileOrDirSrc_shortname, $fileOrDirSrc) <> "")
EndFunc   ;==>Easy7ZipTo

;-------------------------------------------------------------------------------
; Name .........: EasyUnZip
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func EasyUnZip($fileOrDirSrc)
	Return EasyUnZipTo($fileOrDirSrc, StringTrimRight($fileOrDirSrc, StringLen(".zip")))
EndFunc   ;==>EasyUnZip

;-------------------------------------------------------------------------------
; Name .........: EasyUnZipTo
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func EasyUnZipTo($fileOrDirSrc, $fileOrDirDst)
	Return EasyUnZipEltTo($fileOrDirSrc, $fileOrDirDst, "")
EndFunc   ;==>EasyUnZipTo

;-------------------------------------------------------------------------------
; Name .........: EasyUnZipEltTo
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func EasyUnZipEltTo($fileOrDirSrc, $fileOrDirDst, $subElt = "", $useDirStruct = 1)
;~ 	Return EasyUnZipTo($fileOrDirSrc, $fileOrDirDst)
	FuncTrace("EasyUnZipEltTo", $fileOrDirSrc, $fileOrDirDst, $subElt, $useDirStruct)
	While ( StringEndsWith($fileOrDirSrc, "\") Or StringEndsWith($fileOrDirSrc, "/"))
		$fileOrDirSrc = StringTrimRight($fileOrDirSrc, 1)
	WEnd
	$fileOrDirSrc_shortname = FileGetShortNameV2($fileOrDirSrc)
	$fileOrDirDst_shortname = $fileOrDirSrc_shortname & ".zip"
	If ($fileOrDirDst <> Default) Or ($fileOrDirDst <> "") Then
		$fileOrDirDst_shortname = FileGetShortNameV2($fileOrDirDst)
	EndIf
	$ccc = "x"
	If Not $useDirStruct Then $ccc = "e"
	$command_line = StringFormat($7ZIP_EXEC & ' ' & $ccc & ' "%s" -y -o"%s"', $fileOrDirSrc_shortname, $fileOrDirDst_shortname)
	If $subElt <> "" Then
		$command_line = StringFormat($7ZIP_EXEC & ' ' & $ccc & ' "%s" -y -o"%s" %s', $fileOrDirSrc_shortname, $fileOrDirDst_shortname, $subElt)
	EndIf
	$sub_exit_code = RunWait($command_line, "", @SW_HIDE)
	Return Not $sub_exit_code;(RunAndCheckFileExists($command_line, $fileOrDirSrc_shortname, $fileOrDirSrc) <> "")
EndFunc   ;==>EasyUnZipEltTo

;-------------------------------------------------------------------------------
; Name .........: EasyZip
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func EasyZip($fileOrDirSrc)
	Return EasyZipTo($fileOrDirSrc, Default)
EndFunc   ;==>EasyZip

;-------------------------------------------------------------------------------
; Name .........: EasyZipTo
; Description ..:
; Return .......: TRUE if succeeded - FALSE otherwise
;-------------------------------------------------------------------------------
Func EasyZipTo($fileOrDirSrc, $fileOrDirDst = "")
	FuncTrace("EasyZipTo", $fileOrDirSrc)
	While ( StringEndsWith($fileOrDirSrc, "\") Or StringEndsWith($fileOrDirSrc, "/"))
		$fileOrDirSrc = StringTrimRight($fileOrDirSrc, 1)
	WEnd
	$fileOrDirSrc_shortname = FileGetShortNameV2($fileOrDirSrc)
	$fileOrDirDst_shortname = $fileOrDirSrc_shortname & ".zip"
	If ($fileOrDirDst <> Default) Or ($fileOrDirDst <> "") Then
		$fileOrDirDst_shortname = FileGetShortNameV2($fileOrDirDst)
	EndIf
	$command_line = StringFormat($7ZIP_EXEC & ' a -tzip "%s" "%s"', $fileOrDirDst_shortname, $fileOrDirSrc_shortname)
	Return (RunAndCheckFileExists($command_line, $fileOrDirSrc_shortname, $fileOrDirSrc) <> "")
EndFunc   ;==>EasyZipTo

;-------------------------------------------------------------------------------
; Name .........: EasyZipDirContent
; Description ..: Zip the files inside of a directory and put them directly
;                 at the root of the archive
; Parameters ...: See AutoIT user function '_FileListToArray' for more info.
; Return .......: TRUE if succeeded - FALSE otherwise
; History ......: APR 09, 2009 - Created as an extension to 'EasyZipDir'
;-------------------------------------------------------------------------------
Func EasyZipDirContent($directory, $sFilter = "*", $iFlag = 0, $isUse7z = False)
	Return EasyZipDirContentTo($directory, $directory & ".zip", $sFilter, $iFlag, $isUse7z)
EndFunc   ;==>EasyZipDirContent

;-------------------------------------------------------------------------------
; Name .........: Easy7ZipDirContent
; Description ..: Zip the files inside of a directory and put them directly
;                 at the root of the archive
; Parameters ...: See AutoIT user function '_FileListToArray' for more info.
; Return .......: TRUE if succeeded - FALSE otherwise
; History ......: APR 09, 2009 - Created as an extension to 'EasyZipDir'
;-------------------------------------------------------------------------------
Func Easy7ZipDirContent($directory, $sFilter = "*", $iFlag = 0)
	Return EasyZipDirContentTo($directory, $directory & ".zip", $sFilter, $iFlag, True)
EndFunc   ;==>Easy7ZipDirContent

;-------------------------------------------------------------------------------
; Name .........: EasyZipDirContentTo
; Description ..: Zip the files inside of a directory and put them directly
;                 at the root of the archive
; Parameters ...: See AutoIT user function '_FileListToArray' for more info.
; Return .......: TRUE if succeeded - FALSE otherwise
; History ......: APR 09, 2009 - Created as an extension to 'EasyZipDir'
;-------------------------------------------------------------------------------
Func EasyZipDirContentTo($directory, $dst, $sFilter = "*", $iFlag = 0, $isUse7z = False)
	FuncTrace("EasyZipDirContent", $directory, $sFilter, $iFlag)
	While ( StringEndsWith($directory, "\") Or StringEndsWith($directory, "/"))
		$directory = StringTrimRight($directory, 1)
	WEnd
	$files = _FileListToArray($directory, $sFilter, $iFlag)
	If @error Then Return False
	$sum_sub_exit_code = 0
	For $fileIndex = 1 To $files[0]

		$format_opt = " -tzip"
		If $isUse7z Then $format_opt = ""
		If $isUse7z And StringEndsWith($dst, ".zip") Then $dst = StringTrimRight($dst, 4) & ".7z"
		$command_line = StringFormat($7ZIP_EXEC & ' a%s "%s" "%s"', $format_opt, $dst, $directory & "\" & $files[$fileIndex])
		$sub_exit_code = RunWait($command_line, "", @SW_HIDE)
		$sum_sub_exit_code += $sub_exit_code
		;-;ConsoleWrite("[" & $command_line & "]..." & @CRLF)
		;-;ConsoleWrite('     $sub_exit_code = ' & $sub_exit_code& @CRLF)

	Next
	Return Not $sub_exit_code ; WARNING: Some other programs actually use 'RunAndCheckFileExists' -- DEPRECATED or IMPROVED?
EndFunc   ;==>EasyZipDirContentTo
