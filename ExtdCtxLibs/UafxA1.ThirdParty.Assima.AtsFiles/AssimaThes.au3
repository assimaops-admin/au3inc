#include-once
#include <BaseUAFLibs\BaseLib\ProcessV2.au3>
#include <BaseUAFLibs\StdBaseLib\FileV2.au3>
#include <BaseUAFLibs\UDFsx\FileGetTimeV2.au3>
#include <ExtdBaseLibs\FilesPncLib\FilesPncConstants.au3>

Const $ACTION_SECTION_IN_SIMULATION = '[Private "Action,' ; eg. [Private "Action,0180"]
Const $ACTION_SECTION_IN_OLD_TUTORIAL = '[Private "Action,C' ; eg. [Private "Action,C17"]
Const $ACTION_SECTION_IN_NEW_TUTORIAL = '[Private "Graph,State:'

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_Decode
; Description ..: Run EasyDumpTo.bat in its own directory as working directory
; Note .........: Denormalize path when calling commands (REF.PS/40A4213F_1)
; Return .......: The path of the decoded file
;-------------------------------------------------------------------------------
Func AssimaThes_Decode($szFile_in, $szFile_out = Default)
	;FuncTrace("AssimaThes_Decode", $szFile_in, $szFile_out)

	$flag = 0

	If $szFile_out = Default Then $szFile_out = $szFile_in & ".tht"

	$ret = 0

	; file "in" and file "out"
	; NOTE: we don't fully normalize the path
	$szFile_original = NormalizePath($szFile_in, False, False)
	$szFile_decoded = NormalizePath($szFile_out, False, False)

	$szFile_original_shortname = FileGetShortNameV2($szFile_original)
	$szFile_decoded_shortname = FileGetShortNameV2($szFile_decoded)

	; was this file already unpacked before?
	If $flag And FileExists($szFile_decoded_shortname) Then FileDelete($szFile_decoded_shortname)
	If Not $flag And FileExists($szFile_decoded_shortname) Then Return $ret

	; dump thesaurus to text file
	$command_line = StringFormat('assima.stt\dumpw.exe /cptextfile:65001 /width:1020 "%s" "%s"' _
			, $szFile_original_shortname _
			, $szFile_decoded_shortname)
	RunWait($command_line, "", @SW_HIDE)

	$ret = FileExists($szFile_decoded_shortname)
	If $ret Then $ret = $szFile_out
	Return $ret

EndFunc   ;==>AssimaThes_Decode

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_DecodeV2
; Description ..: Decode thesaurus and write file time info
;                 (for optimisation purpose)
; Note .........: Denormalize path when calling commands (REF.PS/40A4213F_1)
; Return .......: The path of the decoded file
;-------------------------------------------------------------------------------
Func AssimaThes_DecodeV2($szFile_in, $eUnpacking = $COMPARATOR_KEEP_UNPACKED_CONTENT, $szFile_out = Default)
	;FuncTrace("AssimaThes_DecodeV2", $szFile_in, $eUnpacking, $szFile_out)

	If $szFile_out = Default Then $szFile_out = $szFile_in & ".tht"

	; file "in" and file "out"
	; NOTE: we don't fully normalize the path
	$szFile_original = NormalizePath($szFile_in, False, False)
	$szFile_decoded = NormalizePath($szFile_out, False, False)

	$szFile_original_shortname = FileGetShortNameV2($szFile_original)
	$szFile_decoded_shortname = FileGetShortNameV2($szFile_decoded)

	; was this file already unpacked before?
	If Not FileExists($szFile_decoded_shortname) Or $eUnpacking <> $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT Then
		; was this file already unpacked before (bis)?
		$last_decoded = FileReadLine($szFile_decoded_shortname & ".origin_info")
		$this_file_date = FileGetTimeV2($szFile_original_shortname, 0, $eFileGetTimeFormat_1)
		If $last_decoded = "" Or $last_decoded <> $this_file_date Then
			; dump thesaurus to text file
			$command_line = StringFormat('assima.stt\dumpw.exe /cptextfile:65001 /width:1020 "%s" "%s"' _
					, $szFile_original_shortname _
					, $szFile_decoded_shortname)
			RunAndCheckFileExists($command_line, $szFile_decoded_shortname, $szFile_decoded)
			; record last dump
			FileDelete($szFile_decoded_shortname & ".origin_info")
			FileWriteLine($szFile_decoded_shortname & ".origin_info", $this_file_date)
		EndIf
	EndIf

	Return $szFile_decoded

EndFunc   ;==>AssimaThes_DecodeV2

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_Read ( ReadAssimaThesaurus )
; Description ..:
; Note .........:
; Return .......: String - The decoded content of the thesaurus
;-------------------------------------------------------------------------------
Func AssimaThes_Read($szFile_in)
	$thtPath = _TempFile()
	AssimaThes_Decode($szFile_in, $thtPath)
	$ret = FileRead($thtPath)
	FileDelete($thtPath)
	Return $ret
EndFunc   ;==>AssimaThes_Read

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_Write ( WriteAssimaThesaurus )
; Description ..:
; Note .........:
; Return .......: String - The decoded content of the thesaurus
;-------------------------------------------------------------------------------
Func AssimaThes_Write($szFile_out, $szFileContent)
	$thtPath = _TempFile()
	FileWrite($thtPath, $szFileContent)
	AssimaThes_Encode($thtPath, $szFile_out)
	$ret = FileRead($thtPath)
	FileDelete($thtPath)
	Return $ret
EndFunc   ;==>AssimaThes_Write

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_Encode
; Description ..: Run EasyCompile.bat in its own directory as working directory
; Return .......:
;-------------------------------------------------------------------------------
Func AssimaThes_Encode($szFile_in, $szFile_out = Default)
	;FuncTrace("AssimaThes_Encode", $szFile_in, $szFile_out)

	If $szFile_out = Default Then
		If StringEndsWith($szFile_in, ".tht") Or StringEndsWith($szFile_in, ".txt") Or StringEndsWith($szFile_in, ".tmp") Then
			MsgBox(48, "Fatal Error", 'StringEndsWith($szFile_in, ".tht") Or StringEndsWith($szFile_in, ".txt") Or StringEndsWith($szFile_in, ".tmp")')
			Exit 1
		EndIf
		$szFile_out = StringTrimRight($szFile_in, StringLen(".tht"))
	EndIf

	; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
	$szFile_original = NormalizePath($szFile_in, False, False)
	$szFile_encoded = NormalizePath($szFile_out, False, False)

	$szFile_original_shortname = FileGetShortNameV2($szFile_original)
	$szFile_encoded_shortname = FileGetShortNameV2($szFile_encoded)

	; encode thesaurus
	$command_line = StringFormat('assima.stt\compilew.exe /cptextfile:65001 "%s" "%s"', $szFile_original_shortname, $szFile_encoded_shortname)
	Return RunAndCheckFileExists($command_line, $szFile_encoded_shortname, $szFile_encoded)

EndFunc   ;==>AssimaThes_Encode

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_GetScreenCountFromSim
; Description ..: Count number of actions in the simulation
; Return .......: Number of screens in the simulation
;-------------------------------------------------------------------------------
Func AssimaThes_GetScreenCountFromSim($simPath)
	;FuncTrace("AssimaThes_GetScreenCountFromSim", $simPath)
	$simContent = AssimaThes_Read($simPath)
	Return StringInStrCount($simContent, $ACTION_SECTION_IN_SIMULATION)
EndFunc   ;==>AssimaThes_GetScreenCountFromSim

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_GetScreenCountFromTut
; Description ..: Count number of actions in the lesson
; Return .......: Number of screens in the lesson
;-------------------------------------------------------------------------------
Func AssimaThes_GetScreenCountFromTut($tutPath)
	;FuncTrace("AssimaThes_GetScreenCountFromTut", $tutPath)
	$tutContent = AssimaThes_Read($tutPath)
	$sectionFormat = $ACTION_SECTION_IN_NEW_TUTORIAL
	If AssimaThes_IsOldTutFormat($tutPath) Then
		$sectionFormat = $ACTION_SECTION_IN_OLD_TUTORIAL
	EndIf
	Return StringInStrCount($tutContent, $sectionFormat)
EndFunc   ;==>AssimaThes_GetScreenCountFromTut

;-------------------------------------------------------------------------------
; Name .........: AssimaThes_IsOldTutFormat
; Description ..: n/a
; Return .......: Boolean
;-------------------------------------------------------------------------------
Func AssimaThes_IsOldTutFormat($tutPath)
	Return StringEndsWith($tutPath, "_t.the")
EndFunc   ;==>AssimaThes_IsOldTutFormat
