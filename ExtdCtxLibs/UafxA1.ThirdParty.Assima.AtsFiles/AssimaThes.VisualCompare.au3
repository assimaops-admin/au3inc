#include-once
#include ".\AssimaThes.au3"

;-------------------------------------------------------------------------------
; Name .........: AssimaThesEx_VisualCompare
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func AssimaThesEx_VisualCompare($REF_LESSON, $OUT_LESSON)
	FileDelete($REF_LESSON & ".tht")
	FileDelete($OUT_LESSON & ".tht")
	$decoded1 = FileGetShortName(AssimaThes_Decode($REF_LESSON))
	$decoded2 = FileGetShortName(AssimaThes_Decode($OUT_LESSON))
	$compProg = FileGetShortName(EnvGet("DEFAULT_FILE_COMPARISON_PROGRAM"))
	Return RunWait($compProg & " " & $decoded1 & " " & $decoded2)
EndFunc   ;==>AssimaThesEx_VisualCompare
