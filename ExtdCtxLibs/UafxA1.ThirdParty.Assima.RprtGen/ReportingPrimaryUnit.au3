#include-once
#include ".\ReportingUnit.au3"

Const $MAIN_PRIMARY_REPORTING_SUB_SCRIPT_NAME_NX = 'ReportingVx.Sub1'

Const $RPRT_CODE_PRISTINE = 0
Const $RPRT_CODE_DIFFS = 1
Const $RPRT_CODE_NOREF = 2
Const $RPRT_CODE_INVALID_L2 = 4
Const $RPRT_CODE_LOCKED = 8
Const $RPRT_CODE_ERROR = 16
Const $RPRT_CODE_INVALID_L3 = 32
Const $RPRT_CODE_NORPRT = 64


;-------------------------------------------------------------------------------
; Name .........: ReportingPrimaryUnit_MainSubUnit
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingPrimaryUnit_MainSubUnit($targetPath)
	$allTMOs = HostInfo_GetDataDir()
	$err_msg = ReportingPrimaryUnit_GenerateTMODiffReport($allTMOs, $targetPath)
	If IsString($err_msg) And $err_msg <> "" Then
		ReportingUnit_SetAsNotValid($targetPath, $err_msg)
		Return False
	Else
		Return True
	EndIf
EndFunc   ;==>ReportingPrimaryUnit_MainSubUnit

;-------------------------------------------------------------------------------
; Name .........: ReportingPrimaryUnit_GenerateTMODiffReport
; Description ..: Generate the report
; Preconditions : IsProcessingToken must return false for '$szResultRepositoryPath_root'.
; Return .......: empty string "" if it's all good
;                 - or - error message
;                 (Caller should use 'ReportingUnit_SetAsNotValid' upon error.)
;-------------------------------------------------------------------------------
Func ReportingPrimaryUnit_GenerateTMODiffReport($allTMOs, $szResultRepositoryPath_root)

	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE

	; Generic

	Const $TIME_TRACE_ROUND_PRECISION_IN_SECONDS = 1

	$globalTimer = TimerInit()

	$err_msg = ""
	$err_tag = ""

	; DISABLED MAR 19, 2015
	; exception
	;If Not FileExists($szResultRepositoryPath_conf) Then
	;	$err_msg = "Missing '" & $MAIN_REPORTING_INFO_FILE & "' configuration file."
	;	Return $err_msg
	;EndIf

	; flag directory as being used
	; WARNING: an exception could be raised here!
	SetProcessingToken($szResultRepositoryPath_root, 1)

	; delete previous bold report
	If FileExists($szResultRepositoryPath_root & "\Reporting") Then
		DirRemove($szResultRepositoryPath_root & "\Reporting", 1)
	EndIf

	#cs
		; minor profile and script name
		$minorProfile = TMOInfoIni_GetMinorProfile($szResultRepositoryPath_conf, $DEFAULT_PLATFORM_PROFILE)
		$testModuleName = TMOInfoIni_GetTestModuleName($szResultRepositoryPath_conf)
		$testModuleGuid = TMOInfoIni_GetTestModuleGuid($szResultRepositoryPath_conf)
		ValTrace($testModuleGuid, '$testModuleGuid')
		If $testModuleGuid = "" Then $testModuleGuid = $testModuleName
		ValTrace($testModuleGuid, '$testModuleGuid')
		If $testModuleGuid = "--tmguid=" Then $testModuleGuid = $testModuleName ; Quick bug fix - this line of code can be removed in *very* near future - REF.PS150123-1
		ValTrace($testModuleGuid, '$testModuleGuid')
		If $testModuleGuid = "0" Then $testModuleGuid = $testModuleName ; Quick bug fix - this line of code can be removed in *very* near future - REF.PS150123-1
		ValTrace($testModuleGuid, '$testModuleGuid')

		; data-oriented testing implementation(s)
		; RUN-TIME MODIFICATION
		$testModuleGuid = ReportingUnit_HackTmoGuid($testModuleGuid)
		ValTrace($testModuleGuid, '$testModuleGuid')

		; read other info in the ini file
		$scriptParams = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "ScriptParams", "")
		$buildUTID = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "BuildUTID", "")
		$productUTID = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "ProductUTID", "")

		; set environmment variable
		ValTrace($testModuleGuid, '$testModuleGuid')
		EnvSet("@TestModuleProc", $testModuleGuid)
		ValTrace(EnvGet("@TestModuleProc"), 'EnvGet("@TestModuleProc")')

		; where do we search the references?
		$szAllRefs &= "\" & $minorProfile
		ValTrace($szAllRefs, '$szAllRefs')

	#ce

	; get the name of the directory containing automation results
	$szResultRepositoryPath_key = FileReadLine($szResultRepositoryPath_root & "\key.txt")
	$szResultRepositoryPath_data = $szResultRepositoryPath_root & ".txt"

	#cs
		; get the name of the directory containing automation results
		$szResultRepositoryPath_data = $szResultRepositoryPath_root
		$szResultRepositoryPath_data &= "\Automation"
	#ce

	; write diff-report version and get report location
	$szResultRepositoryPath_rdir = ReportingUnit_SetReportingVersion($szResultRepositoryPath_root, $MAIN_REPORTING_CURRENT_VERSION)
	$reportCount = ReportingUnit_IncrementVistCount($szResultRepositoryPath_root, $MAIN_REPORTING_CURRENT_VERSION)

	#cs
		If Not StringInStr(FileRead($szAllRefs & "\All_Refs.txt"), $testModuleGuid & @CRLF) Then

			FileWrite($szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_NO_REF_TOKEN, "") ; <<< ORIGINAL VERSION
			FileWrite($szResultRepositoryPath_root & "\" & $MAIN_REPORTING_NO_REF_TOKEN, "") ; <<< ADDED 2016

			$err_tag = $MAIN_REPORTING_NO_REF_TAG

			; exit sequence (REF.PS/20100821_174719)
			If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
			ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ReportFormatVersion", $MAIN_REPORTING_CURRENT_VERSION)
			ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ReportSubFormatVersion", $MAIN_REPORTING_CURRENT_SUB_VERSION)
			SetProcessingToken($szResultRepositoryPath_root, 0)

			Return $err_msg ; <<< EARLY EXIT

		EndIf
	#ce

	#cs
		If IsProcessingToken($szAllRefs & "\" & $testModuleGuid) Then

			; - NOP -
			; TODO: notify something... ?

			$err_tag = "ProcessingTokenExists"

			; exit sequence (REF.PS/20100821_174719)
			If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
			SetProcessingToken($szResultRepositoryPath_root, 0)
			Return $err_msg

		EndIf
	#ce

	#cs
		; get the name of the directory containing automation reference
		$szRefRepositoryPath = $szAllRefs & "\" & $testModuleGuid
		$szRefRepositoryPath &= "\Automation"

		ReportingUnit_SetInfo($szResultRepositoryPath_root, "RefRepositoryPath", $szRefRepositoryPath)
		ReportingUnit_SetInfo($szResultRepositoryPath_root, "ResRepositoryPath", $szResultRepositoryPath_data)
		ReportingUnit_SetInfo($szResultRepositoryPath_root, "NormRefRepositoryPath", NormalizePath($szRefRepositoryPath, True))
		ReportingUnit_SetInfo($szResultRepositoryPath_root, "NormResRepositoryPath", NormalizePath($szResultRepositoryPath_data, True))
	#ce

	; Get reference repository and compare to the actual result
	; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
	; HISTORY:
	; 2008-10-27: 'EasyCompareDirectories' was initially called with
	;          $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT parameter;
	;          we don't need this anymore because every '.the' file
	;          is now updated at the same time than its '.the.tht.filtered'
	;          counterpart.
	; 2010-02-09: Unpack '.zip' and '.dtb' files before comparing directories
	;          (do this recursively + reuse unpacked content)
	;
;~ EasyUnpackRec($szRefRepositoryPath, $isEntryToSkipWhenComparingRepositoriesCallback, $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT)
;~ EasyUnpackRec($szResultRepositoryPath_data, $isEntryToSkipWhenComparingRepositoriesCallback, $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT)

	#cs
		$timerInit = TimerInit()
		$timerName = "EasyUnpackRecRef-Timer"
		EasyUnpackRec($szRefRepositoryPath)
		ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

		$timerInit = TimerInit()
		$timerName = "EasyUnpackRecRes-Timer"
		EasyUnpackRec($szResultRepositoryPath_data)
		ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))
	#ce

	; TODO: EasyExchangeAltOuts($szRefRepositoryPath, $szResultRepositoryPath_data) -- Or diff on a dir-by-dir basis

	#cs
		$timerInit = TimerInit()
		$timerName = "EasyCompareDirectories-Timer"
		$Differences = EasyCompareDirectories( _
				$szRefRepositoryPath _
				, $szResultRepositoryPath_data _
				, ReportingCallbacks_GetIsEntryToSkipCallback($scriptParams) _
				)
		ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))
	#ce

	$timerInit = TimerInit()
	$timerName = "EasyAggregDiffs-Timer"
	$Differences = EasyAggregDiffs($allTMOs, $szResultRepositoryPath_root, $szResultRepositoryPath_data, $szResultRepositoryPath_rdir, $szResultRepositoryPath_key)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

	#cs
		; display differences if any
		;;$timerName = "ReportWriting-Timer"
		;;$timerInit = TimerInit()
		If $Differences = 0 Then
			; An error occured
			; NOTE: a warning is likely to have been generated in 'EasyCompareDirectories'
			;       so we might as well remove this message... :P
			$err_tag = "EasyCompareDirectoriesFail"
			$err_msg = "An error occured when comparing '" & $szResultRepositoryPath_data & "' to '" & $szRefRepositoryPath & "' - check paths."
		ElseIf $Differences = "" Then
			; No differences found
			; NOP
		ElseIf IsArray($Differences) Then
			$szGatheredMessages = ""

			$storage = $szResultRepositoryPath_rdir & "\Differences.chunks.raw"
			If Not FileExists($storage) Then DirCreate($storage)
			ConvertArrayOfDifferencesToChunks($Differences, $storage, $szRefRepositoryPath, $szResultRepositoryPath_data, 0)

			$storage = $szResultRepositoryPath_rdir & "\Differences.chunks.html"
			If Not FileExists($storage) Then DirCreate($storage)
			ConvertArrayOfDifferencesToChunks($Differences, $storage, $szRefRepositoryPath, $szResultRepositoryPath_data, 1)

			$nResult = ConvertArrayOfDifferencesToString($Differences, $szGatheredMessages, $szRefRepositoryPath, $szResultRepositoryPath_data)
			If $szGatheredMessages <> "" Then
				; last bold report
				$reportPath = $szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_REPORT_FILENAME
				If FileExists($reportPath) Then
					If Not FileMove($reportPath, $reportPath & ".back." & MyYEAR() & MyMON() & "-" & MyMDAY() & "-" & MyHOUR() & "-" & MyMIN() & MySEC()) Then
						$err_tag = "FileReplaceFail"
						$err_msg = "Impossible to move '" & $reportPath & "' file."
					EndIf
				EndIf
				; write
				If $err_tag = "" Then
					$reportHandle = FileOpen($reportPath, 1 + 256)
					FileWrite($reportHandle, $szGatheredMessages)
					FileClose($reportHandle)
				EndIf
			EndIf
		Else
			$err_tag = "EasyCompareDirectoriesUnexpectedRet"
			$err_msg = "'EasyCompareDirectories' returned unexpected value."
		EndIf
		;;ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit)/1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))
	#ce

	; exit sequence (REF.PS/20100821_174719)
	If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "GlobalTimer", Round(TimerDiff($globalTimer) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))
	SetProcessingToken($szResultRepositoryPath_root, 0)
	Return $err_msg

EndFunc   ;==>ReportingPrimaryUnit_GenerateTMODiffReport

Func EasyAggregDiffs($allTMOs, $sessionRootDir, $sessionIndexFile, $sessionRprtDir, $sessionKey)

	Dim $tmosList

	If Not _FileReadToArray($sessionIndexFile, $tmosList) Then
		Return ; <<< Early exit !!!
	EndIf

	; Define special processing sequence
	; REMARK: replace user-defined, use default.
	$items_list = $tmosList
	$start_mod = 1
	$end_mod = -1

	$total_count = $items_list[0]
	$start = ModV2($start_mod, $total_count, 1)
	$end = ModV2($end_mod, $total_count, 1)
	$step = QuestionMark($start <= $end, 1, -1)

	; Process reports
	ConsoleWrite('LoopFor()...' & @CRLF)
	For $i = $start To $end Step $step
		$targetName = $items_list[$i]
		$targetPath = $allTMOs & "\" & $targetName
		ConsoleWrite('... $i = ' & $i & @CRLF)
		ConsoleWrite('... $targetPath = ' & $targetPath & @CRLF)
		;If StringStartsWith($targetName, $SECONDARY_LEVEL_ENTRY_SUBSTR_PREFIX) _
		;	And FileExists($targetPath) And IsDirectory($targetPath) Then
		If FileExists($targetPath) And IsDirectory($targetPath) Then
			;ConsoleWrite('... ... HERE (1/2): StringStartsWith($targetName, "TMO-") And IsDirectory($targetPath)' & @CRLF)
			;$targetStamp = StringRight($targetName, StringLen("XXXXXX-XX-XX-XXXX"))
			;$ageInDays = TimestampV2_GetAgeInDays($targetStamp)
			$rprtVersn = ReportingUnit_GetReportVersion($targetPath)
			;ConsoleWrite('... $ageInDays = ' & $ageInDays & @CRLF)
			ConsoleWrite('... $rprtVersn = ' & $rprtVersn & @CRLF)
			;If (($ageInDays <= $ACTUAL_MAX_PROCESSING_DAYS) Or $IS_DEV_COMP) And $rprtVersn = 0 Then

			$rprtCode = $RPRT_CODE_PRISTINE

			If $rprtVersn = 0 Then

				; no reporting has run,
				; or the directory was deleted
				$rprtCode += $RPRT_CODE_NORPRT

			ElseIf $rprtVersn = 1 Then

				; the repository was already flagged as invalid
				; during the secondary processing
				$rprtCode += $RPRT_CODE_INVALID_L2

			Else

				$testModuleGuid = TMOInfoIni_GetTestModuleGuid($targetPath & "\" & $MAIN_REPORTING_INFO_FILE)
				ConsoleWrite('... ... $testModuleGuid = ' & $testModuleGuid & @CRLF)

				; Post-process the first reporting
				; deal with Locked, Invalid and NoRefs results first
				; + check that the result was properly generated
				$diffFilePath = $targetPath & "\" & $MAIN_REPORTING_SUB_DIRECTORY & "\" & $MAIN_REPORTING_REPORT_FILENAME
				If False Then
					; placeholder for Locked TMOs
					; TODO: find out when this happens ? ...
					$rprtCode += $RPRT_CODE_LOCKED
				EndIf
				If FileExists($targetPath & "\" & $MAIN_REPORTING_NO_REF_TOKEN) _
					Or FileExists($targetPath & "\" & $MAIN_REPORTING_SUB_DIRECTORY & "\" & $MAIN_REPORTING_NO_REF_TOKEN) _
					Then
					; reference file is missing, there was no differences to compute
					$rprtCode += $RPRT_CODE_NOREF
				EndIf
				If FileExists($targetPath & "\" & $MAIN_REPORTING_INVALIDITY_TOKEN) Then
					; there was a serious problem, or a crash during reporting ?
					$rprtCode += $RPRT_CODE_INVALID_L3
				EndIf

				; here, we either already noticed a problem,
				; or the report is available for processing
				If Not $rprtCode Then
					If Not FileExists($diffFilePath) Then
						; missing results file, without any valid reason
						$rprtCode += $RPRT_CODE_ERROR
					Else
						; Read differences
						$diffFileContent = FileRead($diffFilePath)
						; Aggregate differences
						FileWriteLine($sessionRprtDir & "\" & $MAIN_REPORTING_REPORT_FILENAME, '<DiffPrimaryEntry id="' & $targetName & '">')
						FileWrite($sessionRprtDir & "\" & $MAIN_REPORTING_REPORT_FILENAME, $diffFileContent)
						FileWriteLine($sessionRprtDir & "\" & $MAIN_REPORTING_REPORT_FILENAME, '</DiffPrimaryEntry>')
						; Process differences
						If ($diffFileContent = "") Or ($diffFileContent = @CRLF) Or ($diffFileContent = @CRLF & @CRLF) Then
							; there was no differences between Ref. and Res.
							; => no operation
						Else
							; report differences
							$rprtCode += $RPRT_CODE_DIFFS
						EndIf
					EndIf
				EndIf

			EndIf


			; ---------------------------------------------------------------
			; TODO: Associate the automation exit code by extracting it from the file
			; Dim $trlList
			; _FileReadToArray($rprtCode & "\" & $sessionKey & ".trl", $trlList)
			; ---------------------------------------------------------------

			ConsoleWrite('... ... $rprtCode = ' & $rprtCode & @CRLF)
			; Write the reporting code (diffs or no diffs?)
			FileWriteLine($sessionRootDir & "\" & $sessionKey & ".trl2", $rprtCode & @TAB & '"' & $testModuleGuid & '"')


			;Sleep(3000) ; leave some time so that others may generate a report too (REF.PS/20111108_223803)

		EndIf

	Next

EndFunc
