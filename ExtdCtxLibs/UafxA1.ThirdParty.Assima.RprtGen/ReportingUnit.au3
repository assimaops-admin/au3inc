#include-once
#include <CtxConf\HostInfo.Lite.au3>
#include <ExtdCoreLibs\RepoMgtLib\ProcessingToken.au3>
#include <ExtdCtxLibs\UafxA1.ThirdParty.Assima.DiffTool\EasyCompareFiles.au3>
#include <ExtdCtxLibs\UafxA1.ThirdParty.Assima.DiffTool\ReportingCallbacks.au3>
#include "..\UafxA1.EasyPack.au3"
#include ".\ReportingUnit.HackGuid.au3"
#include ".\TMOInfoIni.Read.au3"

; meta
Const $MAIN_REPORTING_SUB_SCRIPT_NAME_NX = 'ReportingVx.Sub'

; constants
Const $MAIN_REPORTING_CURRENT_VERSION = 2
Const $MAIN_REPORTING_CURRENT_SUB_VERSION = 2
Const $MAIN_REPORTING_INFO_FILE = 'TMOInfo.ini'
Const $MAIN_REPORTING_INVALIDITY_TOKEN = 'Invalid.txt'
Const $MAIN_REPORTING_NO_REF_TAG = 'RefNotFound'
Const $MAIN_REPORTING_NO_REF_TOKEN = 'RefNotFound.txt'
Const $MAIN_REPORTING_SUB_DIRECTORY = 'Reporting\Bold'
Const $MAIN_REPORTING_REPORT_FILENAME = 'Differences.xml'

; prototype
Const $MAIN_REPORTING_RAW_CHUNKS_DIR = 'Differences.chunks.raw'
Const $MAIN_REPORTING_HTM_CHUNKS_DIR = 'Differences.chunks.html'


;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_MainSubUnit
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingUnit_MainSubUnit($targetPath)
	$allRefs = HostInfo_GetBoldDir()
	$err_msg = ReportingUnit_GenerateTMODiffReport($allRefs, $targetPath)
	If IsString($err_msg) And $err_msg <> "" Then
		Trace(@TAB & @TAB & "INVALID REPORT.")
		ReportingUnit_SetAsNotValid($targetPath, $err_msg)
		Return False
	Else
		Trace(@TAB & @TAB & "VALID REPORT, OK.")
		Return True
	EndIf
EndFunc   ;==>ReportingUnit_MainSubUnit

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_SetReportingVersion
; Description ..:
; Return .......: outputs directory - or die
;-------------------------------------------------------------------------------
Func ReportingUnit_SetReportingVersion($szResultRepositoryPath_root, $version)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	$szResultRepositoryPath_rdir = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_SUB_DIRECTORY
	; Generic
	$ret = True
	If Not FileExists($szResultRepositoryPath_rdir) Then $ret = DirCreate($szResultRepositoryPath_rdir)
	If $ret Then $ret = IniWrite($szResultRepositoryPath_conf, "ReportingInfo(Bold)", "ReportVersion", $version)
	If Not $ret Then FatalError_PostCondition("ReportingUnit_SetReportingVersion", $szResultRepositoryPath_root)
	If $ret Then Return $szResultRepositoryPath_rdir
	If Not $ret Then Return ""
EndFunc   ;==>ReportingUnit_SetReportingVersion

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_SetInfo
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ReportingUnit_SetInfo($szResultRepositoryPath_root, $key, $value)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	; Generic
	Return IniWrite($szResultRepositoryPath_conf, "ReportingInfo(Bold)", $key, $value)
EndFunc   ;==>ReportingUnit_SetInfo

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_SetExtraInfo
; Description ..:
; Return .......: n/a
;-------------------------------------------------------------------------------
Func ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $key, $value)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	; Generic
	$iniSection = StringFormat("ReportingInfo(Bold)/R%02d", $reportCount)
	Return IniWrite($szResultRepositoryPath_conf, $iniSection, $key, $value)
EndFunc   ;==>ReportingUnit_SetExtraInfo

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_GetReportVersion
; Description ..:
; Return .......: the version number or '0' if not set
;-------------------------------------------------------------------------------
Func ReportingUnit_GetReportVersion($szResultRepositoryPath_root)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	; Generic
	Const $MISSING_REPORT_VERSION = 0
	Const $INVALID_OUTPUT_VERSION = 1
	If FileExists($szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INVALIDITY_TOKEN) Then Return $INVALID_OUTPUT_VERSION
	; Retrieve the version
	$version = IniRead($szResultRepositoryPath_conf, "ReportingInfo(Bold)", "ReportVersion", $MISSING_REPORT_VERSION)
	; If the version is an invalid string...
	If Not StringIsInt($version) Then FatalError_PreCondition("ReportingUnit_GetVistCount")
	; If the report was deleted...
	If Not FileExists($szResultRepositoryPath_root & "\" & $MAIN_REPORTING_SUB_DIRECTORY) Then Return $MISSING_REPORT_VERSION
	; Otherwise...
	Return Int($version)
EndFunc   ;==>ReportingUnit_GetReportVersion

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_IncrementVistCount
; Description ..:
; Return .......: true - or die
;-------------------------------------------------------------------------------
Func ReportingUnit_IncrementVistCount($szResultRepositoryPath_root, $version)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	; Generic
	$count = IniRead($szResultRepositoryPath_conf, "ReportingInfo(Bold)", "ReportCount", "0")
	If Not StringIsInt($count) Then $count = 0
	$count = Int($count) + 1
	$ret = IniWrite($szResultRepositoryPath_conf, "ReportingInfo(Bold)", "ReportCount", $count)
	If Not $ret Then FatalError_PostCondition("ReportingUnit_IncrementVistCount", $szResultRepositoryPath_root)
	Return $count
EndFunc   ;==>ReportingUnit_IncrementVistCount

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_GetVistCount
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingUnit_GetVistCount($szResultRepositoryPath_root, $version)
	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE
	; Generic
	$count = IniRead($szResultRepositoryPath_conf, "ReportingInfo(Bold)", "ReportCount", "0")
	If Not StringIsInt($count) Then FatalError_PreCondition("ReportingUnit_GetVistCount")
	Return Int($count)
EndFunc   ;==>ReportingUnit_GetVistCount

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_SetAsNotValid
; Description ..:
; Parameters ...: $szReason - set to '0' to be quiet (no warning)
;                           - set to empty string to use the default warning
;                           - set to any string to add extra information
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingUnit_SetAsNotValid($szResultRepositoryPath_root, $szReason = "")
	If $szReason <> 0 Then
		$szWarning = "Invalidating TMO '" & $szResultRepositoryPath_root & "'..."
		If $szReason <> "" Then $szWarning &= @CRLF & "(" & $szReason & ")"
		Warning($szWarning)
	EndIf
	FileWrite($szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INVALIDITY_TOKEN, $szReason)
EndFunc   ;==>ReportingUnit_SetAsNotValid

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_GenerateTMODiffReport
; Description ..: Generate the report
; Preconditions : IsProcessingToken must return false for '$szResultRepositoryPath_root'.
; Return .......: empty string "" if it's all good
;                 - or - error message
;                 (Caller should use 'ReportingUnit_SetAsNotValid' upon error.)
;-------------------------------------------------------------------------------
Func ReportingUnit_GenerateTMODiffReport($szAllRefs, $szResultRepositoryPath_root)

	; Custom
	$szResultRepositoryPath_conf = $szResultRepositoryPath_root & "\" & $MAIN_REPORTING_INFO_FILE

	; Generic

	Const $TIME_TRACE_ROUND_PRECISION_IN_SECONDS = 1

	$globalTimer = TimerInit()

	$err_msg = ""
	$err_tag = ""

	; DISABLED MAR 19, 2015
	; exception
	;If Not FileExists($szResultRepositoryPath_conf) Then
	;	$err_msg = "Missing '" & $MAIN_REPORTING_INFO_FILE & "' configuration file."
	;	Return $err_msg
	;EndIf

	; flag directory as being used
	; WARNING: an exception could be raised here!
	SetProcessingToken($szResultRepositoryPath_root, 1)

	; delete previous bold report
	If FileExists($szResultRepositoryPath_root & "\Reporting") Then
		DirRemove($szResultRepositoryPath_root & "\Reporting", 1)
	EndIf

	; minor profile and script name
	$minorProfile = TMOInfoIni_GetMinorProfile($szResultRepositoryPath_conf, $DEFAULT_PLATFORM_PROFILE)
	$testModuleName = TMOInfoIni_GetTestModuleName($szResultRepositoryPath_conf)
	$testModuleGuid = TMOInfoIni_GetTestModuleGuid($szResultRepositoryPath_conf)
	ValTrace($testModuleGuid, '$testModuleGuid')
	If $testModuleGuid = "" Then $testModuleGuid = $testModuleName
	ValTrace($testModuleGuid, '$testModuleGuid')
	If $testModuleGuid = "--tmguid=" Then $testModuleGuid = $testModuleName ; Quick bug fix - this line of code can be removed in *very* near future - REF.PS150123-1
	ValTrace($testModuleGuid, '$testModuleGuid')
	If $testModuleGuid = "0" Then $testModuleGuid = $testModuleName ; Quick bug fix - this line of code can be removed in *very* near future - REF.PS150123-1
	ValTrace($testModuleGuid, '$testModuleGuid')

	; data-oriented testing implementation(s)
	; RUN-TIME MODIFICATION
	$testModuleGuid = ReportingUnit_HackTmoGuid($testModuleGuid)
	ValTrace($testModuleGuid, '$testModuleGuid')

	; read other info in the ini file
	$scriptParams = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "ScriptParams", "")
	$buildUTID = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "BuildUTID", "")
	$productUTID = IniRead($szResultRepositoryPath_conf, "AutomationInfo", "ProductUTID", "")

	; set environmment variable
	ValTrace($testModuleGuid, '$testModuleGuid')
	EnvSet("@TestModuleProc", $testModuleGuid)
	ValTrace(EnvGet("@TestModuleProc"), 'EnvGet("@TestModuleProc")')

	; where do we search the references?
	$szAllRefs &= "\" & $minorProfile
	ValTrace($szAllRefs, '$szAllRefs')

	; get the name of the directory containing automation results
	;$szResultRepositoryPath_key = FileReadLine($szResultRepositoryPath_root & "\key.txt") ; <<< EITHER NOT IMPLEMENTATED OR NOT USED
	$szResultRepositoryPath_data = $szResultRepositoryPath_root & "\Automation"

	; write diff-report version and get report location
	$szResultRepositoryPath_rdir = ReportingUnit_SetReportingVersion($szResultRepositoryPath_root, $MAIN_REPORTING_CURRENT_VERSION)
	$reportCount = ReportingUnit_IncrementVistCount($szResultRepositoryPath_root, $MAIN_REPORTING_CURRENT_VERSION)

	If Not StringInStr(FileRead($szAllRefs & "\All_Refs.txt"), $testModuleGuid & @CRLF) Then

		FileWrite($szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_NO_REF_TOKEN, "") ; <<< ORIGINAL VERSION
		FileWrite($szResultRepositoryPath_root & "\" & $MAIN_REPORTING_NO_REF_TOKEN, "") ; <<< ADDED 2016

		$err_tag = $MAIN_REPORTING_NO_REF_TAG

		Trace(@TAB & @TAB & $err_tag)
		Trace(@TAB & @TAB & "Exit sequence REF.PS/20100821_174719-1...")

		If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
		ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ReportFormatVersion", $MAIN_REPORTING_CURRENT_VERSION)
		ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ReportSubFormatVersion", $MAIN_REPORTING_CURRENT_SUB_VERSION)
		SetProcessingToken($szResultRepositoryPath_root, 0)

		Return $err_msg ; <<< EARLY EXIT

	EndIf

	If IsProcessingToken($szAllRefs & "\" & $testModuleGuid) Then

		; - NOP -
		; TODO: notify something... ?

		$err_tag = "ProcessingTokenExists"

		Trace(@TAB & @TAB & $err_tag)
		Trace(@TAB & @TAB & "Exit sequence REF.PS/20100821_174719-2...")

		If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
		SetProcessingToken($szResultRepositoryPath_root, 0)
		Return $err_msg

	EndIf

	; get the name of the directory containing automation reference
	$szRefRepositoryPath = $szAllRefs & "\" & $testModuleGuid
	$szRefRepositoryPath &= "\Automation"

	ReportingUnit_SetInfo($szResultRepositoryPath_root, "RefRepositoryPath", $szRefRepositoryPath)
	ReportingUnit_SetInfo($szResultRepositoryPath_root, "ResRepositoryPath", $szResultRepositoryPath_data)
	ReportingUnit_SetInfo($szResultRepositoryPath_root, "NormRefRepositoryPath", NormalizePath($szRefRepositoryPath, True))
	ReportingUnit_SetInfo($szResultRepositoryPath_root, "NormResRepositoryPath", NormalizePath($szResultRepositoryPath_data, True))


	; Get reference repository and compare to the actual result
	; NOTE: Avoid gratuitous path normalization... (REF.PS/40A4213F_2)
	; HISTORY:
	; 2008-10-27: 'EasyCompareDirectories' was initially called with
	;          $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT parameter;
	;          we don't need this anymore because every '.the' file
	;          is now updated at the same time than its '.the.tht.filtered'
	;          counterpart.
	; 2010-02-09: Unpack '.zip' and '.dtb' files before comparing directories
	;          (do this recursively + reuse unpacked content)
	;
;~ EasyUnpackRec($szRefRepositoryPath, $isEntryToSkipWhenComparingRepositoriesCallback, $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT)
;~ EasyUnpackRec($szResultRepositoryPath_data, $isEntryToSkipWhenComparingRepositoriesCallback, $COMPARATOR_KEEP_AND_REUSE_UNPACKED_CONTENT)

	$timerInit = TimerInit()
	$timerName = "EasyUnpackRecRef-Timer"
	EasyUnpackRec($szRefRepositoryPath)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

	$timerInit = TimerInit()
	$timerName = "EasyUnpackRecRes-Timer"
	EasyUnpackRec($szResultRepositoryPath_data)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

	; TODO: EasyExchangeAltOuts($szRefRepositoryPath, $szResultRepositoryPath_data) -- Or diff on a dir-by-dir basis

	$timerInit = TimerInit()
	$timerName = "EasyCompareDirectories-Timer"
	$Differences = EasyCompareDirectories( _
			$szRefRepositoryPath _
			, $szResultRepositoryPath_data _
			, ReportingCallbacks_GetIsEntryToSkipCallback($scriptParams) _
			)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

	; display differences if any
	;;$timerName = "ReportWriting-Timer"
	;;$timerInit = TimerInit()
	If $Differences = 0 Then
		; An error occured
		; NOTE: a warning is likely to have been generated in 'EasyCompareDirectories'
		;       so we might as well remove this message... :P
		Trace(@TAB & @TAB & "ERROR.")
		$err_tag = "EasyCompareDirectoriesFail"
		$err_msg = "An error occured when comparing '" & $szResultRepositoryPath_data & "' to '" & $szRefRepositoryPath & "' - check paths."
	ElseIf $Differences = "" Then
		Trace(@TAB & @TAB & "No differences found.")
		; NOP
	ElseIf IsArray($Differences) Then
		Trace(@TAB & @TAB & "IsArray($Differences) is TRUE.")
		$szGatheredMessages = ""

		$storage = $szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_RAW_CHUNKS_DIR
		If Not FileExists($storage) Then DirCreate($storage)
		ConvertArrayOfDifferencesToChunks($Differences, $storage, $szRefRepositoryPath, $szResultRepositoryPath_data, 0)

		$storage = $szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_HTM_CHUNKS_DIR
		If Not FileExists($storage) Then DirCreate($storage)
		ConvertArrayOfDifferencesToChunks($Differences, $storage, $szRefRepositoryPath, $szResultRepositoryPath_data, 1)

		$nResult = ConvertArrayOfDifferencesToString($Differences, $szGatheredMessages, $szRefRepositoryPath, $szResultRepositoryPath_data)
		If $szGatheredMessages <> "" Then
			; last bold report
			$reportPath = $szResultRepositoryPath_rdir & "\" & $MAIN_REPORTING_REPORT_FILENAME
			If FileExists($reportPath) Then
				If Not FileMove($reportPath, $reportPath & ".back." & MyYEAR() & MyMON() & "-" & MyMDAY() & "-" & MyHOUR() & "-" & MyMIN() & MySEC()) Then
					Trace(@TAB & @TAB & "ERROR.")
					$err_tag = "FileReplaceFail"
					$err_msg = "Impossible to move '" & $reportPath & "' file."
				EndIf
			EndIf
			; write
			If $err_tag = "" Then
				Trace(@TAB & @TAB & "FileWrite...")
				$reportHandle = FileOpen($reportPath, 1 + 256)
				FileWrite($reportHandle, $szGatheredMessages)
				FileClose($reportHandle)
			EndIf
		EndIf
	Else
		Trace(@TAB & @TAB & "ERROR.")
		$err_tag = "EasyCompareDirectoriesUnexpectedRet"
		$err_msg = "'EasyCompareDirectories' returned unexpected value."
	EndIf
	;;ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, $timerName, Round(TimerDiff($timerInit)/1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))

	Trace(@TAB & @TAB & "Exit sequence REF.PS/20100821_174719-0...")
	If $err_tag <> "" Then ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "ErrorTag", $err_tag)
	ReportingUnit_SetExtraInfo($szResultRepositoryPath_root, $reportCount, "GlobalTimer", Round(TimerDiff($globalTimer) / 1000, $TIME_TRACE_ROUND_PRECISION_IN_SECONDS))
	SetProcessingToken($szResultRepositoryPath_root, 0)
	Trace(@TAB & @TAB & "Final message from ReportingUnit_GenerateTMODiffReport.")
	Return $err_msg

EndFunc   ;==>ReportingUnit_GenerateTMODiffReport
