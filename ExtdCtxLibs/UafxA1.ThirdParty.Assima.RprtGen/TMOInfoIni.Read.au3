#include-once

Const $DEFAULT_PLATFORM_PROFILE = "9999"

Func TMOInfoIni_GetMinorProfile($iniFilePath, $dfltMinorProfile = "")
	$minorProfile = IniRead($iniFilePath, "AutomationInfo", "MinorProfile", "")
	; deprecated data
	; TODO: REMOVE IN OCT 2011
	$platformProfile = IniRead($iniFilePath, "AutomationInfo", "PlatformProfile", "")
	If ($minorProfile = "") And ($platformProfile <> "") Then $minorProfile = $platformProfile
	; Default
	If ($minorProfile = "") Then $minorProfile = $dfltMinorProfile
	Return $minorProfile
EndFunc   ;==>TMOInfoIni_GetMinorProfile

Func TMOInfoIni_GetTestModuleName($iniFilePath)
	$oldKeyVal = IniRead($iniFilePath, "AutomationInfo", "TestModule", "") ; This field will become DEPRECATED after some time. REF.PS/20150127_10
	Return IniRead($iniFilePath, "AutomationInfo", "TestModuleName", $oldKeyVal)
EndFunc   ;==>TMOInfoIni_GetTestModuleName

Func TMOInfoIni_GetTestModuleGuid($iniFilePath)
	ConsoleWrite('$iniFilePath = ' & $iniFilePath & @CRLF)
	Return IniRead($iniFilePath, "AutomationInfo", "TestModuleGuid", "")
EndFunc   ;==>TMOInfoIni_GetTestModuleGuid
