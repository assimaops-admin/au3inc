#include-once
; This module is the Static Report Generator.
;   Internally called Reporting Monitor.
#include <___.au3>
#include <ExtdCoreLibs\RepoMgtLib\TimestampV2.au3>
#include ".\ReportingPrimaryUnit.au3" ; $MAIN_PRIMARY_REPORTING_SUB_SCRIPT_NAME_NX
#include ".\ReportingUnit.au3" ; $MAIN_REPORTING_SUB_SCRIPT_NAME_NX, ReportingUnit_GetReportVersion

; reports to process
$DEFAULT_MAX_PROCESSING_DAYS = 2
$ACTUAL_MAX_PROCESSING_DAYS = Get_("ACTUAL_MAX_PROCESSING_DAYS", "")
If $ACTUAL_MAX_PROCESSING_DAYS = "" Then $ACTUAL_MAX_PROCESSING_DAYS = $DEFAULT_MAX_PROCESSING_DAYS

; computer names
Const $DEFAULT_DEVELOPMENT_COMPUTER_NAME = "PERSES"
$ACTUAL_DEVELOPMENT_COMPUTER_NAME = Get_("ACTUAL_DEVELOPMENT_COMPUTER_NAME", "")
If $ACTUAL_DEVELOPMENT_COMPUTER_NAME = "" Then $ACTUAL_DEVELOPMENT_COMPUTER_NAME = $DEFAULT_DEVELOPMENT_COMPUTER_NAME
Const $IS_DEV_COMP = (@ComputerName = $ACTUAL_DEVELOPMENT_COMPUTER_NAME)


Const $INTER_PROC_SLEEP_TIME = 5000
Const $INTER_REPEAT_SLEEP_TIME = 10 * 1000


;-------------------------------------------------------------------------------
; Name .........: ReportingMonitor_InfiniteLoop
; Description ..: ReportingMonitor_InfiniteLoop() eq to ReportingMonitor_InfiniteLoop(1, -1)
;                 ReportingMonitor_InfiniteLoop(-40) process only the 40 last outputs
;-------------------------------------------------------------------------------
Func ReportingMonitor_InfiniteLoop($rootDataDir, $start_mod = 1, $end_mod = -1)
	While 1
		ReportingMonitor_OneShotOnly($rootDataDir, $start_mod, $end_mod)

		ConsoleWrite(@CRLF & 'Sleep ' & ($INTER_REPEAT_SLEEP_TIME/1000) & ' seconds...' _
			& @CRLF & @CRLF)
		Sleep($INTER_REPEAT_SLEEP_TIME)

	WEnd
EndFunc   ;==>ReportingMonitor_InfiniteLoop

;-------------------------------------------------------------------------------
; Name .........: ReportingMonitor_IsRelevant
;-------------------------------------------------------------------------------
Func ReportingMonitor_IsRelevant($targetName, $targetPath)

	; Check the age and relevance of the data

	; eg. $targetName = "TMO-TESTSERV--XP-201610-06-22-0632"
	; eg. $targetPath = "V:/AutoTestingV2.Data/TMO-TESTSERV--XP-201610-06-22-0632"

	; Age of the data
	$targetStamp = StringRight($targetName, StringLen("XXXXXX-XX-XX-XXXX"))
	$ageInDays = TimestampV2_GetAgeInDays($targetStamp)
	ConsoleWrite('... $ageInDays = ' & $ageInDays & @CRLF)

	; Version of the data
	$rprtVersn = ReportingUnit_GetReportVersion($targetPath)
	ConsoleWrite('... $rprtVersn = ' & $rprtVersn & @CRLF)

	; Automation status
	$timeoutStatus = FileExists($targetPath & "\" & $SQLC_SUBPATH_TIMEOUT_PREVIEW1) _
		Or FileExists($targetPath & "\" & $SQLC_SUBPATH_TIMEOUT_PREVIEW2)
	$tmoStatus = Not $timeoutStatus
	ConsoleWrite('... $tmoStatus = ' & $tmoStatus & @CRLF)

	;
	;   Heuristic
	;
	; The data is relevant if it's creation data is recent, + if no previous
	; report exist, + if the reporting is to be performed statically, ie. not
	; taken care of dynamically by the dynamic UafxUI (LegacyTestCenter).
	; REMARK: For instance, the Timeout Report is fully dynamic, so this kind
	; of output is not relevant for the Static Report Generator.
	;
	$DATA_RELEVANCE_CHECK = $rprtVersn = 0 And _
		(($ageInDays <= $ACTUAL_MAX_PROCESSING_DAYS) Or $IS_DEV_COMP) And _
		$tmoStatus
	ConsoleWrite('$DATA_RELEVANCE_CHECK = ' & $DATA_RELEVANCE_CHECK & @CRLF)
	Return $DATA_RELEVANCE_CHECK

EndFunc

;-------------------------------------------------------------------------------
; Name .........: ReportingMonitor_OneShotOnly
;-------------------------------------------------------------------------------
Func ReportingMonitor_OneShotOnly($rootDataDir, $start_mod = 1, $end_mod = -1)

	Const $PRIMARY_LEVEL_DEFAULT_INDEX = $rootDataDir & "\_SSDB_\All_Super_Sessions\indexUnlocked.txt"
	Const $PRIMARY_LEVEL_DFLT_REPOSITORY = $rootDataDir & "\_SSDB_\All_Super_Sessions"
	Const $PRIMARY_LEVEL_ENTRY_SUBSTR_PREFIX = "" ; <<< THE SUPER-SESSION PACKAGE NAME IS ONLY MADE OF THE SESSION ID

	Const $SECONDARY_LEVEL_DEFAULT_INDEX = $rootDataDir & "\_SSDB_\All_TMOs\index.txt"
	Const $SECONDARY_LEVEL_DFLT_REPOSITORY = $rootDataDir
	Const $SECONDARY_LEVEL_ENTRY_SUBSTR_PREFIX = "TMO-" ; + COMPUTERNAME + TEST-MODULE SESSION ID

	; ============================
	;      SECONDARY-LEVEL
	; ============================
	ConsoleWrite(@CRLF & 'SECONDARY-LEVEL REPORTING STARTED' _
		& @CRLF & @CRLF)
	Sleep(1000)

	; Read outputs list
	Dim $dynList
	$outputsListPath = $SECONDARY_LEVEL_DEFAULT_INDEX

	If Not _FileReadToArray($outputsListPath, $dynList) Then
		Return ; <<< Early exit !!!
	EndIf

	ConsoleWrite(@CRLF)
	ConsoleWrite(@CRLF)
	ConsoleWrite(@CRLF)
	ConsoleWrite("SECONDARY-LEVEL PROCESSING STARTED." & @CRLF)
	ConsoleWrite(@CRLF)

	; Define processing sequence
	; REMARK: user-defined
	$items_list = $dynList
	$total_count = $items_list[0]
	$start = ModV2($start_mod, $total_count, 1)
	$end = ModV2($end_mod, $total_count, 1)
	$step = QuestionMark($start <= $end, 1, -1)

	ConsoleWrite('$total_count = ' & $total_count & ', $step = ' & $step & @CRLF)
	ConsoleWrite('$start = ' & $start & ', $end = ' & $end & @CRLF)

	; HACK DEBUG
	;~ $end = $start

	; Process outputs
	ConsoleWrite('LoopFor()...' & @CRLF)
	For $i = $start To $end Step $step
		$targetName = $items_list[$i]
		$targetPath = $SECONDARY_LEVEL_DFLT_REPOSITORY & "\" & $targetName
		ConsoleWrite('... $i = ' & $i & @CRLF)
		ConsoleWrite('... $targetPath = ' & $targetPath & @CRLF)

		; check the format and validity

		$DATA_VALIDITY_CHECK_L2 = StringStartsWith($targetName, $SECONDARY_LEVEL_ENTRY_SUBSTR_PREFIX) _
			And FileExists($targetPath) And IsDirectory($targetPath)

		ConsoleWrite('$DATA_VALIDITY_CHECK_L2 = ' & $DATA_VALIDITY_CHECK_L2 & @CRLF)

		If $DATA_VALIDITY_CHECK_L2 Then

			If ReportingMonitor_IsRelevant($targetName, $targetPath) Then
				ConsoleWrite('ReportingMonitor_ProcessOutput("' & $targetPath & '")...' & @CRLF)
				ReportingMonitor_ProcessOutput($targetPath)
				Sleep(3000) ; leave some time so that others may generate a report too (REF.PS/20111108_223803)
			EndIf

		EndIf
	Next

	ConsoleWrite(@CRLF & 'Sleep ' & ($INTER_PROC_SLEEP_TIME/1000) & ' seconds...' _
		& @CRLF & @CRLF)
	Sleep($INTER_PROC_SLEEP_TIME)

	; ============================
	;       PRIMARY-LEVEL
	; ============================
	ConsoleWrite(@CRLF & 'PRIMARY-LEVEL REPORTING STARTED' _
		& @CRLF & @CRLF)
	Sleep(1000)

	; Read primary-level outputs list
	Dim $sssList
	$outputsListPath = $PRIMARY_LEVEL_DEFAULT_INDEX

	; REMARK: this processing is optional (IMPLEMENTED SEP 16, 2016)
	; => Cancel further processing if "indexUnlocked.txt" file does not exist
	If Not FileExists($outputsListPath) Then
		Return ; <<< Early exit !!!
	EndIf

	If Not _FileReadToArray($outputsListPath, $sssList) Then
		Return ; <<< Early exit !!!
	EndIf

	; Define primary-level processing sequence
	; REMARK: replace user-defined, use default.
	$items_list = $sssList
	$start_mod = 1
	$end_mod = -1

	$total_count = $items_list[0]
	$start = ModV2($start_mod, $total_count, 1)
	$end = ModV2($end_mod, $total_count, 1)
	$step = QuestionMark($start <= $end, 1, -1)

	; Process primary-level outputs
	ConsoleWrite('LoopFor()...' & @CRLF)
	For $i = $start To $end Step $step
		$targetName = $items_list[$i]
		$targetPath = $PRIMARY_LEVEL_DFLT_REPOSITORY & "\" & $targetName
		ConsoleWrite('... $i = ' & $i & @CRLF)
		ConsoleWrite('... $targetPath = ' & $targetPath & @CRLF)

		; check the format and validity

		$DATA_VALIDITY_CHECK_L1 = StringStartsWith($targetName, $PRIMARY_LEVEL_ENTRY_SUBSTR_PREFIX) _
			And FileExists($targetPath) And IsDirectory($targetPath)

		ConsoleWrite('$DATA_VALIDITY_CHECK_L1 = ' & $DATA_VALIDITY_CHECK_L1 & @CRLF)

		If $DATA_VALIDITY_CHECK_L1 Then

			If ReportingMonitor_IsRelevant($targetName, $targetPath) Then
				ConsoleWrite('ReportingMonitor_ProcessPrimaryOutput("' & $targetPath & '")...' & @CRLF)
				ReportingMonitor_ProcessPrimaryOutput($targetPath)
				Sleep(3000) ; leave some time so that others may generate a report too (REF.PS/20111108_223803)
			EndIf
		EndIf

	Next

EndFunc   ;==>ReportingMonitor_OneShotOnly

;-------------------------------------------------------------------------------
; Name .........: ReportingMonitor_ProcessOutput
;-------------------------------------------------------------------------------
Func ReportingMonitor_ProcessOutput($targetPath)
	;
	; $MAIN_REPORTING_SUB_SCRIPT_NAME_NX:
	;   ReportingVx.Sub
	;   C:\AutoTestingVx.Ctrl\UserData\Suites\TmoQa699v01\ReportingVx\ReportingVx.Sub.au3
	;      `-> C:\w_\UserData1507.mod.y\Suites\TmoQa699v01\ReportingVx\ReportingVx.Sub.au3
	;
	; SAMPLE CODE FOR DEVELOPERS:
	;
	;$tmoId = "TMO-PERSES-201605-05-16-3707"
	;If $tmoId <> "" Then $targetPath = "C:\AutoTestingV2.Data\" & $tmoId
	;
	$mainAutoSubNameNx = $MAIN_REPORTING_SUB_SCRIPT_NAME_NX
	;
	$mainAutoSubExt = ".au3"
	If @AutoItExe = @ScriptFullPath Then $mainAutoSubExt = ".exe"
	$mainAutoSubScr = $mainAutoSubNameNx & $mainAutoSubExt
	$mainCmdLinePart = StringFormat('%s "%s"', $mainAutoSubScr, $targetPath)
	$prefixCmdLine__ = @AutoItExe & " "
	;; FAILED: If @AutoItExe = @ScriptFullPath Then $prefixCmdLine__ &= " /AutoIt3ExecuteScript" ; <<< INCLUDES DON'T WORK
	If @AutoItExe = @ScriptFullPath Then $prefixCmdLine__ = "" ; <<< STANDALONE EXECUTABLE RUN
	$fullCmdLine = $prefixCmdLine__ & $mainCmdLinePart
	ConsoleWrite("[" & $fullCmdLine & "]...")
	$err_code = RunWait($fullCmdLine, "", @SW_HIDE)
	;
	; SUB-PROCESS EXIT CODE
	;
	If $err_code <> 0 Then
		; consider removing the TMO from the list?!
	EndIf
	;
	;
EndFunc   ;==>ReportingMonitor_ProcessOutput

;-------------------------------------------------------------------------------
; Name .........: ReportingMonitor_ProcessPrimaryOutput
;-------------------------------------------------------------------------------
Func ReportingMonitor_ProcessPrimaryOutput($targetPath)
	;
	; $MAIN_PRIMARY_REPORTING_SUB_SCRIPT_NAME_NX:
	;   ReportingVx.Sub1
	;   C:\AutoTestingVx.Ctrl\UserData\Suites\TmoQa699v01\ReportingVx\ReportingVx.Sub1.au3
	;      `-> C:\w_\UserData1507.mod.y\Suites\TmoQa699v01\ReportingVx\ReportingVx.Sub1.au3
	;
	; SAMPLE CODE FOR DEVELOPERS:
	;
	;$trlId = "201605-05-16-3707"
	;If $trlId <> "" Then $targetPath = "C:\AutoTestingV2.Data\_SSDB_\All_Super_Sessions\" & $trlId
	;
	$mainAutoSubNameNx = $MAIN_PRIMARY_REPORTING_SUB_SCRIPT_NAME_NX
	;
	$mainAutoSubExt = ".au3"
	If @AutoItExe = @ScriptFullPath Then $mainAutoSubExt = ".exe"
	$mainAutoSubScr = $mainAutoSubNameNx & $mainAutoSubExt
	$mainCmdLinePart = StringFormat('%s "%s"', $mainAutoSubScr, $targetPath)
	$prefixCmdLine__ = @AutoItExe & " "
	;; FAILED: If @AutoItExe = @ScriptFullPath Then $prefixCmdLine__ &= " /AutoIt3ExecuteScript" ; <<< INCLUDES DON'T WORK
	If @AutoItExe = @ScriptFullPath Then $prefixCmdLine__ = "" ; <<< STANDALONE EXECUTABLE RUN
	$fullCmdLine = $prefixCmdLine__ & $mainCmdLinePart
	$err_code = RunWait($fullCmdLine, "", @SW_HIDE)

	;
	; SUB-PROCESS EXIT CODE
	;
	If $err_code <> 0 Then
		; consider removing the TSO from the list?!
	EndIf
	;
	;
EndFunc   ;==>ReportingMonitor_ProcessPrimaryOutput
