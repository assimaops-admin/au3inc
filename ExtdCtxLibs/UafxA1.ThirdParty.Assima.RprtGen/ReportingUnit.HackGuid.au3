#include-once

;-------------------------------------------------------------------------------
; Name .........: ReportingUnit_HackTmoGuid
; Description ..:
; Return .......:
;-------------------------------------------------------------------------------
Func ReportingUnit_HackTmoGuid($tmoId)
	$replCount = 0
	$tmoId = StringReplace($tmoId, "http://htmlqa.assima.net/static/autoindex/default", "[hq9")
	$replCount += @extended
	$tmoId = StringReplace($tmoId, "http://htmlqa.assima.net", "[hqx")
	$replCount += @extended
	$tmoId = StringReplace($tmoId, "/", "][")
	If $replCount Then
		$tmoId &= "]"
	EndIf
	Return $tmoId
EndFunc   ;==>ReportingUnit_HackTmoGuid
